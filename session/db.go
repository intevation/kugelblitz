// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package session

import (
	db "bitbucket.org/intevation/kugelblitz/database"
)

var (
	touchStmt  = db.Must("UPDATE benutzer.sessions SET zeit = $2 WHERE sessionid = $1")
	updateStmt = db.Must("UPDATE benutzer.sessions SET daten = $1, zeit = now(), benutzer = $2 WHERE sessionid = $3")
	deleteStmt = db.Must("DELETE FROM benutzer.sessions WHERE sessionid = $1")
	insertStmt = db.Must("INSERT INTO benutzer.sessions (sessionid, daten, zeit, benutzer) VALUES ($1, $2, now(), $3)")
	selectStmt = db.Must("SELECT daten, zeit, benutzer FROM benutzer.sessions WHERE sessionid = $1")
)
