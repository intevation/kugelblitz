// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package session

import "sync"

var (
	sessionDeathCallbacksMu sync.Mutex
	sessionDeathCallbacks   []func(string)
)

// AddSessionDeathCallback adds a function to the
// list of function to be called when a session dies.
// The key of the session is passed the function to
// identify the dying session in other data structures.
func AddSessionDeathCallback(fn func(string)) {
	sessionDeathCallbacksMu.Lock()
	defer sessionDeathCallbacksMu.Unlock()
	sessionDeathCallbacks = append(sessionDeathCallbacks, fn)
}

func sessionDied(key string) {
	sessionDeathCallbacksMu.Lock()
	defer sessionDeathCallbacksMu.Unlock()
	for _, fn := range sessionDeathCallbacks {
		fn(key)
	}
}
