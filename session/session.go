// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package session

import (
	"database/sql"
	"encoding/json"
	"errors"
	"log"
	"strings"
	"sync"
	"time"
)

const maxSessionAge = time.Minute * 45

var errNoSuchSession = errors.New("no such session")

type sessionState int

const (
	sessionFresh sessionState = 1 << iota
	sessionModified
	sessionInvalid
	sessionOutdated
)

// Session encapsulates the session state,
type Session struct {
	sync.Mutex

	key string

	state sessionState

	user sql.NullInt64
	time time.Time

	data map[string]interface{}

	invalidationHook func(*Session)
}

func (ss sessionState) is(mask sessionState) bool {
	return ss&mask == mask
}

// NewSession creates a new session with a given key.
func NewSession(key string) *Session {
	return &Session{
		key:   key,
		state: sessionFresh,
	}
}

func (s *Session) access() (t time.Time) {
	s.Lock()
	t = s.time
	s.Unlock()
	return
}

func (s *Session) serializeData() sql.NullString {
	var data sql.NullString
	// omit JSON serialization if there's no data.
	if s.state.is(sessionModified) && len(s.data) > 0 {
		var out strings.Builder
		if err := json.NewEncoder(&out).Encode(&s.data); err != nil {
			log.Printf("error: %v\n", err)
		} else {
			data.String = out.String()
			data.Valid = true
		}
	}
	return data
}

func lg(msg string, err error) {
	if err != nil {
		log.Printf("error: %s %v\n", msg, err)
	}
}

// Persist writes the session to the backend or
// deletes it from the infrastructure if its invalid.
func (s *Session) Persist() bool {
	switch {
	case s.state.is(sessionOutdated) || s.state.is(sessionFresh|sessionInvalid):
		return false
	case s.state.is(sessionInvalid):
		// remove from databae
		deleteSessionFromDB(s.key)
		return false
	case s.state.is(sessionFresh):
		// insert
		_, err := insertStmt.Exec(s.key, s.serializeData(), s.user)
		lg("insert failed", err)
		s.state = 0
		return err == nil
	case s.state.is(sessionModified):
		_, err := updateStmt.Exec(s.serializeData(), s.user, s.key)
		lg("update failed", err)
		s.state = 0
		return err == nil
	default:
		// simply touch
		s.time = time.Now()
		_, err := touchStmt.Exec(s.key, s.time)
		lg("touch failed", err)
		return err == nil
	}
}

// Logout does a logout of the session so
// there is no longer a user attached to it.
func (s *Session) Logout() {
	s.Lock()
	defer s.Unlock()
	s.state |= sessionModified
	s.user.Valid = false
	s.user.Int64 = 0
}

func (s *Session) outdate() {
	s.Lock()
	s.state |= sessionOutdated
	s.Unlock()
}

func (s *Session) outdated() (value bool) {
	s.Lock()
	value = s.state.is(sessionOutdated)
	s.Unlock()
	return
}

// Invalid returns true if the session is invalid.
func (s *Session) Invalid() (value bool) {
	s.Lock()
	value = s.state.is(sessionInvalid)
	s.Unlock()
	return
}

// Invalidate marks a session as invalid.
func (s *Session) Invalidate() {
	s.Lock()
	defer s.Unlock()
	s.state |= sessionInvalid
	if s.invalidationHook != nil {
		s.invalidationHook(s)
	}
}

// Key returns the session key.
func (s *Session) Key() string {
	return s.key
}

// Get returns the value associated with the given key.
// The second return value indicates if the value was found.
func (s *Session) Get(key string) (interface{}, bool) {
	s.Lock()
	defer s.Unlock()
	if s.data == nil {
		return nil, false
	}
	v, found := s.data[key]
	return v, found
}

// Put stores a given value under a given key in the session.
func (s *Session) Put(key string, value interface{}) {
	s.Lock()
	defer s.Unlock()
	s.state |= sessionModified
	if s.data[key] == nil {
		s.data = make(map[string]interface{})
	}
	s.data[key] = value
}

// Del deletes the value of a given key from the session.
func (s *Session) Del(key string) {
	s.Lock()
	defer s.Unlock()
	s.state |= sessionModified
	delete(s.data, key)
}

// GetUser returns the ID of the user associated with the session.
// The second return value is false if there is no user
// bound to the session.
func (s *Session) GetUser() (user int64, valid bool) {
	s.Lock()
	user, valid = s.user.Int64, s.user.Valid
	s.Unlock()
	return
}

// SetUser binds the user by a given ID to the session.
func (s *Session) SetUser(value int64) {
	s.Lock()
	defer s.Unlock()
	s.user.Int64 = value
	s.user.Valid = true
	s.state |= sessionModified
}
