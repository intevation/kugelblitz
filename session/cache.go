// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package session

import (
	"database/sql"
	"encoding/json"
	"strings"
	"sync"
	"time"
)

type sessionCache struct {
	sessions map[string]*Session
	mu       sync.Mutex
}

var cache = newSessionCache()

func newSessionCache() *sessionCache {
	sc := &sessionCache{
		sessions: make(map[string]*Session),
	}
	go sc.check()
	return sc
}

func (sc *sessionCache) check() {
	for {
		time.Sleep(10 * time.Minute)
		sc.collectGarbage()
	}
}

func (sc *sessionCache) scanOutDated() []*Session {
	bestBefore := time.Now().Add(-maxSessionAge)
	sc.mu.Lock()
	defer sc.mu.Unlock()
	var outdated []*Session
	for _, s := range sc.sessions {
		if s.access().Before(bestBefore) {
			s.outdate()
			outdated = append(outdated, s)
			delete(sc.sessions, s.key)
		}
	}
	return outdated
}

func deleteSessionFromDB(key string) {
	_, err := deleteStmt.Exec(key)
	lg("deleting session failed", err)
}

func (sc *sessionCache) collectGarbage() {
	outdated := sc.scanOutDated()
	if len(outdated) == 0 {
		return
	}

	// can do this in background
	go func() {
		for _, s := range outdated {
			deleteSessionFromDB(s.key)
		}
	}()

	for _, s := range outdated {
		sessionDied(s.key)
	}
}

func (sc *sessionCache) removeSession(key string) {
	sc.mu.Lock()
	defer sc.mu.Unlock()
	s, found := sc.sessions[key]
	if found {
		delete(sc.sessions, key)
		if !s.outdated() {
			s.outdate()
			sessionDied(key)
		}
	}
}

func (sc *sessionCache) getSession(key string) (*Session, error) {
	sc.mu.Lock()
	defer sc.mu.Unlock()

	bestBefore := time.Now().Add(-maxSessionAge)

	s, found := sc.sessions[key]
	if found {
		if s.access().Before(bestBefore) {
			delete(sc.sessions, key)
			s.outdate()
			deleteSessionFromDB(key)
			sessionDied(key)
			return nil, errNoSuchSession
		}
		return s, nil
	}
	// if not in cache load from database.
	session := &Session{key: key}
	var data sql.NullString

	err := selectStmt.QueryRow(key).Scan(&data, &session.time, &session.user)
	switch {
	case err == sql.ErrNoRows:
		return nil, errNoSuchSession
	case err != nil:
		return nil, err
	}

	// session too old?
	if session.time.Before(bestBefore) {
		sessionDied(key)
		deleteSessionFromDB(key)
		return nil, errNoSuchSession
	}

	if data.Valid { // XXX: Do this lazy?
		dec := json.NewDecoder(strings.NewReader(data.String))
		if err := dec.Decode(&session.data); err != nil {
			return nil, err
		}
	}

	// register in cache
	sc.sessions[key] = session

	return session, nil
}
