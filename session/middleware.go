// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package session

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/securecookie"
)

const cookieName = "BLIDS_SESSION"

type ctxKey int

const sessionCtxKey ctxKey = 0

var secCookie *securecookie.SecureCookie

func init() {
	sessionHash := []byte(os.Getenv("SESSION_HASH"))
	if len(sessionHash) == 0 {
		sessionHash = securecookie.GenerateRandomKey(20)
	}
	sessionCrypt := []byte(os.Getenv("SESSION_CRYPT"))
	if len(sessionCrypt) == 0 {
		sessionCrypt = nil
	}
	secCookie = securecookie.New(sessionHash, sessionCrypt)
}

// Sessioned generates a middleware which attributes a handler with a
// cookie based session.
func Sessioned(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {

		var sessionID string
		var session *Session

		if cookie, err := req.Cookie(cookieName); err != nil {
			// New session
			if err == http.ErrNoCookie {
				sessionID = generateSessionKey()
				if enc, err := secCookie.Encode(cookieName, sessionID); err == nil {
					cookie := &http.Cookie{
						Name:  cookieName,
						Value: enc,
						Path:  "/",
					}
					// XXX: Set cookie.Secure if over HTTPS?
					http.SetCookie(rw, cookie)
					session = NewSession(sessionID)
				}
			} else {
				internalError(rw, err)
				return
			}
		} else if err = secCookie.Decode(cookieName, cookie.Value, &sessionID); err != nil {
			// Delete cookie and redirect to front page
			deleteCookie(rw)
			http.Redirect(rw, req, "/", http.StatusSeeOther)
			return
		} else {
			session, err = cache.getSession(sessionID)
			switch {
			case err == errNoSuchSession:
				deleteCookie(rw)
				http.Error(rw, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			case err != nil:
				// Don't delete the cookie here. It may just be a temporary glitch.
				internalError(rw, err)
				return
			}
		}

		// A bit of a hack. Cookies are stored in the header fields.
		// So we must delete it early before the content is going to
		// be produced.
		session.invalidationHook = func(*Session) { deleteCookie(rw) }

		defer func() {
			if session != nil && !session.Persist() {
				cache.removeSession(session.key)
			}
		}()

		ctx := req.Context()
		ctx = context.WithValue(ctx, sessionCtxKey, session)
		req = req.WithContext(ctx)

		next.ServeHTTP(rw, req)
	})
}

// FromContext extracts a session from a given context.
func FromContext(ctx context.Context) (*Session, bool) {
	s, ok := ctx.Value(sessionCtxKey).(*Session)
	return s, ok
}

func internalError(rw http.ResponseWriter, err error) {
	msg := fmt.Sprintf("error: %v\n", err)
	log.Print(msg)
	http.Error(rw, msg, http.StatusInternalServerError)
}

func deleteCookie(rw http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:     cookieName,
		Value:    "",
		Path:     "/",
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
	}
	http.SetCookie(rw, cookie)
}

// Chain is a bootstrapping mechanism to wrap the results of given
// middleware constructors around each other to build a
// fully decorated middleware which chains them all.
func Chain(mw ...func(http.Handler) http.Handler) http.Handler {
	var last http.Handler
	for i := len(mw) - 1; i >= 0; i-- {
		last = mw[i](last)
	}
	return last
}

// Term is a middleware constructor which simple generates
// a handler which calls the final endpoint.
func Term(f func(http.ResponseWriter, *http.Request)) func(http.Handler) http.Handler {
	return func(http.Handler) http.Handler {
		return http.HandlerFunc(f)
	}
}

// Checking is a middleware constructor which checks the session
// with a given test. When the test passed the inner handler is called.
// Otherwise the chain excecution is terminated with given HTTP code.
func Checking(check func(*Session) bool, code int) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
			ctx := req.Context()
			session, ok := ctx.Value(sessionCtxKey).(*Session)
			if ok {
				ok = check(session)
			}
			if !ok {
				http.Error(rw, http.StatusText(code), code)
				return
			}
			next.ServeHTTP(rw, req)
		})
	}
}

// Not simply negates a given session test function.
func Not(fn func(*Session) bool) func(*Session) bool {
	return func(s *Session) bool {
		return !fn(s)
	}
}

// LoggedIn is a session test function which checks if the
// session is associated with user.
func LoggedIn(s *Session) bool {
	_, ok := s.GetUser()
	return ok
}

// Logging is a middleware to write the called endpoint and
// the HTTP method to the log.
func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		log.Printf("%s %v\n", req.Method, req.URL)
		next.ServeHTTP(rw, req)
	})
}
