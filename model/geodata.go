// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import (
	"database/sql"
	"encoding/json"
	"errors"
	"math"
	"strings"
	"time"

	"github.com/mb0/wkt"
)

var (
	// ErrUnknownStatisticsFilter is returned if an unknown statistics filter
	// is given.
	ErrUnknownStatisticsFilter = errors.New("unknown statistics filter")
	// ErrUnknownCountFilter is returned if an unknown counting filter
	// is given.
	ErrUnknownCountFilter = errors.New("unknown count filter type")
)

// BBox is a bounding box of an area.
type BBox struct {
	MinX float64
	MinY float64
	MaxX float64
	MaxY float64
}

// Box is used a JSON serialisation of a bounding box
// with optional fields.
type Box struct {
	Left         *float64 `json:"left"`
	Right        *float64 `json:"right"`
	Top          *float64 `json:"top"`
	Bottom       *float64 `json:"bottom"`
	StatisticBox *string  `json:"statistikbox"`
	SWindowed    FakeBool `json:"swindow"`
}

// StatisticsFilter is a filter to extract special statistical features.
type StatisticsFilter int

const (
	// StatisticsWithoutFilter does not filter at all.
	StatisticsWithoutFilter StatisticsFilter = iota
	// StatisticsType1Filter only shows statistics where type = 1.
	StatisticsType1Filter
)

// StatisticsItem is a pair of a date and a count. Used for
// JSON serialisation.
type StatisticsItem struct {
	Date  string  `json:"date"`
	Count float64 `json:"count"`
}

// CountFilter limits counts to certain attributes.
type CountFilter int

const (
	// CountWithoutFilter does not filter at all.
	CountWithoutFilter CountFilter = iota
	// CountType4Filter only counts type = 4 events.
	CountType4Filter
	// CountType1PosFilter only counts type = 1 and positive events.
	CountType1PosFilter
	// CountType1NegFilter only counts type = 1 and negative events.
	CountType1NegFilter
)

type sqlBox struct {
	box    sql.NullString
	left   sql.NullFloat64
	right  sql.NullFloat64
	top    sql.NullFloat64
	bottom sql.NullFloat64
}

// GetBox returns the bounding box for a given userID.
func GetBox(userID int64) (*Box, error) {

	var (
		left, right, top, bottom sql.NullFloat64
		sbox                     sql.NullString
		swindowed                bool
	)

	err := getBoxStmt.QueryRow(userID).Scan(
		&left,
		&right,
		&bottom,
		&top,
		&sbox,
		&swindowed)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	var b Box
	if left.Valid {
		b.Left = &left.Float64
	}
	if right.Valid {
		b.Right = &right.Float64
	}
	if top.Valid {
		b.Top = &top.Float64
	}
	if bottom.Valid {
		b.Bottom = &bottom.Float64
	}
	if sbox.Valid {
		b.StatisticBox = &sbox.String
	}
	b.SWindowed = FakeBool(swindowed)

	return &b, nil
}

func (cf CountFilter) stmt() *sql.Stmt {
	switch cf {
	case CountWithoutFilter:
		return getCountStmt
	case CountType4Filter:
		return getCountType4Stmt
	case CountType1PosFilter:
		return getCountType1PosStmt
	case CountType1NegFilter:
		return getCountType1NeqStmt
	default:
		return nil
	}
}

func opNil(op func(float64, float64) bool) func(*float64, *float64) *float64 {
	return func(a, b *float64) *float64 {
		switch {
		case a == nil && b == nil:
			return nil
		case a == nil:
			return b
		case b == nil:
			return a
		default:
			if op(*a, *b) {
				return a
			}
			return b
		}
	}
}

var (
	minNil = opNil(func(a, b float64) bool { return a < b })
	maxNil = opNil(func(a, b float64) bool { return a > b })
)

func nullFloat64(x *float64, y *sql.NullFloat64) {
	if x != nil {
		*y = sql.NullFloat64{Float64: *x, Valid: true}
	}
}

func (sb *sqlBox) fill(bound, box *Box) {
	switch {
	case bound != nil && box != nil && bool(box.SWindowed):
		nullFloat64(maxNil(bound.Left, box.Left), &sb.left)
		nullFloat64(minNil(bound.Right, box.Right), &sb.right)
		nullFloat64(minNil(bound.Top, box.Top), &sb.top)
		nullFloat64(maxNil(bound.Bottom, box.Bottom), &sb.bottom)
	case box != nil:
		nullFloat64(box.Left, &sb.left)
		nullFloat64(box.Right, &sb.right)
		nullFloat64(box.Top, &sb.top)
		nullFloat64(box.Bottom, &sb.bottom)
	case bound != nil:
		nullFloat64(bound.Left, &sb.left)
		nullFloat64(bound.Right, &sb.right)
		nullFloat64(bound.Top, &sb.top)
		nullFloat64(bound.Bottom, &sb.bottom)
	}

	if box != nil && box.StatisticBox != nil {
		sb.box = sql.NullString{String: *box.StatisticBox, Valid: true}
	}
}

// GetCount returns a event count for a given time range between start and stop,
// an optional bounding box and a count filter.
func GetCount(start, stop int64, bound, box *Box, filter CountFilter) (int64, error) {

	stmt := filter.stmt()
	if stmt == nil {
		return 0, ErrUnknownCountFilter
	}

	var sb sqlBox
	sb.fill(bound, box)

	var count int64

	err := stmt.QueryRow(
		sb.box,
		start, stop,
		sb.left, sb.right, sb.top, sb.bottom).Scan(&count)

	return count, err
}

func (sf StatisticsFilter) stmt() *sql.Stmt {
	switch sf {
	case StatisticsWithoutFilter:
		return statisticStmt
	case StatisticsType1Filter:
		return statisticType1Stmt
	default:
		return nil
	}
}

// GetStatistics creates time series for a given time range,
// an area, an optional bounding box and a filter.
func GetStatistics(
	start, stop int64,
	bound, box *Box,
	filter StatisticsFilter) ([]*StatisticsItem, error) {

	stmt := filter.stmt()
	if stmt == nil {
		return nil, ErrUnknownStatisticsFilter
	}

	if start == stop {
		return []*StatisticsItem{
			&StatisticsItem{Date: time.Now().UTC().Format(statFormat)},
		}, nil
	}

	var sb sqlBox
	sb.fill(bound, box)

	interval := (float64(start) - float64(stop)) / 60
	start2 := math.Floor(float64(start) + interval)

	rows, err := stmt.Query(
		interval,
		start,
		stop,
		sb.box,
		start2,
		sb.left,
		sb.right,
		sb.top,
		sb.bottom)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []*StatisticsItem

	for rows.Next() {
		var item StatisticsItem
		if err := rows.Scan(&item.Date, &item.Count); err != nil {
			return nil, err
		}
		items = append(items, &item)
	}

	return items, rows.Err()
}

// GetLiveBlitzgebiet returns the geometry of a given userID.
func GetLiveBlitzgebiet(userID int64) (wkt.Geom, error) {

	var txt sql.NullString
	err := liveBlitzgebietStmt.QueryRow(userID).Scan(&txt)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}
	if !txt.Valid {
		return nil, nil
	}
	return wkt.Parse([]byte(txt.String))
}

// GeomContainsPoint checks if a point is inside a given geometry.
func GeomContainsPoint(g wkt.Geom, point *wkt.Point) bool {
	switch o := g.(type) {
	case *wkt.Polygon:
		return PolygonContainsPoint(o, point)
	case *wkt.MultiPolygon:
		return MultiPolygonContainsPoint(o, point)
	}
	// TODO: Is it worth to implement the others?
	return false
}

// PolygonContainsPoint checks if a polygon contains a given point.
func PolygonContainsPoint(poly *wkt.Polygon, point *wkt.Point) bool {
	return ringsContainsPoint(poly.Rings, point)
}

// MultiPolygonContainsPoint checks if a multi-polygon contains a given point.
func MultiPolygonContainsPoint(poly *wkt.MultiPolygon, point *wkt.Point) bool {
	for _, polygon := range poly.Polygons {
		if ringsContainsPoint(polygon, point) {
			return true
		}
	}
	return false
}

func ringsContainsPoint(rings [][]wkt.Coord, point *wkt.Point) bool {

	// Test if in shell
	if len(rings) == 0 || !ringContainsPoint(rings[0], &point.Coord) {
		return false
	}

	// Test if its in a hole.
	for _, hole := range rings[1:] {
		// Does the reverse order of the holes matter?
		if ringContainsPoint(hole, &point.Coord) {
			return false
		}
	}

	return true
}

func ringIsClosed(ring []wkt.Coord) bool {
	return len(ring) >= 3
}

func ringContainsPoint(ring []wkt.Coord, point *wkt.Coord) bool {
	if !ringIsClosed(ring) {
		return false
	}

	start := len(ring) - 1
	end := 0

	contains := intersectsWithRaycast(ring, point, &ring[start], &ring[end])

	for i := 1; i < len(ring); i++ {
		if intersectsWithRaycast(ring, point, &ring[i-1], &ring[i]) {
			contains = !contains
		}
	}

	return contains
}

// Using the raycast algorithm, this returns whether or not the passed in point
// Intersects with the edge drawn by the passed in start and end points.
// Original implementation: http://rosettacode.org/wiki/Ray-casting_algorithm#Go
func intersectsWithRaycast(ring []wkt.Coord, point, start, end *wkt.Coord) bool {

	// Always ensure that the the first point
	// has a y coordinate that is less than the second point
	if start.Y > end.Y {
		// Switch the points if otherwise.
		start, end = end, start
	}

	// Move the point's y coordinate
	// outside of the bounds of the testing region
	// so we can start drawing a ray
	for point.Y == start.Y || point.Y == end.Y {
		y := math.Nextafter(point.Y, math.Inf(1))
		point = &wkt.Coord{X: point.X, Y: y}
	}

	// If we are outside of the polygon, indicate so.
	if point.Y < start.Y || point.Y > end.Y {
		return false
	}

	if start.X > end.X {
		if point.X > start.X {
			return false
		}
		if point.X < end.X {
			return true
		}
	} else {
		if point.X > end.X {
			return false
		}
		if point.X < start.X {
			return true
		}
	}

	raySlope := (point.Y - start.Y) / (point.X - start.X)
	diagSlope := (end.Y - start.Y) / (end.X - start.X)

	return raySlope >= diagSlope
}

// Contains checks if a given point is inside these bounding box.
func (bb *BBox) Contains(p *wkt.Point) bool {
	return p.X >= bb.MinX && p.X <= bb.MaxX &&
		p.Y >= bb.MinY && p.Y <= bb.MaxY
}

func bboxCoords(coords []wkt.Coord, bbox *BBox) {
	for i := range coords {
		x, y := coords[i].X, coords[i].Y
		if x < bbox.MinX {
			bbox.MinX = x
		}
		if x > bbox.MaxX {
			bbox.MaxX = x
		}
		if y < bbox.MinY {
			bbox.MinY = y
		}
		if y > bbox.MaxY {
			bbox.MaxY = y
		}
	}
}

// CalculateBBox return the bounding box of given geometry.
func CalculateBBox(g wkt.Geom) *BBox {
	if o, ok := g.(*wkt.Point); ok {
		return &BBox{
			MinX: o.X,
			MinY: o.Y,
			MaxX: o.X,
			MaxY: o.Y,
		}
	}
	bbox := BBox{
		MinX: math.MaxFloat64,
		MaxX: -math.MaxFloat64,
		MinY: math.MaxFloat64,
		MaxY: -math.MaxFloat64,
	}
	switch o := g.(type) {
	case *wkt.Polygon:
		if len(o.Rings) > 0 {
			bboxCoords(o.Rings[0], &bbox)
		}
	case *wkt.MultiPolygon:
		for _, p := range o.Polygons {
			if len(p) > 0 {
				bboxCoords(p[0], &bbox)
			}
		}
	case *wkt.MultiPoint:
		bboxCoords(o.Coords, &bbox)
	case *wkt.LineString:
		bboxCoords(o.Coords, &bbox)
	}
	return &bbox
}

// AlarmAreaInfo is used as a JSON serialisation of the
// attributes associated with an alarm area.
type AlarmAreaInfo struct {
	Name  string `json:"name"`
	Count int64  `json:"count"`
	Geom  string `json:"geom"`
}

// GetAlarmArea return an AlarmAreaInfo for a given userID.
func GetAlarmArea(id int64) (*AlarmAreaInfo, error) {
	var data string
	err := alarmAreaStmt.QueryRow(id).Scan(&data)
	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}
	aai := new(AlarmAreaInfo)
	return aai, json.NewDecoder(strings.NewReader(data)).Decode(aai)
}
