// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import db "bitbucket.org/intevation/kugelblitz/database"

var (
	loadThemeStmt = db.Must(`
SELECT
  name,
  header,
  footer,
  style,
  title
FROM
    benutzer.themes WHERE id = $1`)

	themeIDFromHostStmt = db.Must(`
SELECT theme_id
FROM benutzer.host_themes WHERE host ILIKE $1 LIMIT 1`)
)
