// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import (
	db "bitbucket.org/intevation/kugelblitz/database"
)

var (
	showLogStmt = db.Must(`
SELECT
  t1.id,
  t1.benutzer,
  t1.datum,
  t1.request,
  t1.ip,
  t1.session
FROM benutzer.logging t1, benutzer.benutzer t2
WHERE
  t1.benutzer = t2.benutzername AND
  home_gruppen_id = $1 AND
  datum BETWEEN $2 AND $3`)
)
