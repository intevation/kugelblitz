// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import db "bitbucket.org/intevation/kugelblitz/database"

var (
	checkUserStmt = db.Must(`
SELECT id FROM benutzer.benutzer WHERE benutzername = $1 AND passwort = MD5($2)`)

	userLoggingNoSessionStmt = db.Must(`
INSERT INTO benutzer.logging (benutzer, datum, request, ip, session)
VALUES ('No valid session', now(), $1, $2, '')`)

	userLoggingStmt = db.Must(`
INSERT INTO benutzer.logging (benutzer, datum, request, ip, session)
SELECT benutzername, now(), $2, $3, $4 FROM benutzer.benutzer WHERE id = $1`)

	validateUserStmt = db.Must(`
SELECT
  id,
  username,
  start,
  stop,
  bounds,
  max_displayzeit,
  archiv_tage,
  archiv_ab,
  max_zoom,
  min_zoom,
  loginallowed,
  liveblids,
  animation,
  sound,
  theme_id,
  statistik_windowed,
  statistikgebiet
FROM benutzer.getSession($1, $2)`)

	loadLayersStmt = db.Must(`
SELECT
  ebenen.geoserver_ebene,
  ebenen.externer_server,
  ebenen.externer_parameter,
  ebenen.blitzlayer,
  gruppen_ebenen.reload_time,
  gruppen_ebenen.anzeige_name,
  gruppen_ebenen.feature_info,
  gruppen_ebenen.checked_onlogin,
  gruppen_ebenen.permanent,
  gruppen_ebenen.single_tile,
  gruppen_ebenen.cql_filter,
  gruppen_ebenen.popup_template 
FROM benutzer.ebenen, benutzer.gruppen_ebenen, benutzer.benutzer
WHERE ebenen.id = gruppen_ebenen.ebenen_id
  AND gruppen_ebenen.gruppen_id = benutzer.home_gruppen_id
  AND benutzer.id = $1`)

	groupTreeStmt = db.Must(`
SELECT id, vatergruppe FROM benutzer.gruppen`)
)
