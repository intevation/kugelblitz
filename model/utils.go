// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import "database/sql"

// Theme is used send JSON encoded information about a theme to be applied.
type Theme struct {
	Name   string `json:"name"`
	Header string `json:"header"`
	Footer string `json:"footer"`
	Style  string `json:"style"`
	Title  string `json:"title"`
}

// LoadTheme returns a theme for a given id.
func LoadTheme(id int64) (*Theme, error) {
	var name, header, footer, style, title sql.NullString
	err := loadThemeStmt.QueryRow(id).Scan(
		&name, &header, &footer, &style, &title)
	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}
	var theme Theme
	if name.Valid {
		theme.Name = name.String
	}
	if header.Valid {
		theme.Header = header.String
	}
	if footer.Valid {
		theme.Footer = footer.String
	}
	if style.Valid {
		theme.Style = style.String
	}
	if title.Valid {
		theme.Title = title.String
	}
	return &theme, nil
}

func ThemeIDFromHost(host string, def int64) (int64, error) {
	var themeID int64
	host = "%" + host + "%"
	err := themeIDFromHostStmt.QueryRow(host).Scan(&themeID)
	switch {
	case err == sql.ErrNoRows:
		return def, nil
	case err != nil:
		return def, err
	}
	return themeID, nil
}
