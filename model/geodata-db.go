// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import db "bitbucket.org/intevation/kugelblitz/database"

var (
	getBoxStmt = db.Must(`
SELECT
  st_xmin(st_transform(blitzgebiet, 3857)) AS left,
  st_xmax(st_transform(blitzgebiet, 3857)) AS right,
  st_ymin(st_transform(blitzgebiet, 3857)) AS bottom,
  st_ymax(st_transform(blitzgebiet, 3857)) AS top,
  st_astext(statistikgebiet)               AS statistikbox,
  statistik_windowed                       AS swindow
FROM benutzer.gruppen, benutzer.benutzer
WHERE benutzer.id = $1 AND benutzer.home_gruppen_id = gruppen.id`)
)

const (
	// $1: interval
	// S2: start
	// $3: stop
	// $4: statisticbox
	// $5: start2
	// $6: left
	// $7: right
	// $8: top
	// $9: bottom
	statisticPrefix = `
SELECT
  to_char(x AT TIME ZONE 'UTC','YYYY-MM-DD HH24:MI:SS') AS date,
  count(blitzlive.*)::float*60 / $1                     AS count
FROM
  generate_series(
    now() - $2 * interval'1 second',
    now() - $3 * interval'1 second',
    $1         * interval'1 second') AS x
  LEFT JOIN blitzlive(
    CAST($4 AS character varying),
    CAST($5 AS integer),
    CAST($3 AS integer),
    CAST($6 AS numeric),
    CAST($7 AS numeric),
    CAST($8 AS numeric),
    CAST($9 AS numeric))
  ON (blitzlive.zeit BETWEEN x - $1 * interval'1 second' AND x `

	statisticSuffix = `) GROUP BY 1 ORDER BY date`
)

var (
	statisticStmt = db.Must(
		statisticPrefix + `` + statisticSuffix)
	statisticType1Stmt = db.Must(
		statisticPrefix + `AND typ = 1` + statisticSuffix)
)

// $1: statisticbox
// $2: start
// $3: stop
// $4: left
// $5: right
// $6: top
// $7: bottom
const countPrefix = `
SELECT count(*)
FROM blitzlive(
  CAST($1 AS character varying),
  CAST($2 AS integer),
  CAST($3 AS integer),
  CAST($4 AS numeric),
  CAST($5 AS numeric),
  CAST($6 AS numeric),
  CAST($7 AS numeric))
WHERE `

var (
	getCountStmt         = db.Must(countPrefix + `1 = 1`)
	getCountType4Stmt    = db.Must(countPrefix + `typ = 4`)
	getCountType1PosStmt = db.Must(countPrefix + `typ = 1 AND strom > 0`)
	getCountType1NeqStmt = db.Must(countPrefix + `typ = 1 AND strom < 0`)
)

var (
	liveBlitzgebietStmt = db.Must(`
SELECT st_astext(blitzgebiet)
FROM benutzer.benutzer b JOIN benutzer.gruppen g ON b.home_gruppen_id = g.id
WHERE b.id = $1 and g.liveblids`)

	alarmAreaStmt = db.Must(`
SELECT get_alarm_geom FROM alarmserver.get_alarm_geom($1)`)
)
