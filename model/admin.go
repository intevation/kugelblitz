// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import (
	"crypto/rand"
	"database/sql"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/intevation/kugelblitz/database/config"
	"bitbucket.org/intevation/kugelblitz/utils"
	"github.com/lib/pq"
	gomail "gopkg.in/gomail.v2"
)

// DependendObjectsError models an DB error with a number
// of depended objects. Used eg. if a deletion fails because
// there are dependend objects.
type DependendObjectsError struct {
	ObjCount int64
	ObjName  string
}

// Error fulfill the error interface.
func (e DependendObjectsError) Error() string {
	return fmt.Sprintf("model: Cascade failed for %d of %s",
		e.ObjCount, e.ObjName)
}

// Permission models a permission.
type Permission string

// Permissions is the typical set of permissions of a logged in user.
type Permissions struct {
	Rolename         string     `json:"-"`
	AllowLogin       bool       `json:"-"`
	AlarmAdmin       Permission `json:"alarm_admin"`
	AlarmAreas       Permission `json:"alarm_gebiete"`
	AlarmAreaGroup   Permission `json:"alarm_gebietgruppen"`
	AlarmParticipant Permission `json:"alarm_teilnehmer"`
	AlarmAlarms      Permission `json:"alarm_alarme"`
	UserAdmin        Permission `json:"user_admin"`
	GroupAdmin       Permission `json:"gruppen_admin"`
	// Arifical permission
	AlarmSuperAdmin Permission `json:"alarm_super_admin"`
}

// GroupDetailSmall are the short informations about a group.
type GroupDetailSmall struct {
	ID             int64        `json:"id"`
	FatherID       *int64       `json:"vatergruppenid"`
	Length         *float64     `json:"laenge"`
	Width          *float64     `json:"breite"`
	Area           *string      `json:"alarmgebiet"`
	GroupName      string       `json:"gruppenname"`
	AlarmCustomers int64        `json:"alarmkunden"`
	Rights         *Permissions `json:"rights"`
}

// UserShort are the short informations about a user.
type UserShort struct {
	ID        int64  `json:"id"`
	UserName  string `json:"benutzername"`
	LastName  string `json:"lastname"`
	FirstName string `json:"firstname"`
}

// UserDetails are the in-depth informations about a user.
type UserDetails struct {
	ID          *int64  `json:"id"`
	UserName    string  `json:"benutzername"`
	HomeGroupID int64   `json:"home_gruppen_id"`
	GroupName   *string `json:"gruppenname"`
	StartDate   string  `json:"startdatum"`
	StopDatum   string  `json:"stopdatum"`
	FirstName   *string `json:"firstname"`
	LastName    string  `json:"lastname"`
	Company     string  `json:"company"`
	Division    *string `json:"division"`
	// The DB is using streetadress w/o second 'd'.
	StreetAddress *string `json:"streetaddress"`
	ZIP           *string `json:"zip"`
	City          *string `json:"city"`
	PhoneNumber   *string `json:"phonenumber"`
	Annotation    *string `json:"annotation"`
	Password      *string `json:"passwort"`
}

// Group are the essential group informations.
type Group struct {
	ID        int64  `json:"id"`
	GroupName string `json:"gruppenname"`
}

// GroupDetail are the in-depth informations about a group.
type GroupDetail struct {
	ID             int64     `json:"id"`
	FatherGroupID  *int64    `json:"vatergruppe,omitempty"`
	GroupName      *string   `json:"gruppenname,omitempty"`
	ThemeID        *int64    `json:"theme_id,omitempty"`
	Area           *string   `json:"blitzgebiet,omitempty"`
	MaxDisplayTime *int64    `json:"max_displayzeit,omitempty"`
	ArchiveDays    *int64    `json:"archiv_tage,omitempty"`
	ArchiveSince   *string   `json:"archiv_ab"`
	MaxZoom        int64     `json:"max_zoom"`
	MinZoom        int64     `json:"min_zoom"`
	Sound          *FakeBool `json:"sound,omitempty"`
	LiveBlids      *FakeBool `json:"liveblids,omitempty"`
	Animation      *FakeBool `json:"animation"`
}

// GroupParameters are used to create or update a group.
type GroupParameters struct {
	ID             *int64    `json:"id"`
	GroupName      *string   `json:"gruppenname"`
	ParentID       *int64    `json:"vatergruppe"`
	ArchiveSince   *string   `json:"archiv_ab"`
	ArchiveDays    *int64    `json:"archiv_tage"`
	LatMin         *string   `json:"latmin"`
	LatMax         *string   `json:"latmax"`
	LonMin         *string   `json:"lonmin"`
	LonMax         *string   `json:"lonmax"`
	MinZoom        *int64    `json:"min_zoom"`
	MaxZoom        *int64    `json:"max_zoom"`
	Sound          *FakeBool `json:"sound"`
	ThemeID        *int64    `json:"theme_id"`
	MaxDisplayTime *int64    `json:"max_displayzeit"`
	LiveBlids      *FakeBool `json:"liveblids"`
	Animation      *FakeBool `json:"animation"`
}

type groupParametersDB struct {
	id             sql.NullInt64
	groupName      sql.NullString
	fatherID       sql.NullInt64
	archiveSince   pq.NullTime
	archiveDays    sql.NullInt64
	latMin         sql.NullFloat64
	latMax         sql.NullFloat64
	lonMin         sql.NullFloat64
	lonMax         sql.NullFloat64
	minZoom        sql.NullInt64
	maxZoom        sql.NullInt64
	sound          sql.NullBool
	themeID        sql.NullInt64
	maxDisplayTime sql.NullInt64
	liveBlids      sql.NullBool
	animation      sql.NullBool
}

// GroupUpdateResult represents the update result of a group.
type GroupUpdateResult struct {
	ID        int64   `json:"id"`
	GroupName *string `json:"gruppenname"`
	Box       *string `json:"box"`
}

// LayerInfo is used to represents the details of a layer.
type LayerInfo struct {
	ID             int64    `json:"id"`
	LayerID        *int64   `json:"ebenen_id"`
	ReloadTime     int64    `json:"reload_time"`
	DisplayName    string   `json:"anzeige_name"`
	FeatureInfo    FakeBool `json:"feature_info"`
	CheckedOnLogin FakeBool `json:"checked_onlogin"`
	Permanent      FakeBool `json:"permanent"`
	StandardName   *string  `json:"standardname"`
}

// RoleDetail is used the represents the in-depth details of a role.
type RoleDetail struct {
	ID                int64     `json:"id"`
	RoleName          string    `json:"rollenname"`
	CanLogIn          *FakeBool `json:"kann_einloggen"`
	AlarmAdmin        string    `json:"alarm_admin"`
	AlarmArea         string    `json:"alarm_gebiete"`
	AlarmAreaGroups   string    `json:"alarm_gebietgruppen"`
	AlarmParticipants string    `json:"alarm_teilnehmer"`
	AlarmAlarms       string    `json:"alarm_alarme"`
	UserAdmin         string    `json:"user_admin"`
	GroupsAdmin       string    `json:"gruppen_admin"`
	BlidsCounter      string    `json:"blids_counter"`
}

// RoleDetailParameters are used to update or create a role.
type RoleDetailParameters struct {
	ID                *int64    `json:"id"`
	RoleName          *string   `json:"rollenname"`
	CanLogIn          *FakeBool `json:"kann_einloggen"`
	AlarmAdmin        *string   `json:"alarm_admin"`
	AlarmArea         *string   `json:"alarm_gebiete"`
	AlarmAreaGroups   *string   `json:"alarm_gebietgruppen"`
	AlarmParticipants *string   `json:"alarm_teilnehmer"`
	AlarmAlarms       *string   `json:"alarm_alarme"`
	UserAdmin         *string   `json:"user_admin"`
	GroupsAdmin       *string   `json:"gruppen_admin"`
}

type roleDetailParametersDB struct {
	ID                sql.NullInt64
	RoleName          sql.NullString
	CanLogIn          sql.NullBool
	AlarmAdmin        sql.NullString
	AlarmArea         sql.NullString
	AlarmAreaGroups   sql.NullString
	AlarmParticipants sql.NullString
	AlarmAlarms       sql.NullString
	UserAdmin         sql.NullString
	GroupsAdmin       sql.NullString
}

// PermissionList models the permission of user in respect to a group
// and a role.
type PermissionList struct {
	UserName  string `json:"benutzername"`
	UserID    int64  `json:"benutzer_id"`
	GroupName string `json:"gruppenname"`
	GroupID   int64  `json:"gruppen_id"`
	RoleName  string `json:"rollenname"`
	RoleID    int64  `json:"rollen_id"`
}

// PermissionParameters is used to update the role permissions.
type PermissionParameters struct {
	GroupID    *int64 `json:"groupid"`
	OldGroupID *int64 `json:"oldgroupid"`
	RoleID     *int64 `json:"roleid"`
	OldRoleID  *int64 `json:"oldroleid"`
	UserID     *int64 `json:"userid"`
}

// AlarmDetail are the in-depth informations of an alarm.
type AlarmDetail struct {
	AreasBlidsToAlarm      int64     `json:"blitzezumalarm"`
	Duration               int64     `json:"dauer"`
	AreasBlidsNumber       int64     `json:"blitzanzahl"`
	AreasRadius            float64   `json:"radius"`
	AreasAlarm             *FakeBool `json:"alarm"`
	AreasArea              string    `json:"gebiet"`
	AreaID                 int64     `json:"gebietid"`
	ParticipantStartTime   string    `json:"startzeit"`
	ParticipantStopTime    string    `json:"stopzeit"`
	Start                  string    `json:"beginn"`
	End                    string    `json:"ende"`
	ParticipantMon         *FakeBool `json:"mo"`
	ParticipantTue         *FakeBool `json:"di"`
	ParticipantWed         *FakeBool `json:"mi"`
	ParticipantThu         *FakeBool `json:"don"`
	ParticipantFri         *FakeBool `json:"fr"`
	ParticipantSat         *FakeBool `json:"sa"`
	ParticipantSun         *FakeBool `json:"so"`
	ParticipantOnlyLog     *FakeBool `json:"nurlog"`
	ParticipantUTC         *FakeBool `json:"utc"`
	Protocol               *FakeBool `json:"protocol"`
	CSV                    *FakeBool `json:"csv"`
	ParticipantSMS         string    `json:"sms"`
	ParticipantEMail       string    `json:"email"`
	ParticipantFax         string    `json:"fax"`
	ParticipantTel         string    `json:"tel"`
	ParticipantParticipant string    `json:"teilnehmer"`
	ParticipantID          int64     `json:"teilnehmerid"`
	AreaGroupsGroupsName   string    `json:"gruppenname"`
	AlarmID                int64     `json:"alarmid"`
	AlarmCustomerID        int64     `json:"alarmkundeid"`
	CustomerCustomer       string    `json:"kunde"`
	Last                   string    `json:"letzter"`
	Centerx                float64   `json:"centerx"`
	Centery                float64   `json:"centery"`
	Area                   float64   `json:"flach"`
}

// AlarmIDs maps an alarm ID to a customer and a group.
type AlarmIDs struct {
	ID         *int64 `json:"alarmid"`
	CustomerID *int64 `json:"kundeid"`
	GroupID    *int64 `json:"groupid"`
}

// AlarmArea are the in-depth informations about an alarm area.
type AlarmArea struct {
	AreaName        *string  `json:"gebietname"`
	AreaRadius      *float64 `json:"radius"`
	AreaInnerRadius *float64 `json:"innerradius"`
	AreaGeom        *string  `json:"geometry"`
	AlarmDuration   *int64   `json:"dauer"`
	AlarmThreshold  *int64   `json:"blitzezumalarm"`
	AreaLat         *float64 `json:"breite"`
	AreaLon         *float64 `json:"laenge"`
	CustomerID      *int64   `json:"kundenid"`
	GroupID         *int64   `json:"gruppe"`
	ID              *int64   `json:"id"`
}

// AlarmAreaIDs maps an alarm to a customer and a group.
type AlarmAreaIDs struct {
	ID         *int64 `json:"id"`
	CustomerID *int64 `json:"kundeid"`
	GroupID    *int64 `json:"gruppe"`
	Cascade    *bool  `json:"cascade"`
}

// AlarmAreaListSimple binds an alarm area to a customer.
type AlarmAreaListSimple struct {
	ID         int64  `json:"id"`
	Area       string `json:"gebiet"`
	CustomerID int64  `json:"kundenid"`
}

// AlarmAreaList is an item of a alarm area list in the JS client.
type AlarmAreaList struct {
	ID                 int64        `json:"id"`
	Area               string       `json:"gebiet"`
	Length             float64      `json:"laenge"`
	Wide               float64      `json:"breite"`
	AreasRadius        float64      `json:"radius"`
	AreasInnerRadius   float64      `json:"innerRadius"`
	AreasBlidsAlarm    int64        `json:"blitzezumalarm"`
	Last               string       `json:"letzter"`
	Number             int64        `json:"anzahl"`
	Surface            float64      `json:"flach"`
	AreasAlarmDuration int64        `json:"alarmdauer"`
	CustomerID         int64        `json:"kundenid"`
	Customer           string       `json:"kunde"`
	Geometry           *string      `json:"geometry"`
	GeoJSON            *interface{} `json:"geojson"`
}

// AlarmParticipantsListSimple binds a participant to a customer.
type AlarmParticipantsListSimple struct {
	ID          int64  `json:"id"`
	Participant string `json:"teilnehmer"`
	Customer    string `json:"kunde"`
}

// AlarmParticipants are the details about an alarm participant.
type AlarmParticipants struct {
	ID              int64     `json:"id"`
	Participant     string    `json:"teilnehmer"`
	SMS             string    `json:"sms"`
	EMail           string    `json:"email"`
	Fax             string    `json:"fax"`
	Tel             string    `json:"tel"`
	Alarm           string    `json:"alarmierung"`
	AllClear        string    `json:"entwarnung"`
	Customer        int64     `json:"kunde"`
	StartDate       string    `json:"startdatum"`
	StopDate        string    `json:"stopdatum"`
	StartTime       string    `json:"startzeit"`
	StopTime        string    `json:"stopzeit"`
	Mon             *FakeBool `json:"mo"`
	Tue             *FakeBool `json:"di"`
	Wed             *FakeBool `json:"mi"`
	Thu             *FakeBool `json:"don"`
	Fri             *FakeBool `json:"fr"`
	Sat             *FakeBool `json:"sa"`
	Sun             *FakeBool `json:"so"`
	OnlyLog         *FakeBool `json:"nurlog"`
	UTC             *FakeBool `json:"utc"`
	SubjectAlarm    string    `json:"betreffalarm"`
	SubjectAllClear string    `json:"betreffentwarn"`
	BlidsLog        *FakeBool `json:"blitzprotokoll"`
	BlidsASCSV      *FakeBool `json:"blitzascsv"`
	Begin           string    `json:"anfang"`
	End             string    `json:"ende"`
	Active          *FakeBool `json:"aktiv"`
	CustomerName    string    `json:"kundenname"`
}

// AlarmParticipantIDs are used to pass the customer IDs
// from the JS client to the server.
type AlarmParticipantIDs struct {
	ID         *int64 `json:"id"`
	CustomerID *int64 `json:"kunde"`
	GroupID    *int64 `json:"gruppe"`
	Cascade    *bool  `json:"cascade"`
}

// AlarmAreaGroup are the detail passed to the JS client about
// an alarm area group.
type AlarmAreaGroup struct {
	ID           int64       `json:"id"`
	GroupName    string      `json:"gruppenname"`
	Alarm        *FakeBool   `json:"alarm"`
	Customer     int64       `json:"kunde"`
	CustomerName string      `json:"kundenname"`
	Areas        interface{} `json:"gebiete"`
}

// Area is the ID and the name of an area.
type Area struct {
	ID   int64  `json:"id"`
	Area string `json:"gebiet"`
}

// AlarmCustomer is the ID and the name of a customer.
type AlarmCustomer struct {
	ID   int64  `json:"id"`
	Name string `json:"alarmkunde"`
}

// AlarmMessages is an item in the the list of alarm messages.
type AlarmMessages struct {
	Start    string `json:"anf"`
	End      string `json:"ende"`
	ID       int64  `json:"id"`
	Alarm    int64  `json:"alarm"`
	Entry    string `json:"eintrag"`
	Finished string `json:"finished"`
	Typ      string `json:"typ"`
}

// ParticipantInfo are the in-depth informations about an alarm participant.
type ParticipantInfo struct {
	ID               *int64        `json:"id"`
	GroupID          int64         `json:"gruppe"`
	GroupName        string        `json:"gruppenname"`
	CustomerID       int64         `json:"kundenid"`
	Participant      string        `json:"teilnehmer"`
	SelectedCustomer AlarmCustomer `json:"selectedkunde"`
	Start            string        `json:"anfang"`
	End              string        `json:"ende"`
	StartTime        string        `json:"startzeit"`
	StopTime         string        `json:"stopzeit"`
	Typ              string        `json:"typ"`
	UTC              string        `json:"utc"`
	BlitzASCSV       FakeBool      `json:"blitzascsv"`
	BlidsLog         FakeBool      `json:"blitzprotokoll"`
	OnlyLog          FakeBool      `json:"nurlog"`
	Mon              FakeBool      `json:"mo"`
	Tue              FakeBool      `json:"di"`
	Wed              FakeBool      `json:"mi"`
	Thu              FakeBool      `json:"don"`
	Fri              FakeBool      `json:"fr"`
	Sat              FakeBool      `json:"sa"`
	Sun              FakeBool      `json:"so"`
	EMail            string        `json:"email"`
	SMS              string        `json:"sms"`
	Fax              string        `json:"fax"`
	Tel              string        `json:"tel"`
	Alarming         string        `json:"alarmierung"`
	AllClear         string        `json:"entwarnung"`
	Default          *FakeBool     `json:"default"`
}

// TestAlarm is used to pass the test alarm parameters to the server.
type TestAlarm struct {
	ID         *int64 `json:"alarmid"`
	CustomerID *int64 `json:"kundeid"`
	GroupID    *int64 `json:"groupid"`
	Typ        *int32 `json:"alarmtyp"`
}

// Any checks if the permission is any from the given arguments.
func (p Permission) Any(vs ...string) bool {
	for _, v := range vs {
		if p == Permission(v) {
			return true
		}
	}
	return false
}

// GetUserPermission fetches the permissions of a user in
// respect of a given group.
func GetUserPermission(userID, groupID int64) (*Permissions, error) {

	var rolename sql.NullString
	var p Permissions

	err := getUserPermStmt.QueryRow(userID, groupID).Scan(
		&rolename,
		&p.AllowLogin, &p.AlarmAdmin, &p.AlarmAreas, &p.AlarmAreaGroup,
		&p.AlarmParticipant, &p.AlarmAlarms, &p.UserAdmin, &p.GroupAdmin)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	if rolename.Valid {
		p.Rolename = rolename.String
	}

	return &p, nil
}

func GetHomeGroup(userID int64) (*int64, error) {
	var hg int64
	err := getHomeGroupStmt.QueryRow(userID).Scan(&hg)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}
	return &hg, nil
}

func LoadAdminRights(userID int64) (Permission, error) {
	if userID <= 0 {
		return "F", nil
	}

	p, err := GetUserPermission(userID, 1)
	if err != nil {
		return "", err
	}
	if p != nil && p.UserAdmin.Any("A", "W") {
		return "A", nil
	}
	hg, err := GetHomeGroup(userID)
	if hg == nil || err != nil {
		return "", err
	}
	if p, err = GetUserPermission(userID, *hg); err != nil {
		return "", err
	}

	if p != nil && p.GroupAdmin.Any("A", "W", "R", "C") {
		return "HG", nil
	}

	return "N", nil
}

// CountAreaInAlarms counts the alarms in a given area.
func CountAreaInAlarms(areaID int64) (int64, error) {
	var count int64
	err := countAreaInAlarms.QueryRow(areaID).Scan(&count)

	if err == sql.ErrNoRows {
		return 0, nil
	}

	return count, err
}

// CountParticipantInAlarms counts the alarm participants,
func CountParticipantInAlarms(participantID int64) (int64, error) {
	var count int64
	err := countParticipantInAlarms.QueryRow(participantID).Scan(&count)

	if err == sql.ErrNoRows {
		return 0, nil
	}

	return count, err
}

// CheckGroupAlarmCustomer checks if there are any constomers in a group.
func CheckGroupAlarmCustomer(groupID int64, customerID int64) (bool, error) {
	var count int64
	err := checkGroupAlarmCustomerStmt.QueryRow(groupID, customerID).Scan(&count)

	if err == sql.ErrNoRows {
		return false, nil
	}

	return count > 0, err
}

// CheckCustomerAlarm checks if there is any customer for a given alarm.
func CheckCustomerAlarm(customerID int64, alarmID int64) (bool, error) {
	var count int64
	err := checkCustomerAlarmStmt.QueryRow(customerID, alarmID).Scan(&count)

	if err == sql.ErrNoRows {
		return false, nil
	}

	return count > 0, err
}

// CheckCustomerParticipant checks if there is any customer for a participant.
func CheckCustomerParticipant(customerID int64, participantID int64) (bool, error) {
	var count int64
	err := checkCustomerParticipantStmt.QueryRow(customerID, participantID).Scan(&count)

	if err == sql.ErrNoRows || err != nil {
		return false, err
	}

	return count > 0, err
}

func CheckUser(email string) (*int64, error) {

	var userID int64
	err := checkEmailStmt.QueryRow(email).Scan(&userID)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &userID, err
}

func CheckLink(userID int64, hash string) (bool, error) {
	var count int64
	err := checkLinkStmt.QueryRow(userID, hash).Scan(&count)
	if err == sql.ErrNoRows {
		return false, nil
	}
	return count > 0, nil
}

func UpdatePassword(password string, userID int64, hash string, ip string) error {
	_, err := ipToHashStmt.Exec(userID, hash, ip)
	if err != nil {
		return err
	}

	_, err = updatePasswordStmt.Exec(password, userID)
	return err
}

// CheckCustomerArea checks if there are any customers for an area.
func CheckCustomerArea(customerID int64, areaID int64) (bool, error) {
	var count int64
	err := checkCustomerAreaStmt.QueryRow(customerID, areaID).Scan(&count)

	if err == sql.ErrNoRows {
		return false, nil
	}

	return count > 0, err
}

func GetGroupDetailSmall(userID, groupID int64) (*GroupDetailSmall, error) {

	var gds GroupDetailSmall

	var (
		fatherid      sql.NullInt64
		length, width sql.NullFloat64
		area          sql.NullString
	)

	err := getGroupDetailSmallStmt.QueryRow(groupID).Scan(
		&gds.ID,
		&fatherid,
		&length,
		&width,
		&area,
		&gds.GroupName,
		&gds.AlarmCustomers)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	if gds.Rights, err = GetUserPermission(userID, groupID); err != nil {
		return nil, err
	}

	if fatherid.Valid {
		gds.FatherID = &fatherid.Int64
	}
	if length.Valid {
		gds.Length = &length.Float64
	}
	if width.Valid {
		gds.Width = &width.Float64
	}
	if area.Valid {
		gds.Area = &area.String
	}

	return &gds, nil
}

func GetChildren(id int64) ([]int64, error) {
	rows, err := getChildrenStmt.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var children []int64
	for rows.Next() {
		var child int64
		if err := rows.Scan(&child); err != nil {
			return nil, err
		}
		children = append(children, child)
	}
	return children, rows.Err()
}

func GetUsers(groupID int64) ([]*UserShort, error) {
	rows, err := getUsersStmt.Query(groupID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var username, lastname, firstname sql.NullString
	var users []*UserShort
	for rows.Next() {
		var u UserShort
		if err := rows.Scan(
			&u.ID,
			&username,
			&lastname,
			&firstname); err != nil {
			return users, err
		}
		if username.Valid {
			u.UserName = username.String
		}
		if lastname.Valid {
			u.LastName = lastname.String
		}
		if firstname.Valid {
			u.FirstName = firstname.String
		}
		users = append(users, &u)
	}
	return users, rows.Err()
}

func GetUserDetails(userID, adminID int64) (*UserDetails, error) {
	groupID, err := GetHomeGroup(userID)
	if err != nil {
		return nil, err
	}
	if groupID == nil {
		log.Println("no home group")
		return nil, nil
	}
	perm, err := GetUserPermission(adminID, *groupID)
	if err != nil {
		return nil, err
	}
	if perm == nil || !perm.UserAdmin.Any("R", "W", "A") {
		log.Printf("Not enough rights: %s\n", perm.UserAdmin)
		return nil, nil
	}

	var (
		groupname     sql.NullString
		startdate     pq.NullTime
		stopdate      pq.NullTime
		firstname     sql.NullString
		lastname      sql.NullString
		company       sql.NullString
		division      sql.NullString
		streetaddress sql.NullString
		zip           sql.NullString
		city          sql.NullString
		phonenumber   sql.NullString
		annotation    sql.NullString
	)

	var ud UserDetails

	err = getUserDetailsStmt.QueryRow(userID).Scan(
		&ud.ID,
		&ud.UserName,
		&ud.HomeGroupID,
		&groupname,
		&startdate,
		&stopdate,
		&firstname,
		&lastname,
		&company,
		&division,
		&streetaddress,
		&zip,
		&city,
		&phonenumber,
		&annotation)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	if groupname.Valid {
		ud.GroupName = &groupname.String
	}
	if startdate.Valid {
		ud.StartDate = startdate.Time.Format(dateFormat)
	}
	if stopdate.Valid {
		ud.StopDatum = stopdate.Time.Format(dateFormat)
	}
	if firstname.Valid {
		ud.FirstName = &firstname.String
	}
	if lastname.Valid {
		ud.LastName = lastname.String
	}
	if company.Valid {
		ud.Company = company.String
	}
	if division.Valid {
		ud.Division = &division.String
	}
	if streetaddress.Valid {
		ud.StreetAddress = &streetaddress.String
	}
	if zip.Valid {
		ud.ZIP = &zip.String
	}
	if city.Valid {
		ud.City = &city.String
	}
	if phonenumber.Valid {
		ud.PhoneNumber = &phonenumber.String
	}
	if annotation.Valid {
		ud.Annotation = &annotation.String
	}

	return &ud, nil
}

func GetGroupList() ([]*Group, error) {
	rows, err := getGroupListStmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	groups := make([]*Group, 0, 10)
	for rows.Next() {
		group := new(Group)
		if err := rows.Scan(&group.ID, &group.GroupName); err != nil {
			return nil, err
		}
		groups = append(groups, group)
	}
	return groups, rows.Err()
}

func GetGroupDetail(groupID, adminID int64) (*GroupDetail, error) {

	// TODO: Check if admin is allowed to access the group details.

	var (
		gd             GroupDetail
		fathergroup    sql.NullInt64
		groupname      sql.NullString
		themeid        sql.NullInt64
		area           sql.NullString
		maxdisplayzeit sql.NullInt64
		archivedays    sql.NullInt64
		archivesince   pq.NullTime
		sound          sql.NullBool
		liveblids      sql.NullBool
		animation      sql.NullBool
	)

	err := getGroupDetailStmt.QueryRow(groupID).Scan(
		&gd.ID,
		&fathergroup,
		&groupname,
		&themeid,
		&area,
		&maxdisplayzeit,
		&archivedays,
		&archivesince,
		&gd.MaxZoom,
		&gd.MinZoom,
		&sound,
		&liveblids,
		&animation)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	if fathergroup.Valid {
		gd.FatherGroupID = &fathergroup.Int64
	}
	if groupname.Valid {
		gd.GroupName = &groupname.String
	}
	if themeid.Valid {
		gd.ThemeID = &themeid.Int64
	}
	if area.Valid {
		gd.Area = &area.String
	}
	if maxdisplayzeit.Valid {
		gd.MaxDisplayTime = &maxdisplayzeit.Int64
	}
	if archivedays.Valid {
		gd.ArchiveDays = &archivedays.Int64
	}
	if archivesince.Valid {
		date := archivesince.Time.Format(dateFormat)
		gd.ArchiveSince = &date
	}
	if sound.Valid {
		gd.Sound = FakeBoolPointer(sound.Bool)
	}
	if liveblids.Valid {
		gd.LiveBlids = FakeBoolPointer(liveblids.Bool)
	}
	if animation.Valid {
		gd.Animation = FakeBoolPointer(animation.Bool)
	}

	return &gd, nil
}

func (in *GroupParameters) fill(out *groupParametersDB) error {

	if in.ID != nil {
		out.id = sql.NullInt64{Int64: *in.ID, Valid: true}
	}
	if in.GroupName != nil {
		out.groupName = sql.NullString{String: *in.GroupName, Valid: true}
	}
	if in.ParentID != nil {
		out.fatherID = sql.NullInt64{Int64: *in.ParentID, Valid: true}
	}
	if in.ArchiveSince != nil {
		t, err := stringToTime(*in.ArchiveSince, isoFormat, dateFormat)
		if err != nil {
			return err
		}
		out.archiveSince = pq.NullTime{Time: t, Valid: true}
	}
	if in.ArchiveDays != nil {
		out.archiveDays = sql.NullInt64{Int64: *in.ArchiveDays, Valid: true}
	}

	set := func(x *string, y *sql.NullFloat64) error {
		v, err := strconv.ParseFloat(*x, 64)
		if err == nil {
			*y = sql.NullFloat64{Float64: v, Valid: true}
		}
		return err
	}
	if err := set(in.LatMin, &out.latMin); err != nil {
		return err
	}
	if err := set(in.LatMax, &out.latMax); err != nil {
		return err
	}
	if err := set(in.LonMin, &out.lonMin); err != nil {
		return err
	}
	if err := set(in.LonMax, &out.lonMax); err != nil {
		return err
	}
	if in.MinZoom != nil {
		out.minZoom = sql.NullInt64{Int64: *in.MinZoom, Valid: true}
	}
	if in.MaxZoom != nil {
		out.maxZoom = sql.NullInt64{Int64: *in.MaxZoom, Valid: true}
	}
	if in.Sound != nil {
		out.sound = sql.NullBool{Bool: bool(*in.Sound), Valid: true}
	}
	if in.ThemeID != nil {
		out.themeID = sql.NullInt64{Int64: *in.ThemeID, Valid: true}
	}
	if in.MaxDisplayTime != nil {
		out.maxDisplayTime = sql.NullInt64{Int64: *in.MaxDisplayTime, Valid: true}
	}
	if in.LiveBlids != nil {
		out.liveBlids = sql.NullBool{Bool: bool(*in.LiveBlids), Valid: true}
	}
	if in.Animation != nil {
		out.animation = sql.NullBool{Bool: bool(*in.Animation), Valid: true}
	}
	return nil
}

func (in *RoleDetailParameters) fill(out *roleDetailParametersDB) error {

	if in.ID != nil {
		out.ID = sql.NullInt64{Int64: *in.ID, Valid: true}
	}
	if in.RoleName != nil {
		out.RoleName = sql.NullString{String: *in.RoleName, Valid: true}
	}
	if in.CanLogIn != nil {
		out.CanLogIn = sql.NullBool{Bool: bool(*in.CanLogIn), Valid: true}
	}
	if in.AlarmAdmin != nil {
		out.AlarmAdmin = sql.NullString{String: *in.AlarmAdmin, Valid: true}
	}
	if in.AlarmArea != nil {
		out.AlarmArea = sql.NullString{String: *in.AlarmArea, Valid: true}
	}
	if in.AlarmAreaGroups != nil {
		out.AlarmAreaGroups = sql.NullString{String: *in.AlarmAreaGroups, Valid: true}
	}
	if in.AlarmAreaGroups != nil {
		out.AlarmAreaGroups = sql.NullString{String: *in.AlarmAreaGroups, Valid: true}
	}
	if in.AlarmParticipants != nil {
		out.AlarmParticipants = sql.NullString{String: *in.AlarmParticipants, Valid: true}
	}
	if in.AlarmAlarms != nil {
		out.AlarmAlarms = sql.NullString{String: *in.AlarmAlarms, Valid: true}
	}
	if in.UserAdmin != nil {
		out.UserAdmin = sql.NullString{String: *in.UserAdmin, Valid: true}
	}
	if in.GroupsAdmin != nil {
		out.GroupsAdmin = sql.NullString{String: *in.GroupsAdmin, Valid: true}
	}

	return nil
}

func AddRole(params *RoleDetailParameters) (int64, error) {

	var out roleDetailParametersDB
	if err := params.fill(&out); err != nil {
		return 0, err
	}

	row := addRoleStmt.QueryRow(
		out.RoleName,
		out.CanLogIn,
		out.AlarmAdmin,
		out.AlarmArea,
		out.AlarmAreaGroups,
		out.AlarmParticipants,
		out.AlarmAlarms,
		out.UserAdmin,
		out.GroupsAdmin)

	var id int64
	err := row.Scan(&id)
	return id, err
}

func RemoveRole(roleID int64) error {
	_, err := removeRoleStmt.Exec(roleID)
	return err
}

func UpdateRole(role *RoleDetail) error {

	var canLogIn sql.NullBool
	if role.CanLogIn != nil {
		canLogIn = sql.NullBool{Bool: bool(*role.CanLogIn), Valid: true}
	}

	_, err := updateRoleStmt.Exec(
		role.ID,
		role.RoleName,
		canLogIn,
		role.AlarmAdmin,
		role.AlarmArea,
		role.AlarmAreaGroups,
		role.AlarmParticipants,
		role.AlarmAlarms,
		role.UserAdmin,
		role.GroupsAdmin)
	return err
}

func AddGroup(params *GroupParameters) (int64, error) {

	var out groupParametersDB
	if err := params.fill(&out); err != nil {
		return 0, err
	}

	row := addGroupStmt.QueryRow(
		out.fatherID,
		out.groupName,
		out.themeID,
		out.lonMin,
		out.latMin,
		out.lonMax,
		out.latMax,
		out.maxDisplayTime,
		out.archiveDays,
		out.archiveSince,
		out.maxZoom,
		out.minZoom,
		out.liveBlids,
		out.animation,
		out.sound)

	var id int64
	err := row.Scan(&id)
	return id, err
}

// AddGroup2Alarm adds a grouo to an alarm.
func AddGroup2Alarm(groupID int64, customerID int64) (int64, error) {
	r, err := addGroup2AlarmStmt.Exec(groupID, customerID)
	if err != nil {
		return 0, err
	}
	return r.RowsAffected()
}

func AddLayer(groupID, layerID int64) (int64, error) {
	row := addLayerStmt.QueryRow(groupID, layerID)
	var id int64
	err := row.Scan(&id)
	return id, err
}

func CopyLayer(childID, parentID int64) error {
	_, err := copyLayerStmt.Exec(parentID, childID)
	return err
}

func RemoveGroup(groupID int64) error {
	_, err := removeGroupStmt.Exec(groupID)
	return err
}

// RemoveGroup2Alarm removes a group from an alarm.
func RemoveGroup2Alarm(groupID int64, customerID int64) (int64, error) {
	r, err := removeGroup2AlarmStmt.Exec(groupID, customerID)
	if err != nil {
		return 0, err
	}
	return r.RowsAffected()
}

func RemoveGroupLayers(groupID int64) error {
	_, err := removeGroupLayersStmt.Exec(groupID)
	return err
}

func GetLayerList(groupID int64) ([]*LayerInfo, error) {

	rows, err := getLayerListStmt.Query(groupID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var lis []*LayerInfo

	for rows.Next() {
		var (
			li      LayerInfo
			stdname sql.NullString
			layerid sql.NullInt64
		)
		if err := rows.Scan(
			&li.ID,
			&layerid,
			&li.ReloadTime,
			&li.DisplayName,
			&li.FeatureInfo,
			&li.CheckedOnLogin,
			&li.Permanent,
			&stdname); err != nil {
			return nil, err
		}
		if layerid.Valid {
			li.LayerID = &layerid.Int64
		}
		if stdname.Valid {
			li.StandardName = &stdname.String
		}
		lis = append(lis, &li)
	}

	return lis, rows.Err()
}

func RemoveLayer(id, userID int64) (bool, error) {

	var groupID int64
	err := groupIDStmt.QueryRow(id).Scan(&groupID)

	switch {
	case err == sql.ErrNoRows:
		return false, nil
	case err != nil:
		return false, err
	}

	perms, err := GetUserPermission(userID, groupID)
	if err != nil {
		return false, err
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "W") {
		return false, nil
	}

	if _, err = deleteLayerIDStmt.Exec(id); err != nil {
		return false, err
	}
	return true, nil
}

func UpdateGroup(in *GroupParameters) (*GroupUpdateResult, error) {

	var out groupParametersDB
	if err := in.fill(&out); err != nil {
		return nil, err
	}

	row := updateGroupStmt.QueryRow(
		out.id,
		out.groupName,
		out.themeID,
		out.lonMin,
		out.latMin,
		out.lonMax,
		out.latMax,
		out.maxDisplayTime,
		out.archiveDays,
		out.archiveSince,
		out.maxZoom,
		out.minZoom,
		out.liveBlids,
		out.animation,
		out.sound)

	var (
		gur  GroupUpdateResult
		name sql.NullString
		box  sql.NullString
	)

	err := row.Scan(&gur.ID, &name, &box)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	if name.Valid {
		gur.GroupName = &name.String
	}
	if box.Valid {
		gur.Box = &box.String
	}

	return &gur, nil
}

func UpdateLayer(layer *LayerInfo, groupID int64) error {
	_, err := updateLayerStmt.Exec(
		layer.DisplayName,
		layer.FeatureInfo,
		layer.CheckedOnLogin,
		layer.Permanent,
		layer.ReloadTime,
		layer.ID,
		layer.LayerID,
		groupID)
	return err
}

func UpdateUser(user *UserDetails) error {
	start, err := time.Parse(isoFormat, user.StartDate)
	if err != nil {
		return err
	}
	stop, err := time.Parse(isoFormat, user.StopDatum)
	if err != nil {
		return err
	}

	// UserName is email.
	if user.Password != nil && *user.Password != "" {
		_, err = updateUserAndPasswordStmt.Exec(
			user.UserName,
			user.HomeGroupID,
			start,
			stop,
			nullString(user.FirstName),
			user.LastName,
			user.Company,
			nullString(user.Division),
			nullString(user.StreetAddress),
			nullString(user.ZIP),
			nullString(user.City),
			nullString(user.PhoneNumber),
			nullString(user.Annotation),
			nullString(user.Password),
			nullInt64(user.ID))
	} else {
		_, err = updateUserStmt.Exec(
			user.UserName,
			user.HomeGroupID,
			start,
			stop,
			nullString(user.FirstName),
			user.LastName,
			user.Company,
			nullString(user.Division),
			nullString(user.StreetAddress),
			nullString(user.ZIP),
			nullString(user.City),
			nullString(user.PhoneNumber),
			nullString(user.Annotation),
			nullInt64(user.ID))
	}
	return err
}

func generateHash(sz int) string {
	data := make([]byte, sz)
	_, err := io.ReadFull(rand.Reader, data)
	if err != nil {
		log.Fatalf("No entropy today: %v\n", err)
	}
	return hex.EncodeToString(data)
}

func GenerateLink(
	https bool, email string,
	userID int64, serverName string,
) (string, error) {

	// email is user
	var hash string

	row := getLinkStmt.QueryRow(userID)

	err := row.Scan(&hash)
	switch {
	case err == sql.ErrNoRows:
		hash = generateHash(32)
		_, err = generateLinkStmt.Exec(userID, hash)
		fallthrough
	case err != nil:
		return "", err
	}

	body := getMessage(https, email, hash, serverName)

	// using uncached config
	cfg, err := config.LoadConfigurationPrefix(
		utils.Env("CONFIG", ""), "pw-reset-mail-")
	if err != nil {
		log.Printf("warn: configuration loading failed: %v\n", err)
	}

	var (
		host     = cfg.String("pw-reset-mail-host", "localhost")
		port     = int(cfg.Int64("pw-reset-mail-port", 25))
		username = cfg.String("pw-reset-mail-user", "")
		password = cfg.String("pw-reset-mail-pw", "")
		helo     = cfg.String("pw-reset-mail-helo", "")
		mailFrom = cfg.String("pw-reset-mail-from", "")
	)

	m := gomail.NewMessage()
	m.SetHeader("From", mailFrom)
	m.SetHeader("To", email)
	m.SetHeader("Subject", "Password Reset Link")
	m.SetBody("text/plain", body)

	d := gomail.Dialer{
		Host:      host,
		Port:      port,
		Username:  username,
		Password:  password,
		LocalName: helo,
		SSL:       port == 465,
	}

	if err := d.DialAndSend(m); err != nil {
		return "", err
	}

	result := email + " - " + hash

	return result, nil
}

func AddUser(user *UserDetails) (int64, error) {

	start, err := time.Parse(isoFormat, user.StartDate)
	if err != nil {
		return 0, err
	}
	stop, err := time.Parse(isoFormat, user.StopDatum)
	if err != nil {
		return 0, err
	}

	row := addUserStmt.QueryRow(
		user.UserName,
		user.Password,
		user.HomeGroupID,
		start,
		stop,
		user.FirstName,
		user.LastName,
		user.Company,
		user.Division,
		user.StreetAddress,
		user.ZIP,
		user.City,
		user.PhoneNumber,
		user.Annotation,
	)

	var id int64
	err = row.Scan(&id)
	return id, err
}

func AddPerm(userID int64, groupID int64, roleID int64) error {

	rows, err := addPermStmt.Query(
		userID,
		groupID,
		roleID,
	)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var perm string
		if err := rows.Scan(&perm); err != nil {
			return err
		}
	}
	return rows.Err()
}

func RemoveUser(userID int64) error {
	_, err := removeUserStmt.Exec(userID)
	return err
}

func RemovePerm(userID int64) error {
	_, err := removePermStmt.Exec(userID)
	return err
}

func GetRoleList() ([]*RoleDetail, error) {
	rows, err := getRoleListStmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		id                 sql.NullInt64
		rollenName         sql.NullString
		kannEinloggen      sql.NullBool
		alarmAdmin         sql.NullString
		alarmGebiete       sql.NullString
		alarmGebietgruppen sql.NullString
		alarmTeilnehmer    sql.NullString
		alarmAlarme        sql.NullString
		userAdmin          sql.NullString
		gruppenAdmin       sql.NullString
		blidsCounter       sql.NullString
	)

	var rolsDetail []*RoleDetail

	for rows.Next() {
		err := rows.Scan(
			&id,
			&rollenName,
			&kannEinloggen,
			&alarmAdmin,
			&alarmGebiete,
			&alarmGebietgruppen,
			&alarmTeilnehmer,
			&alarmAlarme,
			&userAdmin,
			&gruppenAdmin,
			&blidsCounter)

		if err != nil {
			return nil, err
		}

		var rd RoleDetail

		if id.Valid {
			rd.ID = id.Int64
		}
		if rollenName.Valid {
			rd.RoleName = rollenName.String
		}
		if kannEinloggen.Valid {
			rd.CanLogIn = FakeBoolPointer(kannEinloggen.Bool)
		}
		if alarmAdmin.Valid {
			rd.AlarmAdmin = alarmAdmin.String
		}
		if alarmGebiete.Valid {
			rd.AlarmArea = alarmGebiete.String
		}
		if alarmGebietgruppen.Valid {
			rd.AlarmAreaGroups = alarmGebietgruppen.String
		}
		if alarmTeilnehmer.Valid {
			rd.AlarmParticipants = alarmTeilnehmer.String
		}
		if alarmAlarme.Valid {
			rd.AlarmAlarms = alarmAlarme.String
		}
		if userAdmin.Valid {
			rd.UserAdmin = userAdmin.String
		}
		if gruppenAdmin.Valid {
			rd.GroupsAdmin = gruppenAdmin.String
		}

		if blidsCounter.Valid {
			rd.BlidsCounter = blidsCounter.String
		}
		rolsDetail = append(rolsDetail, &rd)

	}
	return rolsDetail, rows.Err()
}

func GetRoleDetail(roleID int64) (*RoleDetail, error) {

	var (
		rd                 RoleDetail
		id                 sql.NullInt64
		rollenName         sql.NullString
		kannEinloggen      sql.NullBool
		alarmAdmin         sql.NullString
		alarmGebiete       sql.NullString
		alarmGebietgruppen sql.NullString
		alarmTeilnehmer    sql.NullString
		alarmAlarme        sql.NullString
		userAdmin          sql.NullString
		gruppenAdmin       sql.NullString
	)

	err := getRoleDetailStmt.QueryRow(roleID).Scan(
		&id,
		&rollenName,
		&kannEinloggen,
		&alarmAdmin,
		&alarmGebiete,
		&alarmGebietgruppen,
		&alarmTeilnehmer,
		&alarmAlarme,
		&userAdmin,
		&gruppenAdmin)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	if id.Valid {
		rd.ID = id.Int64
	}
	if rollenName.Valid {
		rd.RoleName = rollenName.String
	}
	if kannEinloggen.Valid {
		rd.CanLogIn = FakeBoolPointer(kannEinloggen.Bool)
	}
	if alarmAdmin.Valid {
		rd.AlarmAdmin = alarmAdmin.String
	}
	if alarmGebiete.Valid {
		rd.AlarmArea = alarmGebiete.String
	}
	if alarmGebietgruppen.Valid {
		rd.AlarmAreaGroups = alarmGebietgruppen.String
	}
	if alarmTeilnehmer.Valid {
		rd.AlarmParticipants = alarmTeilnehmer.String
	}
	if alarmAlarme.Valid {
		rd.AlarmAlarms = alarmAlarme.String
	}
	if userAdmin.Valid {
		rd.UserAdmin = userAdmin.String
	}
	if gruppenAdmin.Valid {
		rd.GroupsAdmin = gruppenAdmin.String
	}

	return &rd, nil
}

func GetPermissionList() ([]*PermissionList, error) {
	rows, err := getPermissionListStmt.Query()

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		benutzername sql.NullString
		benutzerID   sql.NullInt64
		gruppenname  sql.NullString
		gruppenID    sql.NullInt64
		rollenname   sql.NullString
		rollenID     sql.NullInt64
	)

	var permissionsDetail []*PermissionList

	for rows.Next() {
		err := rows.Scan(
			&benutzername,
			&benutzerID,
			&gruppenname,
			&gruppenID,
			&rollenname,
			&rollenID)

		if err != nil {
			return nil, err
		}

		var pl PermissionList

		if benutzername.Valid {
			pl.UserName = benutzername.String
		}
		if benutzerID.Valid {
			pl.UserID = benutzerID.Int64
		}
		if gruppenname.Valid {
			pl.GroupName = gruppenname.String
		}
		if gruppenID.Valid {
			pl.GroupID = gruppenID.Int64
		}
		if rollenname.Valid {
			pl.RoleName = rollenname.String
		}
		if rollenID.Valid {
			pl.RoleID = rollenID.Int64
		}
		permissionsDetail = append(permissionsDetail, &pl)

	}
	return permissionsDetail, rows.Err()
}

func UpdatePermission(permission *PermissionParameters) error {

	_, err := updatePermissionStmt.Exec(
		permission.UserID,
		permission.GroupID,
		permission.RoleID,
		permission.OldGroupID,
		permission.OldRoleID)
	return err
}

func UpdateAlarm(areaID, participantID int64, alarmID *int64, customerID int64) (int64, error) {
	var r sql.Result
	var err error

	if alarmID == nil {
		r, err = insertAlarmStmt.Exec(
			areaID,
			participantID,
			customerID)
	} else {
		r, err = updateAlarmStmt.Exec(
			areaID,
			participantID,
			alarmID,
			customerID)
	}
	if err != nil {
		return 0, err
	}

	return r.RowsAffected()
}

func RemoveAlarm(alarm AlarmIDs) (int64, error) {
	r, err := removeAlarmStmt.Exec(alarm.ID, alarm.CustomerID)
	if err != nil {
		return 0, err
	}
	return r.RowsAffected()
}

func RemoveAlarmArea(area AlarmAreaIDs) (int64, error) {
	if *area.Cascade {
		// XXX: As in many cases this should be in one transaction with
		//      the other delete operation.
		_, err := removeAlarmByAreaStmt.Exec(area.ID, area.CustomerID)
		if err != nil {
			return 0, err
		}
	}

	if used, err := CountAreaInAlarms(*area.ID); err != nil {
		return 0, err
	} else if used > 0 {
		return 0, &DependendObjectsError{used, "alarm"}
	}

	r, err := removeAlarmAreaStmt.Exec(area.ID, area.CustomerID)
	if err != nil {
		return 0, err
	}

	return r.RowsAffected()
}

func GetAlarmList(groupID int64) ([]*AlarmDetail, error) {
	rows, err := getAlarmListStmt.Query(groupID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		gebieteBlitzeZumAlarm    sql.NullInt64
		dauer                    sql.NullInt64
		gebieteBlitzAnzahl       sql.NullInt64
		gebieteRadius            sql.NullFloat64
		gebieteAlarm             sql.NullBool
		gebieteGebiet            sql.NullString
		gebietID                 sql.NullInt64
		teilnehmerStartzeit      pq.NullTime
		stopTimeHour             sql.NullInt64
		stopTimeMinute           sql.NullInt64
		stopTimeSecond           sql.NullInt64
		beginn                   sql.NullString
		ende                     sql.NullString
		teilnehmerMo             sql.NullBool
		teilnehmerDi             sql.NullBool
		teilnehmerMi             sql.NullBool
		teilnehmerDon            sql.NullBool
		teilnehmerFr             sql.NullBool
		teilnehmerSa             sql.NullBool
		teilnehmerSo             sql.NullBool
		teilnehmerNurLog         sql.NullBool
		teilnehmerUTC            sql.NullBool
		protocol                 sql.NullBool
		csv                      sql.NullBool
		teilnehmerSMS            sql.NullString
		teilnehmerEmail          sql.NullString
		teilnehmerFax            sql.NullString
		teilnehmerTel            sql.NullString
		teilnehmerTeilnehmer     sql.NullString
		teilnehmerID             sql.NullInt64
		gebietGruppenGruppenName sql.NullString
		alarmID                  sql.NullInt64
		alarmKundeID             sql.NullInt64
		kundenKunde              sql.NullString
		letzter                  sql.NullString
		centerx                  sql.NullFloat64
		centery                  sql.NullFloat64
		flach                    sql.NullFloat64
	)

	var adList []*AlarmDetail

	for rows.Next() {
		err := rows.Scan(
			&gebieteBlitzeZumAlarm,
			&dauer,
			&gebieteBlitzAnzahl,
			&gebieteRadius,
			&gebieteAlarm,
			&gebieteGebiet,
			&gebietID,
			&teilnehmerStartzeit,
			&stopTimeHour,
			&stopTimeMinute,
			&stopTimeSecond,
			&beginn,
			&ende,
			&teilnehmerMo,
			&teilnehmerDi,
			&teilnehmerMi,
			&teilnehmerDon,
			&teilnehmerFr,
			&teilnehmerSa,
			&teilnehmerSo,
			&teilnehmerNurLog,
			&teilnehmerUTC,
			&protocol,
			&csv,
			&teilnehmerSMS,
			&teilnehmerEmail,
			&teilnehmerFax,
			&teilnehmerTel,
			&teilnehmerTeilnehmer,
			&teilnehmerID,
			&gebietGruppenGruppenName,
			&alarmID,
			&alarmKundeID,
			&kundenKunde,
			&letzter,
			&centerx,
			&centery,
			&flach,
		)

		if err != nil {
			return nil, err
		}

		var ad AlarmDetail

		if gebieteBlitzeZumAlarm.Valid {
			ad.AreasBlidsToAlarm = gebieteBlitzeZumAlarm.Int64
		}
		if dauer.Valid {
			ad.Duration = dauer.Int64
		}
		if gebieteBlitzAnzahl.Valid {
			ad.AreasBlidsNumber = gebieteBlitzAnzahl.Int64
		}
		if gebieteRadius.Valid {
			ad.AreasRadius = gebieteRadius.Float64
		}
		if gebieteAlarm.Valid {
			ad.AreasAlarm = FakeBoolPointer(gebieteAlarm.Bool)
		}
		if gebieteGebiet.Valid {
			ad.AreasArea = gebieteGebiet.String
		}
		if gebietID.Valid {
			ad.AreaID = gebietID.Int64
		}
		if teilnehmerStartzeit.Valid {
			ad.ParticipantStartTime = teilnehmerStartzeit.Time.Format(timeFormat)
		}
		if stopTimeHour.Valid && stopTimeMinute.Valid && stopTimeSecond.Valid {
			ad.ParticipantStopTime = fmt.Sprintf("%02d:%02d:%02d",
				stopTimeHour.Int64,
				stopTimeMinute.Int64,
				stopTimeSecond.Int64)
		}
		if beginn.Valid {
			ad.Start = beginn.String
		}
		if ende.Valid {
			ad.End = ende.String
		}
		if teilnehmerMo.Valid {
			ad.ParticipantMon = FakeBoolPointer(teilnehmerMo.Bool)
		}
		if teilnehmerDi.Valid {
			ad.ParticipantTue = FakeBoolPointer(teilnehmerDi.Bool)
		}
		if teilnehmerMi.Valid {
			ad.ParticipantWed = FakeBoolPointer(teilnehmerMi.Bool)
		}
		if teilnehmerDon.Valid {
			ad.ParticipantThu = FakeBoolPointer(teilnehmerDon.Bool)
		}
		if teilnehmerFr.Valid {
			ad.ParticipantFri = FakeBoolPointer(teilnehmerFr.Bool)
		}
		if teilnehmerSa.Valid {
			ad.ParticipantSat = FakeBoolPointer(teilnehmerSa.Bool)
		}
		if teilnehmerSo.Valid {
			ad.ParticipantSun = FakeBoolPointer(teilnehmerSo.Bool)
		}
		if teilnehmerNurLog.Valid {
			ad.ParticipantOnlyLog = FakeBoolPointer(teilnehmerNurLog.Bool)
		}
		if teilnehmerUTC.Valid {
			ad.ParticipantUTC = FakeBoolPointer(teilnehmerUTC.Bool)
		}
		if protocol.Valid {
			ad.Protocol = FakeBoolPointer(protocol.Bool)
		}
		if csv.Valid {
			ad.CSV = FakeBoolPointer(csv.Bool)
		}
		if teilnehmerSMS.Valid {
			ad.ParticipantSMS = teilnehmerSMS.String
		}
		if teilnehmerEmail.Valid {
			ad.ParticipantEMail = teilnehmerEmail.String
		}
		if teilnehmerFax.Valid {
			ad.ParticipantFax = teilnehmerFax.String
		}
		if teilnehmerTel.Valid {
			ad.ParticipantTel = teilnehmerTel.String
		}
		if teilnehmerTeilnehmer.Valid {
			ad.ParticipantParticipant = teilnehmerTeilnehmer.String
		}
		if teilnehmerID.Valid {
			ad.ParticipantID = teilnehmerID.Int64
		}
		if gebietGruppenGruppenName.Valid {
			ad.AreaGroupsGroupsName = gebietGruppenGruppenName.String
		}
		if alarmID.Valid {
			ad.AlarmID = alarmID.Int64
		}
		if alarmKundeID.Valid {
			ad.AlarmCustomerID = alarmKundeID.Int64
		}
		if kundenKunde.Valid {
			ad.CustomerCustomer = kundenKunde.String
		}
		if letzter.Valid {
			ad.Last = letzter.String
		}
		if centerx.Valid {
			ad.Centerx = centerx.Float64
		}
		if centery.Valid {
			ad.Centery = centery.Float64
		}
		if flach.Valid {
			ad.Area = flach.Float64
		}

		adList = append(adList, &ad)
	}

	return adList, rows.Err()
}

func GetAlarmAreaListSimple(groupID int64) ([]*AlarmAreaListSimple, error) {
	rows, err := getAlarmAreaListSimpleStmt.Query(groupID)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		id       sql.NullInt64
		gebiet   sql.NullString
		kundenid sql.NullInt64
	)

	var aglsList []*AlarmAreaListSimple

	for rows.Next() {
		err := rows.Scan(
			&id,
			&gebiet,
			&kundenid,
		)

		if err != nil {
			return nil, err
		}

		var agls AlarmAreaListSimple

		if id.Valid {
			agls.ID = id.Int64
		}
		if gebiet.Valid {
			agls.Area = gebiet.String
		}
		if kundenid.Valid {
			agls.CustomerID = kundenid.Int64
		}
		aglsList = append(aglsList, &agls)
	}
	return aglsList, rows.Err()
}

func GetAlarmParticipantsListSimple(groupID int64) ([]*AlarmParticipantsListSimple, error) {
	rows, err := getAlarmParticipantsListSimpleStmt.Query(groupID)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		id         sql.NullInt64
		teilnehmer sql.NullString
		kunde      sql.NullString
	)

	var aplsList []*AlarmParticipantsListSimple

	for rows.Next() {
		err := rows.Scan(
			&id,
			&teilnehmer,
			&kunde)

		if err != nil {
			return nil, err
		}

		var apls AlarmParticipantsListSimple

		if id.Valid {
			apls.ID = id.Int64
		}
		if teilnehmer.Valid {
			apls.Participant = teilnehmer.String
		}
		if kunde.Valid {
			apls.Customer = kunde.String
		}

		aplsList = append(aplsList, &apls)
	}
	return aplsList, rows.Err()
}

func GetAlarmParticipantsList(groupID int64, participantID *int64) ([]*AlarmParticipants, error) {
	var err error
	var rows *sql.Rows
	if participantID != nil {
		rows, err = getAlarmParticipantsListParticipantIDStmt.Query(groupID, *participantID)
	} else {
		rows, err = getAlarmParticipantsListStmt.Query(groupID)
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		teilnehmerID             sql.NullInt64
		teilnehmerTeilnehmer     sql.NullString
		teilnehmerSMS            sql.NullString
		teilnehmerEmail          sql.NullString
		teilnehmerTel            sql.NullString
		teilnehmerFax            sql.NullString
		teilnehmerAlarmierung    sql.NullString
		teilnehmerEntwarnung     sql.NullString
		teilnehmerKunde          sql.NullInt64
		teilnehmerStartdatum     pq.NullTime
		teilnehmerStopdatum      pq.NullTime
		teilnehmerStartZeit      pq.NullTime
		stopTimeHour             sql.NullInt64
		stopTimeMinute           sql.NullInt64
		stopTimeSecond           sql.NullInt64
		teilnehmerMo             sql.NullBool
		teilnehmerDi             sql.NullBool
		teilnehmerMi             sql.NullBool
		teilnehmerDon            sql.NullBool
		teilnehmerFr             sql.NullBool
		teilnehmerSa             sql.NullBool
		teilnehmerSo             sql.NullBool
		teilnehmerNurLog         sql.NullBool
		teilnehmerUTC            sql.NullBool
		teilnehmerBetreffAlarm   sql.NullString
		teilnehmerBetreffentWarn sql.NullString
		teilnehmerBlitzProtokoll sql.NullBool
		teilnehmerBlitzascsv     sql.NullBool
		anfang                   sql.NullString
		ende                     sql.NullString
		teilnehmerAktivAktiv     sql.NullBool
		kundenname               sql.NullString
	)

	var apList []*AlarmParticipants
	for rows.Next() {
		err := rows.Scan(
			&teilnehmerID,
			&teilnehmerTeilnehmer,
			&teilnehmerSMS,
			&teilnehmerEmail,
			&teilnehmerTel,
			&teilnehmerFax,
			&teilnehmerAlarmierung,
			&teilnehmerEntwarnung,
			&teilnehmerKunde,
			&teilnehmerStartdatum,
			&teilnehmerStopdatum,
			&teilnehmerStartZeit,
			&stopTimeHour,
			&stopTimeMinute,
			&stopTimeSecond,
			&teilnehmerMo,
			&teilnehmerDi,
			&teilnehmerMi,
			&teilnehmerDon,
			&teilnehmerFr,
			&teilnehmerSa,
			&teilnehmerSo,
			&teilnehmerNurLog,
			&teilnehmerUTC,
			&teilnehmerBetreffAlarm,
			&teilnehmerBetreffentWarn,
			&teilnehmerBlitzProtokoll,
			&teilnehmerBlitzascsv,
			&anfang,
			&ende,
			&teilnehmerAktivAktiv,
			&kundenname,
		)
		if err != nil {
			return nil, err
		}

		var ap AlarmParticipants

		if teilnehmerID.Valid {
			ap.ID = teilnehmerID.Int64
		}
		if teilnehmerTeilnehmer.Valid {
			ap.Participant = teilnehmerTeilnehmer.String
		}
		if teilnehmerSMS.Valid {
			ap.SMS = teilnehmerSMS.String
		}
		if teilnehmerEmail.Valid {
			ap.EMail = teilnehmerEmail.String
		}
		if teilnehmerTel.Valid {
			ap.Tel = teilnehmerTel.String
		}
		if teilnehmerFax.Valid {
			ap.Fax = teilnehmerFax.String
		}
		if teilnehmerAlarmierung.Valid {
			ap.Alarm = teilnehmerAlarmierung.String
		}
		if teilnehmerEntwarnung.Valid {
			ap.AllClear = teilnehmerEntwarnung.String
		}
		if teilnehmerKunde.Valid {
			ap.Customer = teilnehmerKunde.Int64
		}
		if anfang.Valid {
			ap.Begin = anfang.String
		}
		if ende.Valid {
			ap.End = ende.String
		}

		if teilnehmerStartdatum.Valid {
			ap.StartDate = teilnehmerStartdatum.Time.Format(dateFormat)
		}
		if teilnehmerStopdatum.Valid {
			ap.StopDate = teilnehmerStopdatum.Time.Format(dateFormat)
		}

		if teilnehmerStartZeit.Valid {
			ap.StartTime = teilnehmerStartZeit.Time.Format(timeFormat)
		}
		if stopTimeHour.Valid && stopTimeMinute.Valid && stopTimeSecond.Valid {
			stopTimeHourString := fmt.Sprintf("%02d", stopTimeHour.Int64)
			stopTimeMinuteString := fmt.Sprintf("%02d", stopTimeMinute.Int64)
			stopTimeSecondString := fmt.Sprintf("%02d", stopTimeSecond.Int64)

			ap.StopTime = stopTimeHourString + `:` + stopTimeMinuteString + `:` + stopTimeSecondString
		}
		if teilnehmerMo.Valid {
			ap.Mon = FakeBoolPointer(teilnehmerMo.Bool)
		}
		if teilnehmerDi.Valid {
			ap.Tue = FakeBoolPointer(teilnehmerDi.Bool)
		}
		if teilnehmerMi.Valid {
			ap.Wed = FakeBoolPointer(teilnehmerMi.Bool)
		}
		if teilnehmerDon.Valid {
			ap.Thu = FakeBoolPointer(teilnehmerDon.Bool)
		}
		if teilnehmerFr.Valid {
			ap.Fri = FakeBoolPointer(teilnehmerFr.Bool)
		}
		if teilnehmerSa.Valid {
			ap.Sat = FakeBoolPointer(teilnehmerSa.Bool)
		}
		if teilnehmerSo.Valid {
			ap.Sun = FakeBoolPointer(teilnehmerSo.Bool)
		}
		if teilnehmerNurLog.Valid {
			ap.OnlyLog = FakeBoolPointer(teilnehmerNurLog.Bool)
		}
		if teilnehmerUTC.Valid {
			ap.UTC = FakeBoolPointer(teilnehmerUTC.Bool)
		}
		if teilnehmerBetreffAlarm.Valid {
			ap.SubjectAlarm = teilnehmerBetreffAlarm.String
		}
		if teilnehmerBetreffentWarn.Valid {
			ap.SubjectAllClear = teilnehmerBetreffentWarn.String
		}
		if teilnehmerBlitzProtokoll.Valid {
			ap.BlidsLog = FakeBoolPointer(teilnehmerBlitzProtokoll.Bool)
		}
		if teilnehmerBlitzascsv.Valid {
			ap.BlidsASCSV = FakeBoolPointer(teilnehmerBlitzascsv.Bool)
		}
		if teilnehmerAktivAktiv.Valid {
			ap.Active = FakeBoolPointer(teilnehmerAktivAktiv.Bool)
		}
		if kundenname.Valid {
			ap.CustomerName = kundenname.String
		}

		apList = append(apList, &ap)
	}

	return apList, rows.Err()
}

func GetAlarmGebietList(groupID int64, areaID *int64) ([]*AlarmAreaList, error) {
	var (
		err                   error
		gebieteID             sql.NullInt64
		gebieteGebiet         sql.NullString
		laenge                sql.NullFloat64
		breite                sql.NullFloat64
		gebieteRadius         sql.NullFloat64
		gebieteInnerRadius    sql.NullFloat64
		gebieteBlitzeZumAlarm sql.NullInt64
		letzter               sql.NullString
		anzahl                sql.NullInt64
		flach                 sql.NullFloat64
		gebieteAlarmDauer     sql.NullInt64
		kundenID              sql.NullInt64
		kundenKunde           sql.NullString
	)
	var aaList []*AlarmAreaList

	var rows *sql.Rows

	if areaID != nil {
		// TODO Test
		var (
			geometry sql.NullString
			geojson  sql.NullString
		)

		rows, err = getAlarmAreaListAreaIDStmt.Query(groupID, *areaID)
		if err != nil {
			return nil, err
		}
		defer rows.Close()

		for rows.Next() {
			err = rows.Scan(
				&gebieteID,
				&gebieteGebiet,
				&laenge,
				&breite,
				&gebieteRadius,
				&gebieteInnerRadius,
				&gebieteBlitzeZumAlarm,
				&letzter,
				&anzahl,
				&flach,
				&gebieteAlarmDauer,
				&kundenID,
				&geometry,
				&geojson,
				&kundenKunde,
			)
			if err != nil {
				return nil, err
			}

			var aa AlarmAreaList

			if gebieteID.Valid {
				aa.ID = gebieteID.Int64
			}
			if gebieteGebiet.Valid {
				aa.Area = gebieteGebiet.String
			}
			if laenge.Valid {
				aa.Length = laenge.Float64
			}
			if breite.Valid {
				aa.Wide = breite.Float64
			}
			if gebieteRadius.Valid {
				aa.AreasRadius = gebieteRadius.Float64
			}
			if gebieteInnerRadius.Valid {
				aa.AreasInnerRadius = gebieteInnerRadius.Float64
			}
			if gebieteBlitzeZumAlarm.Valid {
				aa.AreasBlidsAlarm = gebieteBlitzeZumAlarm.Int64
			}
			if letzter.Valid {
				aa.Last = letzter.String
			}
			if anzahl.Valid {
				aa.Number = anzahl.Int64
			}
			if flach.Valid {
				aa.Surface = flach.Float64
			}
			if gebieteAlarmDauer.Valid {
				aa.AreasAlarmDuration = gebieteAlarmDauer.Int64
			}
			if kundenID.Valid {
				aa.CustomerID = kundenID.Int64
			}
			if geometry.Valid {
				aa.Geometry = &geometry.String
			}
			if geojson.Valid {
				dec := json.NewDecoder(strings.NewReader(geojson.String))
				if err := dec.Decode(&aa.GeoJSON); err != nil {
					return nil, err
				}
			}
			if kundenKunde.Valid {
				aa.Customer = kundenKunde.String
			}
			aaList = append(aaList, &aa)

		}

	} else {
		rows, err = getAlarmAreaListStmt.Query(groupID)
		if err != nil {
			return nil, err
		}
		defer rows.Close()

		for rows.Next() {
			err = rows.Scan(
				&gebieteID,
				&gebieteGebiet,
				&laenge,
				&breite,
				&gebieteRadius,
				&gebieteInnerRadius,
				&gebieteBlitzeZumAlarm,
				&letzter,
				&anzahl,
				&flach,
				&gebieteAlarmDauer,
				&kundenID,
				&kundenKunde,
			)
			if err != nil {
				return nil, err
			}

			var aa AlarmAreaList

			if gebieteID.Valid {
				aa.ID = gebieteID.Int64
			}
			if gebieteGebiet.Valid {
				aa.Area = gebieteGebiet.String
			}
			if laenge.Valid {
				aa.Length = laenge.Float64
			}
			if breite.Valid {
				aa.Wide = breite.Float64
			}
			if gebieteRadius.Valid {
				aa.AreasRadius = gebieteRadius.Float64
			}
			if gebieteInnerRadius.Valid {
				aa.AreasInnerRadius = gebieteInnerRadius.Float64
			}
			if gebieteBlitzeZumAlarm.Valid {
				aa.AreasBlidsAlarm = gebieteBlitzeZumAlarm.Int64
			}
			if letzter.Valid {
				aa.Last = letzter.String
			}
			if anzahl.Valid {
				aa.Number = anzahl.Int64
			}
			if flach.Valid {
				aa.Surface = flach.Float64
			}
			if gebieteAlarmDauer.Valid {
				aa.AreasAlarmDuration = gebieteAlarmDauer.Int64
			}
			if kundenID.Valid {
				aa.CustomerID = kundenID.Int64
			}
			if kundenKunde.Valid {
				aa.Customer = kundenKunde.String
			}
			aaList = append(aaList, &aa)
		}
	}
	return aaList, rows.Err()
}

func GetAlarmAreaGroupList(groupID int64) ([]*AlarmAreaGroup, error) {
	rows, err := getAlarmAreaGroupListStmt.Query(groupID)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		id          sql.NullInt64
		gruppenname sql.NullString
		alarm       sql.NullBool
		kunde       sql.NullInt64
		kundenName  sql.NullString
	)

	var aagList []*AlarmAreaGroup

	for rows.Next() {
		err = rows.Scan(
			&id,
			&gruppenname,
			&alarm,
			&kunde,
			&kundenName)
		if err != nil {
			return nil, err
		}

		var aag AlarmAreaGroup

		if id.Valid {
			aag.ID = id.Int64
		}
		if gruppenname.Valid {
			aag.GroupName = gruppenname.String
		}
		if alarm.Valid {
			aag.Alarm = FakeBoolPointer(alarm.Bool)
		}
		if kunde.Valid {
			aag.Customer = kunde.Int64
		}
		if kundenName.Valid {
			aag.CustomerName = kundenName.String
		}

		aagList = append(aagList, &aag)
	}

	return aagList, rows.Err()
}

func GetAlarmAreaGroup(groupListID int64) ([]*Area, error) {

	rows, err := getAlarmAreaGroupStmt.Query(groupListID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		gebiet sql.NullString
		id     sql.NullInt64
	)

	var aList []*Area
	for rows.Next() {
		if err = rows.Scan(&gebiet, &id); err != nil {
			return nil, err
		}
		var a Area

		if gebiet.Valid {
			a.Area = gebiet.String
		}
		if id.Valid {
			a.ID = id.Int64
		}
		aList = append(aList, &a)
	}

	return aList, rows.Err()
}

func GetAlarmCustomerList(groupID int64) ([]*AlarmCustomer, error) {

	var (
		rows         *sql.Rows
		err          error
		customerID   sql.NullInt64
		customerName sql.NullString
		customerList []*AlarmCustomer
	)
	if groupID > 0 {
		rows, err = getAlarmCustomerListGroupIDStmt.Query(groupID)
	} else {
		rows, err = getAlarmCustomerListStmt.Query()
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&customerID, &customerName)

		if err != nil {
			return nil, err
		}

		var customer AlarmCustomer
		if customerID.Valid {
			customer.ID = customerID.Int64
		}
		if customerName.Valid {
			customer.Name = customerName.String
		}
		customerList = append(customerList, &customer)

	}

	return customerList, rows.Err()
}

func GetAlarmMessages(alarmID int64, startString, endString string) ([]*AlarmMessages, error) {
	startTime, err := stringToTime(startString, germanFormat)
	if err != nil {
		return nil, err
	}
	endTime, err := stringToTime(endString, germanFormat)
	if err != nil {
		return nil, err
	}

	var (
		anf      sql.NullString
		ende     sql.NullString
		id       sql.NullInt64
		alarm    sql.NullInt64
		eintrag  sql.NullString
		erledigt sql.NullString
		typ      sql.NullString
	)

	rows, err := getAlarmMessagesStmt.Query(alarmID, startTime, endTime)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var amList []*AlarmMessages

	for rows.Next() {
		err := rows.Scan(
			&anf,
			&ende,
			&id,
			&alarm,
			&eintrag,
			&erledigt,
			&typ,
		)
		if err != nil {
			return nil, err
		}

		var am AlarmMessages

		if anf.Valid {
			am.Start = anf.String
		}
		if ende.Valid {
			am.End = ende.String
		}
		if id.Valid {
			am.ID = id.Int64
		}
		if alarm.Valid {
			am.Alarm = alarm.Int64
		}
		if eintrag.Valid {
			am.Entry = eintrag.String
		}
		if erledigt.Valid {
			am.Finished = erledigt.String
		}
		if typ.Valid {
			am.Typ = typ.String
		}

		amList = append(amList, &am)
	}

	return amList, rows.Err()
}

func UpdateAlarmArea(area *AlarmArea) (*int64, error) {
	var id int64
	var row *sql.Row

	if area.ID != nil && *area.ID > 0 {
		if area.AreaInnerRadius != nil && *area.AreaInnerRadius > 0 {
			row = updateAlarmAreaRingStmt.QueryRow(
				area.AreaName,
				area.CustomerID,
				area.AlarmThreshold,
				area.AlarmDuration,
				area.AreaRadius,
				area.AreaInnerRadius,
				area.AreaLon,
				area.AreaLat,
				area.ID)
		} else if area.AreaRadius != nil && *area.AreaRadius > 0 {
			row = updateAlarmAreaCircleStmt.QueryRow(
				area.AreaName,
				area.CustomerID,
				area.AlarmThreshold,
				area.AlarmDuration,
				area.AreaRadius,
				area.AreaLon,
				area.AreaLat,
				area.ID)
		} else {
			row = updateAlarmAreaGeomStmt.QueryRow(
				area.AreaName,
				area.CustomerID,
				area.AlarmThreshold,
				area.AlarmDuration,
				area.AreaGeom,
				area.ID)
		}
	} else {
		if area.AreaInnerRadius != nil && *area.AreaInnerRadius > 0 {
			row = insertAlarmAreaRingStmt.QueryRow(
				area.AreaName,
				area.CustomerID,
				area.AlarmThreshold,
				area.AlarmDuration,
				area.AreaRadius,
				area.AreaInnerRadius,
				area.AreaLon,
				area.AreaLat)
		} else if area.AreaRadius != nil && *area.AreaRadius > 0 {
			row = insertAlarmAreaCircleStmt.QueryRow(
				area.AreaName,
				area.CustomerID,
				area.AlarmThreshold,
				area.AlarmDuration,
				area.AreaRadius,
				area.AreaLon,
				area.AreaLat)
		} else {
			row = insertAlarmAreaGeomStmt.QueryRow(
				area.AreaName,
				area.CustomerID,
				area.AlarmThreshold,
				area.AlarmDuration,
				area.AreaGeom)
		}
	}
	err := row.Scan(&id)
	return &id, err
}

func UpdateAlarmParticipant(participantInfo *ParticipantInfo) (*int64, error) {

	start, err := stringToNullTime(participantInfo.Start, isoFormat)
	if err != nil {
		return nil, err
	}
	end, err := stringToNullTime(participantInfo.End, isoFormat)
	if err != nil {
		return nil, err
	}
	startTime, err := stringToNullTime(participantInfo.StartTime, timeFormat)
	if err != nil {
		return nil, err
	}

	var stopHour, stopMinute, stopSecond int64

	if participantInfo.StopTime == "" {
		stopHour, stopMinute, stopSecond = 24, 0, 0
	} else {
		s := strings.Split(participantInfo.StopTime, ":")
		stopHour, err = strconv.ParseInt(s[0], 0, 64)
		if err != nil {
			return nil, err
		}
		stopMinute, err = strconv.ParseInt(s[1], 0, 64)
		if err != nil {
			return nil, err
		}
		stopSecond, err = strconv.ParseInt(s[2], 0, 64)
		if err != nil {
			return nil, err
		}
	}

	var row *sql.Row

	switch {

	case participantInfo.ID != nil && *participantInfo.ID > 0:
		row = updateAlarmParticipantIDStmt.QueryRow(
			participantInfo.Participant,
			participantInfo.SMS,
			participantInfo.EMail,
			participantInfo.Tel,
			participantInfo.Fax,
			participantInfo.Alarming,
			participantInfo.AllClear,
			start,
			end,
			startTime,
			stopHour,
			stopMinute,
			stopSecond,
			participantInfo.Mon,
			participantInfo.Tue,
			participantInfo.Wed,
			participantInfo.Thu,
			participantInfo.Fri,
			participantInfo.Sat,
			participantInfo.Sun,
			participantInfo.OnlyLog,
			participantInfo.UTC,
			participantInfo.BlidsLog,
			participantInfo.BlitzASCSV,
			*participantInfo.ID,
			participantInfo.CustomerID,
		)
	case participantInfo.Default != nil && bool(*participantInfo.Default):
		row = updateAlarmParticipantDefaultStmt.QueryRow(
			participantInfo.Participant,
			participantInfo.SMS,
			participantInfo.EMail,
			participantInfo.Alarming,
			participantInfo.AllClear,
			start,
			end,
			participantInfo.CustomerID,
		)
	default:
		row = updateAlarmParticipantStmt.QueryRow(
			participantInfo.Participant,
			participantInfo.SMS,
			participantInfo.EMail,
			participantInfo.Tel,
			participantInfo.Fax,
			participantInfo.Alarming,
			participantInfo.AllClear,
			start,
			end,
			startTime,
			stopHour,
			stopMinute,
			stopSecond,
			participantInfo.Mon,
			participantInfo.Tue,
			participantInfo.Wed,
			participantInfo.Thu,
			participantInfo.Fri,
			participantInfo.Sat,
			participantInfo.Sun,
			participantInfo.OnlyLog,
			participantInfo.UTC,
			participantInfo.BlidsLog,
			participantInfo.BlitzASCSV,
			participantInfo.CustomerID,
		)
	}

	var id int64
	err = row.Scan(&id)

	return &id, err
}

func RemoveAlarmParticipant(participant *AlarmParticipantIDs) (int64, error) {
	if *participant.Cascade {
		// XXX: As in many cases this should be in one transaction with
		//      the other delete operation.
		_, err := removeAlarmByParticipantStmt.Exec(
			*participant.ID,
			*participant.CustomerID)
		if err != nil {
			return 0, err
		}
	}

	if used, err := CountParticipantInAlarms(*participant.ID); err != nil {
		return 0, err
	} else if used > 0 {
		return 0, &DependendObjectsError{used, "alarm"}
	}

	r, err := removeAlarmParticipantStmt.Exec(
		*participant.ID,
		*participant.CustomerID)
	if err != nil {
		return 0, err
	}

	return r.RowsAffected()
}

func SendTestAlarm(alarm TestAlarm) (int64, error) {
	var (
		id     int64
		typstr = [3]string{"", "Testalarm", "Testentwarnung"}
	)
	if *alarm.Typ < 1 || *alarm.Typ > 2 {
		return 0, errors.New("Invalid Alarm Typ")
	}
	err := sendTestAlarmStmt.QueryRow(
		alarm.ID, typstr[*alarm.Typ]).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func AddAlarmCustomer(groupID int64, customer string) (int64, error) {
	var id int64
	if err := addAlarmCustomerStmt.QueryRow(customer).Scan(&id); err != nil {
		return 0, err
	}
	_, err := addGroup2AlarmStmt.Exec(groupID, id)
	return id, err
}
