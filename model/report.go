// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import (
	"strings"
	"time"

	"github.com/lib/pq"
)

// ShowLogParameters are used as JSON serialisation
// to request logging data.
type ShowLogParameters struct {
	GroupID   *int64  `json:"group"`
	StartTime *string `json:"start"`
	StopTime  *string `json:"stop"`
}

// Log is used as a JSON serialisation send containing
// a logging data set.
type Log struct {
	ID       int64  `json:"id"`
	UserName string `json:"benutzer"`
	Date     string `json:"datum"`
	Request  string `json:"request"`
	IP       string `json:"ip"`
	Session  string `json:"session"`
}

// ShowLog is used to fetch logging informations for given ShowLogParameters.
func ShowLog(slp *ShowLogParameters) ([]*Log, error) {
	var start, stop pq.NullTime
	if slp.StartTime != nil {
		v, err := time.Parse(germanFormat, *slp.StartTime)
		if err != nil {
			return nil, err
		}
		start = pq.NullTime{Time: v, Valid: true}
	}
	if slp.StopTime != nil {
		v, err := time.Parse(germanFormat, *slp.StopTime)
		if err != nil {
			return nil, err
		}
		stop = pq.NullTime{Time: v, Valid: true}
	}

	rows, err := showLogStmt.Query(*slp.GroupID, start, stop)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var lgs []*Log
	for rows.Next() {
		var lg Log
		var date pq.NullTime
		if err := rows.Scan(
			&lg.ID,
			&lg.UserName,
			&date,
			&lg.Request,
			&lg.IP,
			&lg.Session); err != nil {
			return nil, err
		}
		if date.Valid {
			lg.Date = date.Time.Format(dateFormat)
		}
		lgs = append(lgs, &lg)
	}

	return lgs, rows.Err()
}

// CountLoginSuccess counts the successfull logins.
func CountLoginSuccess(logs []*Log) int {
	// We already have the data  in memory so there is
	// no need to fetch it again from the database.
	var count int
	for _, lg := range logs {
		if strings.Contains(
			strings.ToLower(lg.Request), "login successfull") {
			count++
		}
	}
	return count
}
