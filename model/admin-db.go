// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import (
	db "bitbucket.org/intevation/kugelblitz/database"
)

var (
	getUserPermStmt = db.Must(`
SELECT
  rollenname,
  kann_einloggen,
  alarm_admin,
  alarm_gebiete,
  alarm_gebietgruppen,
  alarm_teilnehmer,
  alarm_alarme,
  user_admin,
  gruppen_admin
FROM
  benutzer.getUserPerm($1, $2)`)

	/* // This is from the original PHP code but
	   // it's not necessary to join over the session key
	   // if you have the user id ... which we have.
	   getHomeGroupStmt = db.Must(`
	   SELECT
	     t1.home_gruppen_id AS id
	   FROM
	     benutzer.benutzer t1, benutzer.sessions t2
	   WHERE t1.id = t2.benutzer AND t2.sessionid = $1 LIMIT 1`)
	*/
	getHomeGroupStmt = db.Must(`
SELECT
  home_gruppen_id
FROM
  benutzer.benutzer
WHERE id = $1`)

	countAreaInAlarms = db.Must(`
SELECT count(*) FROM alarmserver.alarme WHERE gebiet = $1`)

	countParticipantInAlarms = db.Must(`
SELECT count(*) FROM alarmserver.alarme WHERE teilnehmer = $1`)

	checkGroupAlarmCustomerStmt = db.Must(`
SELECT
  count(*)
FROM
  benutzer.gruppe2alarm
WHERE
  mastergruppe = $1
  AND slavekunde = $2`)

	checkCustomerAlarmStmt = db.Must(`
SELECT
  count(*)
FROM
  alarmserver.alarme
WHERE
  id = $2
  AND kunde = $1`)

	checkCustomerParticipantStmt = db.Must(`
SELECT
  count(*)
FROM
  alarmserver.teilnehmer
WHERE
  id = $2
  AND kunde = $1`)

	checkCustomerAreaStmt = db.Must(`
SELECT
  count(*)
FROM
   alarmserver.gebiete
WHERE
  id = $2
AND
  kunde = $1`)

	ipToHashStmt = db.Must(`
UPDATE
  benutzer.resetpw
SET
  ip= $3
WHERE
  benutzer = $1
  AND hash = $2`)

	updatePasswordStmt = db.Must(`
UPDATE
  benutzer.benutzer
SET
  passwort = md5($1)
WHERE id = $2`)

	getGroupDetailSmallStmt = db.Must(`
SELECT
  id,
  vatergruppe                    AS vatergruppenid,
  st_x(st_centroid(alarmgebiet)) AS laenge,
  st_y(st_centroid(alarmgebiet)) AS breite,
  box2d(alarmgebiet)             AS alarmgebiet,
  gruppenname,
  count(a.*)                     AS alarmkunden
FROM benutzer.gruppen LEFT OUTER JOIN benutzer.gruppe2alarm a
  ON (mastergruppe = id)
WHERE id = $1 GROUP BY id`)

	getChildrenStmt = db.Must(`SELECT id FROM benutzer.gruppen WHERE vatergruppe = $1`)

	getUsersStmt = db.Must(`
SELECT
  id,
  benutzername,
  lastname,
  firstname
FROM benutzer.benutzer
WHERE home_gruppen_id = $1`)

	getUserDetailsStmt = db.Must(`
SELECT
  t1.id,
  t1.benutzername,
  t1.home_gruppen_id,
  t2.gruppenname,
  t1.startdatum,
  t1.stopdatum,
  t1.firstname,
  t1.lastname,
  t1.company,
  t1.division,
  t1.streetadress,
  t1.zip,
  t1.city,
  t1.phonenumber,
  t1.annotation
FROM benutzer.benutzer t1, benutzer.gruppen t2
WHERE t2.id = t1.home_gruppen_id AND t1.id = $1`)

	getGroupListStmt = db.Must(`
SELECT
  id,
  gruppenname
FROM benutzer.gruppen ORDER BY gruppenname`)

	getGroupDetailStmt = db.Must(`
SELECT
  id,
  vatergruppe,
  gruppenname,
  theme_id,
  box2d(blitzgebiet) AS blitzgebiet,
  max_displayzeit,
  archiv_tage,
  archiv_ab,
  max_zoom,
  min_zoom,
  sound,
  liveblids,
  animation
FROM benutzer.gruppen t1
WHERE t1.id = $1`)

	addRoleStmt = db.Must(`
INSERT INTO benutzer.rollen(
  rollenname,
  kann_einloggen,
  alarm_admin,
  alarm_gebiete,
  alarm_gebietgruppen,
  alarm_teilnehmer,
  alarm_alarme,
  user_admin,
  gruppen_admin)
VALUES (
  $1,
  $2,
  $3,
  $4,
  $5,
  $6,
  $7,
  $8,
  $9)
RETURNING id`)

	addGroupStmt = db.Must(`
INSERT INTO benutzer.gruppen (
  vatergruppe,
  gruppenname,
  theme_id,
  blitzgebiet,
  max_displayzeit,
  archiv_tage,
  archiv_ab,
  max_zoom,
  min_zoom,
  liveblids,
  animation,
  sound,
  alarmgebiet)
VALUES (
  $1,
  $2,$3,
  ST_MakeEnvelope($4, $5, $6, $7, 4326),
  $8,
  $9,
  $10,
  $11,
  $12,
  $13,
  $14,
  $15,
  ST_MakeEnvelope($4, $5, $6, $7, 4326))
RETURNING id`)

	addLayerStmt = db.Must(`
INSERT INTO benutzer.gruppen_ebenen (gruppen_id, ebenen_id)
VALUES ($1, $2) RETURNING id`)

	copyLayerStmt = db.Must(`
UPDATE benutzer.gruppen_ebenen o SET
  anzeige_name    = c.anzeige_name,
  feature_info    = c.feature_info,
  checked_onlogin = c.checked_onlogin,
  permanent       = c.permanent,
  reload_time     = c.reload_time,
  cql_filter      = c.cql_filter
FROM benutzer.gruppen_ebenen c
WHERE c.id = $1 and o.id = $2`)

	removeGroupStmt = db.Must(`
DELETE FROM benutzer.gruppen WHERE id = $1`)

	removeGroupLayersStmt = db.Must(`
DELETE from benutzer.gruppen_ebenen WHERE gruppen_id = $1`)

	getLayerListStmt = db.Must(`
SELECT
  t1.id,
  t1.ebenen_id,
  t1.reload_time,
  t1.anzeige_name,
  t1.feature_info,
  t1.checked_onlogin,
  t1.permanent,
  t2.standardname
FROM benutzer.gruppen_ebenen t1, benutzer.ebenen t2
WHERE t1.gruppen_id = $1 AND t1.ebenen_id = t2.id`)

	groupIDStmt = db.Must(`
SELECT gruppen_id FROM benutzer.gruppen_ebenen WHERE id = $1`)

	deleteLayerIDStmt = db.Must(`
DELETE FROM benutzer.gruppen_ebenen WHERE id = $1`)

	updateGroupStmt = db.Must(`
UPDATE benutzer.gruppen SET
  gruppenname     = $2,
  theme_id        = $3,
  blitzgebiet     = ST_MakeEnvelope($4, $5, $6, $7, 4326),
  max_displayzeit = $8,
  archiv_tage     = $9,
  archiv_ab       = $10,
  max_zoom        = $11,
  min_zoom        = $12,
  liveblids       = $13,
  animation       = $14,
  sound           = $15
WHERE id = $1
  RETURNING id, gruppenname, box2d(blitzgebiet) AS box`)

	updateLayerStmt = db.Must(`
UPDATE benutzer.gruppen_ebenen SET
  anzeige_name    = $1,
  feature_info    = $2,
  checked_onlogin = $3,
  permanent       = $4,
  reload_time     = $5
WHERE id = $6 AND ebenen_id = $7 AND gruppen_id = $8`)

	updateUserStmt = db.Must(`
UPDATE benutzer.benutzer SET
  benutzername     = $1,
  home_gruppen_id  = $2,
  startdatum       = $3,
  stopdatum        = $4,
  firstname        = $5,
  lastname         = $6,
  company          = $7,
  division         = $8,
  streetadress     = $9,
  zip              = $10,
  city             = $11,
  phonenumber      = $12,
  annotation       = $13
WHERE id = $14`)

	updateUserAndPasswordStmt = db.Must(`
UPDATE benutzer.benutzer SET
  benutzername     = $1,
  home_gruppen_id  = $2,
  startdatum       = $3,
  stopdatum        = $4,
  firstname        = $5,
  lastname         = $6,
  company          = $7,
  division         = $8,
  streetadress     = $9,
  zip              = $10,
  city             = $11,
  phonenumber      = $12,
  annotation       = $13,
  passwort         = MD5($14)
WHERE id = $15`)

	checkEmailStmt = db.Must(`
SELECT id FROM benutzer.benutzer WHERE benutzername = $1`)

	getLinkStmt = db.Must(`
SELECT
  hash
FROM
  benutzer.resetpw
WHERE
  benutzer = $1
AND
  stopdatum between NOW() - '1 hour'::INTERVAL and NOW()`)

	generateLinkStmt = db.Must(`
INSERT INTO
  benutzer.resetpw(
    benutzer,
    hash,
    stopdatum)
VALUES (
  $1,
  $2,
  now())`)

	checkLinkStmt = db.Must(`
SELECT
  count(*)
FROM
  benutzer.resetpw
WHERE
  benutzer = $1
  AND hash = $2
  AND stopdatum BETWEEN NOW() - '1 hour'::INTERVAL AND NOW()`)

	addUserStmt = db.Must(`
INSERT INTO benutzer.benutzer(
  benutzername,
  passwort,
  home_gruppen_id,
  startdatum,
  stopdatum,
  firstname,
  lastname,
  company,
  division,
  streetadress,
  zip,
  city,
  phonenumber,
  annotation)
VALUES(
 $1,
 MD5($2),
 $3,
 $4,
 $5,
 $6,
 $7,
 $8,
 $9,
 $10,
 $11,
 $12,
 $13,
 $14)
RETURNING id`)

	addPermStmt = db.Must(`
INSERT INTO benutzer.berechtigungen(
  benutzer_id,
  gruppen_id,
  rollen_id)
VALUES (
  $1,
  $2,
  $3)`)

	removeUserStmt = db.Must(`
DELETE from benutzer.benutzer WHERE id = $1`)

	removePermStmt = db.Must(`
DELETE from benutzer.berechtigungen WHERE benutzer_id = $1`)

	getRoleListStmt = db.Must(`
SELECT
  id,
  rollenname,
  kann_einloggen,
  alarm_admin,
  alarm_gebiete,
  alarm_gebietgruppen,
  alarm_teilnehmer,
  alarm_alarme,
  user_admin,
  gruppen_admin,
  blids_counter
FROM benutzer.rollen`)

	getRoleDetailStmt = db.Must(`
SELECT
  id,
  rollenname,
  kann_einloggen,
  alarm_admin,
  alarm_gebiete,
  alarm_gebietgruppen,
  alarm_teilnehmer,
  alarm_alarme,
  user_admin,
  gruppen_admin
FROM benutzer.rollen
WHERE id = $1`)

	getAlarmListStmt = db.Must(`
SELECT
  gebiete.blitzezumalarm,
  gebiete.alarmdauer AS dauer,
  gebiete.blitzanzahl,
  gebiete.radius,
  gebiete.alarm,
  gebiete.gebiet,
  gebiete.id AS gebietid,
  teilnehmer.startzeit,
  EXTRACT(HOUR FROM teilnehmer.stopzeit),
  EXTRACT(MINUTE FROM teilnehmer.stopzeit),
  EXTRACT(SECOND FROM teilnehmer.stopzeit),
  to_char(teilnehmer.startdatum,'DD.MM.YY') AS beginn,
  to_char(teilnehmer.stopdatum,'DD.MM.YY') AS ende,
  teilnehmer.mo,
  teilnehmer.di,
  teilnehmer.mi,
  teilnehmer.don,
  teilnehmer.fr,
  teilnehmer.sa,
  teilnehmer.so,
  teilnehmer.nurlog,
  teilnehmer.utc,
  teilnehmer.blitzprotokoll AS protocol,
  teilnehmer.blitzascsv AS csv,
  teilnehmer.sms,
  teilnehmer.email,
  teilnehmer.fax,
  teilnehmer.tel,
  teilnehmer.teilnehmer,
  teilnehmer.id AS teilnehmerid,
  gebietgruppen.gruppenname,
  alarme.id AS alarmid,
  alarme.kunde AS alarmkundeid,
  kunden.kunde,
  to_char(letzterblitz, 'DD.MM.YYYY HH24:MI:SS') AS letzter,
  st_x(st_centroid(gebiete.gebietgeom)) AS centerx,
  st_y(st_centroid(gebiete.gebietgeom)) AS centery,
  flaeche as flach
FROM
  alarmserver.alarme AS alarme
  LEFT OUTER JOIN alarmserver.gebiete AS gebiete
    ON (alarme.gebiet = gebiete.id),
  alarmserver.alarme     AS gruppenalarme
  LEFT OUTER JOIN alarmserver.gebietgruppen AS gebietgruppen
    ON (gruppenalarme.gruppe = gebietgruppen.id),
  alarmserver.teilnehmer AS teilnehmer,
  alarmserver.kunden     AS kunden
WHERE
   alarme.teilnehmer = teilnehmer.id
   AND alarme.id = gruppenalarme.id
   AND (alarme.kunde IN
      (SELECT slavekunde FROM benutzer.gruppe2alarm WHERE mastergruppe = $1))
   AND alarme.kunde = kunden.id`)

	getAlarmAreaListSimpleStmt = db.Must(`
SELECT
  gebiete.id,
  gebiete.gebiet,
  gebiete.kunde AS kundenid
FROM
alarmserver.gebiete AS gebiete,
alarmserver.kunden
WHERE
  gebiete.kunde IN
     (SELECT slavekunde FROM benutzer.gruppe2alarm WHERE mastergruppe = $1)
  AND gebiete.kunde = kunden.id`)

	getAlarmParticipantsListSimpleStmt = db.Must(`
SELECT
  teilnehmer.id,
  teilnehmer.teilnehmer,
  teilnehmer.kunde
FROM
  alarmserver.teilnehmer,
  alarmserver.teilnehmer_aktiv,
  alarmserver.kunden
WHERE
  teilnehmer.kunde in (
    SELECT slavekunde FROM benutzer.gruppe2alarm WHERE mastergruppe = $1)
  AND teilnehmer_aktiv.id = teilnehmer.id
  AND teilnehmer.kunde = kunden.id`)

	getAlarmParticipantsListStmt = db.Must(`
SELECT
  teilnehmer.id,
  teilnehmer.teilnehmer,
  teilnehmer.sms,
  teilnehmer.email,
  teilnehmer.tel,
  teilnehmer.fax,
  teilnehmer.alarmierung,
  teilnehmer.entwarnung,
  teilnehmer.kunde,
  teilnehmer.startdatum,
  teilnehmer.stopdatum,
  teilnehmer.startzeit,
  EXTRACT(HOUR FROM teilnehmer.stopzeit),
  EXTRACT(MINUTE FROM teilnehmer.stopzeit),
  EXTRACT(SECOND FROM teilnehmer.stopzeit),
  teilnehmer.mo,
  teilnehmer.di,
  teilnehmer.mi,
  teilnehmer.don,
  teilnehmer.fr,
  teilnehmer.sa,
  teilnehmer.so,
  teilnehmer.nurlog,
  teilnehmer.utc,
  teilnehmer.betreffalarm,
  teilnehmer.betreffentwarn,
  teilnehmer.blitzprotokoll,
  teilnehmer.blitzascsv,
  to_char(teilnehmer.startdatum,'DD.MM.YYYY') AS anfang,
  to_char(teilnehmer.stopdatum,'DD.MM.YYYY') AS ende,
  teilnehmer_aktiv.aktiv,
  kunden.kunde AS kundenname
FROM
  alarmserver.teilnehmer,
  alarmserver.teilnehmer_aktiv,
  alarmserver.kunden
WHERE
  teilnehmer.kunde IN (
     SELECT slavekunde
     FROM benutzer.gruppe2alarm
     WHERE mastergruppe = $1)
AND teilnehmer_aktiv.id = teilnehmer.id
AND teilnehmer.kunde = kunden.id`)

	getAlarmParticipantsListParticipantIDStmt = db.Must(`
SELECT
  teilnehmer.id,
  teilnehmer.teilnehmer,
  teilnehmer.sms,
  teilnehmer.email,
  teilnehmer.tel,
  teilnehmer.fax,
  teilnehmer.alarmierung,
  teilnehmer.entwarnung,
  teilnehmer.kunde,
  teilnehmer.startdatum,
  teilnehmer.stopdatum,
  teilnehmer.startzeit,
  EXTRACT(HOUR FROM teilnehmer.stopzeit),
  EXTRACT(MINUTE FROM teilnehmer.stopzeit),
  EXTRACT(SECOND FROM teilnehmer.stopzeit),
  teilnehmer.mo,
  teilnehmer.di,
  teilnehmer.mi,
  teilnehmer.don,
  teilnehmer.fr,
  teilnehmer.sa,
  teilnehmer.so,
  teilnehmer.nurlog,
  teilnehmer.utc,
  teilnehmer.betreffalarm,
  teilnehmer.betreffentwarn,
  teilnehmer.blitzprotokoll,
  teilnehmer.blitzascsv,
  to_char(teilnehmer.startdatum,'DD.MM.YYYY') AS anfang,
  to_char(teilnehmer.stopdatum,'DD.MM.YYYY') AS ende,
  teilnehmer_aktiv.aktiv,
  kunden.kunde AS kundenname
FROM
  alarmserver.teilnehmer,
  alarmserver.teilnehmer_aktiv,
  alarmserver.kunden
WHERE
  teilnehmer.kunde IN (
     SELECT slavekunde
     FROM benutzer.gruppe2alarm
     WHERE mastergruppe = $1)
AND teilnehmer_aktiv.id = teilnehmer.id
AND teilnehmer.id = $2
AND teilnehmer.kunde = kunden.id`)

	getAlarmAreaListStmt = db.Must(`
SELECT
  gebiete.id,
  gebiete.gebiet,
  st_x(gebiete.zentrum) AS laenge,
  st_y(gebiete.zentrum) AS breite,
  gebiete.radius,
  gebiete.innerradius,
  gebiete.blitzezumalarm,
  to_char(gebiete.letzterblitz,'DD.MM.YYYY HH24:MI:SS') AS letzter,
  gebiete.blitzanzahl AS anzahl,
  flaeche as flach,
  gebiete.alarmdauer,
  gebiete.kunde AS kundenid,
  kunden.kunde
FROM
  alarmserver.gebiete AS gebiete,
  alarmserver.kunden
WHERE
  gebiete.kunde IN (
    SELECT slavekunde FROM benutzer.gruppe2alarm
    WHERE mastergruppe = $1)
AND gebiete.kunde = kunden.id`)

	getAlarmAreaListAreaIDStmt = db.Must(`
SELECT
  gebiete.id,
  gebiete.gebiet,
  st_x(gebiete.zentrum) AS laenge,
  st_y(gebiete.zentrum) AS breite,
  gebiete.radius,
  gebiete.innerradius,
  gebiete.blitzezumalarm,
  to_char(gebiete.letzterblitz,'DD.MM.YYYY HH24:MI:SS') AS letzter,
  gebiete.blitzanzahl AS anzahl,
  flaeche as flach,
  gebiete.alarmdauer,
  gebiete.kunde AS kundenid,
  st_astext(gebiete.gebietgeom) AS geometry,
  st_asgeojson(gebiete.gebietgeom)::json AS geojson,
  kunden.kunde
FROM
  alarmserver.gebiete AS gebiete,
  alarmserver.kunden
WHERE
  gebiete.kunde IN (
    SELECT slavekunde FROM benutzer.gruppe2alarm
    WHERE mastergruppe = $1)
AND gebiete.kunde = kunden.id
AND gebiete.id = $2`)

	getAlarmAreaGroupListStmt = db.Must(`
SELECT
  gruppe.id,
  gruppe.gruppenname,
  gruppe.alarm,
  gruppe.kunde,
  kunden.kunde as kundenname
FROM
  alarmserver.gebietgruppen as gruppe,
  alarmserver.kunden
WHERE
  gruppe.kunde IN (
          SELECT slavekunde
          FROM benutzer.gruppe2alarm
          WHERE mastergruppe = $1)
  AND kunden.id = gruppe.kunde`)

	getAlarmAreaGroupStmt = db.Must(`
SELECT
  gebiete.gebiet,
  gebiete.id
FROM alarmserver.gebiete,
  alarmserver.gebiet2gruppe
WHERE
  gebiete.id = gebiet2gruppe.gebiet
  AND gebiet2gruppe.gruppe = $1
  ORDER BY gebiete.gebiet`)

	getPermissionListStmt = db.Must(`
SELECT
  t1.benutzername,
  t4.benutzer_id,
  t2.gruppenname,
  t4.gruppen_id,
  t3.rollenname,
  t4.rollen_id
FROM benutzer.benutzer t1,
  benutzer.gruppen t2,
  benutzer.rollen t3,
  benutzer.berechtigungen t4
WHERE
  t4.benutzer_id = t1.id
  AND t4.gruppen_id = t2.id
  AND t4.rollen_id = t3.id`)

	updatePermissionStmt = db.Must(`
UPDATE benutzer.berechtigungen SET
  gruppen_id = $2,
  rollen_id = $3
WHERE
  benutzer_id = $1
  AND gruppen_id = $4
  AND rollen_id = $5`)

	removeRoleStmt = db.Must(`
DELETE FROM benutzer.rollen WHERE id = $1`)

	updateRoleStmt = db.Must(`
UPDATE benutzer.rollen SET
  rollenname = $2,
  kann_einloggen = $3,
  alarm_admin = $4,
  alarm_gebiete = $5,
  alarm_gebietgruppen = $6,
  alarm_teilnehmer = $7,
  alarm_alarme = $8,
  user_admin = $9,
  gruppen_admin = $10
WHERE
  id = $1`)

	getAlarmCustomerListGroupIDStmt = db.Must(`
SELECT
  id,
  kunde AS alarmkunde
FROM
  alarmserver.kunden t1,
  benutzer.gruppe2alarm t2
WHERE mastergruppe = $1
  AND t2.slavekunde = t1.id ORDER BY id`)

	getAlarmCustomerListStmt = db.Must(`
SELECT
  id,
  kunde AS alarmkunde
FROM alarmserver.kunden t1 ORDER BY id`)

	getAlarmMessagesStmt = db.Must(`
SELECT
  to_char(eintrag,'DD.MM.YYYY HH24:MI:SS') AS anf,
  to_char(erledigt,'DD.MM.YYYY HH24:MI:SS') AS ende,
  id,
  alarm,
  eintrag,
  erledigt,
  typ
FROM
  alarmserver.meldungen
WHERE
  alarm = $1
  AND eintrag between $2
  AND $3
  ORDER BY erledigt DESC`)

	updateAlarmAreaGeomStmt = db.Must(`
UPDATE alarmserver.gebiete
  SET (gebiet,kunde, blitzezumalarm, alarmdauer, gebietgeom, radius,
       innerradius, zentrum)
      = ($1, $2, $3, $4, st_geomfromtext($5, 4326),
         null, null, null)
  WHERE id = $6
  RETURNING id;
`)

	updateAlarmAreaCircleStmt = db.Must(`
UPDATE alarmserver.gebiete
  SET (gebiet,kunde, blitzezumalarm, alarmdauer, gebietgeom, radius, 
       innerradius, zentrum)
      = ($1, $2, $3, $4,
         st_multi(st_transform(st_buffer(
                                 st_transform(st_setsrid(st_point($6,$7),
                                                         4326),31467),
                                 $5 * 1000),4326)),
        $5, null, st_setsrid(st_point($6,$7),4326))
  WHERE id = $8
  RETURNING id;
`)

	updateAlarmAreaRingStmt = db.Must(`
UPDATE alarmserver.gebiete
  SET (gebiet, kunde, blitzezumalarm, alarmdauer, gebietgeom, radius,
       innerradius, zentrum)
      = ($1, $2, $3, $4,
         st_multi(st_transform(
                    st_difference(
                      st_buffer(st_transform(st_setsrid(st_point($7,$8),
                                                        4326),31467),
                                $5 * 1000),
                      st_buffer(st_transform(st_setsrid(st_point($7,$8),
                                                        4326),31467),
                                $6 * 1000)),
                    4326)),
         $5, $6, st_setsrid(st_point($7,$8),4326))
  WHERE id = $9
  RETURNING id;
`)

	insertAlarmAreaGeomStmt = db.Must(`
INSERT INTO alarmserver.gebiete (gebiet,kunde, blitzezumalarm, 
                                 alarmdauer, gebietgeom)
  VALUES ($1, $2, $3, $4, st_geomfromtext($5, 4326))
  RETURNING id;
`)

	insertAlarmAreaCircleStmt = db.Must(`
INSERT INTO alarmserver.gebiete (gebiet,kunde, blitzezumalarm,
                                 alarmdauer, gebietgeom, radius, zentrum)
  VALUES ($1, $2, $3, $4,
          st_multi(st_transform(st_buffer(
                                  st_transform(st_setsrid(st_point($6,$7),
                                                          4326),31467),
                                  $5 * 1000),4326)),
          $5, st_setsrid(st_point($6,$7),4326))
  RETURNING id;
`)

	insertAlarmAreaRingStmt = db.Must(`
INSERT INTO alarmserver.gebiete (gebiet, kunde, blitzezumalarm,
                                 alarmdauer, gebietgeom, radius,
                                 innerradius, zentrum)
  VALUES ($1, $2, $3, $4,
          st_multi(st_transform(
                     st_difference(
                       st_buffer(st_transform(st_setsrid(st_point($7,$8),
                                                         4326),31467),
                                 $5 * 1000),
                       st_buffer(st_transform(st_setsrid(st_point($7,$8),
                                                         4326),31467),
                                 $6 * 1000)),
                     4326)),
          $5, $6, st_setsrid(st_point($7,$8),4326))
  RETURNING id;
`)

	updateAlarmParticipantIDStmt = db.Must(`
UPDATE
  alarmserver.teilnehmer
SET
  teilnehmer     = $1,
  sms            = $2,
  email          = $3,
  tel            = $4,
  fax            = $5,
  alarmierung    = $6,
  entwarnung     = $7,
  startdatum     = $8,
  stopdatum      = $9,
  startzeit      = $10,
  stopzeit       = make_time($11, $12, $13),
  mo             = $14,
  di             = $15,
  mi             = $16,
  don            = $17,
  fr             = $18,
  sa             = $19,
  so             = $20,
  nurlog         = $21,
  utc            = $22,
  blitzprotokoll = $23,
  blitzascsv     = $24
WHERE
  id             = $25
  AND kunde      = $26
RETURNING id`)

	updateAlarmParticipantDefaultStmt = db.Must(`
INSERT INTO
  alarmserver.teilnehmer (
    teilnehmer,
    sms,
    email,
    alarmierung,
    entwarnung,
    startdatum,
    stopdatum,
    kunde)
VALUES(
  $1,
  $2,
  $3,
  $4,
  $5,
  $6,
  $7,
  $8)
RETURNING id`)

	updateAlarmParticipantStmt = db.Must(`
INSERT INTO
  alarmserver.teilnehmer (
  teilnehmer,
  sms,
  email,
  tel,
  fax,
  alarmierung,
  entwarnung,
  startdatum,
  stopdatum,
  startzeit,
  stopzeit,
  mo,
  di,
  mi,
  don,
  fr,
  sa,
  so,
  nurlog,
  utc,
  blitzprotokoll,
  blitzascsv,
  kunde)
VALUES(
  $1,
  $2,
  $3,
  $4,
  $5,
  $6,
  $7,
  $8,
  $9,
  $10,
  make_time($11, $12, $13),
  $14,
  $15,
  $16,
  $17,
  $18,
  $19,
  $20,
  $21,
  $22,
  $23,
  $24,
  $25)
RETURNING id`)

	updateAlarmStmt = db.Must(`
UPDATE
  alarmserver.alarme
SET
  (gebiet,teilnehmer) = ($1,$2)
WHERE
  id = $3
AND
  kunde = $4;
`)

	removeAlarmStmt = db.Must(`
DELETE FROM alarmserver.alarme where id = $1 and kunde = $2`)

	insertAlarmStmt = db.Must(`
INSERT INTO alarmserver.alarme (gebiet, teilnehmer, kunde)
VALUES ($1, $2, $3)`)

	removeAlarmAreaStmt = db.Must(`
DELETE FROM alarmserver.gebiete where id = $1 and kunde = $2`)

	removeAlarmByAreaStmt = db.Must(`
DELETE FROM alarmserver.alarme where gebiet = $1 and kunde = $2`)

	removeAlarmParticipantStmt = db.Must(`
DELETE FROM alarmserver.teilnehmer WHERE id = $1 AND kunde = $2`)

	removeAlarmByParticipantStmt = db.Must(`
DELETE FROM alarmserver.alarme WHERE teilnehmer = $1 AND kunde = $2`)

	addGroup2AlarmStmt = db.Must(`
INSERT INTO benutzer.gruppe2alarm (mastergruppe,slavekunde) VALUES ($1,$2)`)

	removeGroup2AlarmStmt = db.Must(`
DELETE FROM benutzer.gruppe2alarm WHERE mastergruppe = $1 AND slavekunde = $2`)

	sendTestAlarmStmt = db.Must(`
INSERT INTO alarmserver.meldungen (id,alarm,typ) VALUES (DEFAULT,$1,$2) RETURNING id`)

	addAlarmCustomerStmt = db.Must(`
INSERT INTO alarmserver.kunden (kunde) VALUES ($1) RETURNING id`)
)
