// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import (
	"database/sql"
	"encoding/json"
	"time"

	"github.com/lib/pq"
)

const (
	dateFormat   = "2006-01-02 15:04:05-07"
	isoFormat    = "2006-01-02T15:04:05Z07:00"
	germanFormat = "2.1.2006"
	statFormat   = "2006-01-02 15:04:05"
	timeFormat   = "15:04:05"
)

func stringToNullTime(s string, formats ...string) (pq.NullTime, error) {
	if s == "" {
		return pq.NullTime{}, nil
	}
	t, err := stringToTime(s, formats...)
	if err != nil {
		return pq.NullTime{}, err
	}
	return pq.NullTime{Time: t, Valid: true}, nil
}

// stringToTime tries all the given formats to parse a time from a string.
func stringToTime(s string, formats ...string) (time.Time, error) {
	var err error
	for _, format := range formats {
		var t time.Time
		if t, err = time.Parse(format, s); err == nil {
			return t, nil
		}
	}
	return time.Time{}, err
}

// FakeBool is a bool read/written from/to as "t" and "f"
// when used in JSON serialisations.
type FakeBool bool

// UnmarshalJSON fulfill the JSON readinng part.
func (fb *FakeBool) UnmarshalJSON(data []byte) error {
	var in interface{}
	if err := json.Unmarshal(data, &in); err != nil {
		return err
	}
	switch x := in.(type) {
	case bool:
		*fb = FakeBool(x)
	case string:
		*fb = FakeBool(x == "t")
	}
	return nil
}

// MarshalJSON fulfill the JSON writing part.
func (fb FakeBool) MarshalJSON() ([]byte, error) {
	if fb {
		return json.Marshal("t")
	}
	return json.Marshal("f")
}

// FakeBoolPointer returns a pointer to a FakeBool with
// the value of the given bool.
func FakeBoolPointer(x bool) *FakeBool {
	fb := FakeBool(x)
	return &fb
}

func nullString(s *string) sql.NullString {
	if s != nil {
		return sql.NullString{String: *s, Valid: true}
	}
	return sql.NullString{}
}

func nullInt64(i *int64) sql.NullInt64 {
	if i != nil {
		return sql.NullInt64{Int64: *i, Valid: true}
	}
	return sql.NullInt64{}
}
