// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

// User is used to send JSON encoded data about the logged in user to
// the JS client.
type User struct {
	ID                int64    `json:"id"`
	UserName          string   `json:"username"`
	Start             string   `json:"start"`
	Stop              string   `json:"stop"`
	Bounds            string   `json:"bounds"`
	MaxDisplayZeit    int64    `json:"max_displayzeit"`
	ArchivTage        int64    `json:"archiv_tage"`
	ArchivAb          string   `json:"archiv_ab"`
	MaxZoom           int64    `json:"max_zoom"`
	MinZoom           int64    `json:"min_zoom"`
	LoginAllowed      FakeBool `json:"loginallowed"`
	LiveBlids         FakeBool `json:"liveblids"`
	Animation         FakeBool `json:"animation"`
	Sound             FakeBool `json:"sound"`
	ThemeID           int64    `json:"theme_id"`
	StatistikWindowed FakeBool `json:"statistik_windowed"`
	Statistikgebiet   FakeBool `json:"statistikgebiet"`
}

// Layer is used to send JSON encoded data about a layer to the
// JS client.
type Layer struct {
	GeoName           string      `json:"geoname"`
	DisplayName       string      `json:"displayname"`
	Filter            interface{} `json:"filter"`
	PopupTemplate     string      `json:"popup_template"`
	ReloadTime        int64       `json:"reload_time"`
	FeatureInfo       FakeBool    `json:"feature_info"`
	CheckedOnLogin    FakeBool    `json:"checked_onlogin"`
	Permanent         FakeBool    `json:"permanent"`
	SingleTile        FakeBool    `json:"single_tile"`
	TimeDependent     FakeBool    `json:"timedependent"`
	ExternerServer    string      `json:"externer_server"`
	ExternerParameter string      `json:"externer_parameter"`
}

// FindUser looks for a user with a password. If they both match
// the ID of the user is returned.
func FindUser(username, password string) (sql.NullInt64, error) {
	var user sql.NullInt64
	err := checkUserStmt.QueryRow(username, password).Scan(&user)
	switch {
	case err == sql.ErrNoRows:
		return sql.NullInt64{}, nil
	case err != nil:
		return sql.NullInt64{}, err
	}
	return user, nil
}

// UserLogging loggs in a user an binds her to the session.
func UserLogging(user int64, request string, ip net.IP, session string) error {
	var ips string
	if ip != nil {
		ips = ip.String()
	}
	_, err := userLoggingStmt.Exec(user, request, ips, session)
	return err
}

// UserLoggingNoSession loggs in a user.
func UserLoggingNoSession(request string, ip net.IP) error {
	var ips string
	if ip != nil {
		ips = ip.String()
	}
	_, err := userLoggingNoSessionStmt.Exec(request, ips)
	return err
}

// ValidateUser loggs a user and extracts the relevant user data
// to be return to the JS client.
func ValidateUser(username, password string) (*User, error) {
	var user User
	var start, stop, archivAb time.Time
	err := validateUserStmt.QueryRow(username, password).Scan(
		&user.ID,
		&user.UserName,
		&start,
		&stop,
		&user.Bounds,
		&user.MaxDisplayZeit,
		&user.ArchivTage,
		&archivAb,
		&user.MaxZoom,
		&user.MinZoom,
		&user.LoginAllowed,
		&user.LiveBlids,
		&user.Animation,
		&user.Sound,
		&user.ThemeID,
		&user.StatistikWindowed,
		&user.Statistikgebiet)
	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	user.Start = start.Format(dateFormat)
	user.Stop = stop.Format(dateFormat)
	user.ArchivAb = archivAb.Format(dateFormat)

	return &user, nil
}

// Check checks if the user is allowed to logg in.
// Returns an empty string if everything is alright.
func (u *User) Check() string {
	if u == nil {
		return "Invalid user data."
	}

	if !u.LoginAllowed {
		return fmt.Sprintf("User %s is not allowed to log in.", u.UserName)
	}

	now := time.Now()

	start, err := time.Parse(dateFormat, u.Start)
	if err != nil || start.After(now) {
		return fmt.Sprintf("User %s is not active yet.", u.UserName)
	}

	stop, err := time.Parse(dateFormat, u.Stop)
	if err != nil || stop.Before(now) {
		return fmt.Sprintf("User %s is expired.", u.UserName)
	}

	return ""
}

// LoadLayers returns the list of layers belonging to a given user.
func LoadLayers(id int64) ([]*Layer, error) {

	rows, err := loadLayersStmt.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	layers := make([]*Layer, 0, 3)

	for rows.Next() {
		var layer Layer
		var externerServer, externerParameter, filter, popupTemplate sql.NullString
		if err := rows.Scan(
			&layer.GeoName,
			&externerServer,
			&externerParameter,
			&layer.TimeDependent,
			&layer.ReloadTime,
			&layer.DisplayName,
			&layer.FeatureInfo,
			&layer.CheckedOnLogin,
			&layer.Permanent,
			&layer.SingleTile,
			&filter,
			&popupTemplate); err != nil {
			log.Printf("scan layer: %v\n", err)
			break
		}
		if externerServer.Valid {
			layer.ExternerServer = externerServer.String
		}
		if externerParameter.Valid {
			layer.ExternerParameter = externerParameter.String
		}
		if popupTemplate.Valid {
			layer.PopupTemplate = popupTemplate.String
		}
		if filter.Valid {
			if f := strings.TrimSpace(filter.String); f != "" {
				dec := json.NewDecoder(strings.NewReader(f))
				if err := dec.Decode(&layer.Filter); err != nil {
					return nil, err
				}
			}
		}
		layers = append(layers, &layer)
	}

	return layers, rows.Err()
}

// GroupNode is a node in the tree of groups users belong to.
type GroupNode struct {
	ID     int64
	Parent *GroupNode
}

// GroupTree maps user IDs to the trees of groups the users
// belong to.
type GroupTree map[int64]*GroupNode

// BuildGroupTree loads the complete tree of groups into memory
// allowing a fast lookup of which groups a user is in.
func BuildGroupTree() (GroupTree, error) {

	rows, err := groupTreeStmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	gt := make(GroupTree)
	pending := make(map[int64][]**GroupNode)

	for rows.Next() {
		var gn GroupNode
		var parentID sql.NullInt64
		if err := rows.Scan(&gn.ID, &parentID); err != nil {
			return nil, err
		}
		if parentID.Valid {
			if parent := gt[parentID.Int64]; parent != nil {
				gn.Parent = parent
			} else {
				pending[parentID.Int64] = append(pending[parentID.Int64], &gn.Parent)
			}
		}
		gt[gn.ID] = &gn
		if pens, ok := pending[gn.ID]; ok {
			for _, pen := range pens {
				*pen = &gn
			}
			delete(pending, gn.ID)
		}
	}

	return gt, rows.Err()
}

// InGroup checks if a given user is in a given group.
func (gt GroupTree) InGroup(homeGroupID, groupID int64) bool {
	for gn := gt[homeGroupID]; gn != nil; gn = gn.Parent {
		if gn.ID == groupID {
			return true
		}
	}
	return false
}
