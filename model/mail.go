// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import "fmt"

const mailTmpl = `You have requested a password change for your account on
%s://%s

Please follow this link to get to the page, where you can change your password.

%s://%s/#/user/resetPW/%s/%s

The link is only valid for 1 hour.

Best regards

Your Siemens Blids team`

func getMessage(https bool, email, hash, serverName string) string {

	var proto string

	if https {
		proto = "https"
	} else {
		proto = "http"
	}

	return fmt.Sprintf(mailTmpl,
		proto, serverName,
		proto, serverName,
		email, hash)
}
