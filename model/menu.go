// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package model

import (
	"encoding/json"
	"log"
	"strings"
)

const (
	// TODO: Move these into files!
	userMenu = `{
    "header": "nav.menu",
    "items": [{
        "title": "map.legend",
        "href": "map.legend_url",
        "popupLarge": true
    }]
}`

	adminMenu = `{
    "header": "nav.admin",
    "items": [{
            "title": "nav.map",
            "href": "../#/map"
        },
        {
            "title": "nav.groups",
            "href": "../#/groups"
        }
    ]
}`
	rolesMenu = `{
    "title": "nav.roles",
    "href": "../#/roles"
}`
)

// GetMenu returns the menu structure for a given user used
// in the JS client.
func GetMenu(userID int64) ([]interface{}, error) {

	perm, err := LoadAdminRights(userID)
	if err != nil {
		return nil, err
	}

	menu := make([]interface{}, 0, 2)

	if !perm.Any("N", "HG", "A") {
		return menu, nil
	}

	var user interface{}
	if err := json.NewDecoder(strings.NewReader(userMenu)).Decode(&user); err != nil {
		log.Printf("error: %v\n", err)
	} else {
		menu = append(menu, user)
	}

	if perm == "N" {
		return menu, nil
	}

	var admin interface{}
	if err := json.NewDecoder(strings.NewReader(adminMenu)).Decode(&admin); err != nil {
		log.Printf("error: %v\n", err)
	} else {
		menu = append(menu, admin)
	}

	if perm == "HG" {
		return menu, nil
	}

	var roles interface{}
	if err := json.NewDecoder(strings.NewReader(rolesMenu)).Decode(&roles); err != nil {
		log.Printf("error: %v\n", err)
	} else {
		if m, ok := menu[1].(map[string]interface{}); ok {
			if s, ok := m["items"].([]interface{}); ok {
				m["items"] = append(s, roles)
			}
		}
	}

	return menu, nil
}
