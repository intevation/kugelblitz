// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package common

import (
	"fmt"
	"os"
)

// Version of the software. Don't forget to adjust before tagging.
const Version = "0.1"

// PrintVersionAndExit prints the name of the program and the version
// and exits.
func PrintVersionAndExit() {
	fmt.Printf("%s version %s\n", os.Args[0], Version)
	os.Exit(0)
}
