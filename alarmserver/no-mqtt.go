// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Functionality to send alarm messages via mqtt.

// +build !mqtt

package alarmserver

import "errors"

func (as *AlarmServer) sendMQTT(broker, payload string) error {
	return errors.New("ERROR: Could not send MQTT msg," +
		" alarmserver was buildwithout MQTT support!")
}
