// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Read configuration from db.

package alarmserver

import (
	"database/sql"
	"fmt"
	"log"
	"reflect"
)

func (as *AlarmServer) readConfigFromDB() error {

	log.Print("Reading Configuration from db")

	var conf config
	var err error

	ve := reflect.ValueOf(&conf).Elem()

	for n := 0; n < ve.NumField(); n++ {
		fName := ve.Type().Field(n).Name
		fType := fmt.Sprintf("%T", ve.Field(n).Interface())

		query := fmt.Sprintf(
			"SELECT %s FROM alarmserver.config WHERE option='%s';",
			fType, fName,
		)
		r := as.db.QueryRow(query)

		switch fType {
		case "string":
			var s string
			err = r.Scan(&s)
			if err == nil {
				ve.Field(n).SetString(s)
			}
		case "int":
			var i int64
			err = r.Scan(&i)
			if err == nil {
				ve.Field(n).SetInt(i)
			}
		case "bool":
			var b bool
			err = r.Scan(&b)
			if err == nil {
				ve.Field(n).SetBool(b)
			}
		default:
			panic("Unhandled type in config struct.")
		}

		if err != nil && err != sql.ErrNoRows {
			return err
		}
	}

	as.conf = &conf

	return err
}
