// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Definition of configuration options.  These are to be filled in by
// the various configuration mechanisms.

package alarmserver

type config struct {
	Debug          bool
	SmtpHost       string
	SmtpPort       int
	SmtpHelo       string
	SmtpUser       string
	SmtpPasswd     string
	MailFrom       string
	EcallUser      string
	EcallPasswd    string
	EcallVoiceFrom string
	EcallFaxFrom   string
	MQTTHost       string
	MQTTPort       int
	MQTTUsername   string
	MQTTPassword   string
	MQTTUseSSL     bool
	MQTTCertPath   string
}
