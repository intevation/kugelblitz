// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Read configuration from JSON file.

package alarmserver

import (
	"encoding/json"
	"os"
)

func readConfigJSON(filename string) (*config, error) {
	var conf config
	var err error

	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	d := json.NewDecoder(file)
	d.DisallowUnknownFields()
	err = d.Decode(&conf)
	if err != nil {
		file.Close()
		return nil, err
	}
	err = file.Close()

	return &conf, err
}
