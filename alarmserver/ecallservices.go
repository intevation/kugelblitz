// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Functionality to send alarm messages via eCall services:
// sms, fax and phone.

package alarmserver

import (
	"bitbucket.org/intevation/kugelblitz/ecall"
)

func (as *AlarmServer) SendSms(to, body string) error {

	sms := ecall.SendSMSBasic{
		AccountName:     as.conf.EcallUser,
		AccountPassword: as.conf.EcallPasswd,
		Address:         to,
		Message:         body,
	}
	ec := ecall.NewECallSoap("", true, nil)
	_, err := ec.SendSMSBasic(&sms)
	return err
}

func (as *AlarmServer) SendVoicemail(to, body string) error {

	phone := ecall.SendVoiceBasic{
		AccountName:     as.conf.EcallUser,
		AccountPassword: as.conf.EcallPasswd,
		Address:         to,
		Message:         body,
		FromText:        as.conf.EcallVoiceFrom,
	}
	ec := ecall.NewECallSoap("", true, nil)
	_, err := ec.SendVoiceBasic(&phone)
	return err
}

func (as *AlarmServer) SendFax(to, subject, body string) error {

	fax := ecall.SendFaxBasic{
		AccountName:     as.conf.EcallUser,
		AccountPassword: as.conf.EcallPasswd,
		Address:         to,
		Message:         body,
		FromText:        as.conf.EcallFaxFrom,
		Subject:         subject,
	}
	ec := ecall.NewECallSoap("", true, nil)
	_, err := ec.SendFaxBasic(&fax)
	return err
}
