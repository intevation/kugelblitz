// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Functionality to send alrm messages via email.

package alarmserver

import "gopkg.in/gomail.v2"

func (as *AlarmServer) sendMail(to, subject, body string) error {
	m := gomail.NewMessage()
	m.SetHeader("From", as.conf.MailFrom)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", body)

	d := gomail.NewDialer(as.conf.SmtpHost, as.conf.SmtpPort,
		as.conf.SmtpUser, as.conf.SmtpPasswd)

	d.LocalName = as.conf.SmtpHelo

	return d.DialAndSend(m)
}
