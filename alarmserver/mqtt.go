// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Functionality to send alarm messages via mqtt.

// +build mqtt

package alarmserver

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func (as *AlarmServer) certpool() (*x509.CertPool, error) {

	// Start with system cert pool aned add out specific cert on demand
	// default openssl CA bundle.

	certpool, err := x509.SystemCertPool()
	if err != nil {
		return nil, err
	}

	if as.conf.MQTTCertPath == "" {
		return certpool, nil
	}

	for _, root := range filepath.SplitList(as.conf.MQTTCertPath) {
		err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if info == nil {
				return nil
			}

			if info.Mode().IsRegular() {
				if fn := strings.ToLower(info.Name()); strings.HasSuffix(fn, ".crt") || strings.HasSuffix(fn, ".pem") {
					pem, err := ioutil.ReadFile(path)
					if err != nil {
						return err
					}
					if !certpool.AppendCertsFromPEM(pem) {
						return fmt.Errorf("Cannot add invalid cert file '%s'", path)
					}
				}
			}
			return nil
		})

		if err != nil {
			return nil, err
		}
	}

	return certpool, nil
}

func (as *AlarmServer) makeTLSConfig() (*tls.Config, error) {

	if as.conf.MQTTCertPath == "" {
		return nil, nil
	}

	certpool, err := as.certpool()
	if err == nil {
		return nil, err
	}

	// Create tls.Config with desired tls properties
	return &tls.Config{
		RootCAs:            certpool,
		InsecureSkipVerify: true,
		ClientAuth:         tls.NoClientCert,
		MinVersion:         tls.VersionTLS12,
		MaxVersion:         tls.VersionTLS12,
	}, nil
}

func (as *AlarmServer) sendMQTT(broker, payload string) error {

	mqtt.ERROR = log.New(os.Stdout, "MQTT", 0)

	var proto string
	if as.conf.MQTTUseSSL {
		proto = "tls"
	} else {
		proto = "tcp"
	}

	opts := mqtt.NewClientOptions().AddBroker(
		fmt.Sprintf("%s://%s:%d", proto, broker, as.conf.MQTTPort))
	opts.SetClientID("alarmserver")
	opts.SetKeepAlive(2 * time.Second)
	opts.SetPingTimeout(1 * time.Second)

	if as.conf.MQTTUsername != "" {
		opts.SetUsername(as.conf.MQTTUsername)
	}
	if as.conf.MQTTPassword != "" {
		opts.SetPassword(as.conf.MQTTPassword)
	}

	tls, err := as.makeTLSConfig()
	if err != nil {
		return err
	}
	if tls != nil {
		opts.SetTLSConfig(tls)
	}

	c := mqtt.NewClient(opts)

	if token := c.Connect(); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	token := c.Publish("kugelblitz/alarm", 2, false, payload)
	if token.Wait() && token.Error() != nil {
		return token.Error()
	}

	c.Disconnect(250)

	return nil
}
