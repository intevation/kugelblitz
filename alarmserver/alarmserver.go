// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// BLIDS alarmserver.

package alarmserver

import (
	"database/sql"
	"time"

	"github.com/lib/pq"
)

type AlarmServer struct {
	conf     *config
	listener *pq.Listener
	db       *sql.DB
	stmts    *asStmts
}

func NewAlarmServer(confFileName string, pgConnInfo string,
	notifyChannel string) (*AlarmServer, error) {

	var as AlarmServer
	var err error

	as.db, err = sql.Open("postgres", pgConnInfo)
	if err != nil {
		return nil, err
	}

	as.prepareStatements()

	if confFileName != "" {
		as.conf, err = readConfigJSON(confFileName)
		if err != nil {
			return nil, err
		}
	} else {
		err = as.readConfigFromDB()
		if err != nil {
			return nil, err
		}
	}

	minReconn := time.Second
	maxReconn := 10 * time.Second
	as.listener = pq.NewListener(pgConnInfo, minReconn,
		maxReconn, handleConnError)
	err = as.listener.Listen(notifyChannel)
	if err != nil {
		return nil, err
	}

	return &as, nil
}
