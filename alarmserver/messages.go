// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// BLIDS alarmserver.

package alarmserver

import (
	"encoding/json"
	"errors"
	"strings"
)

type RawData struct {
	Type      string  `json:"type"`
	Time      string  `json:"time"`
	Lan       float32 `json:"lat"`
	Lon       float32 `json:"lon"`
	AlarmArea string  `json:"alarmarea"`
	Group     string  `json:"group"`
	Radius    float32 `json:"radius"`
	Threshold int     `json:"threshold"`
	Count     int     `json:"count"`
	Density   float32 `json:"density"`
	Minutes   int     `json:"minutes"`
}

type Contact struct {
	Recipient, Body, Subject, Email string
	Sms, Phone, Fax, Mqtt           string
	RawData                         RawData `json:"raw_data"`
	Logonly                         bool
}

type DispatchResult struct {
	task string
	err  error
}

func dispatchingError(msg string) error {
	return errors.New("Dispatching Message failed: " + msg)
}

func (as *AlarmServer) GetContact(id int) (Contact, error) {
	var c Contact
	var jtxt string
	as.Debug("Fetching contact data for message id %d", id)
	err := as.stmts.getAlarmMsg.QueryRow(id).Scan(&jtxt)
	if err != nil {
		as.Debug("Getting msg details from db failed: %s", err)
		return c, err
	}

	d := json.NewDecoder(strings.NewReader(jtxt))
	err = d.Decode(&c)

	return c, err
}

func (as *AlarmServer) DispatchMessage(rcpt Contact) []DispatchResult {

	email := strings.TrimSpace(rcpt.Email)
	sms := strings.TrimSpace(rcpt.Sms)
	phone := strings.TrimSpace(rcpt.Phone)
	fax := strings.TrimSpace(rcpt.Fax)
	mqtt := strings.TrimSpace(rcpt.Mqtt)

	var contactFound = false
	var result []DispatchResult

	as.Debug("Dispatching Message for %s\n", rcpt.Recipient)

	if email != "" {
		contactFound = true
		as.Debug("Sending mail to: %s\n", email)
		var err error = nil
		if !rcpt.Logonly {
			err = as.sendMail(email, rcpt.Subject, rcpt.Body)
		}
		result = append(result, DispatchResult{"mail", err})
	}
	if sms != "" {
		contactFound = true
		as.Debug("Sending sms to: %s\n", sms)
		var err error = nil
		if !rcpt.Logonly {
			err = as.SendSms(sms, rcpt.Body)
		}
		result = append(result, DispatchResult{"sms", err})
	}
	if phone != "" {
		contactFound = true
		as.Debug("Calling: %s\n", phone)
		var err error = nil
		if !rcpt.Logonly {
			err = as.SendVoicemail(phone, rcpt.Body)
		}
		result = append(result, DispatchResult{"phone", err})
	}
	if fax != "" {
		contactFound = true
		as.Debug("Sending fax to: %s\n", fax)
		var err error = nil
		if !rcpt.Logonly {
			err = as.SendFax(fax, rcpt.Subject, rcpt.Body)
		}
		result = append(result, DispatchResult{"fax", err})
	}
	if mqtt != "" {
		contactFound = true
		as.Debug("Sending to mqtt broker: %s\n", mqtt)

		var payload = struct {
			Recipient string  `json:"recipient"`
			Subject   string  `json:"subject"`
			Body      string  `json:"body"`
			RawData   RawData `json:"raw_data"`
		}{
			Recipient: rcpt.Recipient,
			Subject:   rcpt.Subject,
			Body:      rcpt.Body,
			RawData:   rcpt.RawData,
		}

		var err error
		var jsontxt strings.Builder

		if err = json.NewEncoder(&jsontxt).Encode(&payload); err == nil {
			if !rcpt.Logonly {
				err = as.sendMQTT(mqtt, jsontxt.String())
			}
		}

		result = append(result, DispatchResult{"mqtt", err})
	}

	if !contactFound {
		result = append(result,
			DispatchResult{"DISPATCHING",
				dispatchingError("No valid contact found")})
	}

	return result
}
