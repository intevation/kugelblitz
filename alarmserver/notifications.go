// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// BLIDS alarmserver.

package alarmserver

import (
	"encoding/json"
	"log"
	"strings"
	"time"

	"github.com/lib/pq"
)

type dbMsgHandle struct {
	ID int `json:"id"`
	// groupid : we dont care for the value of that field
}

type alarm struct {
	Type string      `json:"type"`
	Data dbMsgHandle `json:"data"`
}

func handleConnError(ev pq.ListenerEventType, err error) {
	// We do _not_ die on errors, as there are expected problems,
	// linke the db server being temporarily unavailable, which
	// are expected and no reason to give up.
	// FIXME: a more differentiated handling of problems would be
	// great, but the neccessary information is not available from
	// the thrown errors...
	if err != nil {
		log.Print(err.Error())
	}
}

func parseJSONAlarm(jtxt string) (alarm, error) {
	var a alarm
	d := json.NewDecoder(strings.NewReader(jtxt))
	err := d.Decode(&a)
	return a, err
}

func (as *AlarmServer) sendMessage(jtxt string) {
	a, err := parseJSONAlarm(jtxt)
	if err != nil {
		log.Panicf("Decoding json failed: %s\nin: %s\n",
			err, jtxt)
		return
	}
	// We only handle known types, other notificaitons (like
	// "stroke") can be savely ignored.
	switch a.Type {
	case "Alarm", "Entwarnung", "Testalarm", "Testentwarnung":
		as.Debug("Got %s notificartion.\n", a.Type)
		c, err := as.GetContact(a.Data.ID)
		if err != nil {
			log.Panicf("Couldn't get message details for id: %d"+
				" Error: %s", a.Data.ID, err)
			return
		}

		status := as.DispatchMessage(c)
		statusStr, detailStr := as.ProcessDispatcherStatus(status)
		if c.Logonly {
			detailStr = "LOGONLY: " + detailStr
		}
		as.UpdateMsgStatus(a.Data.ID, statusStr, detailStr)
	}
}

func (as *AlarmServer) ProcessNotifications() {
	select {
	case n := <-as.listener.Notify:
		if n == nil {
			return
		}
		go as.sendMessage(n.Extra)
	case <-time.After(90 * time.Second):
		go as.listener.Ping()
	}
}
