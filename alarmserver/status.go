// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Functionality to send alrm messages via email.

package alarmserver

import (
	"fmt"
	"log"
)

const (
	statusSuccess     = "success"
	statusPartSuccess = "partial success"
	statusErr         = "error"
)

func addStatus(old, new string) string {
	if old == "" || old == new {
		return new
	}
	return statusPartSuccess
}

func (as *AlarmServer) ProcessDispatcherStatus(
	status []DispatchResult,
) (string, string) {
	var statusStr, statusDetailStr string

	if len(status) == 0 {
		log.Panic("Empty Message Dispatch status!")
	} else {
		for _, res := range status {
			if res.err != nil {
				s := fmt.Sprintf("FAILED: %s : %s;",
					res.task, res.err)
				as.Debug(s)
				statusDetailStr += s
				statusStr = addStatus(statusStr, statusErr)
			} else {
				s := fmt.Sprintf("SUCCESS: %s;", res.task)
				as.Debug(s)
				statusDetailStr += s
				statusStr =
					addStatus(statusStr, statusSuccess)
			}
		}
	}
	return statusStr, statusDetailStr
}

func (as *AlarmServer) UpdateMsgStatus(id int, status, comment string) {
	if id == 0 {
		log.Print("WARNING: Failed to update db: " +
			"No valid Id in message.")
		return
	}

	as.Debug("Updating status in db for msg id %d to %s", id, status)
	_, err := as.stmts.updateMsgStatus.Exec(status, comment, id)
	if err != nil {
		log.Printf("WARNING: Failed to update db: %s", err)
	}
}
