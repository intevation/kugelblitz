// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// Super simple conditional logging for debugging

package alarmserver

import "log"

func (as *AlarmServer) Debug(format string, args ...interface{}) {
	if as.conf.Debug {
		log.Printf("DEBUG: "+format, args...)
	}
}
