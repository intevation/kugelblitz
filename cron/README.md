# Cron Jobs

## Alarmserver

There are two conjobs which must be active for the Alarmserver-system
to work as intended:

 - expire-alarms : This script sets the 'all clear' status if no new
                   strokes were registered for the configured duration
                   of an alarm in the relevant area.  This triggers a
                   coresponding alarm message.
 
 - retry-msgs :    This script checks for unhandled alarm messages and
                   either triggers them anew or, when to old, sets
                   them to an error state.

Both scripts take the name of the database to operate on as argument.

### crontab Configuration

The recommended configuration in /etc/crontab looks like this:

```
 * *    * * *   postgres /path/to/kugelblitz/cron/expire-alarms blids_test01
*/3 *   * * *   postgres /path/to/kugelblitz/cron/retry-msgs blids_test01
```

## Stale user sessions

The logged in users are tracked in the database. The web frontend
refreshes the sessions every minute and the backend invalidates them
if the last refreshment is more then 45 minutes ago.
These session are removed from the database.

For the case of failing to remove timed out sessions from the database
use the `remove-stale-sessions` script as a cron job:

```
*/30 *  * * *   postgres /path/to/kugelblitz/cron/remove-stale-sessions blids_test01
```

