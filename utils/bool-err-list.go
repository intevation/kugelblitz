// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package utils

type be struct {
	b   bool
	err error
}

// BoolErrList piles up a tuples of success values and errors.
type BoolErrList []be

// Append adds a tuple of a success value and an error to the list.
func (l *BoolErrList) Append(b bool, e error) {
	*l = append(*l, be{b, e})
}

// Reduce returns the first error in the list.
// If there is no error it returns if there is a no-sucess value.
func (l BoolErrList) Reduce() (bool, error) {
	b := true
	for _, x := range l {
		if x.err != nil {
			return false, x.err
		}
		b = b && x.b
	}
	return b, nil
}
