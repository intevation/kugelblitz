// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package utils

import "os"

// Env looks up a value for environment variable and returns it.
// If not found it returns the given default value.
func Env(key, def string) string {
	if value, found := os.LookupEnv(key); found {
		return value
	}
	return def
}
