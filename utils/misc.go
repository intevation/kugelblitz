// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package utils

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"runtime"
)

// Trace generates a string containing the current execution
// position on the call stack consisting of the function name
// and the line number.
// d = 0 is inside the Trace function itself, d = 1 the caller
// of Trace and d = 2 the caller of the caller and so on.
func Trace(d int) string {
	pc := make([]uintptr, 3+d)
	n := runtime.Callers(0, pc)
	if n == 0 {
		return ""
	}
	pc = pc[:n]
	frames := runtime.CallersFrames(pc)
	for {
		frame, more := frames.Next()
		if !more {
			return fmt.Sprintf("%s:%d", frame.Function, frame.Line)
		}
	}
}

// DumpJSON dumps a pretty printed JSON representation of the
// given data to stdout.
func DumpJSON(data interface{}) {
	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "  ")
	if err := enc.Encode(data); err != nil {
		log.Printf("error: %v\n", err)
	}
}
