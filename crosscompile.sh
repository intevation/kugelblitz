#! /bin/bash
# cross-compile kugelblitz for various os/cpus and zip/tar.gz the output
# requires at least Go 1.5
# authors: Oliver Tonnhofer <olt@omniscale.de>, Bjoern Schilberg <bjoern.schilberg@intevation.de>


set -e

BUILD_DATE=`date +%Y%m%d`
BUILD_REF=`hg id -i`
BUILD_VERSION=dev-$BUILD_DATE-$BUILD_REF
VERSION_LDFLAGS="-X bitbucket.org/intevation/kugelblitz/common.buildVersion=${BUILD_VERSION}"

function install_deps() {
    
    go get -u -v github.com/lib/pq
    go get -u -v github.com/gorilla/securecookie
    go get -u -v github.com/gorilla/mux
    go get -u -v gopkg.in/gomail.v2
    go get -u -v github.com/mb0/wkt
    # Used to generate eCall backend from WSDL.
    # As we include the pregenerated cor this is no compile or runtime
    # requirement.
    # go get -u -v github.com/hooklift/gowsdl
}

function prepare_assets() {
    mkdir -p bin-assets
    cp -r cmd/kugelblitz/web bin-assets/
    cp -r LICENSE README.md doc schema bin-assets
}

# build os arch
function build() {
    os=$1
    arch=$2
    build_name=kugelblitz-$BUILD_VERSION-$os-$arch
    mkdir -p $build_name
    echo building $build_name
    cd $build_name
    cp -r ../../../bin-assets/* .
    env GOOS=$os GOARCH=$arch go build -x -ldflags "${VERSION_LDFLAGS}" bitbucket.org/intevation/kugelblitz/cmd/kugelblitz
    cd ..
    if [ $os = windows ]; then
        rm -f $build_name.zip
        zip -q -r $build_name.zip $build_name
    else
        tar -czf $build_name.tar.gz $build_name
    fi
    rm -r $build_name
}

install_deps
prepare_assets

mkdir -p dist/$BUILD_VERSION
cd dist/$BUILD_VERSION

# build for these os/arch combinations
#build windows 386
build windows amd64
#build linux 386
build linux amd64
#build darwin amd64
build linux arm

cd ../../
