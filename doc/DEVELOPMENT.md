# Development

You need a working [Go](https://golang.org) build environment (Tested
successfully with Go 1.10+).

Go get ist with:

```shell
go get -u -v bitbucket.org/intevation/kugelblitz/cmd/kugelblitz
go get -u -v bitbucket.org/intevation/kugelblitz/cmd/alarmserver
```

or build manually after a check-out with:

```shell
./3rdpartylibs.sh
cd cmd/kugelblitz
go build
./kugelblitz
```

A simple web server listing on `localhost:5000` will be started, which serves
the contents from the `web` folder.

To build the alarmserver:

```shell
cd cmd/alarmserver
go build
./alarmserver
```
