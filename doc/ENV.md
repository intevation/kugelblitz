# Environment variables

You need following environments variables to be set:

- `DB_CONNECTION`
- `GEOSERVER`
- `SESSION_HASH`
- `CONFIG`

## DB_CONNECTION

You need to configure the database connection with the
environment variable `DB_CONNECTION`.
The value is the Base64 encoded version of a string of the form
`u=dbuser pw=dbpassword h=dbhost d=dbname p=dbport s=require`
with `dbuser` the database user, `dbpassword` the password, `dbhost` and
`dbport` the host and the port of the database server and `dbname` the name of the
database. For example:

```shell
export DB_CONNECTION=$(echo -n 'u=user pw=secret h=localhost d=thunderbolt p=5432 s=require' | base64 -w0)
```

If run in a Elastic Beanstalk you can set this in the configuration dialog
of the environment in the `Software` Tab under `Environment properties`.
Don't forget to save and apply!

## GEOSERVER

The WMS proxy is configured by the environment variable `GEOSERVER`
e.g.

```shell
export GEOSERVER=http://127.0.0.1:8080/geoserver
```

## SESSION_HASH

The session keys are signed by the kugelblitz server. The secret can
be configured with the environment variable `SESSION_HASH`. Use
something long, secure and unique here, e.g `pwgen 32 1`. Keep
this value or the session got invalid each time the server restarts.

## CONFIG

To use the database based configuration set environment variable `CONFIG`
to the name of the configuration set in the database.

To configure the credentials of the mail server used to send password reset mails:

```sql
INSERT INTO configuration (name, description) VALUES ('kugelblitz', 'The configuration of the kugelblitz');

INSERT INTO configuration_kv (configuration_id, key, value) SELECT id, 'pw-reset-mail-port', '465' FROM configuration WHERE name = 'kugelblitz';
INSERT INTO configuration_kv (configuration_id, key, value) SELECT id, 'pw-reset-mail-host', 'mail.example.com' FROM configuration WHERE name = 'kugelblitz';
INSERT INTO configuration_kv (configuration_id, key, value) SELECT id, 'pw-reset-mail-user', 'mailuser' FROM configuration WHERE name = 'kugelblitz';
INSERT INTO configuration_kv (configuration_id, key, value) SELECT id, 'pw-reset-mail-pw',   'SUPERSECRET!' FROM configuration WHERE name = 'kugelblitz';
INSERT INTO configuration_kv (configuration_id, key, value) SELECT id, 'pw-reset-mail-from', 'noreply@mail.example.com' FROM configuration WHERE name = 'kugelblitz';
INSERT INTO configuration_kv (configuration_id, key, value) select id, 'pw-reset-mail-helo', 'kugelblitz.example.com' FROM configuration WHERE name = 'kugelblitz';
```

To add the Google Maps API key:

```sql
INSERT INTO configuration_kv (configuration_id, key, value) select id, 'google-maps-key', 'YOUR_API_KEY' FROM configuration WHERE name = 'kugelblitz';
```

To activate this configuration use `export CONFIG=kugelblitz`.

## Sample shell script to set the environments variables

```shell
#!/bin/sh

export GEOSERVER=http://<SERVER>/geoserver

UNENC='u=postgres pw=<PASSWORD> h=<SERVER> d=<DATABASE> p=15432 s=require'
export DB_CONNECTION=$(echo $UNENC | base64 -w0)

# To use the database based configuration set environment variable CONFIG to
# the name of the configuration set in the database.
export CONFIG=<CONFIG>

# Create SESSION_HASH with pwgen 32 1
export SESSION_HASH=<SESSION_HASH>

EX="$1"

exec $EX "$@"
```

Start the kugelblitz with the `start.sh` shell script from above:

```shell
./start.sh ./kugelblitz -log=true
```
