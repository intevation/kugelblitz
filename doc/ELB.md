# Usage in an AWS Elastic Beanstalk (ELB) Environment

Pack for ELB deployment:

```shell
cd cmd/kugelblitz
go clean
go build
./pack-elb.sh
```

Upload the resulting `kugelblitz-*.zip` file to an ELB instance.
