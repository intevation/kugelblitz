// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package broadcast

import "sync"

// Queue is a queue to exchange byte slices between go routines
// without limited size of channels.
type Queue struct {
	rep      [][]byte
	frontPos int
	backPos  int
	length   int

	closed bool
	cond   *sync.Cond
}

// NewQueue creates a new queue.
func NewQueue() *Queue {
	q := &Queue{
		rep:  make([][]byte, 1),
		cond: sync.NewCond(new(sync.Mutex)),
	}
	return q
}

// empty returns true if the queue q has no elements.
func (q *Queue) empty() bool {
	return q.length == 0
}

// full returns true if the queue q is at capacity.
func (q *Queue) full() bool {
	return q.length == len(q.rep)
}

// sparse returns true if the queue q has excess capacity.
func (q *Queue) sparse() bool {
	return 1 < q.length && q.length < len(q.rep)/4
}

// resize adjusts the size of queue q's underlying slice.
func (q *Queue) resize(size int) {
	adjusted := make([][]byte, size)
	if q.frontPos < q.backPos {
		// rep not "wrapped" around, one copy suffices
		copy(adjusted, q.rep[q.frontPos:q.backPos])
	} else {
		// rep is "wrapped" around, need two copies
		n := copy(adjusted, q.rep[q.frontPos:])
		copy(adjusted[n:], q.rep[:q.backPos])
	}
	q.rep = adjusted
	q.frontPos = 0
	q.backPos = q.length
}

// lazyGrow grows the underlying slice if necessary.
func (q *Queue) lazyGrow() {
	if q.full() {
		q.resize(len(q.rep) * 2)
	}
}

// lazyShrink shrinks the underlying slice if advisable.
func (q *Queue) lazyShrink() {
	if q.sparse() {
		q.resize(len(q.rep) / 2)
	}
}

// inc returns the next integer position wrapping around queue q.
func (q *Queue) inc(i int) int {
	return (i + 1) & (len(q.rep) - 1) // requires l = 2^n
}

// Append inserts a new value v at the back of queue q.
func (q *Queue) Append(v []byte) {
	q.cond.L.Lock()
	defer q.cond.L.Unlock()

	if !q.closed {
		q.lazyGrow()
		q.rep[q.backPos] = v
		q.backPos = q.inc(q.backPos)
		q.length++
	}
	q.cond.Broadcast()
}

// Remove removes and returns the first element of queue q or nil.
func (q *Queue) Remove() ([]byte, bool) {
	q.cond.L.Lock()
	defer q.cond.L.Unlock()

	for !q.closed && q.empty() {
		q.cond.Wait()
	}

	if q.closed {
		return nil, false
	}

	v := q.rep[q.frontPos]
	q.rep[q.frontPos] = nil // unused slots must be nil
	q.frontPos = q.inc(q.frontPos)
	q.length--
	q.lazyShrink()
	return v, true
}

// Close closes the queue.
func (q *Queue) Close() {
	q.cond.L.Lock()
	q.closed = true
	q.cond.L.Unlock()
	q.cond.Broadcast()
}
