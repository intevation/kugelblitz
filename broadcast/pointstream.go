// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package broadcast

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/intevation/kugelblitz/model"
	"github.com/mb0/wkt"
)

type pointstreamClient struct {
	bbox *model.BBox
	geom wkt.Geom
	outs []*Queue
}

// PointstreamBroker registers to a Receiver and broadcasts
// the transcripted lighting messages to the SSE clients.
type PointstreamBroker struct {
	cmds    chan func()
	clients map[string]*pointstreamClient
}

// blitz.blitze
type incoming struct {
	ID       int64    `json:"id"`
	Time     jsonTime `json:"zeit"`
	Type     int64    `json:"typ"`
	Amperage float64  `json:"strom"`
	Entry    jsonTime `json:"eintrag"`
	Coord    wktPoint `json:"koordinate"`
	//BigAxis    float64  `json:"grossachse"`
	//SmallAxis  float64  `json:"kleinachse"`
	//AngleAxis  float64  `json:"winkelachse"`
	//DoF        int64    `json:"freigrade"`
	//ChiSquare  float64  `json:"chiquadrat"`
	//RaiseTime  float64  `json:"steigzeit"`
	//TopZero    float64  `json:"spitzenull"`
	NumSensors float64 `json:"sensorzahl"`
	//Height     float64  `json:"height"`
	TLP float64 `json:"tlp"`
}

type outgoing struct {
	ID       int64    `json:"id"`
	Time     jsonTime `json:"zeit"`
	Type     int64    `json:"typ"`
	Amperage float64  `json:"strom"`
	Entry    jsonTime `json:"eintrag"`
	Lat      float64  `json:"lat"`
	Lon      float64  `json:"lon"`
	//BigAxis    float64  `json:"grossachse"`
	//SmallAxis  float64  `json:"kleinachse"`
	//AngleAxis  float64  `json:"winkelachse"`
	//DoF        int64    `json:"freigrade"`
	//ChiSquare  float64  `json:"chiquadrat"`
	//RaiseTime  float64  `json:"steigzeit"`
	//TopZero    float64  `json:"spitzenull"`
	NumSensors float64 `json:"sensorzahl"`
	//Height     float64  `json:"height"`
	TLP float64 `json:"tlp"`
}

type jsonTime struct{ time.Time }

type wktPoint struct{ *wkt.Point }

// UnmarshalJSON fulfills the JSON Unmarshaler interface.
func (it *jsonTime) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	s = strings.Trim(s, `"`)
	t, err := time.Parse(time.RFC3339Nano, s)
	if err == nil {
		*it = jsonTime{t}
	}
	return err
}

// UnmarshalJSON fulfills the JSON Marshaler interface.
func (it *jsonTime) MarshalJSON() ([]byte, error) {
	s := it.Format(time.RFC3339Nano)
	return json.Marshal(s)
}

// UnmarshalJSON fulfills the JSON Unmarshaler interface.
func (ic *wktPoint) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	s = strings.Trim(s, `"`)
	g, err := wkt.Parse([]byte(s))
	if err != nil {
		return err
	}
	if p, ok := g.(*wkt.Point); ok {
		*ic = wktPoint{p}
		return nil
	}
	return fmt.Errorf("unexpected geom type: %T", g)
}

func (ic *incoming) encode() []byte {
	out := outgoing{
		ID:       ic.ID,
		Time:     ic.Time,
		Type:     ic.Type,
		Amperage: ic.Amperage,
		Entry:    ic.Entry,
		Lat:      ic.Coord.Y,
		Lon:      ic.Coord.X,
		//BigAxis:    ic.BigAxis,
		//SmallAxis:  ic.SmallAxis,
		//AngleAxis:  ic.AngleAxis,
		//DoF:        ic.DoF,
		//ChiSquare:  ic.ChiSquare,
		//RaiseTime:  ic.RaiseTime,
		//TopZero:    ic.TopZero,
		NumSensors: ic.NumSensors,
		//Height:     ic.Height,
		TLP: ic.TLP,
	}
	data, err := json.Marshal(&out)
	if err != nil {
		log.Printf("error: %v\n", err)
	}
	return data
}

// NewPointStreamBroker creates a new PointstreamBroker.
func NewPointStreamBroker() *PointstreamBroker {
	return &PointstreamBroker{
		cmds:    make(chan func()),
		clients: make(map[string]*pointstreamClient),
	}
}

func (psb *PointstreamBroker) sessionDied(key string) {
	client := psb.clients[key]
	if client == nil {
		return
	}
	delete(psb.clients, key)
	for _, out := range client.outs {
		out.Close()
	}
	client.bbox = nil
	client.outs = nil
	client.geom = nil
}

// SessionDied is called if a session dies to disconnect the
// respective clients.
func (psb *PointstreamBroker) SessionDied(key string) {
	log.Println("PointstreamBroker.SessionDied called")
	psb.cmds <- func() { psb.sessionDied(key) }
}

// Run starts the dispatching and does not return.
func (psb *PointstreamBroker) Run() {
	for cmd := range psb.cmds {
		cmd()
	}
}

func (psb *PointstreamBroker) dispatch(in *incoming) {
	//log.Printf("stroke (%.4f, %.4f) %v\n",
	//in.Coord.X, in.Coord.Y,
	//in.Time.Local())

	// TODO: This could be sped up with spatial indexing.

	var encoded []byte

	for _, client := range psb.clients {
		if client.bbox.Contains(in.Coord.Point) &&
			model.GeomContainsPoint(client.geom, in.Coord.Point) {
			// encode lazy
			if encoded == nil {
				encoded = in.encode()
			}
			for _, out := range client.outs {
				out.Append(encoded)
			}
		}
	}
}

// Handle is the message entry point from the Listen/Notify receiver.
func (psb *PointstreamBroker) Handle(msg json.RawMessage) error {
	//log.Printf("psb msg: %s\n", msg)
	var in incoming
	if err := json.Unmarshal(msg, &in); err != nil {
		return err
	}

	// Move to pointstream main.
	psb.cmds <- func() { psb.dispatch(&in) }
	return nil
}

func (psb *PointstreamBroker) heartbeat() {
	// Send hearbeat with timestamp
	hbData := []byte(`{"heartbeat":` + strconv.FormatInt(time.Now().Unix(), 10) + `}`)
	for _, client := range psb.clients {
		for _, out := range client.outs {
			out.Append(hbData)
		}
	}
}

// HandleHeartbeat handle the heartbeat from the Listen/Notify receiver.
func (psb *PointstreamBroker) HandleHeartbeat(json.RawMessage) error {
	// Move to pointstream main.
	psb.cmds <- func() { psb.heartbeat() }
	return nil
}

func (psb *PointstreamBroker) registerClient(
	key string,
	geom wkt.Geom,
	out *Queue) {

	client := psb.clients[key]
	if client != nil {
		client.outs = append(client.outs, out)
		return
	}
	client = &pointstreamClient{
		bbox: model.CalculateBBox(geom),
		geom: geom,
		outs: []*Queue{out},
	}
	psb.clients[key] = client
}

// RegisterClient is to be called to register new SSE clients.
func (psb *PointstreamBroker) RegisterClient(
	key string,
	geom wkt.Geom,
	out *Queue) {
	psb.cmds <- func() { psb.registerClient(key, geom, out) }
}

func (psb *PointstreamBroker) unregisterClient(key string, out *Queue) {

	client := psb.clients[key]
	if client == nil {
		return
	}

	outs := client.outs

	for i, o := range outs {
		if o == out {
			out.Close()
			copy(outs[i:], outs[i+1:])
			outs[len(outs)-1] = nil
			outs = outs[:len(outs)-1]
			break
		}
	}
	if len(outs) == 0 {
		delete(psb.clients, key)
	} else {
		client.outs = outs
	}
}

// UnregisterClient is called to remove a SSE client from the broker.
func (psb *PointstreamBroker) UnregisterClient(key string, out *Queue) {
	psb.cmds <- func() { psb.unregisterClient(key, out) }
}
