// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package broadcast

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/lib/pq"
)

const (
	channelName   = "blids"
	retryDuration = 1 * time.Minute
)

// MessageHandler is the function type to receive the dispatched raw message.
type MessageHandler func(json.RawMessage) error

// Receiver encapsulates the de-multiplexing of messages
// from a PostgreSQL Listen/Notify to a number of registered
// handlers.
type Receiver struct {
	dsn      string
	handlers map[string]MessageHandler

	failed chan error
}

// NewReceiver creates a new reciever with a given domain source name.
func NewReceiver(dsn string) *Receiver {
	return &Receiver{
		dsn:      dsn,
		handlers: make(map[string]MessageHandler),
		failed:   make(chan error, 2),
	}
}

// AddHandler registers a handler for a message type.
func (rcv *Receiver) AddHandler(msgType string, handler MessageHandler) {
	rcv.handlers[msgType] = handler
}

func (rcv *Receiver) logListener(event pq.ListenerEventType, err error) {
	if err != nil {
		log.Printf("listener error: %s\n", err)
	}
	if event == pq.ListenerEventConnectionAttemptFailed {
		rcv.failed <- err
	}
}

func (rcv *Receiver) dispatch(notification string) error {
	var msg struct {
		Type string          `json:"type"`
		Data json.RawMessage `json:"data"`
	}
	if err := json.NewDecoder(strings.NewReader(notification)).Decode(&msg); err != nil {
		return err
	}
	if handler := rcv.handlers[msg.Type]; handler != nil {
		return handler(msg.Data)
	}
	return fmt.Errorf("unknown handler for type '%s'", msg.Type)
}

func (rcv *Receiver) runListener() error {
	listener := pq.NewListener(
		rcv.dsn,
		10*time.Second, time.Minute,
		rcv.logListener)

	defer listener.Close()

	if err := listener.Listen(channelName); err != nil {
		return err
	}

	ticker := time.NewTicker(time.Minute)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			rcv.dispatch(`{"type": "hb-light"}`)
			rcv.dispatch(`{"type": "hb-alarm"}`)
		case e := <-listener.Notify:
			if e == nil {
				continue
			}
			if err := rcv.dispatch(e.Extra); err != nil {
				log.Printf("dispatching failed: %v\n", err)
			}
		case err := <-rcv.failed:
			return err
		// XXX: This smells like a Go routine leak.
		case <-time.After(time.Minute):
			go listener.Ping()
		}
	}
}

// Run starts the receiving in a loop and does not return.
func (rcv *Receiver) Run() {
	for {
		if err := rcv.runListener(); err != nil {
			log.Printf("listener error: %v\n", err)
		}
		time.Sleep(retryDuration)
	}
}
