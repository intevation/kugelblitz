// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"strings"
	"time"

	"bitbucket.org/intevation/kugelblitz/common"
	_ "github.com/lib/pq"
)

const (
	// defaultPPM is the default number of points per minute to be generated.
	defaultPPM = 60
	// defaultNumPoints is the default number of points to be generated.
	// < 0 means no limit.
	defaultNumPoints = 1000

	defaultPort = 5432
	defaultSSL  = "require"
	defaultUser = "postgres"
	defaultPW   = ""
	defaultDB   = "blids"
	defaultHost = "localhost"
)

const sqlInsert = `INSERT INTO blitz.blitze%s
   (zeit, typ, strom, koordinate) VALUES
   ($1, 1, 12.5, ST_GeomFromText($2, 4326))`

type latLonRand struct{ *rand.Rand }

// latLng generates a random lat/lon pair.
func (r *latLonRand) latLng() (float64, float64) {
	const (
		latMin, latMax = 5.5, 15.5
		lonMin, lonMax = 47, 55
	)
	return (latMax-latMin)*r.Float64() + latMin,
		(lonMax-lonMin)*r.Float64() + lonMin
}

// pgQuote quotes strings to be able to contain whitespace
// and backslashes in PosgreSQL DSN strings.
func pgQuote(s string) string {
	s = strings.Replace(s, "\\", "\\\\", -1)
	s = strings.Replace(s, "'", "\\'", -1)
	return "'" + s + "'"
}

// pgDSN creates a data source name suitable for sql.Open on
// PostgreSQL databases.
func pgDSN(host string, port uint, dbname, user, password string, sslmode string) string {
	return fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s",
		pgQuote(host), port, pgQuote(dbname),
		pgQuote(user), pgQuote(password), sslmode)
}

// morePoints returns a function which tells if called if there
// should be generated more points.
func morePoints(nPoints int) func() bool {
	if nPoints < 0 {
		return func() bool { return true }
	}
	i := 0
	return func() bool {
		more := i < nPoints
		i++
		return more
	}
}

func main() {

	var (
		pointsPM = flag.Uint("ppm", defaultPPM, "points per minute")
		nPoints  = flag.Int("np", defaultNumPoints, "number of points (< 0 = inf)")
		user     = flag.String("u", defaultUser, "database user")
		password = flag.String("pw", defaultPW, "database password")
		database = flag.String("d", defaultDB, "database name")
		host     = flag.String("h", defaultHost, "database host")
		port     = flag.Uint("p", defaultPort, "database port")
		ssl      = flag.String("ssl", defaultSSL, "Use SSL for connection")
		version  = flag.Bool("version", false, "Print version and exit")
	)

	flag.Parse()

	if *version {
		common.PrintVersionAndExit()
	}

	if *pointsPM == 0 {
		log.Fatalln("Points per minute has to be greater than zero.")
	}

	dsn := pgDSN(*host, *port, *database, *user, *password, *ssl)

	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("DB open failed: %s\n", err)
	}
	defer db.Close()

	stmt, err := db.Prepare(fmt.Sprintf(sqlInsert, time.Now().Format("2006")))
	if err != nil {
		log.Fatalf("prepare insert statement failed: %s\n", err)
	}
	defer stmt.Close()

	log.Printf("points per minute: %d\n", *pointsPM)
	log.Printf("number of points: %d\n", *nPoints)

	wait := time.Minute / time.Duration(*pointsPM)

	rnd := latLonRand{rand.New(rand.NewSource(time.Now().Unix()))}

	for start, more := time.Now(), morePoints(*nPoints); more(); start = time.Now() {
		lat, lon := rnd.latLng()
		log.Printf("insert time=%v lat=%f lon=%f\n", start, lat, lon)
		point := fmt.Sprintf("POINT(%f %f)", lat, lon)
		if _, err := stmt.Exec(start, point); err != nil {
			log.Fatalf("insert failed: %s\n", err)
		}
		took := time.Since(start)
		if realWait := wait - took; realWait > 0 {
			time.Sleep(realWait)
		}
	}
}
