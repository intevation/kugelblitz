// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"database/sql"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/intevation/kugelblitz/common"
	_ "github.com/lib/pq"
)

const (
	defaultPort = 5432
	defaultSSL  = "require"
	defaultUser = "postgres"
	defaultPW   = ""
	defaultDB   = "blids"
	defaultHost = "localhost"
)

const sqlInsert = `INSERT INTO blobstore (name, mimetype, data) VALUES ($1, $2, $3)
ON CONFLICT (name)
DO UPDATE SET mimetype = $2, data = $3`

// pgQuote quotes strings to be able to contain whitespace
// and backslashes in PosgreSQL DSN strings.
func pgQuote(s string) string {
	s = strings.Replace(s, `\`, `\\`, -1)
	s = strings.Replace(s, `'`, `\'`, -1)
	return `'` + s + `'`
}

// pgDSN creates a data source name suitable for sql.Open on
// PostgreSQL databases.
func pgDSN(host string, port uint, dbname, user, password string, sslmode string) string {
	return fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s",
		pgQuote(host), port, pgQuote(dbname),
		pgQuote(user), pgQuote(password), sslmode)
}

func storeFile(src, dst string, insertStmt *sql.Stmt) error {
	f, err := os.Open(src)
	if err != nil {
		return err
	}
	defer f.Close()
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}

	mType := http.DetectContentType(data)
	if dst == "" {
		dst = filepath.Base(src)
	}
	sdata := string(data)

	_, err = insertStmt.Exec(dst, mType, sdata)

	return err
}

func main() {
	var (
		user     = flag.String("u", defaultUser, "database user")
		password = flag.String("pw", defaultPW, "database password")
		database = flag.String("d", defaultDB, "database name")
		host     = flag.String("h", defaultHost, "database host")
		port     = flag.Uint("p", defaultPort, "database port")
		ssl      = flag.String("ssl", defaultSSL, "Use SSL for connection")
		version  = flag.Bool("version", false, "Print version and exit")
	)

	flag.Parse()

	if *version {
		common.PrintVersionAndExit()
	}

	dsn := pgDSN(*host, *port, *database, *user, *password, *ssl)

	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("DB open failed: %s\n", err)
	}
	defer db.Close()

	insertStmt, err := db.Prepare(sqlInsert)
	if err != nil {
		log.Fatalf("prepare insert statement failed: %s\n", err)
	}
	defer insertStmt.Close()

	for i, args := 0, flag.Args(); i < len(args); i += 2 {
		src := args[i]
		var dst string
		if i+1 < len(args) {
			dst = args[i+1]
		}
		if err := storeFile(src, dst, insertStmt); err != nil {
			log.Printf("warn: %v\n", err)
		}
	}
}
