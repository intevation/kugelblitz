# blobpush

`blobpush` is a small tool to fill the `blobstore` table in the database.  
See [blobstore.sql](../../schema/blobstore/blobstore.sql) for the schema definition.

## Build

```
go get -u -v bitbucket.org/intevation/kugelblitz/cmd/blobpush
```

Place the resulting `blobpush` binary into your `PATH`.

## Usage

```
blobpush --help
Usage of blobpush:
  -d string
       database name (default "blids")
  -h string
       database host (default "localhost")
  -p uint
       database port (default 5432)
  -pw string
       database password
  -ssl string
       Use SSL for connection (default "require")
  -u string
       database user (default "postgres")
  -version
        Print version and exit
```

Example usage:

```
blobpush -pw 'SECRET' -d kugelblitz ../my/special/image.png clowns/me.png
```

The first argument is the path to the file to be uploaded. The second argument
is the name in the database. If the second argument is omitted the
basename of the file is used as resulting name.  
You can upload more than one file by given pairs of file names and resulting names.  
Files with the same resulting name already in the database will be overwritten.


The uploaded file can be fetched from a running kugelblitz server with the
`/content` controller.

```
wget http://localhost:5000/content/clowns/me.png
```

The content controller caches the most recently used files for better
serving performance. To force the controller to load the data
from the database again add `?cache=fresh` to the request above.
Only use this during development!

```
wget http://localhost:5000/content/clowns/me.png?cache=fresh
```

To check with the database:

```
SELECT mimetype, length(data) FROM blobstore WHERE name = 'clowns/me.png';
```

