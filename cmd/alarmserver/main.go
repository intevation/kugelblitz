// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// BLIDS alarmserver.

package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/intevation/kugelblitz/alarmserver"
	"bitbucket.org/intevation/kugelblitz/common"
)

func main() {
	var (
		conffile string
		confdb   bool
		conninfo string
		channel  string
		version  bool
	)
	flag.StringVar(&conffile, "conf", "",
		"Configuration file in JSON format.")
	flag.BoolVar(&confdb, "dbconf", false,
		"Read configuration from db instead of file.")
	flag.StringVar(&conninfo, "db", "",
		"PostgreSQL to connect to, in conninfo format.")
	flag.StringVar(&channel, "channel", "blids",
		"PostgreSQL notification channel to listen on.")
	flag.BoolVar(&version, "version", false,
		"Print version and exit.")

	flag.Parse()

	if version {
		common.PrintVersionAndExit()
	}

	if conninfo == "" {
		fmt.Println("Database must be specified.")
		flag.PrintDefaults()
		os.Exit(2)
	}
	if !confdb && conffile == "" {
		fmt.Println("Missing configuration.")
		flag.PrintDefaults()
		os.Exit(2)
	}

	as, err := alarmserver.NewAlarmServer(conffile, conninfo, channel)
	if err != nil {
		log.Fatal(err)
	}

	as.Debug("Alarmserver starting to listen for notifications...")

	for {
		as.ProcessNotifications()
	}
}
