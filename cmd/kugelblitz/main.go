// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"bitbucket.org/intevation/kugelblitz/common"
	"bitbucket.org/intevation/kugelblitz/controllers"
	"bitbucket.org/intevation/kugelblitz/session"
	"bitbucket.org/intevation/kugelblitz/utils"
	"github.com/gorilla/mux"
)

func pathToWeb() string {
	web := os.Getenv("WEB")
	if web != "" {
		return web
	}
	path, err := os.Executable()
	if err == nil {
		return filepath.Join(filepath.Dir(path), "web")
	}
	return "web"
}

func main() {

	lg := flag.Bool("log", false, "Log incoming requests")
	version := flag.Bool("version", false, "Print version and exit")

	flag.Parse()

	if *version {
		common.PrintVersionAndExit()
	}

	port := utils.Env("PORT", "5000")

	c := controllers.NewController()

	m := mux.NewRouter()
	c.BindRoutes(m)

	web := pathToWeb()
	m.PathPrefix("/").Handler(http.FileServer(http.Dir(web)))

	var handler http.Handler

	if *lg {
		handler = session.Logging(m)
	} else {
		handler = m
	}

	if err := c.Start(); err != nil {
		log.Fatalf("error: controller failed: %v\n", err)
	}

	log.Printf("Listen on port %s\n", port)
	log.Fatalln(http.ListenAndServe(":"+port, handler))
}
