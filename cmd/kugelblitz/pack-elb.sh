#!/bin/sh

set -e

zipfile=kugelblitz-$(date +'%Y-%m-%d')-$(git rev-parse --short HEAD).zip

echo $zipfile

rm -f $zipfile

rm -f kugelblitz
go build

zip -9r $zipfile \
    Procfile kugelblitz \
    web -x web/assets/js/OpenLayers-2.12/OpenLayers.debug.js \
           web/assets/js/OpenLayers-2.12/OpenLayers.mobile.debug.js
