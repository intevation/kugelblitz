'use strict';

// Copyright 2012 - 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

(function () {

    var blids = angular.module('blidsApp');
    blids
            .factory('mainMenu', function ($http) {
                var lscope, lelem, lattr;

                var linker = function (scope, elem, attr) {

                    // Remove hide from parent element
                    if (elem)
                        elem.parent().removeClass('navbar-hidden');

                    if (typeof scope === 'undefined') {
                        scope = lscope,
                                elem = lelem,
                                attr = lattr;
                    }
                    else {
                        lscope = scope,
                                lelem = elem,
                                lattr = attr;
                    }

                    var menuItems = [
// Navigation demo
//                {url: './', label: 'Home'},
//                {url: '#/user/login', label: 'Log In'},
//                {url: './index.php/auth/logout', label: 'Log Out'}
                    ];

                    scope.menuItems = menuItems;
                };

                return {
                    reload: linker
                };
            })

            .directive('mainMenu', function (mainMenu) {
                return {
                    restrict: 'E',
                    transclude: true,
                    replace: true,
                    template: '<ul ng-transclude></ul>',
                    link: mainMenu.linker
                };
            })

            .directive('blidsLoginBg', function ($interval) {
//                var _openLayersMap;
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {

//                        var map = new OpenLayers.Map('blids-map', {controls: []});
//
//                        map.addLayer(new OpenLayers.Layer.OSM());
//
//                        var lonLatPairs = [
//                            [48.949966, 8.883075],
//                            [41.317716, -4.962958],
//                            [49.4360936, 11.1011233],
//                            [43.953650, 12.459994],
//                            [53.964419, -1.527339]
//                        ];
//
//                        var randMapCenter = function () {
//                            var rnd = Math.floor(Math.random() * 10 * (lonLatPairs.length / 10)),
//                                    pairs = lonLatPairs[rnd];
//
//                            map.setCenter(
//                                    new OpenLayers.LonLat(pairs[1], pairs[0]).transform(
//                                    new OpenLayers.Projection("EPSG:4326"),
//                                    map.getProjectionObject()
//                                    ),
//                                    Math.round(Math.random() * 10 * .4) + 4
//                                    );
//
//                            map.moveTo(pairs[1], pairs[0]);
//
//                            //$interval(randMapCenter, window.LOGIN_BG_INTERVAL);
//                        };
//
//                        scope.$watch('blids-map', function () {
//                            randMapCenter();
//                        });
//
//                        map.zoomToMaxExtent();
                    },
                    template: '<div class="overlay"></div>' +
//                            '<div id="blids-map" style="width:100%;height:95%;"></div>'
                            '<div style="width:100%;height:95%;"></div>'
                };
            })

            .directive('bsnavmenu', function () {
                return {
                    restrict: 'E',
                    templateUrl: function (elem, attrs) {
                        return 'assets/template/' + attrs.templateUrl;
                    }
                };
            })

            .directive('layerTriCheckbox', function () {
                return {
                    scope: true,
                    require: '?ngModel',
                    link: function (scope, element, attrs, modelCtrl) {
                        var childList = attrs.childList;
                        var property = attrs.property;
                        var index = attrs.id;
                        // Bind the onChange event to update children
                        element.bind('change', function () {
                            scope.$apply(function () {
                                var isChecked = element.prop('checked');
                                // Set each child's selected property to the checkbox's checked property
                                angular.forEach(scope.$eval(childList), function (child) {
                                    child[property] = isChecked;
                                });
                                scope.$parent.content.layer[index].visible = isChecked;
                                scope.filterChange(index);
                            });
                        });
                        // Watch the children for changes
                        scope.$watch(childList, function (newValue) {
                            var hasChecked = false;
                            var hasUnchecked = false;
                            // Loop through the children
                            if (newValue === undefined)
                                return;
                            angular.forEach(newValue, function (child) {
                                if (child[property]) {
                                    hasChecked = true;
                                } else {
                                    hasUnchecked = true;
                                }
                            });

                            // Determine which state to put the checkbox in
                            if (hasChecked && hasUnchecked) {
                                element.prop('checked', false);
                                element.prop('indeterminate', true);
                                scope.$parent.content.layer[element.prop('id')].visible = true;
                                if (modelCtrl) {
                                    modelCtrl.$setViewValue(false);
                                }
                            } else {
                                element.prop('checked', hasChecked);
                                element.prop('indeterminate', false);
                                scope.$parent.content.layer[element.prop('id')].visible = hasChecked;
                                if (modelCtrl) {
                                    modelCtrl.$setViewValue(hasChecked);
                                }
                            }
                            scope.filterChange(index);
                        }, true);
                    }
                };
            });


})();

