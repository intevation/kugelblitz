'use strict()';

// Copyright 2012 - 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

(function() {

    var blids = angular.module('blidsApp');
    blids.controller('LayerCtrl', function($scope, $filter, UserService) {
        var j, i;
        var layers = UserService.getData().layers;
        var menu = [];
        for (j = 0; j < layers.length; j++) {
            if (layers[j].permanent !== "t") {
                var name = layers[j].displayname;
                if (layers[j].filter) {
                    var filter = [];
                    for (var filtername in layers[j].filter) {
                        if (layers[j].filter.hasOwnProperty(filtername)) {
                            filter.push({ "name": filtername, "value": layers[j].filter[filtername], "isSelected": (layers[j].checked_onlogin === 't') });
                        }
                    }
                    menu.push({ "name": name, "allSelected": (layers[j].checked_onlogin === 't'), "visible": false, "filter": filter });
                } else
                    menu.push({ "name": name, "allSelected": (layers[j].checked_onlogin === 't'), "visible": false });
            }
        }
        if (true) {
            menu.push({ "name": $filter('translate')('map.userarea'), "allSelected": true, "visible": false });
        } // every user gets to see his lightning area

        $scope.content = { "layer": menu };
        $scope.filterChange = function(index) {
            var i, cql_filter,
                filterlist = [];
            if ($scope.content.layer[index].filter) {
                var length = $scope.content.layer[index].filter.length;
                for (i = 0; i < length; i++) {
                    if ($scope.content.layer[index].filter[i].isSelected && $scope.content.layer[index].filter[i].name) {
                        $scope.content.layer[index].visible = true;
                        filterlist.push($scope.content.layer[index].filter[i].value);
                    }
                }
                cql_filter = filterlist.join(' OR ');
            } else {
                cql_filter = "";
            }
            $scope.updateLayer($scope.content.layer[index].name, $scope.content.layer[index].visible, cql_filter);
        };
        $scope.initLayerSwitcher = function() {
            if (!$scope.open)
                return;
            if ($scope.map.getControlsByClass('OpenLayers.Control.LayerSwitcher').length > 0)
                return;
            $scope.map.addControl(new OpenLayers.Control.LayerSwitcher({ 'div': OpenLayers.Util.getElement('layerswitcher') }));
        };
        $scope.layerSwitcherOptMobile = function(isOpen) {
            if (window.mobileAndTabletcheck() && (window.innerWidth < 800)) {
                if (isOpen) {
                    document.getElementById("LayerMenu").style.width = "100%";
                    document.getElementById("LayerMenu").style.zIndex = "100";
                    document.getElementById("LayerMenu").style.top = "0%";
                    document.getElementById("layerBtn").style.marginTop = document.getElementById("CSMenu").offsetTop + "px";
                } else {
                    document.getElementById("LayerMenu").style.width = "";
                    document.getElementById("LayerMenu").style.top = "";
                    document.getElementById("LayerMenu").style.zIndex = "";
                    document.getElementById("layerBtn").style.marginTop = "0";
                }
            } else if (window.mobileAndTabletcheck()) {
                if (isOpen) {
                    document.getElementById("LayerMenu").style.zIndex = "100";
                    document.getElementById("LayerMenu").style.height = "90%";
                } else {
                    document.getElementById("LayerMenu").style.zIndex = "";
                    document.getElementById("LayerMenu").style.height = "auto";
                }
            }
        };
    });
    blids.controller('mapCtrl', function($scope, $filter, $interval, UserService, $location, $translate, $rootScope) {

        $rootScope.$on('$translateChangeSuccess', function() {
            var len = document.querySelectorAll(".layer").length - 1; // count Layers in Layerswitcher to find correct DOM Obj for User Area
            var userarea = document.querySelectorAll(".layer")[len].querySelector("span.ng-binding.ng-scope"); // and Translate its text
            userarea.innerHTML = " " + $translate.instant('map.userarea');
            document.querySelector("#hbText").innerHTML = $translate.instant('map.heartbeat');
            if (document.querySelector(".perMin")) { document.querySelector(".perMin").innerHTML = $translate.instant('map.per_minute'); }
            if (document.querySelector(".statWindow")) { document.querySelector(".statWindow").innerHTML = $translate.instant('map.statistic' + UserService.statistic.window + UserService.statistic.area); }
            if (document.querySelector(".MeasureItemInactive,.MeasureItemActive")) { document.querySelector(".MeasureItemInactive,.MeasureItemActive").title = $translate.instant('map.measure'); }
            if (document.querySelector(".HomeItemInactive")) { document.querySelector(".HomeItemInactive").title = $translate.instant('map.home'); }
            if (document.querySelector(".LogoutItemInactive")) { document.querySelector(".LogoutItemInactive").title = $translate.instant('nav.logout'); }
            if (document.querySelector(".ArchiveItemInactive,.ArchiveItemActive")) { document.querySelector(".ArchiveItemInactive,.ArchiveItemActive").title = $translate.instant('map.archive'); }
            if (document.querySelector(".AnimationItemInactive,.AnimationItemActive")) { document.querySelector(".AnimationItemInactive,.AnimationItemActive").title = $translate.instant('map.animation'); }
            if (document.querySelector(".SoundItemInactive,.SoundItemActive")) { document.querySelector(".SoundItemInactive,.SoundItemActive").title = $translate.instant('map.sound'); }
        });

        $scope.statistikOptMobile = function(isOpen) {
            if (window.mobilecheck() && (window.innerWidth < 800)) {
                if (isOpen) {
                    document.getElementById("StatistikWindow").style.width = "100%";
                    document.getElementById("StatistikWindow").style.bottom = "0%";
                    document.getElementById("StatistikWindow").style.zIndex = "2000";
                    document.getElementById("statistikleiste").style.width = "100%";
                } else {
                    document.getElementById("StatistikWindow").style.width = "";
                    document.getElementById("StatistikWindow").style.zIndex = "";
                    document.getElementById("StatistikWindow").style.bottom = "20%";
                }
            }
        };
        $scope.drawStatistic = function(contentDiv, begin, ende) {
            if (typeof $scope.open2 === 'undefined' || !$scope.open2) {
                return 0;
            }
            var time = ($scope.map.slider.options.max - $scope.map.slider.element.value) * 1000;
            var t = begin - ende;
            var dauer = (Math.floor(t / 3600 / 24) ? Math.floor(t / 3600 / 24) + "d " : "") + ('0' + Math.floor(t / 3600) % 24).slice(-2) + 'h ' + ('0' + Math.floor(t / 60) % 60).slice(-2) + 'm';
            var start = begin;
            var stop = ende;
            //            var start = $filter('date')(new Date() - 1000*begin, 'yyyy-MM-dd HH:mm');
            //            var stop = $filter('date')(new Date() - 1000*ende, 'yyyy-MM-dd HH:mm');

            var divWidth = d3.select(contentDiv).node().getBoundingClientRect().width;
            var divHeight = d3.select(contentDiv).node().getBoundingClientRect().height;
            //
            divWidth = (divWidth <= 0) ? 430 : divWidth;
            divHeight = (divHeight <= 0) ? 200 : divHeight - 10;
            //                if (divWidth === 0)
            //                return;
            //            var divWidth = 430;
            //            var divHeight = 200;
            var init = d3.select(contentDiv).select("svg")[0][0] ? 0 : 1;
            var margin = { top: 20, right: 20, bottom: 30, left: 40 };
            var width = divWidth - margin.left - margin.right - 80;
            var height = divHeight - margin.top - margin.bottom - 20;
            var parseDate = d3.time.format.utc("%Y-%m-%d %H:%M:%S").parse;
            var x = d3.time.scale()
                .range([0, width]);
            var y = d3.scale.linear()
                .range([height, 0]);
            var xAxis = d3.svg.axis()
                .tickFormat(d3.time.format('%H:%M'))
                .scale(x)
                .orient("bottom");
            var yAxis = d3.svg.axis()
                .scale(y)
                .ticks(5)
                .orient("left");
            var line = d3.svg.line()
                .x(function(d) {
                    return x(d.date);
                })
                .y(function(d) {
                    return y(d.count);
                });
            if (init) {
                var svg = d3.select(contentDiv).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + (margin.left + 5) + "," + margin.top + ")");
                svg.append("text")
                    .text($translate.instant('map.loading_statistic'))
                    .attr("class", "loadText")
                    .style("font-size", "1.5em")
                    .attr("y", 40)
                    .style("fill", "white");
                d3.json("utils/statistic").header("Content-Type", "application/x-www-form-urlencoded")
                    .post('{"start":"' + start + '","stop":"' + stop + '","bound":' + JSON.stringify($scope.map.filter.bounds) + '}', function(error, data) {
                        if (error)
                            throw error;
                        var hitdata = data.hitstatistic;
                        var countTotal = data.countTotal;
                        var countCloud = data.countCloud;
                        var countPos = data.countPos;
                        var countNeg = data.countNeg;
                        data = data.statistic;
                        data.forEach(function(d) {
                            d.date = parseDate(d.date);
                            d.count = +d.count;
                        });
                        hitdata.forEach(function(d) {
                            d.date = parseDate(d.date);
                            d.count = +d.count;
                        });
                        x.domain(d3.extent(data, function(d) {
                            return d.date;
                        }));
                        y.domain([0, d3.max(data, function(d) {
                            return d.count;
                        })]);
                        xAxis.tickValues([data[0].date, data[10].date, data[20].date, data[30].date, data[40].date, data[50].date, data[60].date]);
                        svg.select(".loadText").remove();
                        svg.append("g")
                            .attr("class", "x axis")
                            .attr("transform", "translate(0," + height + ")")
                            .call(xAxis);
                        svg.append("g")
                            .attr("class", "y axis")
                            .call(yAxis);
                        svg.append("text")
                            .attr("class", "perMin")
                            .attr("fill", "white")
                            .attr("y", -10)
                            .attr("x", 0)
                            .attr("dy", ".35em")
                            .attr("text-anchor", "middle")
                            .text($translate.instant('map.per_minute'));
                        svg.append("text")
                            .attr("class", "statWindow")
                            .attr("fill", "white")
                            .attr("y", -10)
                            .attr("x", 80)
                            .attr("dy", ".35em")
                            .attr("text-anchor", "right")
                            .text($translate.instant('map.statistic' + UserService.statistic.window + UserService.statistic.area));
                        svg.append("path")
                            .datum(data)
                            .attr("class", "line")
                            .attr("d", line);
                        svg.append("path")
                            .datum(hitdata)
                            .attr("class", "line2")
                            .attr("d", line);
                        var color = ["purple", "darkblue", "lightblue", "green", "yellow", "red"]; //colors for color scale
                        var scale = svg.append("g").attr("transform", "translate(0,20)")
                            .selectAll("rect").data(color).enter() //add color scale
                            .append("rect")
                            .attr("x", 0).attr("y", 130)
                            .attr("width", (width / 6)).attr("height", 10)
                            .attr("fill", function(d) {
                                return d;
                            })
                            .attr("transform", function(d, idx) {
                                return "translate(" + (idx * width / 6) + ", 0)";
                            });
                        var counter = d3.select(contentDiv).append("div").text(dauer + ' | ∑ ' + countTotal + ' | ■ ' + countNeg + ' | ◉ ' + countPos + ' | ▲ ' + countCloud).attr("style", 'margin-left:2em');
                    });
            } else {
                //  d3.select(contentDiv).transition().select(".line").duration(750).attr("d", line([0, 0, 0]));  // what is this ??
                //  d3.select(contentDiv).transition().select(".line2").duration(750).attr("d", line([0, 0, 0])); // and what purpose does it have ??
                d3.select(contentDiv).transition().select("div").text($translate.instant('map.loading_statistic')).attr("style", 'margin-left:5em;font-weight:bold;');
                d3.json("utils/statistic").header("Content-Type", "application/x-www-form-urlencoded")
                    .post('{"start":"' + start + '","stop":"' + stop + '","bound":' + JSON.stringify($scope.map.filter.bounds) + '}', function(error, data) {
                        if (error)
                            throw error;
                        var hitdata = data.hitstatistic;
                        var countTotal = data.countTotal;
                        var countCloud = data.countCloud;
                        var countPos = data.countPos;
                        var countNeg = data.countNeg;
                        data = data.statistic;
                        data.forEach(function(d) {
                            d.date = parseDate(d.date);
                            d.count = +d.count;
                        });
                        hitdata.forEach(function(d) {
                            d.date = parseDate(d.date);
                            d.count = +d.count;
                        });
                        // Scale the range of the data again
                        x.domain(d3.extent(data, function(d) {
                            return d.date;
                        }));
                        y.domain([0, d3.max(data, function(d) {
                            return d.count;
                        })]);
                        xAxis.tickValues([data[0].date, data[10].date, data[20].date, data[30].date, data[40].date, data[50].date, data[60].date]);
                        // Select the section we want to apply our changes to
                        var svg = d3.select(contentDiv).transition();
                        // Make the changes
                        svg.select(".line") // change the line
                            .duration(750)
                            .attr("d", line(data));
                        svg.select(".line2") // change the line
                            .duration(750)
                            .attr("d", line(hitdata));
                        svg.select(".x.axis") // change the x axis
                            .duration(750)
                            .call(xAxis);
                        svg.select(".y.axis") // change the y axis
                            .duration(750)
                            .call(yAxis);
                        svg.select("div").text(dauer + ' | ∑ ' + countTotal + ' | ■ ' + countNeg + ' | ◉ ' + countPos + ' | ▲ ' + countCloud).attr("style", 'margin-left:2em');
                    });
            }
        };
        $scope.updateLayer = function(name, visible, filter) {
            if ($scope.map.getLayersByName(name)[0].params) { // only usable for WMS Layers with Params
                var isBlidslayer = (/session/g.test($scope.map.getLayersByName(name)[0].params.LAYERS)); // only Blidslayers contain the session
                if (isBlidslayer && UserService.getLiveBlids() === "t" && $scope.archivActiv === false) {
                    if ($scope.liveLayer.isActive() && !visible) {
                        $scope.liveLayer.deactivate();
                    } else if (visible) {
                        $scope.liveLayer.activate();
                    }
                    $scope.map.getLayersByName("LiveLayer")[0].setVisibility(visible);
                }
                if (filter) {
                    var time = $scope.map.slider.options.max - $scope.map.slider.element.value;
                    var param = "zeit1:" + time + ";";
                    $scope.map.getLayersByName(name)[0].mergeNewParams({ cql_filter: filter }); // update Filter for Layer

                    if (isBlidslayer && UserService.getLiveBlids() === "t") {
                        $scope.liveLayer.setFilter(filter);
                    }
                    $scope.map.getLayersByName(name)[0].redraw(true);
                } else {
                    delete $scope.map.getLayersByName(name)[0].params.CQL_FILTER; // delete Filter
                    $scope.map.getLayersByName(name)[0].redraw(true);
                }
                //if ($scope.map.getLayersByName(name)[0].options.visibility === visible)
            }
            if ($scope.map.getLayersByName(name)[0].visibility !== visible) { // If visibility changes, start / stop reload timer
                $scope.map.getLayersByName(name)[0].setVisibility(visible); // Set Layer Visible and activate Feature Info
                if (visible) {
                    $scope.map.startReloadTimer(name);
                    $scope.map.raiseLayer($scope.map.getLayersByName(name)[0], 15);
                    $scope.map.raiseLayer($scope.map.getLayersBy("blidslayer", "t")[0], 15);
                } else {
                    $scope.map.stopReloadTimer(name);
                }
            }

            if (typeof $scope.map.getControlsBy("title", name).length > 0) {
                if (visible && $scope.map.getControlsBy("title", name)[0] && !($scope.map.getControlsBy("title", name)[0].active)) {
                    $scope.map.getControlsBy("title", name)[0].activate();
                }
                if (!visible && $scope.map.getControlsBy("title", name)[0].active) { // Set Layer Invisible and deactivate Feature Info
                    $scope.map.getControlsBy("title", name)[0].deactivate();
                }
            }
        };
        $scope.updateTime = function(time, offset) {
            var zeit1 = time + offset;
            //            var zeit2 = offset + 5 / 6 * time;
            //            var zeit3 = offset + 2 / 3 * time;
            //            var zeit4 = offset + 1 / 2 * time;
            //            var zeit5 = offset + 1 / 3 * time;
            //            var zeit6 = offset + 1 / 6 * time;
            var zeit7 = offset;
            //var env = 'zeit1:' + zeit1 + ';zeit2:' + zeit2 + ';zeit3:' + zeit3 + ';zeit4:' + zeit4 + ';zeit5:' + zeit5 + ';zeit6:' + zeit6;
            var param = "zeit1:" + zeit1 + ";zeit7:" + zeit7 + ";";
            $scope.map.viewparam.time = param;
            $scope.map.filter.time.zeit1 = zeit1;
            $scope.map.filter.time.zeit7 = zeit7;
            param = param + $scope.map.viewparam.bounds;
            for (var i = 0; i < $scope.map.layers.length; i++) {
                if ($scope.map.layers[i].params && $scope.map.layers[i].options.timedependent === 't') { //if layer is time dependent (timedependent = t)
                    $scope.map.layers[i].mergeNewParams({ viewparams: param }); //, env: env}); // update VIEWPARAMS zeit1 and ENV all times for coloring
                    var name = $scope.map.layers[i].name;
                    if ($scope.map.getControlsBy("title", name).length)
                        $scope.map.getControlsBy("title", name)[0].vendorParams.viewparams = $scope.map.viewparam.time;
                    $scope.map.layers[i].redraw(true);
                }
            }
            $scope.drawStatistic('#statisticDiv', zeit1, zeit7);
        };

        function loadStart() {
            $scope.map.numLoading++;
            var layerText = $scope.map.numLoading > 1 ? $translate.instant('layers') : $translate.instant('map.layer');
            document.getElementById("loaderGif").innerHTML = $translate.instant('map.loading') + " " + $scope.map.numLoading + " " + layerText;
            if ($scope.map.numLoading) {
                document.getElementById("loaderGif").style.visibility = "visible";
            }
        }

        function loadEnd() {
            $scope.map.numLoading--;
            var layerText = $scope.map.numLoading > 1 ? $translate.instant('layers') : $translate.instant('map.layer');
            document.getElementById("loaderGif").innerHTML = $translate.instant('map.loading') + " " + $scope.map.numLoading + " " + layerText;
            if (!$scope.map.numLoading) {
                document.getElementById("loaderGif").style.visibility = "hidden";
            }
        }

        function setHtml(content, xy, layer) {
            var i, nopopup, htmlcontent, name = "<div>";
            var path = "";
            var htmlMobile = "<div id='htmlMobil'>";
            var template = layer.popupTemplate;

            for (i = 0; i < content.length; i++) {
                if (i > 0) {
                    name += "<hr style='background-color:black;height:1px'>";
                }
                var point = OpenLayers.Projection.transform(new OpenLayers.Geometry.Point(content[i].geometry.coordinates[0], content[i].geometry.coordinates[1]), "EPSG:900913", "EPSG:4326");

                var zeit;
                if (content[i].id.indexOf('blitzemerge') === 0) { // Blitzvorhersagecheck
                    zeit = ($filter('date')(new Date(content[i].properties.zeit), 'dd.MM.yyyy HH:mm:ss.sss')); // formatiere Zeitstempel in local time
                    var castZeit = ($filter('date')(new Date(content[i].properties.input_time), 'HH:mm')); // formatiere Zeitstempel in local time
                    name += zeit + " / " + point.x.toFixed(3) + "° " + point.y.toFixed(3) + "° ";
                    name += "| Prognose Time: " + castZeit;
                } else if (content[i].id.indexOf('sessionCurrent') === 0) { // Blitzlayer (Live und Archiv)
                    zeit = ($filter('date')(new Date(content[i].properties.zeit), 'dd.MM.yyyy HH:mm:ss.sss')); // formatiere Zeitstempel in local time
                    name += zeit + " / " + point.x.toFixed(3) + "° " + point.y.toFixed(3) + "° / ";
                    name += content[i].properties.typ === 1 ? "Ground " : "";
                    name += content[i].properties.typ === 4 ? "Cloud " : "";
                    name += content[i].properties.strom + " kA";
                } else if (content[i].id.indexOf('alarmgebiete') === 0 || content[i].properties.hasOwnProperty('gebiet') && content[i].properties.hasOwnProperty('blitzanzahl')) {
                    name += "<b>" + content[i].properties.gebiet + "</b><br/> "+$translate.instant('map.strokecount') + " " + content[i].properties.blitzanzahl;
                } else if (content[i].id.indexOf('wka') === 0) { // WindPower !!!
                    if (content[i].properties.energietyp === null)
                        content[i].properties.energietyp = "";
                    name += "<b>" + content[i].properties.name + "</b> - Typ: " + content[i].properties.energietyp +
                        "<br/>Betreiber: " + content[i].properties.betreiber +
                        "<br/>Leistung: " + content[i].properties.leistung + " - Inbetriebnahme: " + content[i].properties.tatsaechlich +
                        "<br/>Hersteller: " + content[i].properties.hersteller + " - Typ: " + content[i].properties.typ +
                        "<br/>Höhe: " + content[i].properties.nabenhoehe + "m" +
                        "<br/>Rotordurchm.: " + content[i].properties.rotordurchmesser + "m";
                } else if (content[i].id.indexOf('sensorstatus') === 0) { // Sensoren
                    var status;
                    switch (content[i].properties.status_char) {
                        case 'U':
                            status = "Working";
                            break;
                        case 'D':
                            status = "Down";
                            break;
                        default:
                            status = "Noisy";
                    }
                    name += "<b>" + content[i].properties.description + "</b> - " + status;
                } else if (content[i].id.indexOf('futureblids') === 0) { // Blitzvorhersage
                    zeit = ($filter('date')(new Date(content[i].properties.zeit), 'dd.MM.yyyy HH:mm:ss.sss')); // formatiere Zeitstempel in local time
                    name += zeit + " / " + point.x.toFixed(3) + "° " + point.y.toFixed(3) + "° ";
                } else if (content[i].id.indexOf('tcom_asb') === 0) {
                    name += content[i].properties.onkz + " - " + content[i].properties.asb + "<br/>" + content[i].properties.on_name;
                } else if (template != "") {
                    var html = new Function("data", template);
                    name += html(content[i].properties);
                } else {
                    nopopup = true;
                    break;
                }
                if (window.mobileAndTabletcheck() && i < 2) {
                    htmlMobile = name;
                }
            }
            htmlcontent += "</div>";
            name += "</div>";
            if (window.mobileAndTabletcheck() && content.length > 2) {
                var moreList = name;
                var more = content.length - 2;
                var showMore = function() {
                    var parent = document.getElementById('LayerMenu').parentNode;
                    var list = getMobileInfoList(moreList);
                    parent.appendChild(list);
                };
                htmlMobile += '<br><input id="showMoreBtn" value="show ' + more + ' more" type="button"/>';
                htmlMobile += "</div>";
                name = htmlMobile;
                if (!nopopup) {
                    var width = (document.getElementById('map').offsetWidth * 0.5);
                    var height = (document.getElementById('map').offsetHeight * 0.2);
                    var popup = new OpenLayers.Popup.FramedCloud("info", $scope.map.getLonLatFromPixel(xy),
                        new OpenLayers.Size(width, height), name, null, true, onFeatureUnselect($scope.map));
                    //                    window.open('','_blank','toolbar=0,location=0,menubar=0,scrollbars=1,resizable=1,height=600,width=350').document.write(htmlcontent);
                    popup.autoSize = true;
                    $scope.map.addPopup(popup);
                    document.getElementById("showMoreBtn").addEventListener("click", showMore);
                }


            } else {
                if (!nopopup) {
                    $scope.map.addPopup(new OpenLayers.Popup.FramedCloud("info", $scope.map.getLonLatFromPixel(xy),
                        new OpenLayers.Size(300, 110), name, null, true));
                    //                    window.open('','_blank','toolbar=0,location=0,menubar=0,scrollbars=1,resizable=1,height=600,width=350').document.write(htmlcontent);
                }
            }
        }

        function saveMapChanges() {
            var boundleft = Math.max($scope.map.getExtent().left, $scope.map.restrictedExtent.left);
            var boundright = Math.min($scope.map.getExtent().right, $scope.map.restrictedExtent.right);
            if (boundleft > boundright) {
                var temp = boundleft;
                boundleft = boundright;
                boundright = temp;
            }
            $scope.map.filter.bounds.left = boundleft;
            $scope.map.filter.bounds.right = boundright;
            $scope.map.filter.bounds.top = Math.min($scope.map.getExtent().top, $scope.map.restrictedExtent.top);
            $scope.map.filter.bounds.bottom = Math.max($scope.map.getExtent().bottom, $scope.map.restrictedExtent.bottom);
            var bounds = "left:" + boundleft + ";right:" + boundright + ";top:" + $scope.map.filter.bounds.top + ";bottom:" + $scope.map.filter.bounds.bottom + ";";
            $scope.map.viewparam.bounds = bounds;
            bounds = $scope.map.viewparam.time + bounds;
            for (var key in $scope.map.layers) {
                if (/WMS/g.test($scope.map.layers[key].CLASS_NAME) && $scope.map.layers[key].params && // if WMS Layer and timedependent = true
                    ($scope.map.layers[key].options.timedependent)) {
                    $scope.map.layers[key].mergeNewParams({ viewparams: bounds });
                }
            }
        }

        function updateMapChanges() {
            $scope.map.stopReloadTimer();
            saveMapChanges();
            if ($scope.map.archiveActive) {
                if (window.mobileAndTabletcheck() && $scope.archivOpened) {
                    return;
                }
                $scope.archiveClick(); //recalculate time borders for the flash layer, which also redraws statistic
            } else {
                if (UserService.statistic.window == "t") { // only reload statistic if it depends on window
                    $scope.drawStatistic('#statisticDiv', $scope.map.filter.time.zeit1, $scope.map.filter.time.zeit7);
                }
            }
            $scope.map.startReloadTimer();
        }

        OpenLayers.ProxyHost = "/cgi-bin/proxy.cgi?url=";
        var resolution = [];
        var res = 156543,
            zoomOffset = 0;
        var zoomMax = UserService.getZoom().max,
            zoomMin = UserService.getZoom().min;
        var numZoom = zoomMax - zoomMin;
        for (var i = 0; i <= zoomMax; i++) {
            if (i >= zoomMin)
                resolution.push(res);
            else
                zoomOffset++;
            res /= 2;
        }
        $scope.map = new OpenLayers.Map('map', {
            projection: new OpenLayers.Projection('EPSG:900913'), //3857
            units: 'm',
            layers: [
                new OpenLayers.Layer.WMS("OpenStreetMap",
                    "https://osm.blids.net/service/wms", {
                        //"https://gwc.blids.de/geowebcache/service/wms", {
                        layers: 'osm:osm_all',
                        format: 'image/png'
                    }, {
                        zoomOffset: zoomOffset,
                        resolutions: resolution,
                        attribution: '© <a href="http://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>'
                    }),
                //                 new OpenLayers.Layer.WMS( "Forgel Maps",
                //                    "http://192.168.2.64:1337/gwc/wms",
                //                    {layers: 'osm:osm_all',
                //                    format: 'image/png',
                //                    access_token: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IkJMSURTIEdFT0NPREVSIEFQSSIsImVtYWlsIjoiZGF2aWRAc2llbWVucy5kZSIsImV4cCI6MTQ2MjQ2MDUxOCwic2NvcGUiOlsiZ2VvY29kZXIuUXVlcnkiXSwidXNlcl9pZCI6ImdubHB0aCJ9.nJL-GRSLa4ary7ZauuVeQZjgL5_xNtkkJX2b3MGbgm-oauKrWiRcibhoLthV7pyUvkuAWJOoyoG4LGCkVe2KiGpa8N0bxKQhFQPq0ZhkMxkk_KbOw7InDF_i5i-f4NXaw1SgiHEFR9AtQK5qSz5Njhx78z5nN-vE8j2i9NbaeKk'} ),
                new OpenLayers.Layer.Google(
                    "Google Streets", // the default
                    {
                        zoomOffset: zoomOffset,
                        resolutions: resolution,
                        minZoomLevel: zoomMin,
                        maxZoomLevel: zoomMax,
                        wrapDateLine: false
                    }
                ),
                new OpenLayers.Layer.Google(
                    "Google Hybrid", {
                        zoomOffset: zoomOffset,
                        resolutions: resolution,
                        minZoomLevel: zoomMin,
                        maxZoomLevel: zoomMax,
                        wrapDateLine: false,
                        type: google.maps.MapTypeId.HYBRID
                    }
                ),
                //                new OpenLayers.Layer.OSM("OpenStreetMap",
                //                        ["https://a.tile.openstreetmap.org/${z}/${x}/${y}.png",
                //                            "https://b.tile.openstreetmap.org/${z}/${x}/${y}.png",
                //                            "https://c.tile.openstreetmap.org/${z}/${x}/${y}.png" ]),
                new OpenLayers.Layer.Google(
                    "Google Simple", // the default
                    {
                        zoomOffset: zoomOffset,
                        resolutions: resolution,
                        minZoomLevel: zoomMin,
                        maxZoomLevel: zoomMax,
                        wrapDateLine: false,
                        type: 'styled'
                    }
                )
            ]
        });
        if (UserService.getLiveBlids() === "t") {
            $scope.archivActiv = false;
            //if tlp{ "/event"}else{"/eventGLD"}
            $scope.liveLayer = new LiveLayerModule($scope.map, true);
            $scope.liveLayer.addLayerToMap();
            $scope.liveLayer.deleteOnMove = true;
            UserService.setLiveLayer($scope.liveLayer);
            $scope.map.events.register('movestart', $scope.map, function() {
                if ($scope.liveLayer.getLayer().visibility === true && $scope.liveLayer.deleteOnMove) {
                    $scope.liveLayer.deactivateTemp();
                }
            });
            $scope.map.events.register('moveend', $scope.map, function() {
                if ($scope.liveLayer.getLayer().visibility === true && $scope.liveLayer.deleteOnMove && $scope.archivActiv === false) {
                    $scope.liveLayer.clearLayer();
                    $scope.liveLayer.activate();
                    $scope.liveLayer.setClearInterval();
                }
            });
        }
        $scope.map.restrictedExtent = UserService.getData().bounds;
        $scope.map.setCenter(UserService.getCenter(), 0);
        $scope.map.maxExtent = UserService.getData().bounds;
        $scope.map.events.register('moveend', $scope.map, updateMapChanges);
        $scope.map.viewparam = {};
        $scope.map.filter = {};
        $scope.map.filter.time = {};
        $scope.map.filter.bounds = {};
        saveMapChanges();
        var stylez = [{ "stylers": [{ "visibility": "off" }] },
            {
                "featureType": "administrative.country",
                "elementType": "geometry",
                "stylers": [{ "visibility": "on" },
                    { "color": "#303030" }, { "weight": 1 }
                ]
            },
            {
                "featureType": "landscape",
                "stylers": [{ "visibility": "on" },
                    { "color": "#ffffff" }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{ "visibility": "on" }]
            }, {}
        ];
        $scope.map.getLayersByName("Google Simple")[0].mapObject.mapTypes.set('styled', new google.maps.StyledMapType(stylez, { 'name': "Styled Map" }));

        $scope.map.numLoading = 0;
        var layers = UserService.getData().layers; //Get Layers From UserService/Login
        $scope.map.reloadLayers = [];
        var layerUrl = "";
        for (var key in layers) {

            // ADD LAYER
            if (layers[key].reload_time > -1) {
                $scope.map.reloadLayers.push({
                    "name": layers[key].displayname,
                    "geoname": layers[key].geoname,
                    "timeout": layers[key].reload_time,
                    "reloadStatistic": (layers[key].geoname === "blitze:sessionCurrent")
                });
                if (layers[key].geoname === "blitze:sessionCurrent" && UserService.getLiveBlids() === "t") {
                    $scope.liveLayer.setClearInterval(layers[key].reload_time * 60);
                }
            }
            layers[key].parameters = {};
            layers[key].options = { "singleTile": false };
            layers[key].parameters.layers = layers[key].geoname;
            layers[key].parameters.transparent = true;
            layers[key].options.displayInLayerSwitcher = false;
            layers[key].options.visibility = (layers[key].checked_onlogin === 't');
            layers[key].options.singleTile = true;
            layers[key].options.buffer = 20;
            layers[key].options.timedependent = layers[key].timedependent;
            if (layers[key].externer_parameter) {
                var param = layers[key].externer_parameter.split(",");
                for (var val in param) {
                    if (param[val]) {
                        var temp = param[val].match(/('[^']+'|[^':]+)/g);
                        layers[key].parameters[temp[0]] = temp[1].substr(1, temp[1].length - 2);
                    }
                }
            }
            layerUrl = layers[key].externer_server ? layers[key].externer_server + "?" : "/wms?source_url="; // use external URL if existing
            var keyLayer = new OpenLayers.Layer.WMS(layers[key].displayname, layerUrl, layers[key].parameters, layers[key].options);
            //            var keyLayer = new OpenLayers.Layer.WMS(layers[key].displayname, "https://maps.industry.siemens.de/geoserver/wms", layers[key].parameters, layers[key].options);
            if (layers[key].geoname === "blitze:sessionCurrent") {
                keyLayer.blidslayer = "t"; // mark Blidslayer to be searchable with getLayersBy("blidslayer",TRUE)
            }
            keyLayer.popupTemplate = layers[key].popup_template ? layers[key].popup_template : "";
            $scope.map.addLayer(keyLayer);
            keyLayer.events.on({
                "loadstart": loadStart,
                "loadend": loadEnd
            });
            // ADD GetFeatureInfo without Filter (is added later when Layer is updated)
            if (layers[key].feature_info === "t") {
                var info = new OpenLayers.Control.WMSGetFeatureInfo({
                    queryVisible: true,
                    layers: [keyLayer],
                    output: "features",
                    infoFormat: 'application/json',
                    format: new OpenLayers.Format.JSON(),
                    title: layers[key].displayname,
                    eventListeners: {
                        getfeatureinfo: function(event) {
                            //                            console.log(event.features.features.length);
                            if (event.features.features.length) {
                                setHtml(event.features.features, event.xy, event.object.layers[0]);
                            }
                        },
                        beforegetfeatureinfo: function(event) { // Get CQL Filter from layer and set it for GetFeatureInfo
                            var name = event.object.title;
                            var cql = $scope.map.getLayersByName(name)[0].params.CQL_FILTER;
                            event.object.vendorParams.cql_filter = cql;
                        }
                    }
                });
                info.vendorParams.buffer = 20; //6
                $scope.map.addControl(info);
                info.activate();
            }
        }
        if (true) {
            var bound = UserService.getData().bounds.clone().transform('EPSG:3857', 'EPSG:4326');
            //  bounds(-20 30 30 70)

            var outer_left = bound.left - 10;
            var outer_right = bound.right + 10;
            var outer_top = bound.top + 10;
            var outer_bottom = bound.bottom - 10;

            if (bound.left < bound.right &&
                bound.left > -70 &&
                bound.right < 110 &&
                bound.bottom < bound.top &&
                bound.bottom > -25 &&
                bound.top < 75) {
                outer_left = -80;
                outer_right = 100;
                outer_bottom = -35;
                outer_top = 79;
                if ((bound.right - bound.left) < 4) {
                    outer_left = bound.left - 20;
                    outer_right = bound.right + 20;
                }
            }

            var box_extents = [
                [outer_left, bound.top, bound.left, bound.bottom], //left
                [bound.right, bound.top, outer_right, bound.bottom], //right
                [outer_left, outer_top, outer_right, bound.top], //top
                [outer_left, bound.bottom, outer_right, outer_bottom] //bottom
            ];

            var boxarea = new OpenLayers.Layer.Vector($filter('translate')('map.userarea'), {
                style: {
                    strokeColor: '#ff0000',
                    strokeWidth: 0,
                    fillColor: '#000000',
                    fillOpacity: 0.2,
                    fill: true,
                    strokeOpacity: 0.5
                },
                displayInLayerSwitcher: false
            });
            for (i = 0; i < box_extents.length; i++) {
                var boundary = OpenLayers.Bounds.fromArray(box_extents[i]).transform('EPSG:4326', 'EPSG:3857');
                var box = new OpenLayers.Feature.Vector(boundary.toGeometry());
                boxarea.addFeatures(box);
            }
            $scope.map.addLayer(boxarea);
        }

        $scope.map.startReloadTimer = function(name) {
            // Add Timers for Layer Reload
            if ($scope.archivActiv || $scope.map.archiveActive)
                return;
            for (var key in $scope.map.reloadLayers) {
                if (!name || name === $scope.map.reloadLayers[key].name) {
                    if ($scope.map.getLayersByName($scope.map.reloadLayers[key].name)[0].visibility === true) { // only set Timer if layer is visible
                        $scope.map.getLayersByName($scope.map.reloadLayers[key].name)[0].reloadStatistic = $scope.map.reloadLayers[key].reloadStatistic;
                        //console.debug("START INTERVAL " + $scope.map.reloadLayers[key].name);
                        $scope.map.reloadLayers[key].timer = $interval(function(x) {
                            console.debug("Reload Layer " + x);
                            $scope.map.getLayersByName(x)[0].redraw(true);
                            if ($scope.map.getLayersByName(x)[0].reloadStatistic) {
                                $scope.drawStatistic('#statisticDiv', $scope.map.filter.time.zeit1, $scope.map.filter.time.zeit7); // refresh Statistic Window if Blitzlayer is reloaded
                            }
                        }, $scope.map.reloadLayers[key].timeout * 60 * 1000, 0, true, $scope.map.reloadLayers[key].name);
                    }
                }
            }
        };
        $scope.map.stopReloadTimer = function(name) {
            // Stop Timers for Layer Reload
            for (var key in $scope.map.reloadLayers) {
                if (!name || name === $scope.map.reloadLayers[key].name) { // Only cancel Timer of layer "name" if "name" is set
                    if ($scope.map.reloadLayers[key].timer) { // Cancel Reload Timer if it exist
                        $interval.cancel($scope.map.reloadLayers[key].timer);
                        //console.debug($scope.map.reloadLayers[key].name + "STOPPED");
                    }
                }
            }
        };
        //Enable LayerSwitcher, will be gone when LayerMenu is working
        //$scope.map.addControl(new OpenLayers.Control.LayerSwitcher());

        //set different base layer for special theme groups (Bastel Wastel / DIRTY HACK)
        if (UserService.getTheme() == 2) $scope.map.setBaseLayer($scope.map.layers[1]);

        $scope.map.addControl(new OpenLayers.Control.MousePosition({ displayProjection: new OpenLayers.Projection("EPSG:4326") }));
        //Add Scale
        $scope.map.addControl(new OpenLayers.Control.Scale());

        //Enable Zoombar
        //$scope.map.addControl(new OpenLayers.Control.PanZoomBar());
        //Remove Google Zoombar
        $scope.map.removeControl($scope.map.getControlsByClass('OpenLayers.Control.Zoom')[0]);

        // LiveAlarmArea Handling
        // Wird nur "aktiviert" wenn es einen Layer gibt der "Alarmgebiete" heißt.
        if (false && ($scope.map.getLayersByName("Alarmgebiete")[0] !== undefined || $scope.map.getLayersByName("ONKZ")[0] !== undefined)) {    //bis auf weiteres stillgelegt
            var alarmAreaEventSource = new EventSource("/alarmevent");
            var wkt = new OpenLayers.Format.WKT();
            var alarmAreas = $scope.map.getLayersByName("Alarmgebiete")[0] ? $scope.map.getLayersByName("Alarmgebiete")[0] : $scope.map.getLayersByName("ONKZ")[0];
            var alarmAreasLive = new OpenLayers.Layer.Vector(
                "Live Alarmgebiet", {
                    preFeatureInsert: function(feature) {
                        feature.geometry.transform(
                            new OpenLayers.Projection("EPSG:4326"),
                            new OpenLayers.Projection("EPSG:900913")
                        );
                    },
                    styleMap: new OpenLayers.StyleMap({
                        fillColor: "red",
                        strokeColor: "green",
                        strokeWidth: "1px",
                        fillOpacity: 0.5
                    }),
                    displayInLayerSwitcher: false
                }
            );
            $scope.map.addLayer(alarmAreasLive);
            var selectControl = new OpenLayers.Control.SelectFeature(
                alarmAreasLive, {
                    callbacks: {
                        over: function(evt) {
                            var popup = new OpenLayers.Popup.FramedCloud(
                                "popup",
                                OpenLayers.LonLat.fromString(
                                    evt.geometry.getCentroid().toShortString()
                                ),
                                null,
                                "<div><b>" +
                                evt.attributes.name +
                                "</b><br>Anzahl Blitze: " +
                                evt.attributes.count +
                                " </div>",
                                null,
                                true
                            );
                            evt.popup = popup;
                            $scope.map.addPopup(popup);
                        },
                        out: function(evt) {
                            $scope.map.removePopup(evt.popup);
                            evt.popup.destroy();
                            evt.popup = null;
                        }
                    },
                    eventListeners: {
                        featurehighlighted: function(feat) {
                            // Note: Alternative to out
                        },
                        featureunhighlighted: function(feat) {
                            // Note: Alternative to out
                        }
                    }
                }
            );
            $scope.map.addControls([selectControl]);
            selectControl.activate();

            function handleLiveAlarmAreaNotifiction(event) {
                var data = JSON.parse(event.data);
                if (data.type === "Alarm") {
                    var feature = wkt.read(data.geom);
                    feature.attributes.id = data.id;
                    feature.attributes.name = data.name;
                    feature.attributes.count = data.count;
                    alarmAreasLive.addFeatures([feature]);
                } else if (data.type === "Entwarnung") {
                    alarmAreasLive.removeFeatures(alarmAreasLive.getFeaturesByAttribute("id", data.id));
                }
            }

            alarmAreaEventSource.addEventListener("alarm", function(
                event
            ) {
                handleLiveAlarmAreaNotifiction(event);
            });

            alarmAreas.events.register(
                "visibilitychanged",
                this,
                function(e) {
                    if (alarmAreas.visibility) {
                        alarmAreaEventSource = new EventSource("/alarmevent");
                        $scope.map.addLayer(alarmAreasLive);
                        alarmAreaEventSource.addEventListener("alarm", function(
                            event
                        ) {
                            handleLiveAlarmAreaNotifiction(event);
                        });
                    } else {
                        alarmAreaEventSource.removeEventListener(
                            "alarm",
                            function(event) {
                                handleLiveAlarmAreaNotifiction(event);
                            }
                        );
                        alarmAreaEventSource.close();
                        alarmAreasLive.removeAllFeatures();
                        $scope.map.removeLayer(alarmAreasLive);
                    }
                }
            );
        }

        $scope.map.startReloadTimer();

        // ************* ENTFERNUNGSMESSUNG *******************************************************
        $scope.measureActive = false;
        var sketchSymbolizers = { // Style für Punkt und Linien ändern
            "Point": {
                pointRadius: 4,
                graphicName: "circle",
                fillColor: "white",
                fillOpacity: 1,
                strokeWidth: 1,
                strokeOpacity: 1,
                strokeColor: "#333333"
            },
            "Line": {
                strokeWidth: 3,
                strokeOpacity: 1,
                strokeColor: "#666666",
                strokeDashstyle: "dash"
            }
        };
        var style = new OpenLayers.Style();
        style.addRules([
            new OpenLayers.Rule({ symbolizer: sketchSymbolizers })
        ]);
        var styleMap = new OpenLayers.StyleMap({ "default": style });
        // allow testing of specific renderers via "?renderer=Canvas", etc
        var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
        renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
        var measureControls = { //Messtool anlegen
            line: new OpenLayers.Control.Measure(
                OpenLayers.Handler.Path, {
                    persist: true,
                    geodesic: true,
                    handlerOptions: {
                        layerOptions: {
                            renderers: renderer,
                            styleMap: styleMap
                        }
                    }
                }
            )
        };
        var control; // Messtool zu Karte hinzufügen
        for (key in measureControls) {
            control = measureControls[key];
            control.events.on({
                "measure": handleMeasurements,
                "measurepartial": handleMeasurements
            });
            $scope.map.addControl(control);
        }

        function killPopup() { //bereits offene Popups schließen
            if ($scope.map.popups) {
                for (var i = 0; i < $scope.map.popups.length; i++) {
                    if ($scope.map.popups[i].contentDiv.id === 'distance_contentDiv')
                        $scope.map.popups[i].destroy();
                }
            }

        }

        function handleMeasurements(event) { //gemessene Entfernung ausgeben
            var geometry = event.geometry;
            var units = event.units;
            var measure = event.measure;
            var out = "" + measure.toFixed() + " " + units;
            var numPoints = geometry.components.length;
            var endPoint = geometry.components[numPoints - 1];
            var LonLat = new OpenLayers.LonLat(endPoint.x, endPoint.y);
            killPopup();
            if (measure !== 0) {
                $scope.map.addPopup(new OpenLayers.Popup.FramedCloud("distance", LonLat, null, out, null, true)); //nur Popup wenn gemessene Strecke > 0
            }


        }

        var panel = new OpenLayers.Control.Panel({ // Control Panel, welches Button für Messtool enthält
            displayClass: 'Panel',
            defaultControl: OpenLayers.Control.button,
            // override createControlMarkup to create actual buttons
            // including texts wrapped into span elements.
            createControlMarkup: function(control) {
                var button = document.createElement('button'),
                    span = document.createElement('span');
                if (control.text) {
                    span.innerHTML = control.text;
                }
                return button;
            }
        });
        $scope.map.addControl(panel);

        function button_active() {
            control.activate();
        }

        function button_deactive() {
            control.deactivate();
            killPopup();
        }

        function MeasureMobileClick() {
            if ($scope.measureActive) {
                $scope.measureActive = false;
                measureButton.className = "MeasureBtnInActive ctrlBtn";
                button_deactive();
            } else {
                $scope.measureActive = true;
                measureButton.className = "MeasureBtnActive ctrlBtn";
                button_active();
            }
        }
        if (window.mobileAndTabletcheck() === true) {
            var measureButton = document.createElement("button");
            measureButton.id = "MeasureButton";
            measureButton.className = "MeasureBtnInActive ctrlBtn";
            measureButton.title = $filter('translate')('map.measure');
            if ('ontouchstart' in document.documentElement) {
                measureButton.ontouchstart = MeasureMobileClick;
            } else {
                measureButton.onclick = MeasureMobileClick;
            }
            document.getElementById("map").parentNode.appendChild(measureButton);
        } else {
            var button = new OpenLayers.Control.Button({ // Button für Messtool
                title: $filter('translate')('map.measure'),
                id: 'MeasureButton',
                displayClass: 'Measure',
                eventListeners: {
                    'activate': button_active,
                    'deactivate': button_deactive
                },
                type: OpenLayers.Control.TYPE_TOGGLE
            });
            panel.addControls([button]);
        }

        $scope.map.raiseLayer($scope.map.getLayersBy("blidslayer", "t")[0], 20);

        // ************* ENTFERNUNGSMESSUNG *******************************************************
        // *** ARCHIV ***
        function archOn() {

            $scope.map.stopReloadTimer(); // no reloads while in archive mode
            if (UserService.getLiveBlids() === "t" && $scope.liveLayer.isActive()) {
                $scope.map.getLayersByName("LiveLayer")[0].setVisibility(false); // switch of live layer in archive mode
                $scope.liveLayer.deactivate();
            }

            var time = ($scope.map.slider.options.max - $scope.map.slider.element.value) * 1000;
            $scope.map.zeitVon = $filter('date')(new Date() - time, 'yyyy-MM-dd HH:mm:ss');
            $scope.map.zeitBis = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
            $scope.$apply();
            angular.element(document.getElementById('ex1Slider')).css('display', 'none');
            angular.element(document.getElementById('layerleiste')).css('max-height', '60vh');
            angular.element(document.getElementById('archivdiv')).css('display', 'block');
            if (window.mobileAndTabletcheck() === true) {
                archivMobileOptimize();
            }
            $scope.map.archiveActive = true;

        }

        function archOff() {
            if (UserService.getLiveBlids() === "t" && $scope.map.getLayersBy("blidslayer", "t")[0].visibility === true) {
                $scope.map.getLayersByName("LiveLayer")[0].setVisibility(true);
                $scope.liveLayer.activate();
            }

            angular.element(document.getElementById('ex1Slider')).css('display', 'block');
            angular.element(document.getElementById('layerleiste')).css('max-height', '75vh');
            angular.element(document.getElementById('archivdiv')).css('display', 'none');
            var time = $scope.map.slider.options.max - $scope.map.slider.element.value;
            $scope.updateTime(time, 0);
            $scope.map.startReloadTimer();
            $scope.map.archiveActive = false;

        }

        function ArchivMobileClick() {
            //Falls archive aktiv und nochmal anzeigen bei erneutem klicken dann 2. teil weg
            if ($scope.archivOpened || $scope.map.archiveActive) {
                $scope.archivOpened = false;
                archButton.className = "ArchivBtnInActive ctrlBtn";
                archOff();
            } else {
                $scope.archivOpened = true;
                archButton.className = "ArchivBtnActive ctrlBtn";
                archOn();
            }
        }

        if (UserService.getArchiv().tage > 0) {
            var archButton;
            if (window.mobileAndTabletcheck() === true) {
                archButton = document.createElement("button");
                archButton.id = "ArchiveButton";
                archButton.className = "ArchivBtnInActive ctrlBtn";
                archButton.title = $translate.instant('map.archive');
                if ('ontouchstart' in document.documentElement) {
                    archButton.ontouchstart = ArchivMobileClick;
                } else {
                    archButton.onclick = ArchivMobileClick;
                }
                document.getElementById("map").parentNode.appendChild(archButton);
            } else {
                archButton = new OpenLayers.Control.Button({ // Button für Archiv
                    title: $translate.instant('map.archive'),
                    id: 'ArchiveButton',
                    displayClass: 'Archive',
                    eventListeners: {
                        'activate': archOn,
                        'deactivate': archOff
                    },
                    type: OpenLayers.Control.TYPE_TOGGLE
                });
                panel.addControls([archButton]);
            }



            $scope.archiveClick = function() {
                var completeTime = function(time) {
                    var timecomplete = time.length < 11 ? time + " 00:00:01" : time.length < 17 ? time + ":01" : time;
                    return timecomplete;
                };
                var von = new Date(completeTime($scope.map.zeitVon).replace(/-/g, '/'));
                var bis = new Date(completeTime($scope.map.zeitBis).replace(/-/g, '/'));
                var time = (bis.getTime() - von.getTime()) / 1000;
                var offset = Math.round((Date.now() - bis.getTime()) / 1000);
                $scope.updateTime(time, offset);
                if (window.mobilecheck() === true) {
                    document.getElementById('archivdiv').style.display = "none";
                    $scope.archivOpened = false;
                }
            };
            $scope.checkTime = function() {
                var von = new Date($scope.map.zeitVon.replace(/-/g, '/'));
                var bis = new Date($scope.map.zeitBis.replace(/-/g, '/'));
                var archiv = UserService.getArchiv();
                var start = archiv.start;
                var tage = Math.ceil((bis - von) / (1000 * 3600 * 24));
                var valid = false;
                var startString = $filter('date')(start, 'yyyy-MM-dd');
                if (tage > archiv.tage)
                    valid = "Maximum " + archiv.tage + " day" + ((tage > 1) ? "s " : " ") + "allowed";
                if (von < start)
                    valid = "Start must be later than " + startString;
                if (bis > new Date())
                    valid = "No future data available";
                if (!UserService.checkDvD(von, bis))
                    valid = "Enddate must be later than Startdate";
                $scope.archToolTip = valid ? valid : "";
                return valid;
            };
        }
        // *** ARCHIV ***

        // **** HomeButton*****///
        var homeButton;
        if (window.mobileAndTabletcheck() === true) {
            homeButton = document.createElement("button");
            homeButton.id = "HomeButton";
            homeButton.className = "HomeItemInactive ctrlBtn";
            homeButton.title = $translate.instant('map.home');
            if ('ontouchstart' in document.documentElement) {
                homeButton.ontouchstart = home;
            } else {
                homeButton.onclick = home;
            }
            document.getElementById("map").parentNode.appendChild(homeButton);
        } else {
            homeButton = new OpenLayers.Control.Button({
                title: $translate.instant('map.home'),
                displayClass: "Home",
                trigger: home
            });
            panel.addControls([homeButton]);
        }


        function home() {
            $location.path(PATHS.map);
            if ($scope.map.getControlsBy("title", "Archive").length > 0) $scope.map.getControlsBy("title", "Archive")[0].deactivate();
            if ($scope.map.getControlsBy("title", "Animation").length > 0) $scope.map.getControlsBy("title", "Animation")[0].deactivate();
            $scope.map.setCenter(UserService.getCenter(), 0);
        }
        // **** HomeButton*****///

        // **** SoundButton*****///
        var soundButton;
        if (window.mobileAndTabletcheck() !== true) {
            soundButton = new OpenLayers.Control.Button({
                title: $translate.instant('map.sound'),
                displayClass: "Sound",
                eventListeners: {
                    'activate': soundOn,
                    'deactivate': soundOff
                },
                type: OpenLayers.Control.TYPE_TOGGLE
            });
            panel.addControls([soundButton]);
        }

        function soundOn() {
            $scope.liveLayer.Sound(true);
        }

        function soundOff() {
            $scope.liveLayer.Sound(false);
        }
        // **** SoundButton*****///

        // **** LogoutButton*****///
        var logoutButton;
        if (window.mobileAndTabletcheck() === true) {
            logoutButton = document.createElement("button");
            logoutButton.id = "LogoutButton";
            logoutButton.className = "LogoutItemInactive ctrlBtn";
            logoutButton.title = $translate.instant('nav.logout');
            if ('ontouchstart' in document.documentElement) {
                logoutButton.ontouchstart = logout;
            } else {
                logoutButton.onclick = logout;
            }
            document.getElementById("map").parentNode.appendChild(logoutButton);
        } else {
            logoutButton = new OpenLayers.Control.Button({
                title: $translate.instant('nav.logout'),
                displayClass: "Logout",
                trigger: logout
            });
            panel.addControls([logoutButton]);
        }


        function logout() {
            location.href = "../#/user/logout";
        }
        // **** LogoutButton*****///

        $scope.map.slider = new Slider('#ex1', {
            //scale:'logarithmic'
            formatter: function(value) {
                var t = this.max - value;
                var retval = (Math.floor(t / 3600 / 24) ? Math.floor(t / 3600 / 24) + " d : " : "") + ('0' + Math.floor(t / 3600) % 24).slice(-2) + ' h : ' + ('0' + Math.floor(t / 60) % 60).slice(-2) + ' m';
                return retval;
            }
        });
        $scope.map.slider.options.max = UserService.getData().maxTime;
        $scope.map.slider.options.step = 1800; //$scope.map.slider.options.max / 40;
        var initialSlider = 0.2;
        $scope.map.slider.setValue($scope.map.slider.options.max * (1 - initialSlider), true);
        $scope.map.slider.on("slideStop", function(slideEvt) {
            var time = $scope.map.slider.options.max - $scope.map.slider.element.value;
            $scope.updateTime(time, 0); // time + stop offset
        });
        $scope.updateTime(UserService.getData().maxTime * initialSlider, 0);
        $scope.$on('$destroy', function() { // remove all timers when map is left
            $scope.map.stopReloadTimer();
        });

        if (window.mobileAndTabletcheck() === true) {
            sliderMobileOptimize();
        }
        //***** Animation Button ********
        function animOn() {
            if (UserService.getLiveBlids() === "t" && $scope.liveLayer.isActive()) {
                $scope.map.getLayersByName("LiveLayer")[0].setVisibility(false);
                $scope.liveLayer.deactivate();
            }
            angular.element(document.getElementById('animGif')).css('display', 'block');
            for (var i = 0; i < $scope.map.layers.length; i++) {
                if ($scope.map.layers[i].params && $scope.map.layers[i].options.timedependent == "t") {
                    $scope.map.layers[i].mergeNewParams({ animate: "TRUE" });
                    $scope.map.layers[i].redraw(true);
                }
            }
        }

        function animOff() {
            if (UserService.getLiveBlids() === "t" && $scope.map.getLayersBy("blidslayer", "t")[0].visibility === true) {
                $scope.map.getLayersByName("LiveLayer")[0].setVisibility(true);
                $scope.liveLayer.activate();
            }
            angular.element(document.getElementById('animGif')).css('display', 'none');
            for (var i = 0; i < $scope.map.layers.length; i++) {
                if ($scope.map.layers[i].params && $scope.map.layers[i].options.timedependent) {
                    $scope.map.layers[i].mergeNewParams({ animate: "FALSE" });
                    $scope.map.layers[i].redraw(true);
                }
            }

        }

        function AnimationMobileClick() {
            if ($scope.animationActive) {
                $scope.animationActive = false;
                animAbutton.className = "AnimationBtnInActive ctrlBtn";
                animOff();
            } else {
                $scope.animationActive = true;
                animAbutton.className = "AnimationBtnActive ctrlBtn";
                animOn();
            }
        }
        if (UserService.getAnimation() === "t") {
            $scope.animationActive = false;
            if (window.mobileAndTabletcheck() === true) {
                var animAbutton = document.createElement("button");
                animAbutton.id = "AnimationButton";
                animAbutton.className = "AnimationBtnInActive ctrlBtn";
                animAbutton.title = $translate.instant('map.animation');
                if ('ontouchstart' in document.documentElement) {
                    animAbutton.ontouchstart = AnimationMobileClick;
                } else {
                    animAbutton.onclick = AnimationMobileClick;
                }
                document.getElementById("map").parentNode.appendChild(animAbutton);
            } else {
                var animButton = new OpenLayers.Control.Button({ // Button für Archiv
                    title: $translate.instant('map.animation'),
                    id: 'AnimationButton',
                    displayClass: 'Animation',
                    eventListeners: {
                        'activate': animOn,
                        'deactivate': animOff
                    },
                    type: OpenLayers.Control.TYPE_TOGGLE
                });
                panel.addControls([animButton]);
            }

        }
        ////***** Animation Button ********

        ////***** Heartbeat Display ********
        var hbDiv = document.createElement("div");
        hbDiv.id = "heartbeat";
        hbDiv.innerHTML = "<span id='hbText'>"+$translate.instant('map.heartbeat')+"</span>: <span id='hbData'> - </span>";
        document.getElementById("animGif").parentNode.appendChild(hbDiv);
        ////***** Heartbeat Display ********

        // **** END Map Controller
    });
})();