/* 
 *Used to optimize web-app for mobile devices
 * login screen fixed
 */


var mql = window.matchMedia("(orientation: portrait)");


/********************Solves Problem of moving Header, when keyboad opend ******************************/
var initialScreenSize = window.innerHeight;
var previousOrientation = window.orientation;
window.addEventListener("resize", function () {
    if (window.innerHeight < initialScreenSize && (window.orientation === previousOrientation)) {
        hideFooter();
    } else {
        showFooter();
    }
    if (window.orientation !== previousOrientation) {
        previousOrientation = window.orientation;
        showFooter();
    }
});






//Hides the footer-element
function hideFooter() {
    document.getElementsByClassName("footer")[0].style.display = 'none';
}
//Shows the footer-element
function showFooter() {
    document.getElementsByClassName("footer")[0].style.display = 'block';
}

/*****************************************************************************/




/*************************OPTIMIZE ARCHIV ****************************/
/**
 * Optimize Archiv-Div position and width
 * Change parent node from map to LayerMenu -> on mobile in front of map and better access
 */
function archivMobileOptimize() {
    /**ArchivDIV**/
    var newParent = document.getElementById('LayerMenu').parentNode;
    var oldParent = document.getElementById('archivdiv').parentNode;
    var archiv = document.getElementById('archivdiv');

    oldParent.removeChild(document.getElementById('archivdiv'));
    newParent.appendChild(archiv);
}
/************************************************************************/

/******************SLIDER************************************************/
/**
 * Optimize Slider position and width
 * Change parent node from Map to LayerMenu -> slider lays over map div
 */
function sliderMobileOptimize(){
   
    var newParent = document.getElementById('LayerMenu').parentNode;
    var oldParent = document.getElementById('ex1Slider').parentNode;
    var slider = document.getElementById('ex1Slider');

    oldParent.removeChild(document.getElementById('ex1Slider'));
    newParent.appendChild(slider);

    slider.addEventListener("touchstart", function(){ 
       //slider.removeEventListener("touchend", removeToolTip,false);
       document.getElementsByClassName('tooltip')[0].style.opacity = 0.9;
      
   
   });
   slider.addEventListener("touchmove", function(){ 
       addToolTip();
       //slider.addEventListener("touchend", removeToolTip,false);
   
   });
   
}

function addToolTip(){
    document.getElementsByClassName('tooltip')[0].style.opacity = 0.9;
}
function removeToolTip(){
    document.getElementsByClassName('tooltip')[0].style.opacity = 0;
}
/********************************************************************/
function showMobileMore(contentList) {
    var parent = document.getElementById('LayerMenu').parentNode;
    var list = getMobileInfoList(contentList);
    parent.appendChild(list);
}
function onFeatureUnselect(map) {
    for (var i = 0, len = map.popups.length; i < len; i++) {
        map.removePopup(map.popups[i]);
    }
}
function getMobileInfoList(htmlContent) {
    var newParent = document.getElementById('LayerMenu').parentNode;
    var infoListContainer = document.createElement("div");
    infoListContainer.id = "infoListContainer";
    var infoListContent = document.createElement("div");
    infoListContent.className = "scrollBar";
    infoListContent.id = "infoListContent";
    var closeBtn = document.createElement("div");
    closeBtn.id = "infoListCloseBtn";
    closeBtn.onclick = function () {
        newParent.removeChild(infoListContainer);
    };
    infoListContent.innerHTML = htmlContent;
    infoListContainer.appendChild(closeBtn);
    infoListContainer.appendChild(infoListContent);
    newParent.appendChild(infoListContainer);
    return infoListContainer;
}

