'use strict()';

// Copyright 2012 - 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.


(function() {

    var blids = angular.module('blidsApp');

    blids.filter('asNum', function() { // filter to display text as Number with "digits" digits and formatted with /factor
        return function(text, factor, digits) {
            if (text === null || text === '')
                text = '0';
            text = parseFloat(text) / factor;
            return text.toFixed(digits);
        };
    });
    ////////////////////////////////////////////////////////////////////////////////    
    //====================  
    //  Admin-Pages Alarm @ alarme.html
    //====================
    blids.controller('AlarmsTabCtrl', function($http, $uibModal, $location, alarmlist, ModalService, $translate) {
        var $ALARME = this;

        if (alarmlist.data.status === "failure")
            $location.path('/user/logout'); // Log Out if insufficient User Permissions


        //allgemein
        $ALARME.data = {
            predicateAlarme: 'id',
            reverseAlarme: false,
            predicateGebiete: 'id',
            reverseGebiete: false,
            predicateTeilnehmer: 'id',
            reverseTeilnehmer: false
        };

        $ALARME.data.alarmlist = alarmlist.data.alarmlist;
        $ALARME.data.group = alarmlist.data.group;
        $ALARME.data.gebietelist = alarmlist.data.gebietelist;
        $ALARME.data.teilnehmerlist = alarmlist.data.teilnehmerlist;

        $ALARME.data.tabAlarme = function() {
            loadAlarmList();
            loadGebietgruppenList();
        };

        function loadAlarmList() {
            var param = { "groupid": $ALARME.data.group.id };
            $http.post('admin/alarmlist', param)
                .success(function(response) {
                    $ALARME.data.alarmlist = response.alarmlist;

                    //         $ALARME.data.alarmlist.flach = parseFloat($ALARME.data.alarmlist.flach); //toFixed(3)

                })
                //ToDo    //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.get_alarmlist') + ' ' + $translate.instant('message.failed') + '.');
                });
        }

        $ALARME.data.tabTeilnehmer = function() {
            loadTeilnehmerList();
        };

        function loadTeilnehmerList() {
            var param = { "groupid": $ALARME.data.group.id };
            $http.post('admin/alarmteilnehmerlist', param)
                .success(function(response) {
                    $ALARME.data.teilnehmerlist = response.teilnehmerlist;
                })
                //ToDo    //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.get_subscriberlist') + ' ' + $translate.instant('message.failed') + '.');
                });
        }

        $ALARME.data.tabGebiete = function() {
            loadGebieteList();
        };

        function loadGebieteList() {
            var param = { "groupid": $ALARME.data.group.id };
            $http.post('admin/alarmgebietelist', param)
                .success(function(response) {
                    $ALARME.data.gebietelist = response.gebietelist;
                })
                //ToDo    //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.get_arealist') + ' ' + $translate.instant('message.failed') + '.');
                });
        }

        $ALARME.data.tabGebietgruppen = function() {
            loadGebietgruppenList();
        };

        function loadGebietgruppenList() {
            var param = { "groupid": $ALARME.data.group.id };
            $http.post('admin/alarmgebietegrouplist', param)
                .success(function(response) {
                    $ALARME.data.gebietgruppenlist = response.gebietegruppenlist;
                })
                //ToDo    //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.get_areagrouplist') + ' ' + $translate.instant('message.failed') + '.');
                });
        }


        ////////////////////
        // Alarm-Tab

        /*Funktionalitaet fuer das Ordnen der Alarm-Tabelle*/
        $ALARME.data.orderAlarme = function(predicate) {
            $ALARME.data.reverseAlarme = ($ALARME.data.predicateAlarme === predicate) ? !$ALARME.data.reverseAlarme : false;
            $ALARME.data.predicateAlarme = predicate;
        };

        /*Oeffnen des Dialoges zum hinzufügen eines neuen Alarms*/
        $ALARME.data.openAddAlarmDialog = function() {
            if (!$ALARME.data.teilnehmerlist || $ALARME.data.teilnehmerlist.length == 0)
                ModalService.alert('danger', $translate.instant('message.participantlist_empty'));
            else if (!$ALARME.data.gebietelist || $ALARME.data.gebietelist.length == 0)
                ModalService.alert('danger', $translate.instant('message.alarmarealist_empty'));
            else $ALARME.data.openAlarmDialog([], 'Add');
        };

        /*Oeffnen des Dialoges zum editieren eines neuen Alarms*/
        $ALARME.data.openEditAlarmDialog = function(item) {
            $ALARME.data.openAlarmDialog(item, 'EditView');
        };

        $ALARME.data.openAlarmDialog = function(item, action) {
            var options = {
                animation: true,
                backdrop: "static",
                templateUrl: "/partials/admin-pages/AlarmDialog.html",
                controller: "AlarmDialogController as AlarmDialogCtrl",
                size: "",
                resolve: {
                    details: function() {
                        return item;
                    },
                    group: function() {
                        return $ALARME.data.group;
                    },
                    permissions: function() {
                        return $ALARME.data.group.rights;
                    },
                    call: function() {
                        return action;
                    },
                    data: function() {
                        return { "teilnehmer": $ALARME.data.teilnehmerlist, "gebiete": $ALARME.data.gebietelist };
                    }
                }
            };
            var modalInstance = $uibModal.open(options);
            modalInstance.result.then(function() {
                loadAlarmList();
            });
        };

        $ALARME.data.openMeldungen = function(alarm) {
            var end = new Date();
            end.setDate(end.getDate() + 1);
            var start = new Date();
            start.setMonth(start.getMonth() - 3);
            var param = {
                "alarm": {
                    "groupid": $ALARME.data.group.id,
                    "kundeid": alarm.alarmkundeid,
                    "alarmid": alarm.alarmid,
                    "start": start.toLocaleDateString('de-DE'),
                    "ende": end.toLocaleDateString('de-DE')
                }
            };
            $http.post('admin/getAlarmMeldungen', param)
                .then(function(response) {
                    var options = {
                        animation: true,
                        backdrop: "static",
                        templateUrl: "/partials/admin-pages/AlarmMeldungen.html",
                        controller: "AlarmMeldungController as AlarmMeldungCtrl",
                        size: "sm",
                        resolve: {
                            details: function() {
                                var data = {
                                    "answer": response.data.result,
                                    "gruppe": alarm.teilnehmer + " @ " + alarm.gebiet,
                                    "alarm": param.alarm,
                                    "start": start,
                                    "ende": end
                                };
                                return data;
                            }
                        }
                    };
                    $uibModal.open(options).result.finally(function() {

                    });
                }, function(response) {
                    console.debug("error getting alarmMeldungen");
                });
        };


        ////////////////////
        // Gebiete-Tab

        $ALARME.data.orderGebiete = function(predicate) {
            $ALARME.data.reverseGebiete = ($ALARME.data.predicateGebiete === predicate) ? !$ALARME.data.reverseGebiete : false;
            $ALARME.data.predicateGebiete = predicate;
        };


        $ALARME.data.loadGebieteList = function() {
            //            AlarmsTabService.loadAlarms();
        };

        /*Oeffnen des Dialoges zum hinzufügen eines neuen Gebietes*/
        $ALARME.data.openAddGebietDialog = function() {
            $ALARME.data.openGebietDialog([], 'Add');
        };

        $ALARME.data.openEditGebietDialog = function(item) {
            $ALARME.data.openGebietDialog(item, 'EditView');
        };

        /*Oeffnen des Dialoges zum editieren einer neuen Gebietgruppe*/
        $ALARME.data.openGebietDialog = function(item, action) {
            var options = {
                animation: true,
                backdrop: "static",
                templateUrl: "/partials/admin-pages/GebietDialog.html",
                controller: "GebietDialogController as GebietCtrl",
                size: "sm",
                resolve: {
                    details: function() {
                        var param = { "groupid": $ALARME.data.group.id, "gebietid": item.id };
                        return $http.post('admin/alarmgebietelist', param)
                            .then(function(response) {
                                    Object.assign(item, response.data.gebietelist[0]);
                                    return item;
                                },
                                //ToDo    //check ERROR-Habit
                                function(response) {
                                    ModalService.alert('danger', $translate.instant('message.get_arealist') + ' ' + $translate.instant('message.failed') + '.');
                                    return item;
                                });
                    },
                    group: function() {
                        return $ALARME.data.group;
                    },
                    permissions: function() {
                        return $ALARME.data.group.rights;
                    },
                    call: function() {
                        return action;
                    }
                }
            };
            var modalInstance = $uibModal.open(options);
            modalInstance.result.then(function(message) {
                if (item.caller === "AlList")
                    loadAlarmList();
                else
                    loadGebieteList();
                //console.debug("Modal Antwort war: " + message);

            });
        };

        $ALARME.data.openEditGebietDialog2 = function(gebiet) {
            var item = $ALARME.data.gebietelist.filter(function(obj) {
                return obj.id === gebiet;
            })[0];
            item.caller = "AlList";
            $ALARME.data.openEditGebietDialog(item);
        };



        ////////////////////
        // Gebietgruppen-Tab

        $ALARME.data.orderGebietegruppe = function(predicate) {
            $ALARME.data.reverseGebietegruppe = ($ALARME.data.predicateGebietegruppe === predicate) ? !$ALARME.data.reverseGebietegruppe : false;
            $ALARME.data.predicateGebietegruppe = predicate;
        };

        $ALARME.data.loadGebietGruppenList = function() {
            //            AlarmsTabService.loadAlarms();
        };

        /*Oeffnen des Dialoges zum hinzufügen einer neuen Gebietgruppe*/
        $ALARME.data.openAddGebietGruppenDialog = function() {
            var options = {
                animation: true,
                backdrop: "static",
                templateUrl: "/partials/admin-pages/GebietgruppenDialog.html",
                controller: "GebietGruppeDialogController as GebGrpCtrl",
                size: "sm",
                resolve: {
                    details: function() {
                        return [];
                    },
                    group: function() {
                        return $ALARME.data.group;
                    },
                    permissions: function() {
                        return $ALARME.data.group.rights;
                    },
                    call: function() {
                        return 'Add';
                    }
                }
            };
            var modalInstance = $uibModal.open(options);
            modalInstance.result.finally(function() {
                loadGebietGruppenList();
            });
        };

        /*Oeffnen des Dialoges zum editieren einer neuen Gebietgruppe*/
        $ALARME.data.openEditGebietGruppenDialog = function(item) {
            var options = {
                animation: true,
                backdrop: "static",
                templateUrl: "/partials/admin-pages/GebietgruppenDialog.html",
                controller: "GebietGruppeDialogController as GebGrpCtrl",
                size: "sm",
                resolve: {
                    details: function() {
                        return item;
                    },
                    group: function() {
                        return $ALARME.data.group;
                    },
                    permissions: function() {
                        return $ALARME.data.group.rights;
                    },
                    call: function() {
                        return 'EditView';
                    }
                }
            };
            var modalInstance = $uibModal.open(options);
            modalInstance.result.finally(function() {
                loadGebietGruppenList();
            });
        };




        ////////////////////
        // Teilnehmer-Tab

        /*Funktionalitaet fuer das Ordnen der Teilnehmer-Tabelle*/
        $ALARME.data.orderTeilnehmer = function(predicate) {
            $ALARME.data.reverseTeilnehmer = ($ALARME.data.predicateTeilnehmer === predicate) ? !$ALARME.data.reverseTeilnehmer : false;
            $ALARME.data.predicateTeilnehmer = predicate;
        };

        /*Oeffnen des Dialoges zum hinzufügen eines neuen Alarms*/
        $ALARME.data.openAddTeilnehmerDialog = function() {
            $ALARME.data.openAddEditTeilnehmerDialog([], 'Add');
        };

        /*Oeffnen des Dialoges zum editieren eines neuen Alarms*/
        $ALARME.data.openEditTeilnehmerDialog = function(item) {
            $ALARME.data.openAddEditTeilnehmerDialog(item, 'EditView');
        };


        /*Oeffnen des Dialoges zum hinzufügen/editieren eines neuen Alarms*/
        $ALARME.data.openAddEditTeilnehmerDialog = function(item, action) {
            var options = {
                animation: true,
                backdrop: "static",
                templateUrl: "/partials/admin-pages/TeilnehmerDialog.html",
                controller: "TeilnehmerDialogController as TeilnehmerCtrl",
                size: "sm",
                resolve: {
                    details: function() {
                        var param = { "groupid": $ALARME.data.group.id, "teilnehmerid": item.id };
                        return $http.post('admin/alarmteilnehmerlist', param)
                            .then(function(response) {
                                    if (response.data.teilnehmerlist) Object.assign(item, response.data.teilnehmerlist[0]);
                                    return item;
                                },
                                //ToDo    //check ERROR-Habit
                                function(response) {
                                    ModalService.alert('danger', $translate.instant('message.get_arealist') + ' ' + $translate.instant('message.failed') + '.');
                                    return item;
                                });
                    },
                    group: function() {
                        return $ALARME.data.group;
                    },
                    permissions: function() {
                        return $ALARME.data.group.rights;
                    },
                    call: function() {
                        return action;
                    }
                }
            };
            var modalInstance = $uibModal.open(options);
            modalInstance.result.then(function(message) {
                if (item.caller === "AlList")
                    loadAlarmList();
                else
                    loadTeilnehmerList();
                //console.debug("Modal Antwort war: " + message);
            });
        };


        $ALARME.data.openEditTeilnehmerDialog2 = function(teilnehmer) {
            var item = $ALARME.data.teilnehmerlist.filter(function(obj) {
                return obj.id === teilnehmer;
            })[0];
            item.caller = "AlList";
            $ALARME.data.openEditTeilnehmerDialog(item);
        };

    });

    //////////////////// Controller der einzelnen Modale ////////////////////

    blids.controller('AlarmDialogController', function($uibModalInstance, ModalService, $http, details, group, permissions, call, data, $translate) {
        var $AlarmDialog = this;
        var art, title, button;

        // Art des Aufrufes
        if (call === 'Add') {
            art = "Add";
            title = $translate.instant('admin.add_alarm');
            button = $translate.instant('admin.add_alarm');
        } else {
            switch (permissions.alarm_admin) {
                case "A":
                    art = "All";
                    title = $translate.instant('alarm.edit_alarm') + ": " + details.teilnehmer + " @ " + details.gebiet;
                    button = $translate.instant('admin.save') + " & " + $translate.instant('admin.exit');
                    break;
                case "W":
                    art = "Edit";
                    title = $translate.instant('alarm.edit_alarm') + ": " + details.teilnehmer + " @ " + details.gebiet;
                    button = $translate.instant('admin.save') + " & " + $translate.instant('admin.exit');
                    break;
                case "R":
                    art = "View";
                    title = "View Alarm: " + details.teilnehmer + " @ " + details.gebiet;
                    button = $translate.instant('admin.exit');
                    break;
                default: //case "N":
                    art = "Nothing";
                    title = "Alarm";
                    button = $translate.instant('admin.exit');
            }
        }

        var kunde = details.alarmkundeid ? { "id": details.alarmkundeid } : { "id": 0 };
        $AlarmDialog.formAlarm = {
            typ: art,
            selectedkunde: kunde
        };
        $AlarmDialog.formAlarm.selectedgebiet = data.gebiete.filter(function(obj) {
            return obj.id === details.gebietid;
        })[0];
        $AlarmDialog.formAlarm.selectedteilnehmer = data.teilnehmer.filter(function(obj) {
            return obj.id === details.teilnehmerid;
        })[0];
        $AlarmDialog.modalData = {};
        $AlarmDialog.modalData.modalTitle = title;
        $AlarmDialog.modalData.modalButton = button;
        $AlarmDialog.modalData.modalDescription = "";
        $AlarmDialog.modalData.GebieteList = data.gebiete;
        $AlarmDialog.modalData.TeilnehmerList = data.teilnehmer;

        function loadAlarmKundenList(ID) {
            var param = { "groupid": ID };
            $http.post('admin/alarmkundelist', param)
                .success(function(response) {
                    $AlarmDialog.modalData.AlarmkundenList = response.alarmlist;
                    var numGruppe = 0;
                    $AlarmDialog.modalData.AlarmkundenList.find(function(element, index, array) {
                        if (element.id === kunde.id) {
                            $AlarmDialog.formAlarm.selectedkunde = array[index];
                            return 1;
                        } else {
                            $AlarmDialog.formAlarm.selectedkunde = array[0];
                            return 0;
                        }
                    });
                })
                .error(function(response) {
                    console.debug("error getting alarmkundenlist");
                });
        }
        loadAlarmKundenList(group.id);

        $AlarmDialog.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $AlarmDialog.modalData.submit = function() {
            var param = {
                "groupid": group.id,
                "teilnehmerid": $AlarmDialog.formAlarm.selectedteilnehmer.id,
                "kundeid": $AlarmDialog.formAlarm.selectedkunde.id,
                "alarmid": details.alarmid,
                "gebietid": $AlarmDialog.formAlarm.selectedgebiet.id
            };
            $http.post('admin/updatealarm', param)
                .then(function(response) {
                    console.debug(response);
                    var message = $AlarmDialog.formAlarm.typ === "Add" ? $translate.instant('alarm.added') : $translate.instant('alarm.updated');
                    $uibModalInstance.close($translate.instant('alarm.alarm') + " " + details.alarmid + $translate.instant('alarm.has_been') + message);
                }, function(response) {
                    console.debug("error updating alarm");
                });

            //$uibModalInstance.dismiss();                       
        };

        $AlarmDialog.modalData.delete = function() {
            var deleteAlarm = function() {
                var param = { "data": { "groupid": group.id, "kundeid": $AlarmDialog.formAlarm.selectedkunde.id, "alarmid": details.alarmid } };
                $http.post('admin/removealarm', param)
                    .then(function(response) {
                        if (response.data.result == 1) {
                            $uibModalInstance.close($translate.instant('alarm.alarm') + " " + details.alarmid + " " +
                                $translate.instant('alarm.has_been') + " " + $translate.instant('alarm.removed'));
                        } else {
                            window.alert($translate.instant('alarm.alarm_not_deleted'));
                        }

                    }, function(response) {
                        console.debug("error removing alarm");
                    });
            };

            var modalOptions = {
                closeButtonText: $translate.instant('admin.cancel'),
                actionButtonText: $translate.instant('alarm.delete'),
                actionButtonClass: 'btn-danger',
                headerText: $translate.instant('message.sure'),
                bodyText: $translate.instant('message.question_delete_alarm') + ' ' + details.teilnehmer + ' @ ' + details.gebiet + ' ?'
            };
            ModalService.showModal({}, modalOptions) // check if user is sure that he wants to delete 
                .then(function(result) {
                    if (result === "message.ok") {
                        deleteAlarm();
                    }
                });
        };

        $AlarmDialog.modalData.testAlarm = function(typ) {
            if (typ < 1 || typ > 2)
                return;
            var param = { "alarm": { "groupid": group.id, "kundeid": $AlarmDialog.formAlarm.selectedkunde.id, "alarmid": details.alarmid, "alarmtyp": typ } };
            $http.post('admin/sendTestAlarm', param)
                .then(function(response) {
                    var msg;
                    console.debug(response);
                    msg = (typ = 1) ? $translate.instant('alarm.alarm_sent') : $translate.instant('alarm.allclear_sent');
                    $uibModalInstance.close(msg);
                }, function(response) {
                    var msg;
                    msg = (typ = 1) ? $translate.instant('alarm.alarm_failed') : $translate.instant('alarm.allclear_failed');
                    console.debug(msg);
                });
        };

    });

    // Gebiet-Dialog
    blids.controller('GebietDialogController', function($uibModalInstance, $http, details, group, permissions, call, ModalService, $translate) {
        var $GebietDialog = this;
        var art, title;

        var agebiet = group.alarmgebiet.split(/[( ,)]+/); //calculate max alarm area borders from group.alarmgebiet
        var alarmgebiet = {
            'left': agebiet[1],
            'right': agebiet[3],
            'top': agebiet[2],
            'bottom': agebiet[4]
        };

        // Art des Aufrufes
        if (call === 'Add') {
            art = "Add";
            title = $translate.instant('alarm.add_area');
        } else {
            switch (permissions.alarm_gebiete) {
                case "A":
                    art = "All";
                    title = $translate.instant('alarm.edit_area') + ": " + details.gebiet;
                    break;
                case "W":
                    art = "Edit";
                    title = $translate.instant('alarm.edit_area') + ": " + details.gebiet;
                    break;
                case "R":
                    art = "View";
                    title = $translate.instant('alarm.view_area') + ": " + details.gebiet;
                    break;
                default: //case "N":
                    art = "Nothing";
                    title = "Area";
            }
        }

        $GebietDialog.modalData = {};
        $GebietDialog.modalData.modalTitle = title;
        $GebietDialog.modalData.modalButton = (call === "View") ? $translate.instant('admin.exit') : $translate.instant('admin.save') + " & " + $translate.instant('admin.exit');
        $GebietDialog.modalData.modalDescription = "";
        $GebietDialog.modalData.previewDone = false;

        switch (art) {
            // Neues Gebiet hinzufuegen
            case "Add":
                // Template fuer ein neues Gebiet. Default polygontyp = 'circle';
                $GebietDialog.formGebiet = { typ: art };
                $GebietDialog.formGebiet.laenge = group.laenge;
                $GebietDialog.formGebiet.breite = group.breite;
                $GebietDialog.formGebiet.gruppenid = group.id;
                $GebietDialog.formGebiet.polygontyp = 'circle';
                $GebietDialog.formGebiet.radius = 25;
                $GebietDialog.formGebiet.blitzezumalarm = 1;
                $GebietDialog.formGebiet.alarmdauer = 30;
                break;
                // Vorhandenes Gebiete bearbeiten
            case "Edit":
            case "View":
            case "All":
                $GebietDialog.formGebiet = { typ: art };
                $GebietDialog.formGebiet.gebiet = details.gebiet;
                $GebietDialog.formGebiet.laenge = details.laenge ? details.laenge : 8.5;
                $GebietDialog.formGebiet.breite = details.breite ? details.breite : 49;
                $GebietDialog.formGebiet.radius = details.radius ? parseInt(details.radius) : null;
                $GebietDialog.formGebiet.innerRadius = details.innerRadius ? parseInt(details.innerRadius) : null;
                $GebietDialog.formGebiet.alarmdauer = parseInt(details.alarmdauer);
                $GebietDialog.formGebiet.blitzezumalarm = parseInt(details.blitzezumalarm);
                $GebietDialog.formGebiet.gruppenid = group.id;
                $GebietDialog.formGebiet.id = details.id;
                $GebietDialog.formGebiet.blitzanzahl = details.anzahl;
                $GebietDialog.formGebiet.letzterblitz = details.letzter;
                if ($GebietDialog.formGebiet.laenge !== null && $GebietDialog.formGebiet.breite !== null && $GebietDialog.formGebiet.radius !== null) {
                    $GebietDialog.formGebiet.polygontyp = $GebietDialog.formGebiet.innerRadius ? 'ring' : 'circle';
                    $GebietDialog.formGebiet.geometry = details.geometry;
                } else {
                    $GebietDialog.formGebiet.polygontyp = 'polygon';
                    $GebietDialog.formGebiet.geometry = details.geometry;
                }
                break;
            default:
                console.log("Sorry, we are out of " + art + ".");
        }

        // Abholen der möglichen Alarmkunden für das zu bearbeitende Gebiet
        // und Ausfüllen des Drop-Down-Menüs "Alarmgruppe"
        function loadAlarmKundenList(ID) {
            var param = { "groupid": ID };
            $http.post('admin/alarmkundelist', param)
                .success(function(response) {
                    $GebietDialog.modalData.AlarmkundenList = response.alarmlist;
                    var selectKunde = details.kundenid;
                    var numGruppe = 0;
                    $GebietDialog.modalData.AlarmkundenList.find(function(element, index, array) {
                        if (element.id === selectKunde) {
                            $GebietDialog.formGebiet.selectedkunde = array[index];
                            return 1;
                        } else {
                            $GebietDialog.formGebiet.selectedkunde = array[0];
                            return 0;
                        }
                    });

                })
                //ToDo     //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.get_assigned_alarmlist_on') + ' ID:' + ID + ') ' + $translate.instant('message.failed') + '. \nResponse:' + response);
                });
        }
        loadAlarmKundenList(group.id);

        $GebietDialog.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $GebietDialog.modalData.submit = function() {
            var gebiet = {
                "kundenid": $GebietDialog.formGebiet.selectedkunde.id,
                "gruppe": $GebietDialog.formGebiet.gruppenid,
                "gebietname": $GebietDialog.formGebiet.gebiet,
                "blitzezumalarm": $GebietDialog.formGebiet.blitzezumalarm,
                "dauer": $GebietDialog.formGebiet.alarmdauer
            };

            var param = {};

            switch ($GebietDialog.formGebiet.polygontyp) {
                case "circle":
                    gebiet.laenge = Number($GebietDialog.formGebiet.laenge);
                    gebiet.breite = Number($GebietDialog.formGebiet.breite);
                    gebiet.radius = $GebietDialog.formGebiet.radius;
                    param = { "gebiet": gebiet };
                    break;
                case "ring":
                    gebiet.laenge = Number($GebietDialog.formGebiet.laenge);
                    gebiet.breite = Number($GebietDialog.formGebiet.breite);
                    gebiet.radius = $GebietDialog.formGebiet.radius;
                    gebiet.innerRadius = $GebietDialog.formGebiet.innerRadius;
                    param = { "gebiet": gebiet };
                    break;
                case "polygon":
                    gebiet.geometry = $GebietDialog.formGebiet.geometry;
                    param = { "gebiet": gebiet };
                    break;
                default:
                    console.log("Sorry, we are out of " + $GebietDialog.formGebiet.polygontyp + ".");
            }

            if ($GebietDialog.formGebiet.id > 0)
                gebiet.id = $GebietDialog.formGebiet.id;

            $http.post('admin/updatealarmgebiet', param)
                .then(
                    function(response) {
                        var message = $GebietDialog.formGebiet.typ === "Add" ? $translate.instant('alarm.added') : $translate.instant('alarm.updated');
                        $uibModalInstance.close($translate.instant('alarm.area') + " " + response.data.result.id + " " + $translate.instant('alarm.has_been') + " " + message);
                    },
                    function(response) {});

        };

        $GebietDialog.modalData.delete = function() {
            var deleteGebiet = function(cascade) {
                var gebiet = {
                    "id": $GebietDialog.formGebiet.id,
                    "gruppe": $GebietDialog.formGebiet.gruppenid,
                    "kundeid": $GebietDialog.formGebiet.selectedkunde.id,
                    "cascade": cascade
                };
                var param = { "gebiet": gebiet };
                $http.post('admin/removealarmgebiet', param)
                    .then(
                        function(response) {
                            if (response.status !== "failure") {
                                if (response.data.result === "Success") {
                                    var alarmsDeleted = cascade ? " " + $translate.instant('message.with_all_alarms') : "";
                                    $uibModalInstance.close($translate.instant('alarm.area') + " " + gebiet.id + " " + $translate.instant('alarm.has_been') + " " + $translate.instant('alarm.removed') + alarmsDeleted);
                                } else {
                                    var modalOptions = {
                                        closeButtonText: $translate.instant('message.cancel'),
                                        actionButtonText: $translate.instant('message.delete_area_and_alarms'),
                                        actionButtonClass: 'btn-danger',
                                        headerText: $translate.instant('message.area_used_in_alarms') + ' ' + response.data.Used + ' ' + $translate.instant('alarm.alarms'),
                                        bodyText: $translate.instant('message.delete_area_with_all_alarms')
                                    };
                                    ModalService.showModal({}, modalOptions) // check if user wants to delete channel and all alarms that use it
                                        .then(function(result) {
                                            if (result === "message.ok") {
                                                deleteGebiet(true);
                                            }
                                        });
                                }
                            }
                        },
                        function(response) {});
            };

            var modalOptions = {
                closeButtonText: $translate.instant('message.cancel'),
                actionButtonText: $translate.instant('alarm.delete'),
                headerText: $translate.instant('message.sure'),
                bodyText: $translate.instant('message.really_delete_area') + ' ' + $GebietDialog.formGebiet.gebiet + ' ? '
            };
            ModalService.showModal({}, modalOptions) // check if user is sure that he wants to delete 
                .then(function(result) {
                    if (result === "message.ok") {
                        deleteGebiet(false);
                    }
                });


        };

        $GebietDialog.modalData.positionEingabe = function() {
            $GebietDialog.modalData.previewDone = false;
            var laenge = Math.max(alarmgebiet.left, Math.min($GebietDialog.formGebiet.laenge, alarmgebiet.right));
            if (laenge != $GebietDialog.formGebiet.laenge) {
                ModalService.alert('primary', $translate.instant('message.lon_outside_area') + ':\n  ' + alarmgebiet.left + ' - ' + alarmgebiet.right);
            }
            if (laenge !== null) {
                laenge = parseFloat(laenge.toString().replace(/,/, "."));
                if (laenge > 180) {
                    $GebietDialog.formGebiet.laenge = 180;
                } else if (laenge < -180) {
                    $GebietDialog.formGebiet.laenge = -180;
                } else {
                    $GebietDialog.formGebiet.laenge = laenge.toFixed(6);
                }
            }

            var breite = Math.max(alarmgebiet.top, Math.min($GebietDialog.formGebiet.breite, alarmgebiet.bottom));
            if (breite != $GebietDialog.formGebiet.breite) {
                ModalService.alert('primary', $translate.instant('message.lat_outside_area') + ':\n  ' + alarmgebiet.top + ' - ' + alarmgebiet.bottom);
            }
            if (breite !== null) {
                breite = parseFloat(breite.toString().replace(/,/, "."));
                if (breite > 90) {
                    $GebietDialog.formGebiet.breite = 90;
                } else if (breite < -90) {
                    $GebietDialog.formGebiet.breite = -90;
                } else {
                    $GebietDialog.formGebiet.breite = breite.toFixed(6);
                }
            }
        };

        var locitem;
        $GebietDialog.modalData.getLocation = function(val) {
            return $http.post('https://api.blids.de/v1/geocode', { address: val, sensor: false }).then(function(response) {
                return response.data.map(function(item) {
                    return item;
                });
            });
        };

        $GebietDialog.modalData.checkCoordinates = function(lon, lat) {
            if (lon != $GebietDialog.formGebiet.laenge && lat != $GebietDialog.formGebiet.breite) {
                ModalService.alert('primary', $translate.instant('message.coord_outside_area') + ':\n  ' +
                    alarmgebiet.left + ' - ' + alarmgebiet.right + '\n' +
                    alarmgebiet.top + ' - ' + alarmgebiet.bottom);
            } else {
                if (lon != $GebietDialog.formGebiet.laenge) {
                    ModalService.alert('primary', translate.instant('message.lon_outside_area') + ':\n  ' + alarmgebiet.left + ' - ' + alarmgebiet.right);
                }
                if (lat != $GebietDialog.formGebiet.breite) {
                    ModalService.alert('primary', translate.instant('message.lat_outside_area') + ':\n  ' + alarmgebiet.top + ' - ' + alarmgebiet.bottom);
                }
            }
        };


        $GebietDialog.modalData.selectLocation = function(item) {
            $GebietDialog.modalData.previewDone = false;
            var laenge = item.geometry.location.lng.toFixed(6);
            $GebietDialog.formGebiet.laenge = Math.max(alarmgebiet.left, Math.min(laenge, alarmgebiet.right));
            var breite = item.geometry.location.lat.toFixed(6);
            $GebietDialog.formGebiet.breite = Math.max(alarmgebiet.top, Math.min(breite, alarmgebiet.bottom));
            $GebietDialog.modalData.checkCoordinates(laenge, breite);
        };

        function initMap() {
            var lat = $GebietDialog.formGebiet.laenge;
            var lon = $GebietDialog.formGebiet.breite;
            $GebietDialog.modalData.map = L.map('leafmap', {
                dragging: false,
                touchZoom: false,
                scrollWheelZoom: false,
                doubleClickZoom: false,
                boxZoom: false,
                tap: false,
                keyboard: false,
                zoomControl: false,
            }).setView([lon, lat], 6);
            var osm = L.tileLayer.wms("https://osm.blids.net/service/wms", {
                layers: 'osm:osm_all',
                format: 'image/png',
                transparent: true,
                attribution: '© <a href="http://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>'
            });
            osm.addTo($GebietDialog.modalData.map);
        }

        $uibModalInstance.rendered.then(function() {
            initMap();
            $GebietDialog.modalData.refreshMap();
        });

        $GebietDialog.modalData.refreshMap = function() {

            $GebietDialog.modalData.map.eachLayer(function(layer) {
                if (!(layer instanceof L.TileLayer.WMS)) {
                    $GebietDialog.modalData.map.removeLayer(layer);
                }
            });

            if ($GebietDialog.formGebiet.polygontyp === 'circle' || $GebietDialog.formGebiet.polygontyp === 'ring') {
                $GebietDialog.modalData.circle = L.circle([$GebietDialog.formGebiet.breite, $GebietDialog.formGebiet.laenge], $GebietDialog.formGebiet.radius * 1000, {
                    color: 'red',
                    fillColor: '#f03',
                    fillOpacity: 0.3
                }).addTo($GebietDialog.modalData.map);
                $GebietDialog.modalData.map.fitBounds($GebietDialog.modalData.circle.getBounds(), { padding: [30, 30] });
            }
            if ($GebietDialog.formGebiet.polygontyp === 'ring') {
                $GebietDialog.modalData.hole = L.circle([$GebietDialog.formGebiet.breite, $GebietDialog.formGebiet.laenge], $GebietDialog.formGebiet.innerRadius * 1000, {
                    color: 'red',
                    fillColor: '#FFFFFF',
                    fillOpacity: 0.5
                }).addTo($GebietDialog.modalData.map);
                $GebietDialog.modalData.map.fitBounds($GebietDialog.modalData.circle.getBounds(), { padding: [30, 30] });
            }
            if ($GebietDialog.formGebiet.polygontyp === 'polygon') {
                $GebietDialog.modalData.polygon = L.geoJson(wellknown.parse($GebietDialog.formGebiet.geometry), {
                    color: 'red',
                    fillColor: '#f03',
                    fillOpacity: 0.3
                });
                $GebietDialog.modalData.polygon.addTo($GebietDialog.modalData.map);
                $GebietDialog.modalData.map.fitBounds($GebietDialog.modalData.polygon.getBounds(), { padding: [30, 30] });
            }

            $GebietDialog.modalData.previewDone = true;
        };
    });
    // Teilnehmer-Dialog
    blids.controller('TeilnehmerDialogController', function($uibModalInstance, $http, details, group, permissions, call, UserService, ModalService, $translate) {
        var $TeilnehmerDialog = this;
        var art, title;
        // Art des Aufrufes
        if (call === 'Add') {
            art = "Add";
            title = $translate.instant('alarm.add_subscriber');
            button = $translate.instant('alarm.add_subscriber');
        } else {
            switch (permissions.alarm_teilnehmer) {
                case "A":
                    art = "All";
                    title = $translate.instant('alarm.edit_subscriber') + ": " + details.teilnehmer;
                    button = $translate.instant('admin.save') + " & " + $translate.instant('admin.exit');
                    break;
                case "W":
                    art = "Edit";
                    title = $translate.instant('alarm.edit_subscriber') + ": " + details.teilnehmer;
                    button = $translate.instant('admin.save') + " & " + $translate.instant('admin.exit');
                    break;
                case "R":
                    art = "View";
                    title = $translate.instant('alarm.view_subscriber') + ": " + details.teilnehmer;
                    button = $translate.instant('admin.exit');
                    break;
                default: //case "N":
                    art = "Nothing";
                    title = "Subscriber";
                    button = $translate.instant('admin.exit');
            }
        }

        $TeilnehmerDialog.modalData = {};
        $TeilnehmerDialog.modalData.modalTitle = title;
        $TeilnehmerDialog.modalData.modalButton = button;
        $TeilnehmerDialog.modalData.modalDescription = "";
        if (art === "Add") {
            $TeilnehmerDialog.formTeilnehmer = {
                "mo": "t",
                "di": "t",
                "mi": "t",
                "don": "t",
                "fr": "t",
                "sa": "t",
                "so": "t",
                "startzeit": "00:00:00",
                "stopzeit": "24:00:00",
                "blitzascsv": "f",
                "utc": "f",
                "blitzprotokoll": "f",
                "nurlog": "f"
            };
            $TeilnehmerDialog.formTeilnehmer.anfang = new Date();
            $TeilnehmerDialog.formTeilnehmer.anfang.setHours(15, 0, 0, 0);
            $TeilnehmerDialog.formTeilnehmer.ende = new Date();
            $TeilnehmerDialog.formTeilnehmer.ende.setDate(new Date().getDate() + 14);
            $TeilnehmerDialog.formTeilnehmer.ende.setHours(15, 0, 0, 0);
            $TeilnehmerDialog.formTeilnehmer.alarmierung = $translate.instant('alarm.alarmmsg');
            $TeilnehmerDialog.formTeilnehmer.entwarnung = $translate.instant('alarm.allclearmsg');
        } else {
            $TeilnehmerDialog.formTeilnehmer = details;
            $TeilnehmerDialog.formTeilnehmer.anfang = new Date(details.startdatum.substr(0, 10));
            $TeilnehmerDialog.formTeilnehmer.anfang.setUTCHours(0, 0, 0, 0);
            $TeilnehmerDialog.formTeilnehmer.ende = new Date(details.stopdatum.substr(0, 10));
            $TeilnehmerDialog.formTeilnehmer.ende.setUTCHours(0, 0, 0, 0);
        }
        $TeilnehmerDialog.formTeilnehmer.typ = art;
        $TeilnehmerDialog.formTeilnehmer.gruppenname = group.gruppenname;
        $TeilnehmerDialog.formTeilnehmer.gruppe = group.id;

        function loadAlarmKundenList(ID) {
            var param = { "groupid": ID };
            $http.post('admin/alarmkundelist', param)
                .then(
                    function(response) {
                        $TeilnehmerDialog.modalData.AlarmkundenList = response.data.alarmlist;
                        var selectKunde = details.kunde;
                        var numGruppe = 0;
                        $TeilnehmerDialog.modalData.AlarmkundenList.find(function(element, index, array) {
                            if (element.id === selectKunde) {
                                $TeilnehmerDialog.formTeilnehmer.selectedkunde = array[index];
                                return 1;
                            } else {
                                $TeilnehmerDialog.formTeilnehmer.selectedkunde = array[0];
                                return 0;
                            }
                        });
                    },
                    function(response) {});
        }

        loadAlarmKundenList(group.id);


        $TeilnehmerDialog.modalData.submit = function() {
            var teilnehmer = $TeilnehmerDialog.formTeilnehmer;
            teilnehmer.kundenid = $TeilnehmerDialog.formTeilnehmer.selectedkunde.id;
            var param = { "teilnehmer": teilnehmer };
            $http.post('admin/updatealarmteilnehmer', param)
                .then(
                    function(response) {
                        if (response.status !== "failure") {
                            var message = $TeilnehmerDialog.formTeilnehmer.typ === "Add" ? $translate.instant('alarm.added') : $translate.instant('alarm.updated');
                            $uibModalInstance.close($translate.instant('alarm.subscriber') + " " + response.data.result.id + " " + $translate.instant('alarm.has_been') + " " + message);
                        }
                    },
                    function(response) {});

        };

        $TeilnehmerDialog.modalData.delete = function() {
            var deleteTeilnehmer = function(cascade) {
                var teilnehmer = {
                    "id": $TeilnehmerDialog.formTeilnehmer.id,
                    "gruppe": $TeilnehmerDialog.formTeilnehmer.gruppe,
                    "kunde": $TeilnehmerDialog.formTeilnehmer.kunde,
                    "cascade": cascade
                };
                var param = { "teilnehmer": teilnehmer };
                $http.post('admin/removealarmteilnehmer', param)
                    .then(
                        function(response) {
                            if (response.status !== "failure") {
                                if (response.data.result === "Success") {
                                    var alarmsDeleted = cascade ? " " + $translate.instant('message.with_all_alarms') : "";
                                    $uibModalInstance.close($translate.instant('alarm.subscriber') + " " + teilnehmer.id + " " + $translate.instant('alarm.has_been') + " " + $translate.instant('alarm.removed') + " " + alarmsDeleted);
                                } else {
                                    var modalOptions = {
                                        closeButtonText: $translate.instant('message.cancel'),
                                        actionButtonText: $translate.instant('message.delete_subscriber_and_alarms'),
                                        actionButtonClass: 'btn-danger',
                                        headerText: $translate.instant('message.subscriber_used_in_alarms') + " " + response.data.Used + ' alarms',
                                        bodyText: $translate.instant('message.delete_subscriber_with_all_alarms')
                                    };
                                    ModalService.showModal({}, modalOptions) // check if user wants to delete channel and all alarms that use it
                                        .then(function(result) {
                                            if (result === "message.ok") {
                                                deleteTeilnehmer(true);
                                            }
                                        });
                                }
                            }
                        },
                        function(response) {});
            };

            var modalOptions = {
                closeButtonText: $translate.instant('message.cancel'),
                actionButtonText: $translate.instant('alarm.delete'),
                headerText: $translate.instant('message.sure'),
                bodyText: $translate.instant('message.question_delete_subscriber') + ' ' + $TeilnehmerDialog.formTeilnehmer.teilnehmer + ' ? '
            };
            ModalService.showModal({}, modalOptions) // check if user is sure that he wants to delete 
                .then(function(result) {
                    if (result === "message.ok") {
                        deleteTeilnehmer(false);
                    }
                });
        };

        $TeilnehmerDialog.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $TeilnehmerDialog.modalData.checkDatumVorDatum = function() { // WHY NOT WORKING ???
            var datumStart = $TeilnehmerDialog.formTeilnehmer.anfang;
            var datumEnde = $TeilnehmerDialog.formTeilnehmer.ende;
            var result = false;
            result = UserService.checkDvD(datumStart, datumEnde);
            if (result === false) {
                $TeilnehmerDialog.formTeilnehmer.datumVor = true;
            } else {
                $TeilnehmerDialog.formTeilnehmer.datumVor = false;
            }
        };

    });

    blids.controller('AlarmMeldungController', function($http, $uibModalInstance, details, ModalService, $translate) {
        var $AlarmMeldung = this;
        $AlarmMeldung.modalData = {};
        $AlarmMeldung.modalData.gruppe = details.gruppe;
        $AlarmMeldung.modalData.modalTitle = $translate.instant('alarm.messages') + " " + $translate.instant('admin.for') + " " + $AlarmMeldung.modalData.gruppe;
        $AlarmMeldung.modalData.modalDescription = "";
        $AlarmMeldung.modalData.orderBy = "start";
        $AlarmMeldung.modalData.reverseOrder = "false";
        $AlarmMeldung.modalData.data = details.answer;
        $AlarmMeldung.modalData.start = details.start;
        $AlarmMeldung.modalData.stop = details.ende;


        $AlarmMeldung.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $AlarmMeldung.modalData.orderLog = function(orderBy) {
            $AlarmMeldung.modalData.reverseOrder = ($AlarmMeldung.modalData.orderBy === orderBy) ? !$AlarmMeldung.modalData.reverseOrder : false;
            $AlarmMeldung.modalData.orderBy = orderBy;
        };

        $AlarmMeldung.open1 = function() {
            $AlarmMeldung.popup1.opened = true;
        };

        $AlarmMeldung.open2 = function() {
            $AlarmMeldung.popup2.opened = true;
        };

        $AlarmMeldung.refresh = function() {
            var param = {
                "alarm": {
                    "groupid": details.alarm.groupid,
                    "kundeid": details.alarm.kundeid,
                    "alarmid": details.alarm.alarmid,
                    "start": $AlarmMeldung.modalData.start.toLocaleDateString('de-DE'),
                    "ende": $AlarmMeldung.modalData.stop.toLocaleDateString('de-DE')
                }
            };
            $http.post('admin/getAlarmMeldungen', param)
                .then(function(response) {
                        $AlarmMeldung.modalData.data = response.data.result;
                    },
                    function(response) {
                        console.debug("Error :" + response);
                    });
        };

        $AlarmMeldung.popup1 = { opened: false };
        $AlarmMeldung.popup2 = { opened: false };


    });





    ////////////////////////////////////////////////////////////////////////////////
    //===================
    //  Admin-Pages Groups @ groups.html
    //===================

    blids.controller('GroupsTabCtrl', function($http, $uibModal, $location, GroupList, ModalService, $translate) {
        var $GRP = this;

        angular.element(document.getElementById('GroupList')).css('display', 'block'); // show site after data is loaded
        angular.element(document.getElementById('loader')).css('display', 'none'); // show site after data is loaded

        if (GroupList.data.status === "failure")
            $location.path('/user/logout'); // Log Out if insufficient User Permissions

        /* data als Container fuer alle Variablen in der Gruppenseite, und zugleich noetig um ein Aendern der Variablen waehrend der Laufzeit zuzulassen*/
        $GRP.data = {
            predicateGruppe: 'gruppenname',
            reverseGruppe: false,
            predicateUser: 'benutzername',
            reverseUser: false
        };

        /*Vordefinition der Arrays fuer Datenbankabfrage*/
        $GRP.groupHome = GroupList.data.group;
        $GRP.groupParent = GroupList.data.parent;
        $GRP.childrenList = GroupList.data.children;
        $GRP.usersList = GroupList.data.users;

        /*Funktionalitaet fuer das Ordnen der User-Tabelle*/
        $GRP.data.orderUser = function(predicate) {
            $GRP.data.reverseUser = ($GRP.data.predicateUser === predicate) ? !$GRP.data.reverseUser : false;
            $GRP.data.predicateUser = predicate;
        };

        /*Funktionalitaet fuer das Ordnen der Gruppen-Tabelle*/
        $GRP.data.orderGruppe = function(predicate) {
            $GRP.data.reverseGruppe = ($GRP.data.predicateGruppe === predicate) ? !$GRP.data.reverseGruppe : false;
            $GRP.data.predicateGruppe = predicate;
        };

        /*Laden aller Gruppen und User sowie die Vatergruppe der aktuellen Gruppe und deren Informationen (wenn keine ID angegeben - HomeGruppe des aktuellen Nutzers)*/
        function loadGroupViewList(ID) {
            var param = { "groupid": ID };
            $http.post('admin/groupview', param)
                .success(function(response) {
                    $GRP.groupHome = response.group;
                    $GRP.groupParent = response.parent;
                    $GRP.childrenList = response.children;
                    $GRP.usersList = response.users;
                })
                //ToDo    //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.getting_groupview') + ' ( ID: ' + ID + ' ) failed.');
                });
        }
        // Erster Aufruf ohne ID bringt HomeGruppe des aktuellen Nutzers
        //loadGroupViewList(); 


        /*Funktion, um per Bedienelement die Tabellen neu zu laden - AUCH um auf die neue Ansicht einer gewählten Gruppe umzuschalten*/
        $GRP.data.loadGroupsView = function(ID) { //fuer Zugriffe von Aussen
            loadGroupViewList(ID);
        };

        /*Oeffnen des kombinierten Dialoges zum Add-Edit-View der Eigenschaften einer Gruppe*/
        $GRP.data.openGruppeDialog = function(item, head) {
            if (!head) {
                head = false;
            }
            //            console.debug(head);
            var ctrlDetails = this;
            ctrlDetails.groupDetail = [];

            //Aufrufen der Daten der Übergeordneten Gruppe
            var paramPARENT = item ? { "groupid": item.vatergruppenid } : { "groupid": $GRP.groupHome.id };


            //als erstes der Sonderfall
            if (paramPARENT.groupid === null) {
                var param = { "groupid": item.id };
                $http.post('admin/groupdetail', param)
                    .success(function(response) {
                        ctrlDetails.groupDetail = response.groupdetail;
                        var options = {
                            animation: true,
                            backdrop: "static",
                            templateUrl: "/partials/admin-pages/GruppeDialog.html",
                            controller: "GruppeController as GrCtrl",
                            size: "sm",
                            resolve: {
                                PARENTdetails: function() {
                                    return [];
                                },
                                details: function() {
                                    return ctrlDetails.groupDetail;
                                },
                                permissions: function() {
                                    return item.rights;
                                },
                                call: function() {
                                    return 'SPECIAL';
                                },
                                head: function() {
                                    return head;
                                }
                            }
                        };
                        var modalInstance = $uibModal.open(options);
                        modalInstance.result.finally(function() {
                            loadGroupViewList($GRP.groupHome.id); // Angezeigte Liste auf Hauptseite neu laden, wenn Modal geschlossen 
                        });
                    })
                    //ToDo   //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.get_groupdetails') + '( ID:' + item.id + ', Name:' + item.gruppenname + ') ' + $translate.instant('message.failed') + '.');
                    });

            } else {
                $http.post('admin/groupdetail', paramPARENT)
                    .success(function(responsePARENT) {
                        ctrlDetails.PARENTgroupDetail = responsePARENT.groupdetail;
                        //Wenn mit einer Gruppe aufgerufen wird Aendern/Anzeigen
                        if (item) {
                            var param = { "groupid": item.id };
                            $http.post('admin/groupdetail', param)
                                .success(function(response) {
                                    ctrlDetails.groupDetail = response.groupdetail;
                                    var options = {
                                        animation: true,
                                        backdrop: "static",
                                        templateUrl: "/partials/admin-pages/GruppeDialog.html",
                                        controller: "GruppeController as GrCtrl",
                                        size: "sm",
                                        resolve: {
                                            PARENTdetails: function() {
                                                return ctrlDetails.PARENTgroupDetail;
                                            },
                                            details: function() {
                                                return ctrlDetails.groupDetail;
                                            },
                                            permissions: function() {
                                                return item.rights;
                                            },
                                            call: function() {
                                                return 'EDITVIEW';
                                            },
                                            head: function() {
                                                return head;
                                            }
                                        }
                                    };
                                    var modalInstance = $uibModal.open(options);
                                    modalInstance.result.finally(function() {
                                        loadGroupViewList($GRP.groupHome.id); // Angezeigte Liste auf Hauptseite neu laden, wenn Modal geschlossen 
                                    });
                                })
                                //ToDo    //check ERROR-Habit
                                .error(function(response) {
                                    ModalService.alert('danger', $translate.instant('message.get_groupdetails') + ' ( ID:' + item.id + ', Name:' + item.gruppenname + ') ' + $translate.instant('message.failed') + '.');
                                });
                        } else { //wenn als neue Gruppe (ADD) aufgerufen wird
                            var options = {
                                animation: true,
                                backdrop: "static",
                                templateUrl: "/partials/admin-pages/GruppeDialog.html",
                                controller: "GruppeController as GrCtrl",
                                size: "sm",
                                resolve: {
                                    PARENTdetails: function() {
                                        return ctrlDetails.PARENTgroupDetail;
                                    },
                                    details: function() {
                                        return [];
                                    },
                                    permissions: function() {
                                        return $GRP.groupHome.rights;
                                    },
                                    call: function() {
                                        return 'Add';
                                    },
                                    head: function() {
                                        return head;
                                    }
                                }
                            };

                            var modalInstance = $uibModal.open(options);
                            // modalInstance.opened.then(function () {});
                            modalInstance.result.finally(function() {
                                loadGroupViewList($GRP.groupHome.id); // Angezeigte Liste auf Hauptseite neu laden, wenn Modal geschlossen 
                            });
                        }
                    })
                    .error(function(responsePARENT) {
                        ModalService.alert('danger', $translate.instant('messagt.get_parent_groupdetails') + ' ( ID:' + $GRP.groupHome.id + ', Name:' + $GRP.groupHome.gruppenname + ') ' + $translate.instant('message.failed') + '.');
                    });
            }
        };

        /*Oeffnen des kombinierten Dialoges zum Add-Edit-View der Layer einer Gruppe*/
        $GRP.data.openGroupLayerDialog = function(item) {

            var options = {
                animation: true,
                backdrop: "static",
                templateUrl: "/partials/admin-pages/GroupLayerDialog.html",
                controller: "GroupLayerDialogController as LayerCtrl",
                size: "lg",
                resolve: {
                    item_details: function() {
                        return item;
                    }
                }
            };
            var modalInstance = $uibModal.open(options);
            modalInstance.result.finally(function() {
                loadGroupViewList($GRP.groupHome.id); // Angezeigte Liste auf Hauptseite neu laden, wenn Modal geschlossen 
            });

        };

        $GRP.data.openGroupAlarms = function(item) {
            $location.path('/alarme/' + item.id);
        };



        /*Oeffnen des Alarm-Master Dialoges einer Gruppe*/
        $GRP.data.openGroupAlarmDialog = function(item) {

            var options = {
                animation: true,
                backdrop: "static",
                templateUrl: "/partials/admin-pages/AlarmGruppeKunde.html",
                controller: "AlarmGruppeDialogController as AGCtrl",
                size: "sm",
                resolve: {
                    item_details: function() {
                        return item;
                    }
                }
            };
            var modalInstance = $uibModal.open(options);
            modalInstance.result.finally(function() {
                loadGroupViewList($GRP.groupHome.id); // Angezeigte Liste auf Hauptseite neu laden, wenn Modal geschlossen 
            });

        };

        /*Oeffnen des Dialoges zum hinzufügen eines neuen Users*/
        $GRP.data.openAddUserDialog = function() {
            var ctrlDetails = this;
            ctrlDetails.groupDetail = [];
            var options;

            if (['C', 'A'].indexOf($GRP.groupHome.rights.gruppen_admin) > -1 || ['A'].indexOf($GRP.groupHome.rights.alarm_admin) > -1) {
                options = {
                    animation: true,
                    backdrop: "static",
                    templateUrl: "/partials/admin-pages/AddUserDialog.html",
                    controller: "AddUserDialogController as AddUserCtrl",
                    size: "lg",
                    resolve: {
                        details: function() {
                            return [];
                        },
                        permissions: function() {
                            return $GRP.groupHome.rights;
                        },
                        parentid: function() {
                            return $GRP.groupHome.id;
                        },
                        grplist: function($http) {
                            if ($GRP.groupHome.rights.alarm_super_admin == "A") {
                                return $http.post('admin/grouplist').then(function(response) {
                                    return ('status' in response.data) ? {} : response.data.grouplist;
                                });
                            } else {
                                return [{ "id": $GRP.groupHome.id, "gruppenname": $GRP.groupHome.gruppenname }];
                            }
                        }
                    }
                };
            } else {
                options = {
                    animation: true,
                    backdrop: "static",
                    templateUrl: "/partials/admin-pages/EditViewUserDialog.html",
                    controller: "EditViewUserDialogController as EVUserCtrl",
                    size: "",
                    resolve: {
                        details: function() {
                            return [];
                        },
                        permissions: function() {
                            return [];
                        },
                        parentid: function() {
                            return $GRP.groupHome.id;
                        },
                        grplist: function($http) {
                            if ($GRP.groupHome.rights.alarm_super_admin == "A") {
                                return $http.post('admin/grouplist').then(function(response) {
                                    return ('status' in response.data) ? {} : response.data.grouplist;
                                });
                            } else {
                                return [{ "id": $GRP.groupHome.id, "gruppenname": $GRP.groupHome.gruppenname }];
                            }
                        }
                    }
                };
            }
            var modalInstance = $uibModal.open(options);
            modalInstance.result.finally(function() {
                loadGroupViewList($GRP.groupHome.id); //Noch aktuelle Gruppe einfügen
            }); // Angezeigte Liste auf Hauptseite neu laden, wenn Modal geschlossen 
        };

        /*Oeffnen des Dialoges zum editieren/view eines neuen Users*/
        $GRP.data.openEditViewUserDialog = function(item) {
            var ctrlDetails = this;
            ctrlDetails.userDetail = [];

            var param = { "userid": item.id };

            // Laden der Nutzerinformaionen
            $http.post('admin/userdetail', param)
                .success(function(response) {
                    ctrlDetails.userDetail = response.userdetail;
                    var options = {
                        animation: true,
                        backdrop: "static",
                        templateUrl: "/partials/admin-pages/EditViewUserDialog.html",
                        controller: "EditViewUserDialogController as EVUserCtrl",
                        size: "",
                        resolve: {
                            details: function() {
                                return ctrlDetails.userDetail;
                            },
                            permissions: function() {
                                return $GRP.groupHome.rights.user_admin;
                            },
                            parentid: function() {
                                return $GRP.groupHome.id;
                            },
                            grplist: function($http) {
                                if ($GRP.groupHome.rights.alarm_super_admin == "A") {
                                    return $http.post('admin/grouplist').then(function(response) {
                                        return ('status' in response.data) ? {} : response.data.grouplist;
                                    });
                                } else {
                                    return [{ "id": $GRP.groupHome.id, "gruppenname": $GRP.groupHome.gruppenname }];
                                }
                            }
                        }
                    };
                    var modalInstance = $uibModal.open(options);
                    modalInstance.result.finally(function() {
                        loadGroupViewList($GRP.groupHome.id);
                    });
                })
                //ToDo    //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.get_userdetails') + ' ( ID:' + item.id + ', ' + $translate.instant('message.name') + ':' + item.benutzername + ') ' + $translate.instant('message.failed') + '.');
                });
        };

        /*Oeffnen des Userlog Modal*/
        $GRP.data.openUserlogDialog = function(gruppe) {

            if (gruppe) {
                var end = new Date();
                end.setDate(end.getDate() + 1);
                var start = new Date();
                start.setMonth(start.getMonth() - 3);
                var param = {
                    "group": gruppe.id,
                    "start": start.toLocaleDateString('de-DE'),
                    "stop": end.toLocaleDateString('de-DE')
                };
                $http.post('report/showlog', param)
                    .success(function(response) {
                        $GRP.userlog = response.answer;
                        var options = {
                            animation: true,
                            backdrop: "static",
                            templateUrl: "/partials/admin-pages/UserlogDialog.html",
                            controller: "UserlogDialogController as UserlogCtrl",
                            size: "sm",
                            resolve: {
                                details: function() {
                                    var data = {
                                        "answer": $GRP.userlog,
                                        "count": response.count,
                                        "gruppe": gruppe,
                                        "start": start,
                                        "stop": end
                                    };
                                    return data;
                                }
                            }
                        };
                        $uibModal.open(options).result.finally(function() {});
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        console.debug('Getting log for ID:' + item + ' failed.');
                    });
            }
        };


    });

    blids.controller('UserlogDialogController', function($http, $uibModalInstance, details, ModalService, $translate) {
        var $Userlog = this;
        $Userlog.modalData = {};
        $Userlog.modalData.gruppe = details.gruppe;
        $Userlog.modalData.modalTitle = $translate.instant('admin.userlog') + ' ' + $translate.instant('admin.for') + ' ' + $translate.instant('admin.user') + ': ' + $Userlog.modalData.gruppe.gruppenname;
        $Userlog.modalData.modalDescription = "";
        $Userlog.modalData.orderBy = "id";
        $Userlog.modalData.reverseOrder = "false";
        $Userlog.modalData.data = details.answer;
        $Userlog.modalData.count = details.count;
        $Userlog.modalData.start = details.start;
        $Userlog.modalData.stop = details.stop;


        $Userlog.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $Userlog.modalData.orderLog = function(orderBy) {
            $Userlog.modalData.reverseOrder = ($Userlog.modalData.orderBy === orderBy) ? !$Userlog.modalData.reverseOrder : false;
            $Userlog.modalData.orderBy = orderBy;
        };

        $Userlog.open1 = function() {
            $Userlog.popup1.opened = true;
        };

        $Userlog.open2 = function() {
            $Userlog.popup2.opened = true;
        };

        $Userlog.refresh = function() {
            var param = {
                "group": $Userlog.modalData.gruppe.id,
                "start": $Userlog.modalData.start.toLocaleDateString('de-DE'),
                "stop": $Userlog.modalData.stop.toLocaleDateString('de-DE')
            };
            $http.post('report/showlog', param)
                .success(function(response) {
                    $Userlog.modalData.data = response.answer;
                    $Userlog.modalData.count = response.count;
                })
                .error(function(response) {
                    console.debug("Error :" + response);
                });
        };

        $Userlog.popup1 = { opened: false };
        $Userlog.popup2 = { opened: false };


    });


    // Kombinierter Add-Edit-View Controller  -  Gruppe
    blids.controller('GruppeController', function($http, $uibModalInstance, PARENTdetails, details, permissions, call, head, ModalService, $translate) {
        var $Group = this;
        var art, title, boxstring, PARENTboxstring;
        var isinit = false;
        var BoxLonMin, BoxLatMin, BoxLonMax, BoxLatMax, BoxCenterLon, BoxCenterLat;
        var PARENTBoxLonMin, PARENTBoxLatMin, PARENTBoxLonMax, PARENTBoxLatMax, PARENTBoxCenterLon, PARENTBoxCenterLat;
        //-------------------//details und permission für EDIT
        // Art des Aufrufes
        if (call === 'Add') {
            art = "Add";
            title = $translate.instant('admin.add_group');
            button = $translate.instant('admin.add_group');
        } else {
            switch (permissions.gruppen_admin) {
                case "A":
                    art = "All";
                    title = $translate.instant('admin.edit_group') + ": " + details.gruppenname;
                    button = $translate.instant('admin.save') + " & " + $translate.instant('admin.exit');
                    break;
                case "W":
                    art = "Edit";
                    title = $translate.instant('admin.edit_group') + ": " + details.gruppenname;
                    button = $translate.instant('admin.save') + " & " + $translate.instant('admin.exit');
                    break;
                case "R":
                    art = "View";
                    title = $translate.instant('admin.view_group') + ": " + details.gruppenname;
                    button = $translate.instant('admin.exit');
                    break;
                case "C":
                    art = "View";
                    title = $translate.instant('admin.view_group') + ": " + details.gruppenname;
                    button = $translate.instant('admin.exit');
                    break;
                default: //case "N":
                    art = "Nothing";
                    title = "Group";
                    button = $translate.instant('admin.exit');
            }
        }

        $Group.modalData = {};
        $Group.modalData.modalTitle = title;
        $Group.modalData.modalButton = button;
        $Group.modalData.modalDescription = "";

        $Group.formGroups = {
            Fenstergroessen: [{ value: '120', name: '120 x 120 km' },
                { value: '150', name: '150 x 150 km' },
                { value: '400', name: '400 x 400 km' },
                { value: '1', name: $translate.instant('admin.custom') }
            ],
            typ: art,
            spec: call,
            head: head
        };

        // Spezieller Aufruf ohne PARENT
        if (call === 'SPECIAL') {
            $Group.formGroups.PARENTlonMin = -180;
            $Group.formGroups.PARENTlatMin = -90;
            $Group.formGroups.PARENTlonMax = 180;
            $Group.formGroups.PARENTlatMax = 90;
            $Group.formGroups.PARENTliveblids = true;
            $Group.formGroups.PARENTanimation = true;
            $Group.formGroups.PARENTsound = true;
        } else {
            PARENTboxstring = PARENTdetails.blitzgebiet.split(/[( ,)]/);
            $Group.formGroups.PARENTmax_displayzeit = parseInt(PARENTdetails.max_displayzeit);
            $Group.formGroups.PARENTarchiv_tage = parseInt(PARENTdetails.archiv_tage);
            $Group.formGroups.PARENTarchiv_ab = new Date(PARENTdetails.archiv_ab.substr(0, 10));
            $Group.formGroups.PARENTmax_zoom = parseInt(PARENTdetails.max_zoom);
            $Group.formGroups.PARENTmin_zoom = parseInt(PARENTdetails.min_zoom);
            $Group.formGroups.PARENTliveblids = PARENTdetails.liveblids === 't' ? true : false;
            $Group.formGroups.PARENTsound = PARENTdetails.sound === 't' ? true : false;
            $Group.formGroups.PARENTanimation = PARENTdetails.animation === 't' ? true : false;
            PARENTBoxLonMin = parseFloat(PARENTboxstring[1]);
            PARENTBoxLatMin = parseFloat(PARENTboxstring[2]);
            PARENTBoxLonMax = parseFloat(PARENTboxstring[3]);
            PARENTBoxLatMax = parseFloat(PARENTboxstring[4]);
            PARENTBoxCenterLon = (PARENTBoxLonMax + PARENTBoxLonMin) / 2;
            PARENTBoxCenterLat = (PARENTBoxLatMax + PARENTBoxLatMin) / 2;
        }

        // Aufruf per EDIT oder VIEW
        if (art === "All" || art === "Edit" || art === "View") {
            boxstring = details.blitzgebiet.split(/[( ,)]/);
            $Group.formGroups.id = details.id;
            $Group.formGroups.vatergruppenid = details.vatergruppenid;
            $Group.formGroups.gruppenname = details.gruppenname;
            $Group.formGroups.theme_id = details.theme_id;
            $Group.formGroups.max_displayzeit = parseInt(details.max_displayzeit);
            $Group.formGroups.archiv_tage = parseInt(details.archiv_tage);
            $Group.formGroups.archiv_ab = new Date(details.archiv_ab.substr(0, 10));
            $Group.formGroups.max_zoom = parseInt(details.max_zoom);
            $Group.formGroups.min_zoom = parseInt(details.min_zoom);
            $Group.formGroups.liveblids = details.liveblids;
            $Group.formGroups.sound = details.sound;
            $Group.formGroups.animation = details.animation;
        } else { // Aufruf per ADD
            boxstring = PARENTdetails.blitzgebiet.split(/[( ,)]/);
            $Group.formGroups.vatergruppenid = PARENTdetails.id;
            $Group.formGroups.theme_id = PARENTdetails.theme_id;
            $Group.formGroups.max_displayzeit = parseInt(PARENTdetails.max_displayzeit);
            $Group.formGroups.archiv_tage = parseInt(PARENTdetails.archiv_tage);
            $Group.formGroups.archiv_ab = new Date(PARENTdetails.archiv_ab.substr(0, 10));
            $Group.formGroups.max_zoom = parseInt(PARENTdetails.max_zoom);
            $Group.formGroups.min_zoom = parseInt(PARENTdetails.min_zoom);
            $Group.formGroups.liveblids = PARENTdetails.liveblids;
            $Group.formGroups.sound = PARENTdetails.sound;
            $Group.formGroups.animation = PARENTdetails.animation;
        }

        $Group.formGroups.selectedFenstergroesse = { value: '1', name: 'custom' };
        // Aufbereiten der Eingangsdaten für Eingabefelder und Berechnung der Karte
        BoxLonMin = parseFloat(boxstring[1]);
        BoxLatMin = parseFloat(boxstring[2]);
        BoxLonMax = parseFloat(boxstring[3]);
        BoxLatMax = parseFloat(boxstring[4]);
        BoxCenterLon = (BoxLonMax + BoxLonMin) / 2;
        BoxCenterLat = (BoxLatMax + BoxLatMin) / 2;
        $Group.formGroups.laenge = BoxCenterLon.toFixed(6);
        $Group.formGroups.breite = BoxCenterLat.toFixed(6);
        $Group.formGroups.lonMin = BoxLonMin.toFixed(6);
        $Group.formGroups.latMin = BoxLatMin.toFixed(6);
        $Group.formGroups.lonMax = BoxLonMax.toFixed(6);
        $Group.formGroups.latMax = BoxLatMax.toFixed(6);



        // Absenden des Formulars
        $Group.modalData.submit = function() {
            var param;
            if (art === "All" || art === "Edit") {
                param = ({
                    group: {
                        id: details.id,
                        gruppenname: $Group.formGroups.gruppenname,
                        theme_id: parseInt($Group.formGroups.theme_id),
                        lonmin: $Group.formGroups.lonMin,
                        lonmax: $Group.formGroups.lonMax,
                        latmin: $Group.formGroups.latMin,
                        latmax: $Group.formGroups.latMax,
                        max_displayzeit: $Group.formGroups.max_displayzeit,
                        archiv_tage: $Group.formGroups.archiv_tage,
                        archiv_ab: $Group.formGroups.archiv_ab,
                        max_zoom: $Group.formGroups.max_zoom,
                        min_zoom: $Group.formGroups.min_zoom,
                        liveblids: $Group.formGroups.liveblids,
                        animation: $Group.formGroups.animation,
                        sound: $Group.formGroups.sound
                    }
                });
                //                console.debug('param: ', param);
                $http.post('admin/updategroup', param)
                    .success(function(response) {
                        //                        console.debug('Response:', response);
                        // Modal schließen
                        $uibModalInstance.dismiss();
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.update_failed') + ': ' + param.group.id + ' .\nResponse: ' + response);
                    });
            } else { // Aufruf als ADD
                param = ({
                    group: {
                        vatergruppe: PARENTdetails.id,
                        gruppenname: $Group.formGroups.gruppenname,
                        theme_id: parseInt($Group.formGroups.theme_id),
                        lonmin: $Group.formGroups.lonMin,
                        lonmax: $Group.formGroups.lonMax,
                        latmin: $Group.formGroups.latMin,
                        latmax: $Group.formGroups.latMax,
                        max_displayzeit: $Group.formGroups.max_displayzeit,
                        archiv_tage: $Group.formGroups.archiv_tage,
                        archiv_ab: $Group.formGroups.archiv_ab,
                        max_zoom: $Group.formGroups.max_zoom,
                        min_zoom: $Group.formGroups.min_zoom,
                        liveblids: $Group.formGroups.liveblids,
                        animation: $Group.formGroups.animation,
                        sound: $Group.formGroups.sound
                    }
                });
                //                console.debug('param: ', param);
                $http.post('admin/addgroup', param)
                    .success(function(response) {
                        //                        console.debug('Response:', response);
                        // Modal schließen
                        $uibModalInstance.dismiss();
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.add_failed') + ' ' + $translate.instant('admin.for') + ' ' + $translate.instant('admin.group') + ': ' + param.group.gruppenname + ' .\nResponse: ' + response);
                    });
            }
        };

        // Formulareingabe Abbrechen
        $Group.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $Group.modalData.delete = function() {
            ModalService.confirm($translate.instant('message.sure')).then(function(response) {
                var param = { "groupid": details.id };
                // Abfragen, od die Gruppe noch Unterelemente entaelt
                $http.post('admin/groupview', param)
                    .success(function(response) {
                        var children = response.children;
                        var users = response.users;
                        if (children !== undefined || users !== undefined) {
                            ModalService.alert("danger", $translate.instant('message.group_not_empty'));
                        } else {
                            // Löschen der Gruppe
                            $http.post('admin/removegroup', param)
                                .success(function(response) {
                                    $uibModalInstance.dismiss();
                                })
                                .error(function(response) {
                                    ModalService.alert('danger', $translate.instant('message.delete_failed') + ': ' + details.gruppenname + ' .\nResponse :' + response);
                                });
                        }
                    })
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.getting_groupview') + ' ( ID:' + details.id + ') ' + $translate.instant('message.failed') + '.');
                    });
            });

        };

        //////////   Funktionen

        var fromProjection = new OpenLayers.Projection("EPSG:4326"); // transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection (Openstreetmap)
        var googleProjection = new OpenLayers.Projection("EPSG:3875"); // to Spherical Mercator Projection (Openstreetmap)
        //        var Ctrl = this;
        var map, boxLayer;

        function DegToRad(d) {
            return d * Math.PI / 180.0;
        }

        function RadToDeg(r) {
            return r * 180.0 / Math.PI;
        }

        function init() {
            var optionsMap = {
                controls: [],
                theme: null,
                units: 'm'
            };

            $Group.modalData.map = new OpenLayers.Map('minimap', optionsMap);
            var gmap = new OpenLayers.Layer.Google(
                "Google Streets", // the default
                {
                    'displayInLayerSwitcher': false,
                    sphericalMercator: true,
                    zoomOffset: 5,
                    minZoomLevel: 3,
                    maxZoomLevel: 16
                }
            );

            $Group.modalData.map.addLayer(gmap);
            $Group.modalData.map.addControl(new OpenLayers.Control.MousePosition({ 'displayProjection': 'EPSG:4326' }));

            boxLayer = new OpenLayers.Layer.Vector("Box layer");
            $Group.modalData.map.addLayer(boxLayer);

            isinit = true;

        }

        function drawPolygons() {
            var laenge, breite;
            if ($Group.formGroups.selectedFenstergroesse.value !== '1') {
                laenge = parseFloat($Group.formGroups.laenge.toString().replace(/,/, "."));
            } else {
                laenge = (parseFloat($Group.formGroups.latMin.toString().replace(/,/, ".")) + parseFloat($Group.formGroups.latMax.toString().replace(/,/, "."))) / 2;
            }
            if ($Group.formGroups.selectedFenstergroesse.value !== '1') {
                breite = parseFloat($Group.formGroups.breite.toString().replace(/,/, "."));
            } else {
                breite = (parseFloat($Group.formGroups.lonMin.toString().replace(/,/, ".")) + parseFloat($Group.formGroups.lonMax.toString().replace(/,/, "."))) / 2;
            }

            var fenstergroessein = parseFloat($Group.formGroups.selectedFenstergroesse.value);
            //            var fenstergroesse = fenstergroessein * 1000;            
            var center = OpenLayers.LonLat.fromString(laenge + "," + breite);
            var zoomlevel = 5;
            boxLayer.removeAllFeatures();
            $Group.modalData.map.setCenter(center.transform(fromProjection, toProjection), zoomlevel);
            var origin_xy = new OpenLayers.Geometry.Point(laenge, breite);
            origin_xy.transform(fromProjection, toProjection);

            var rad = fenstergroessein / 2;
            var zugabe = 10; //km für Fensterrand
            var kmProLat = 111;
            var kmProLon = (2 * Math.PI * 6371 * Math.cos(DegToRad(breite))) / 360;
            var lonmin, lonmax, latmin, latmax, lonminAnzeige, lonmaxAnzeige, latminAnzeige, latmaxAnzeige;

            if ($Group.formGroups.selectedFenstergroesse.value !== '1') {
                lonmin = laenge - (rad / kmProLon);
                lonmax = laenge + (rad / kmProLon);
                latmin = breite - (rad / kmProLat);
                latmax = breite + (rad / kmProLat);

            } else { //custom-Fenstergroesse einlesen
                lonmin = parseFloat($Group.formGroups.lonMin.toString().replace(/,/, "."));
                lonmax = parseFloat($Group.formGroups.lonMax.toString().replace(/,/, "."));
                latmin = parseFloat($Group.formGroups.latMin.toString().replace(/,/, "."));
                latmax = parseFloat($Group.formGroups.latMax.toString().replace(/,/, "."));
            }

            //Setzen der erlaubten Aussenkanten  (vor allem fuer Eingabe von Longitude/Latitude)
            // LONMIN
            if (lonmin > PARENTBoxLonMax) {
                lonmin = PARENTBoxLonMax;
            } else if (lonmin < PARENTBoxLonMin) {
                lonmin = PARENTBoxLonMin;
            }
            // LONMAX
            if (lonmax > PARENTBoxLonMax) {
                lonmax = PARENTBoxLonMax;
            } else if (lonmax < PARENTBoxLonMin) {
                lonmax = PARENTBoxLonMin;
            }
            // LATMIN
            if (latmin > PARENTBoxLatMax) {
                latmin = PARENTBoxLatMax;
            } else if (latmin < PARENTBoxLatMin) {
                latmin = PARENTBoxLatMin;
            }
            // LATMAX
            if (latmax > PARENTBoxLatMax) {
                latmax = PARENTBoxLatMax;
            } else if (latmax < PARENTBoxLatMin) {
                latmax = PARENTBoxLatMin;
            }
            // Ausenkannten für Anzeige berechnen
            lonminAnzeige = lonmin - (zugabe / kmProLon);
            lonmaxAnzeige = lonmax + (zugabe / kmProLon);
            latminAnzeige = latmin - (zugabe / kmProLat);
            latmaxAnzeige = latmax + (zugabe / kmProLat);
            // Ueberpruefung der aeuseren Anzeigewerte
            // LONMIN
            if (lonminAnzeige > 180) {
                lonminAnzeige = 180;
            } else if (lonminAnzeige < -180) {
                lonminAnzeige = -180;
            }
            // LONMAX
            if (lonmaxAnzeige > 180) {
                lonmaxAnzeige = 180;
            } else if (lonmaxAnzeige < -180) {
                lonmaxAnzeige = -180;
            }
            // LATMIN
            if (latminAnzeige > 90) {
                latminAnzeige = 90;
            } else if (latminAnzeige < -90) {
                latminAnzeige = -90;
            }
            // LATMAX
            if (latmaxAnzeige > 90) {
                latmaxAnzeige = 90;
            } else if (latmaxAnzeige < -90) {
                latmaxAnzeige = -90;
            }


            $Group.formGroups.lonMin = lonmin.toFixed(6);
            $Group.formGroups.lonMax = lonmax.toFixed(6);
            $Group.formGroups.latMin = latmin.toFixed(6);
            $Group.formGroups.latMax = latmax.toFixed(6);


            var left_x = lonminAnzeige;
            var right_x = lonmaxAnzeige;
            var top_y = latmaxAnzeige;
            var bottom_y = latminAnzeige;
            var fensterVorschau = new OpenLayers.Geometry.LinearRing();
            fensterVorschau.addComponent(new OpenLayers.Geometry.Point(left_x, bottom_y).transform(fromProjection, toProjection), 0);
            fensterVorschau.addComponent(new OpenLayers.Geometry.Point(left_x, top_y).transform(fromProjection, toProjection), 1);
            fensterVorschau.addComponent(new OpenLayers.Geometry.Point(right_x, top_y).transform(fromProjection, toProjection), 2);
            fensterVorschau.addComponent(new OpenLayers.Geometry.Point(right_x, bottom_y).transform(fromProjection, toProjection), 3);

            var fensterStyle = { fillOpacity: 0.3 };
            var fensterVorschauFeature = new OpenLayers.Feature.Vector(fensterVorschau, null, fensterStyle);
            $Group.modalData.map.zoomToExtent(fensterVorschau.getBounds());
            boxLayer.addFeatures([fensterVorschauFeature]);
            return fensterVorschau.getBounds().transform(toProjection, fromProjection).toString();
        }

        //////////   an Elementen hinterlegte Funktionen

        $Group.formGroups.preview = function() {
            if (isinit === false) {
                init();
            }
            drawPolygons();
        };

        $Group.formGroups.positionEingabe = function() {
            freigabePreview();
        };

        // Funktion, die die Freigabe des Previewbutton regelt und zugleich die Maximalwerte auf die Felder abbildet
        function freigabePreview() {
            $Group.formGroups.previewable = false;
            var FP_laenge = $Group.formGroups.laenge;
            if (FP_laenge !== null) {
                FP_laenge = parseFloat(FP_laenge.toString().replace(/,/, "."));
                if (FP_laenge > PARENTBoxLonMax) {
                    $Group.formGroups.laenge = PARENTBoxLonMax.toFixed(6);
                } else if (FP_laenge < PARENTBoxLonMin) {
                    $Group.formGroups.laenge = PARENTBoxLonMin.toFixed(6);
                } else {
                    $Group.formGroups.laenge = FP_laenge.toFixed(6);
                }
            }
            var FP_breite = $Group.formGroups.breite;
            if (FP_breite !== null) {
                FP_breite = parseFloat(FP_breite.toString().replace(/,/, "."));
                if (FP_breite > PARENTBoxLatMax) {
                    $Group.formGroups.breite = PARENTBoxLatMax.toFixed(6);
                } else if (FP_breite < PARENTBoxLatMin) {
                    $Group.formGroups.breite = PARENTBoxLatMin.toFixed(6);
                } else {
                    $Group.formGroups.breite = FP_breite.toFixed(6);
                }
            }
            var FP_latmin = $Group.formGroups.latMin;
            if (FP_latmin !== null) {
                FP_latmin = parseFloat(FP_latmin.toString().replace(/,/, "."));
                if (FP_latmin > PARENTBoxLatMax) {
                    FP_latmin = PARENTBoxLatMax;
                } else if (FP_latmin < PARENTBoxLatMin) {
                    FP_latmin = PARENTBoxLatMin;
                    $Group.formGroups.latMin = FP_latmin.toFixed(6);
                } else {
                    $Group.formGroups.latMin = FP_latmin.toFixed(6);
                }
            }
            var FP_latmax = $Group.formGroups.latMax;
            if (FP_latmax !== null) {
                FP_latmax = parseFloat(FP_latmax.toString().replace(/,/, "."));
                if (FP_latmax > PARENTBoxLatMax) {
                    FP_latmax = PARENTBoxLatMax;
                } else if (FP_latmax < PARENTBoxLatMin) {
                    FP_latmax = PARENTBoxLatMin;
                    $Group.formGroups.latMax = FP_latmax.toFixed(6);
                } else {
                    $Group.formGroups.latMax = FP_latmax.toFixed(6);
                }
            }
            var FP_lonmin = $Group.formGroups.lonMin;
            if (FP_lonmin !== null) {
                FP_lonmin = parseFloat(FP_lonmin.toString().replace(/,/, "."));
                if (FP_lonmin > PARENTBoxLonMax) {
                    FP_lonmin = PARENTBoxLonMax;
                } else if (FP_lonmin < PARENTBoxLonMin) {
                    FP_lonmin = PARENTBoxLonMin;
                    $Group.formGroups.lonMin = FP_lonmin.toFixed(6);
                } else {
                    $Group.formGroups.lonMin = FP_lonmin.toFixed(6);
                }
            }
            var FP_lonmax = $Group.formGroups.lonMax;
            if (FP_lonmax !== null) {
                FP_lonmax = parseFloat(FP_lonmax.toString().replace(/,/, "."));
                if (FP_lonmax > PARENTBoxLonMax) {
                    FP_lonmax = PARENTBoxLonMax;
                } else if (FP_lonmax < PARENTBoxLonMin) {
                    FP_lonmax = PARENTBoxLonMin;
                    $Group.formGroups.lonMax = FP_lonmax.toFixed(6);
                } else {
                    $Group.formGroups.lonMax = FP_lonmax.toFixed(6);
                }
            }


            //Wenn NICHT CUSTOM, dann BREITE und LAENGE pruefen            
            if ($Group.formGroups.selectedFenstergroesse.value !== '1' && !isNaN(FP_laenge) && !isNaN(FP_breite)) {
                $Group.formGroups.previewable = true;
            }
            //Wenn CUSTOM, dann LATITUDEMIN/-MAX und LONGITUDEMIN/-MAX pruefen
            else if ($Group.formGroups.selectedFenstergroesse.value === '1') {
                var lonNum = false;
                var latNum = false;
                $Group.formGroups.lonfalse = false;
                $Group.formGroups.latfalse = false;
                if (!isNaN(FP_lonmin) && !isNaN(FP_lonmax)) {
                    lonNum = true;
                    if (FP_lonmin > FP_lonmax) {
                        $Group.formGroups.lonfalse = true;
                        lonNum = false; //wieder als fehlerhaft setzen, trotz jeweils numerisch, ersparrt weitere Abfrage
                    } //wenn nichts eingegeben, keine Anzeige von lonfalse 
                }
                if (!isNaN(FP_latmin) && !isNaN(FP_latmax)) {
                    latNum = true;
                    if (FP_latmin > FP_latmax) {
                        $Group.formGroups.latfalse = true;
                        latNum = false; //wieder als fehlerhaft setzen, trotz jeweils numerisch, ersparrt weitere Abfrage
                    } //wenn nichts eingegeben, keine Anzeige von latfalse
                }
                if (lonNum === true && latNum === true) {
                    $Group.formGroups.previewable = true;
                }
            } else {
                $Group.formGroups.previewable = false;
            }
        }
        freigabePreview();

        $Group.formGroups.sizeChange = function() {
            if ($Group.formGroups.selectedFenstergroesse !== { value: '1', name: 'custom' }) {
                $Group.formGroups.lonfalse = false; // da Werte Berechnet werden, ist die eingabe irrelevant
                $Group.formGroups.latfalse = false; // da Werte Berechnet werden, ist die eingabe irrelevant
            }
            freigabePreview();
        };

        $Group.formGroups.latlonChange = function() {
            $Group.formGroups.selectedFenstergroesse = { value: '1', name: 'custom' };
        };

        $Group.formGroups.zoomChange = function() {
            if ($Group.formGroups.max_zoom < $Group.formGroups.min_zoom) {
                $Group.formGroups.zoomfalse = true;
            } else {
                $Group.formGroups.zoomfalse = false;
            }
        };

        $Group.modalData.initialisiere = function() {
            init();
        };


        var locitem;
        $Group.modalData.getLocation = function(val) {
            return $http.post('https://api.blids.de/v1/geocode', { address: val, sensor: false }).then(function(response) {
                return response.data.map(function(item) {
                    return item;
                });
            });
        };

        $Group.modalData.selectLocation = function(item) {
            $Group.formGroups.laenge = item.geometry.location.lng.toFixed(6);
            $Group.formGroups.breite = item.geometry.location.lat.toFixed(6);
            freigabePreview();
        };


    });

    // Edit-View Controller  -  User
    blids.controller('EditViewUserDialogController', function($http, $uibModalInstance, UserService, details, permissions, parentid, ModalService, grplist, $translate) {
        var $EVUser = this;
        var art, type, oldHomeGroup, title;

        title = $translate.instant('admin.edit_user');
        switch (permissions) {
            case "A":
                art = "All";
                break;
            case "W":
                art = "Edit";
                break;
            case "R":
                art = "View";
                title = $translate.instant('admin.view_user');
                break;
            default:
                art = "Add";
                title = $translate.instant('admin.add_user');
        }
        type = (art = "All") ? "Edit" : art;

        $EVUser.modalData = {};
        $EVUser.modalData.modalTitle = title;
        $EVUser.modalData.modalDescription = "";

        if (art !== "Add") {
            $EVUser.formEVUsers = {
                typ: art,
                id: details.id,
                firstname: details.firstname,
                lastname: details.lastname,
                company: details.company,
                division: details.division,
                streetaddress: details.streetaddress,
                zip: details.zip,
                city: details.city,
                phonenumber: details.phonenumber,
                annotation: details.annotation,
                startdatum: new Date(details.startdatum.substr(0, 10)),
                stopdatum: new Date(details.stopdatum.substr(0, 10)),
                benutzername: details.benutzername,
                home_gruppe: details.home_gruppen_id,
                home_gruppen: grplist
            };
            oldHomeGroup = details.home_gruppen_id;
        } else { //als ADD
            $EVUser.formEVUsers = {
                typ: art,
                startdatum: new Date(),
                stopdatum: new Date(new Date().setYear(new Date().getFullYear() + 1)),
                home_gruppe: parentid,
                home_gruppen: grplist
            };
        }

        //Standardwerte vorgeben
        $EVUser.formEVUsers.pwsEdit = true;
        $EVUser.formEVUsers.pwsok = false;
        $EVUser.formEVUsers.pwsNok = false;
        $EVUser.formEVUsers.groupadmin = Array.isArray(grplist);

        //ToDo
        $EVUser.modalData.submit = function() {
            var param;
            if (art !== "Add") {
                param = {
                    user: {
                        id: details.id,
                        title: $EVUser.formEVUsers.title,
                        firstname: $EVUser.formEVUsers.firstname,
                        lastname: $EVUser.formEVUsers.lastname,
                        company: $EVUser.formEVUsers.company,
                        division: $EVUser.formEVUsers.division,
                        streetaddress: $EVUser.formEVUsers.streetaddress,
                        zip: $EVUser.formEVUsers.zip,
                        city: $EVUser.formEVUsers.city,
                        phonenumber: $EVUser.formEVUsers.phonenumber,
                        annotation: $EVUser.formEVUsers.annotation,
                        startdatum: $EVUser.formEVUsers.startdatum,
                        stopdatum: $EVUser.formEVUsers.stopdatum,
                        benutzername: $EVUser.formEVUsers.benutzername,
                        passwort: $EVUser.formEVUsers.password1,
                        home_gruppen_id: $EVUser.formEVUsers.home_gruppe
                    }
                };
                //            console.debug('param: ', param);
                $http.post('admin/updateuser', param)
                    .success(function(response) {
                        param = {
                            permission: {
                                userid: details.id,
                                groupid: $EVUser.formEVUsers.home_gruppe,
                                roleid: 1,
                                oldgroupid: oldHomeGroup,
                                oldroleid: 1
                            }
                        };
                        if ($EVUser.formEVUsers.home_gruppe != oldHomeGroup) {
                            $http.post('admin/updatepermission', param).then(function(response) {
                                //                    console.debug('Response:', response);    
                                $uibModalInstance.dismiss();
                            }, function(response) {
                                ModalService.alert('danger', $translate.instant('message.update_user_failed') + ': ' + details.id + '.\nResponse: ' + response);
                            });
                        } else { $uibModalInstance.dismiss(); }
                    })
                    //ToDo
                    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.update_failed') + ': ' + details.id + '.\nResponse: ' + response);
                    });

                //CHECK 
            } else { //aufruf als ADD
                param = {
                    user: {
                        title: $EVUser.formEVUsers.title,
                        firstname: $EVUser.formEVUsers.firstname,
                        lastname: $EVUser.formEVUsers.lastname,
                        company: $EVUser.formEVUsers.company,
                        division: $EVUser.formEVUsers.division,
                        streetaddress: $EVUser.formEVUsers.streetaddress,
                        zip: $EVUser.formEVUsers.zip,
                        city: $EVUser.formEVUsers.city,
                        phonenumber: $EVUser.formEVUsers.phonenumber,
                        annotation: $EVUser.formEVUsers.annotation,
                        startdatum: $EVUser.formEVUsers.startdatum,
                        stopdatum: $EVUser.formEVUsers.stopdatum,
                        benutzername: $EVUser.formEVUsers.benutzername,
                        passwort: $EVUser.formEVUsers.password1,
                        home_gruppen_id: parentid
                    }
                };
                $http.post('admin/adduser', param)
                    .success(function(response) {
                        $uibModalInstance.dismiss();
                    })
                    //ToDo//check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.add_failed') + '. \nResponse: ' + response);
                    });
            }
        };

        $EVUser.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $EVUser.modalData.delete = function() {
            ModalService.confirm($translate.instant('message.sure')).then(function(response) {
                var param = { userid: details.id, groupid: parentid };
                //                console.debug('param: ', param);
                $http.post('admin/removeuser', param)
                    .success(function(response) {
                        //                    console.debug('User: ',details.username,' has been deleted!', 'Response:', response);
                        $uibModalInstance.dismiss();
                    })
                    //ToDo//check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.delete_failed') + ': ' + details.username + ' .\nResponse: ' + response);
                    });
            });
        };


        //Funktionen
        $EVUser.modalData.checkDatumVorDatum = function() {
            var datumStart = $EVUser.formEVUsers.startdatum;
            var datumEnde = $EVUser.formEVUsers.stopdatum;
            var result = false;
            result = UserService.checkDvD(datumStart, datumEnde);

            if (result === false) {
                $EVUser.formEVUsers.datumVor = true;
            } else {
                $EVUser.formEVUsers.datumVor = false;
            }
        };

        $EVUser.modalData.checkPW = function() {

            if (typeof $EVUser.formEVUsers.password1 !== 'string' || typeof $EVUser.formEVUsers.password2 !== 'string') {
                $EVUser.formEVUsers.pwsEdit = true;
                $EVUser.formEVUsers.pwsok = false;
                $EVUser.formEVUsers.pwsNok = false;
            } else {
                if ($EVUser.formEVUsers.password1 === $EVUser.formEVUsers.password2) {
                    $EVUser.formEVUsers.pwsEdit = true;
                    $EVUser.formEVUsers.pwsok = true;
                    $EVUser.formEVUsers.pwsNok = false;
                } else {
                    $EVUser.formEVUsers.pwsEdit = false;
                    $EVUser.formEVUsers.pwsok = false;
                    $EVUser.formEVUsers.pwsNok = true;
                }
            }
        };
    });

    // Add Controller   -   User
    blids.controller('AddUserDialogController', function($uibModalInstance, $http, UserService, details, permissions, parentid, ModalService, $translate) {
        var $AddUser = this;
        var boxstring, min_zoom;
        var isinit = false;
        var lonMin, lonMax, latMin, latMax;
        var PARENTBoxLonMin, PARENTBoxLatMin, PARENTBoxLonMax, PARENTBoxLatMax;
        var laenge, breite;
        //        siehe weiter unten
        //        var kmProLat = 111;
        //        var kmProLon = (2 * Math.PI * 6371 * Math.cos(DegToRad(breite))) / 360;
        var lonmin, lonmax, latmin, latmax;

        $AddUser.modalData = {
            modalTitle: $translate.instant('admin.add_user'),
            modalDescription: "",
            PARENTdetails: {}
        };

        $AddUser.formAddUsers = {
            //            home_gruppen: grouplist,
            perm_gruppen: permissions.gruppen_admin,
            perm_alarm: permissions.alarm_admin,
            Benachrichtigungstypen: [{ id: 'email', name: 'E-Mail' },
                { id: 'sms', name: 'SMS' }
            ],
            selectedTyp: { id: 'email', name: 'E-Mail' },
            Fenstergroessen: [{ value: '120', name: '120 x 120 km' },
                { value: '150', name: '150 x 150 km' },
                { value: '400', name: '400 x 400 km' }
            ],
            selectedFenstergroesse: { value: '120', name: '120 x 120 km' },
            Zeitzonen: [{ value: '0', name: 'WE(S)Z (UTC+0)' },
                { value: '1', name: 'ME(S)Z (UTC+1)' }
            ],
            selectedZeitzone: { value: '1', name: 'ME(S)Z (UTC+1)' },
            Radien: [{ value: 15, name: '15km' },
                { value: 25, name: '25km' }
            ],
            selectedRadius: { value: 15, name: '15km' },
            startdatum: new Date(),
            stopdatum: new Date(new Date().setYear(new Date().getFullYear() + 1)),
            latlonlock: false
        };

        //Standardwerte vorgeben
        $AddUser.formAddUsers.previewable = false;
        $AddUser.formAddUsers.pwsok = false;
        $AddUser.formAddUsers.pwsNok = false;
        $AddUser.formAddUsers.PreviewDone = false;


        $AddUser.modalData.submit = function() {
            var group = -1;
            var param_user = {
                user: {
                    firstname: $AddUser.formAddUsers.firstname,
                    lastname: $AddUser.formAddUsers.lastname,
                    company: $AddUser.formAddUsers.company,
                    division: $AddUser.formAddUsers.division,
                    streetaddress: $AddUser.formAddUsers.streetaddress,
                    zip: $AddUser.formAddUsers.zip,
                    city: $AddUser.formAddUsers.city,
                    phonenumber: $AddUser.formAddUsers.phonenumber,
                    annotation: $AddUser.formAddUsers.annotation,
                    startdatum: $AddUser.formAddUsers.startdatum,
                    stopdatum: $AddUser.formAddUsers.stopdatum,
                    benutzername: $AddUser.formAddUsers.benutzername,
                    passwort: $AddUser.formAddUsers.password1,
                    home_gruppen_id: -1
                }
            };

            // Anlegen Gruppe
            if ($AddUser.formAddUsers.AktuellDazu) {

                calculateBorders();
                //                Grenzen berechnen
                //                var lonmin = parseFloat($AddUser.modalData.PARENTdetails.lonMin.toString().replace(/,/, "."));
                //                var lonmax = parseFloat($AddUser.modalData.PARENTdetails.lonMax.toString().replace(/,/, "."));
                //                var latmin = parseFloat($AddUser.modalData.PARENTdetails.latMin.toString().replace(/,/, "."));
                //                var latmax = parseFloat($AddUser.modalData.PARENTdetails.latMax.toString().replace(/,/, "."));
                min_zoom = (min_zoom > 1) ? min_zoom : $AddUser.modalData.PARENTdetails.min_zoom;
                zlevel = parseInt(min_zoom) + 3;
                var param_group = {
                    group: {
                        vatergruppe: parentid,
                        gruppenname: $AddUser.formAddUsers.company,
                        theme_id: parseInt($AddUser.modalData.PARENTdetails.theme_id),
                        lonmin: lonMin,
                        lonmax: lonMax,
                        latmin: latMin,
                        latmax: latMax,
                        max_displayzeit: $AddUser.modalData.PARENTdetails.max_displayzeit,
                        archiv_tage: $AddUser.modalData.PARENTdetails.archiv_tage,
                        archiv_ab: $AddUser.modalData.PARENTdetails.archiv_ab,
                        max_zoom: $AddUser.modalData.PARENTdetails.max_zoom,
                        min_zoom: zlevel,
                        liveblids: $AddUser.modalData.PARENTdetails.liveblids,
                        animation: $AddUser.modalData.PARENTdetails.animation,
                        sound: $AddUser.modalData.PARENTdetails.sound
                    }
                };
                $http.post('admin/addgroup', param_group)
                    .success(function(response) {
                        // neue parentid für Gruppe
                        group = response.result;
                        param_user.user.home_gruppen_id = group;
                        $http.post('admin/adduser', param_user)
                            .success(function(response) {
                                if ($AddUser.formAddUsers.AlarmDazu) {
                                    AlarmDazu();
                                } else {
                                    $uibModalInstance.close($translate.instant('message.user_with_group_added'));
                                }
                            })
                            .error(function(response) {
                                ModalService.alert('danger', $translate.instant('message.add_user_failed') + '. \nResponse: ' + response);
                            });
                    })
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.add_user_failed') + '.\nResponse: ' + response);
                    });

            } else {
                // ohne neue Gruppe ist home_gruppe aktuelle Gruppe
                param_user.user.home_gruppen_id = parentid;
                $http.post('admin/adduser', param_user)
                    .success(function(response) {
                        if (response.status == 'failure') {
                            console.debug("Da lief was schief");
                        } else {
                            $uibModalInstance.close($translate.instant('message.user_added_to_group'));
                        }
                    })
                    .error(function(response) {
                        ModalService.alert('danger', $translate.instant('message.add_user_failed') + '.\nResponse: ' + response);
                    });
            }


            //Anlegen Alarm
            function AlarmDazu() {
                var alarmtyp = $AddUser.formAddUsers.selectedTyp.id;
                var param_alarm = {
                    alarm: {
                        kunde: $AddUser.formAddUsers.company,
                        teilnehmer: {
                            teilnehmer: $AddUser.formAddUsers.lastname,
                            alarmierung: $translate.instant('alarm.alarmmsg'),
                            entwarnung: $translate.instant('alarm.allclearmsg'),
                            anfang: $AddUser.formAddUsers.startdatumService,
                            ende: $AddUser.formAddUsers.stopdatumService,
                            email: '',
                            sms: ''
                        },
                        gebiet: {
                            radius: $AddUser.formAddUsers.selectedRadius.value,
                            laenge: parseFloat($AddUser.formAddUsers.laenge),
                            breite: parseFloat($AddUser.formAddUsers.breite),
                            gebietname: $AddUser.formAddUsers.company,
                            blitzezumalarm: 1,
                            dauer: 30
                        }
                    },
                    'groupid': group
                };
                if (alarmtyp === 'email')
                    param_alarm.alarm.teilnehmer.email = $AddUser.formAddUsers.ben_input_mail;
                if (alarmtyp === 'sms')
                    param_alarm.alarm.teilnehmer.sms = $AddUser.formAddUsers.ben_input_sms;
                $http.post('admin/addalarm', param_alarm)
                    .then(function(response) {
                        $http.post('admin/addlayer', { "groupid": group, "layer": { "ebenen_id": 207, "id": 18 } }).then(function(response) {
                            $uibModalInstance.close($translate.instant('message.user_with_group_and_alarm_added'));
                        }, function(response) {
                            console.debug("Failed to add Alarmlayer for new group");
                        });
                    }, function(response) {
                        console.debug("Failed to add Alarm for new group");
                        ModalService.alert('danger', $translate.instant('message.add_alarm_failed') + '.\nResponse: ' + response);
                    });
            }
        };

        $AddUser.modalData.takeAddress = function() {
            $AddUser.formAddUsers.LocationSelect = "";
            if ($AddUser.formAddUsers.streetaddress) {
                $AddUser.formAddUsers.LocationSelect = $AddUser.formAddUsers.streetaddress;
            }
            if ($AddUser.formAddUsers.zip) {
                if ($AddUser.formAddUsers.LocationSelect) {
                    $AddUser.formAddUsers.LocationSelect = $AddUser.formAddUsers.LocationSelect + ', ';
                }
                $AddUser.formAddUsers.LocationSelect = $AddUser.formAddUsers.LocationSelect + $AddUser.formAddUsers.zip;
            }
            if ($AddUser.formAddUsers.city) {
                if ($AddUser.formAddUsers.LocationSelect) {
                    $AddUser.formAddUsers.LocationSelect = $AddUser.formAddUsers.LocationSelect + ' ';
                }
                $AddUser.formAddUsers.LocationSelect = $AddUser.formAddUsers.LocationSelect + $AddUser.formAddUsers.city;
            }
            //            console.debug($AddUser.formAddUsers.LocationSelect);
            document.getElementById("geosuche").focus();
        };

        //Funktionen
        $AddUser.modalData.ServiceAbwaehlen = function() {
            $AddUser.formAddUsers.AktuellDazu = $AddUser.formAddUsers.ServiceDazu;
            if ($AddUser.formAddUsers.ServiceDazu !== true) {
                //$AddUser.formAddUsers.AktuellDazu = false;
                $AddUser.formAddUsers.AlarmDazu = false;
                $AddUser.formAddUsers.latlonlock = false;
            } else {
                $AddUser.formAddUsers.startdatumService = new Date();
                $AddUser.formAddUsers.stopdatumService = new Date(new Date().setYear(new Date().getFullYear() + 1));
                //beim ersten Anwaehlen -> GruppenInfos der Vatergruppe laden, außer sie sind schon da
                if (!("id" in $AddUser.modalData.PARENTdetails)) {
                    var paramPARENT = { "groupid": parentid };
                    $http.post('admin/groupdetail', paramPARENT)
                        .success(function(responsePARENT) {
                            $AddUser.modalData.PARENTdetails = responsePARENT.groupdetail;
                            var PARENTboxstring = $AddUser.modalData.PARENTdetails.blitzgebiet.split(/[( ,)]/);
                            PARENTBoxLonMin = parseFloat(PARENTboxstring[1]);
                            PARENTBoxLatMin = parseFloat(PARENTboxstring[2]);
                            PARENTBoxLonMax = parseFloat(PARENTboxstring[3]);
                            PARENTBoxLatMax = parseFloat(PARENTboxstring[4]);
                            lonMin = PARENTBoxLonMin;
                            lonMax = PARENTBoxLonMax;
                            latMin = PARENTBoxLatMin;
                            latMax = PARENTBoxLatMax;
                        })
                        .error(function(responsePARENT) {
                            ModalService.alert('danger', $translate.instant('message.get_groupdetails') + ' ( ID:' + parentid + ') ' + $translate.instant('message.failed') + '.');
                        });
                }
            }
        };

        var locitem;
        $AddUser.modalData.getLocation = function(val) {
            return $http.post('https://api.blids.de/v1/geocode', { address: val, sensor: false }).then(function(response) {
                return response.data.map(function(item) {
                    return item;
                });
            });
        };

        $AddUser.modalData.selectLocation = function(item) {
            $AddUser.formAddUsers.laenge = item.geometry.location.lng.toFixed(6);
            $AddUser.formAddUsers.breite = item.geometry.location.lat.toFixed(6);
            $AddUser.formAddUsers.latlonlock = true;
            freigabePreview();
        };

        $AddUser.modalData.checkDatumVorDatum = function() {
            var datumStart = $AddUser.formAddUsers.startdatum;
            var datumEnde = $AddUser.formAddUsers.stopdatum;

            var result = false;

            result = UserService.checkDvD(datumStart, datumEnde);

            if (result === false) {
                $AddUser.formAddUsers.datumVor = true;
            } else {
                $AddUser.formAddUsers.datumVor = false;
            }
        };

        $AddUser.modalData.checkEmail = function() {
            if (typeof $AddUser.formAddUsers.benutzername === 'string') {
                $http.post('admin/checkemail', { 'email': $AddUser.formAddUsers.benutzername })
                    .success(function(response) {
                        $AddUser.formAddUsers.login = (response.valid > 0) ? $AddUser.formAddUsers.benutzername : "Email can not be used";
                    })
                    .error(function(response) {});
            }
        };

        $AddUser.modalData.checkPW = function() {

            if (typeof $AddUser.formAddUsers.password1 !== 'string' || typeof $AddUser.formAddUsers.password2 !== 'string') {
                $AddUser.formAddUsers.pwsok = false;
                $AddUser.formAddUsers.pwsNok = false;

            } else {
                if ($AddUser.formAddUsers.password1 === $AddUser.formAddUsers.password2) {
                    $AddUser.formAddUsers.pwsok = true;
                    $AddUser.formAddUsers.pwsNok = false;
                } else {
                    $AddUser.formAddUsers.pwsok = false;
                    $AddUser.formAddUsers.pwsNok = true;
                }
            }
        };

        $AddUser.modalData.prevEingabe = function() {
            freigabePreview();
        };

        function freigabePreview() {
            $AddUser.formAddUsers.PreviewDone = false;
            $AddUser.formAddUsers.previewable = false;
            var FP_laenge = $AddUser.formAddUsers.laenge;
            if (FP_laenge !== "" && !isNaN(FP_laenge)) {
                FP_laenge = parseFloat(FP_laenge.toString().replace(/,/, "."));
                if (FP_laenge > PARENTBoxLonMax) {
                    $AddUser.formAddUsers.laenge = PARENTBoxLonMax.toFixed(6);
                } else if (FP_laenge < PARENTBoxLonMin) {
                    $AddUser.formAddUsers.laenge = PARENTBoxLonMin.toFixed(6);
                }
            }
            laenge = parseFloat($AddUser.formAddUsers.laenge.toString().replace(/,/, "."));

            var FP_breite = $AddUser.formAddUsers.breite;
            if (FP_breite !== "" && !isNaN(FP_breite)) {
                FP_breite = parseFloat(FP_breite.toString().replace(/,/, "."));
                if (FP_breite > PARENTBoxLatMax) {
                    $AddUser.formAddUsers.breite = PARENTBoxLatMax.toFixed(6);
                } else if (FP_breite < PARENTBoxLatMin) {
                    $AddUser.formAddUsers.breite = PARENTBoxLatMin.toFixed(6);
                }
                breite = parseFloat($AddUser.formAddUsers.breite.toString().replace(/,/, "."));

            }
            var kmProLat = 111;
            var kmProLon = (2 * Math.PI * 6371 * Math.cos(DegToRad(breite))) / 360;

            if (!isNaN(FP_laenge) && !isNaN(FP_breite)) {
                $AddUser.formAddUsers.previewable = true;
            } else {
                $AddUser.formAddUsers.previewable = false;
            }
        }

        $AddUser.formAddUsers.preview = function() {
            if (isinit === false) {
                init();
            }
            drawPolygons();
        };

        var fromProjection = new OpenLayers.Projection("EPSG:4326"); // transform from WGS 1984
        var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection (Openstreetmap)
        var googleProjection = new OpenLayers.Projection("EPSG:3875"); // to Spherical Mercator Projection (Openstreetmap)
        var map, boxLayer;

        function DegToRad(d) {
            return d * Math.PI / 180.0;
        }

        function RadToDeg(r) {
            return r * 180.0 / Math.PI;
        }

        function init() {
            var optionsMap = {
                controls: [],
                theme: null,
                units: 'm'
            };

            $AddUser.modalData.map = new OpenLayers.Map('minimapAdd', optionsMap);
            var gmap = new OpenLayers.Layer.Google(
                "Google Streets", // the default
                {
                    'displayInLayerSwitcher': false,
                    sphericalMercator: true,
                    zoomOffset: 5,
                    minZoomLevel: 3,
                    maxZoomLevel: 16
                }
            );

            $AddUser.modalData.map.addLayer(gmap);
            $AddUser.modalData.map.addControl(new OpenLayers.Control.MousePosition());

            boxLayer = new OpenLayers.Layer.Vector("Box layer");
            $AddUser.modalData.map.addLayer(boxLayer);

            isinit = true;

        }



        function drawPolygons() {

            var kmProLat = 111;
            var kmProLon = (2 * Math.PI * 6371 * Math.cos(DegToRad(breite))) / 360;

            var lonminAnzeige, lonmaxAnzeige, latminAnzeige, latmaxAnzeige;
            var zugabe = 10; //km für Fensterrand


            if (laenge > 180) {
                laenge = 180;
            } else if (laenge < -180) {
                laenge = -180;
            }
            $AddUser.formAddUsers.laenge = laenge.toFixed(6);
            if (breite > 90) {
                breite = 90;
            } else if (breite < -90) {
                breite = -90;
            }
            $AddUser.formAddUsers.breite = breite.toFixed(6);

            var center = OpenLayers.LonLat.fromString(laenge + "," + breite);
            var zoomlevel = 5;
            boxLayer.removeAllFeatures();
            $AddUser.modalData.map.setCenter(center.transform(fromProjection, toProjection), zoomlevel);
            var origin_xy = new OpenLayers.Geometry.Point(laenge, breite);
            origin_xy.transform(fromProjection, toProjection);



            if (!$AddUser.formAddUsers.AktuellDazu && !$AddUser.formAddUsers.AlarmDazu) {
                lonmin = laenge;
                lonmax = laenge;
                latmin = breite;
                latmax = breite;
                lonMin = lonmin.toFixed(6);
                lonMax = lonmax.toFixed(6);
                latMin = latmin.toFixed(6);
                latMax = latmax.toFixed(6);
            } else {
                if ($AddUser.formAddUsers.AktuellDazu) {
                    calculateBorders();
                }
                if ($AddUser.formAddUsers.AlarmDazu) {
                    zugabe = zugabe + parseFloat($AddUser.formAddUsers.selectedRadius.value);
                }

            }

            // Ausenkannten für Anzeige berechnen
            lonminAnzeige = lonmin - (zugabe / kmProLon);
            lonmaxAnzeige = lonmax + (zugabe / kmProLon);
            latminAnzeige = latmin - (zugabe / kmProLat);
            latmaxAnzeige = latmax + (zugabe / kmProLat);
            // Ueberpruefung der Werte (wenn Anzeigewerte im Bereich sind, dann sind es auch die des Gebietes)
            // LONMIN
            if (lonminAnzeige > 180) {
                lonminAnzeige = 180;
            } else if (lonminAnzeige < -180) {
                lonminAnzeige = -180;
            }
            // LONMAX
            if (lonmaxAnzeige > 180) {
                lonmaxAnzeige = 180;
            } else if (lonmaxAnzeige < -180) {
                lonmaxAnzeige = -180;
            }
            // LATMIN
            if (latminAnzeige > 90) {
                latminAnzeige = 90;
            } else if (latminAnzeige < -90) {
                latminAnzeige = -90;
            }
            // LATMAX
            if (latmaxAnzeige > 90) {
                latmaxAnzeige = 90;
            } else if (latmaxAnzeige < -90) {
                latmaxAnzeige = -90;
            }

            var left_x = lonminAnzeige;
            var right_x = lonmaxAnzeige;
            var top_y = latmaxAnzeige;
            var bottom_y = latminAnzeige;
            var fensterVorschau = new OpenLayers.Geometry.LinearRing();
            fensterVorschau.addComponent(new OpenLayers.Geometry.Point(left_x, bottom_y).transform(fromProjection, toProjection), 0);
            fensterVorschau.addComponent(new OpenLayers.Geometry.Point(left_x, top_y).transform(fromProjection, toProjection), 1);
            fensterVorschau.addComponent(new OpenLayers.Geometry.Point(right_x, top_y).transform(fromProjection, toProjection), 2);
            fensterVorschau.addComponent(new OpenLayers.Geometry.Point(right_x, bottom_y).transform(fromProjection, toProjection), 3);
            min_zoom = $AddUser.modalData.map.getZoomForExtent(fensterVorschau.getBounds());
            var fensterStyle = { fillOpacity: 0.3 };
            $AddUser.modalData.map.zoomTo(min_zoom);
            //$AddUser.modalData.map.zoomToExtent(fensterVorschau.getBounds());

            if ($AddUser.formAddUsers.AktuellDazu === true) {
                $AddUser.formAddUsers.PreviewDone = true;
                var fensterVorschauFeature = new OpenLayers.Feature.Vector(fensterVorschau, null, fensterStyle);
                boxLayer.addFeatures([fensterVorschauFeature]);
            }

            if ($AddUser.formAddUsers.AlarmDazu === true) {
                var alarmradius = parseFloat($AddUser.formAddUsers.selectedRadius.value) * 1000; //ag 
                var alarmgebiet = OpenLayers.Geometry.Polygon.createRegularPolygon(origin_xy, alarmradius, 20, 0); //ag
                var alarmgebietStyle = { strokeColor: "black", strokeWidth: 2, fillColor: "yellow", fillOpacity: 0.1 }; //ag
                var alarmgebietFeature = new OpenLayers.Feature.Vector(alarmgebiet, null, alarmgebietStyle); //ag
                boxLayer.addFeatures([alarmgebietFeature]); //ag   
            }

            return fensterVorschau.getBounds().transform(toProjection, fromProjection).toString();
        }

        function calculateBorders() {
            var kmProLat = 111;
            var kmProLon = (2 * Math.PI * 6371 * Math.cos(DegToRad(breite))) / 360;

            var rad;
            rad = parseFloat($AddUser.formAddUsers.selectedFenstergroesse.value) / 2;
            lonmin = laenge - (rad / kmProLon);
            lonmax = laenge + (rad / kmProLon);
            latmin = breite - (rad / kmProLat);
            latmax = breite + (rad / kmProLat);
            //Setzen der erlaubten Aussenkanten  (vor allem fuer Eingabe von Longitude/Latitude)
            // LONMIN
            if (lonmin > PARENTBoxLonMax) {
                lonmin = PARENTBoxLonMax;
            } else if (lonmin < PARENTBoxLonMin) {
                lonmin = PARENTBoxLonMin;
            }
            // LONMAX
            if (lonmax > PARENTBoxLonMax) {
                lonmax = PARENTBoxLonMax;
            } else if (lonmax < PARENTBoxLonMin) {
                lonmax = PARENTBoxLonMin;
            }
            // LATMIN
            if (latmin > PARENTBoxLatMax) {
                latmin = PARENTBoxLatMax;
            } else if (latmin < PARENTBoxLatMin) {
                latmin = PARENTBoxLatMin;
            }
            // LATMAX
            if (latmax > PARENTBoxLatMax) {
                latmax = PARENTBoxLatMax;
            } else if (latmax < PARENTBoxLatMin) {
                latmax = PARENTBoxLatMin;
            }

            lonMin = lonmin.toFixed(6);
            lonMax = lonmax.toFixed(6);
            latMin = latmin.toFixed(6);
            latMax = latmax.toFixed(6);
        }

        $AddUser.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

    });

    //Kombinierter Add-Edit-View Controller  -  Gruppen Layer
    blids.controller('GroupLayerDialogController', function($uibModalInstance, $http, item_details, ModalService, $translate) {
        var $GroupLayer = this;
        var art;

        switch (item_details.rights.gruppen_admin) {
            case "A":
                art = "All";
                if (item_details.id === '1') {
                    art = "View";
                }
                break;
            case "W":
                art = "Edit";
                if (item_details.id === '1') {
                    art = "View";
                }
                break;
            case "R":
                art = "View";
                break;
            default: //case "N":
                art = "Nothing";
        }

        $GroupLayer.formLayers = {
            gruppenname: item_details.gruppenname,
            typ: art
        };

        $GroupLayer.modalData = {
            predicateAssigned: 'anzeige_name',
            reverseAssigned: false,
            predicateAvailable: 'standardname',
            reverseAvailable: false
        };

        $GroupLayer.modalData.assignedLayerList = [];
        $GroupLayer.modalData.availableLayerList = [];
        $GroupLayer.modalData.assigned_array = [];

        $GroupLayer.modalData.modalTitle = $translate.instant('layers');
        $GroupLayer.modalData.modalDescription = "";

        $GroupLayer.modalData.orderAssigned = function(predicate) {
            $GroupLayer.modalData.reverseAssigned = ($GroupLayer.modalData.predicateAssigned === predicate) ? !$GroupLayer.modalData.reverseAssigned : false;
            $GroupLayer.modalData.predicateAssigned = predicate;
        };

        $GroupLayer.modalData.orderAvailable = function(predicate) {
            $GroupLayer.modalData.reverseAvailable = ($GroupLayer.modalData.predicateAvailable === predicate) ? !$GroupLayer.modalData.reverseAvailable : false;
            $GroupLayer.modalData.predicateAvailable = predicate;
        };

        function loadLayerList(ID, PARENT_ID) {
            var param = { "groupid": ID, "parentid": PARENT_ID };
            $http.post('admin/layerview', param)
                .success(function(response) {
                    //                        console.debug('Response:', response);
                    $GroupLayer.modalData.assignedLayerList = response.assignedlayerlist ? response.assignedlayerlist : [];
                    $GroupLayer.modalData.availableLayerList = response.availablelayerlist ? response.availablelayerlist : [];
                    if ($GroupLayer.modalData.assignedLayerList) {
                        for (var i = 0; i < $GroupLayer.modalData.assignedLayerList.length; i++) {
                            $GroupLayer.modalData.assigned_array[i] = $GroupLayer.modalData.assignedLayerList[i].ebenen_id;
                        }
                    }
                })
                //ToDo     //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.get_assigned_layers_on') + ' ID:' + ID + ') ' + $translate.instant('message.failed') + '. ' + $translate.instant('message.or_get_available_layers_on') + 'PARENT_ID:' + PARENT_ID + ') ' + $translate.instant('message.failed') + '.\nResponse: ' + response);
                });
        }
        if (($GroupLayer.formLayers.typ === 'Edit' || $GroupLayer.formLayers.typ === 'All') && item_details.id !== '1') {
            loadLayerList(item_details.id, item_details.vatergruppenid);
        } else {
            loadLayerList(item_details.id, null);
        }


        $GroupLayer.modalData.isInAvailable = function(item) {
            if ($GroupLayer.modalData.assigned_array.indexOf(item.ebenen_id) > -1)
                return false;
            else {
                return true;
            }
        };

        $GroupLayer.modalData.removeLayer = function(item) {
            var param = { "layerid": item.id };
            $http.post('admin/removelayer', param)
                .success(function(response) {
                    //                    console.debug('Response:', response);

                    function checkArray(element) {
                        return element === item.ebenen_id;
                    }
                    var posArray = $GroupLayer.modalData.assigned_array.findIndex(checkArray);
                    $GroupLayer.modalData.assigned_array.splice(posArray, 1);

                    function checkObjects(object) {
                        return object.ebenen_id === item.ebenen_id;
                    }
                    var posObject = $GroupLayer.modalData.assignedLayerList.findIndex(checkObjects);
                    $GroupLayer.modalData.assignedLayerList.splice(posObject, 1);

                })
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.remove_failed') + '. \nResponse: ' + response);
                });
        };

        $GroupLayer.modalData.addLayer = function(item) {
            var param = { "groupid": item_details.id, "layer": item };
            $http.post('admin/addlayer', param)
                .success(function(response) {
                    //                    console.debug('Response:', response);
                    $GroupLayer.modalData.assigned_array.push(item.ebenen_id);
                    $GroupLayer.modalData.assignedLayerList.push({
                        id: response.result,
                        ebenen_id: item.ebenen_id,
                        reload_time: item.reload_time,
                        anzeige_name: item.anzeige_name,
                        feature_info: item.feature_info,
                        checked_onlogin: item.checked_onlogin,
                        permanent: item.permanent,
                        standardname: item.standardname
                    });
                })
                //ToDo   //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.add_failed') + '.\nResponse: ' + response);
                });
        };

        $GroupLayer.modalData.submit = function() {
            //            console.debug({"Update_Layers": {  
            //                    "group_id": item_details.id,
            //                    "layers": $GroupLayer.modalData.assignedLayerList
            //                }});
            // Daten als JSON
            angular.forEach(
                $GroupLayer.modalData.assignedLayerList,
                function(list) {
                    list.reload_time = parseInt(list.reload_time);
                }
            );
            var param = { "groupid": item_details.id, "layers": $GroupLayer.modalData.assignedLayerList };
            $http.post('admin/updatelayer', param)
                .success(function(response) {
                    //                    console.debug('Response:', response);
                })
                //ToDo   //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.update_failed') + ': ' + item_details.id + ' and LayerList: ' + $GroupLayer.modalData.assignedLayerList + ' .\nResponse: ' + response);
                });

            $uibModalInstance.dismiss();
        };

        $GroupLayer.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

    });

    //Edit Controller  -  Gruppen zu Alarmkunden
    blids.controller('AlarmGruppeDialogController', function($uibModalInstance, $http, item_details, ModalService) {
        var $AlarmGruppe = this;
        var art;

        switch (item_details.rights.alarm_admin) {
            case "A":
                art = "All";
                break;
            case "W":
                art = "Edit";
                break;
            case "R":
                art = "View";
                break;
            default: //case "N":
                art = "Nothing";
        }

        $AlarmGruppe.formAlarmGruppe = {
            gruppenname: item_details.gruppenname,
            typ: art
        };

        $AlarmGruppe.modalData = {
            predicateAssigned: 'id',
            reverseAssigned: false,
            predicateAvailable: 'id',
            reverseAvailable: false
        };

        $AlarmGruppe.modalData.assignedLayerList = [];
        $AlarmGruppe.modalData.availableLayerList = [];
        $AlarmGruppe.modalData.assigned_array = [];

        $AlarmGruppe.modalData.modalTitle = "Alarms for Group:";
        $AlarmGruppe.modalData.modalDescription = "";

        $AlarmGruppe.modalData.orderAssigned = function(predicate) {
            $AlarmGruppe.modalData.reverseAssigned = ($AlarmGruppe.modalData.predicateAssigned === predicate) ? !$AlarmGruppe.modalData.reverseAssigned : false;
            $AlarmGruppe.modalData.predicateAssigned = predicate;
        };

        $AlarmGruppe.modalData.orderAvailable = function(predicate) {
            $AlarmGruppe.modalData.reverseAvailable = ($AlarmGruppe.modalData.predicateAvailable === predicate) ? !$AlarmGruppe.modalData.reverseAvailable : false;
            $AlarmGruppe.modalData.predicateAvailable = predicate;
        };

        function loadAlarmList(ID) {
            var param = { "groupid": ID, "show": 'all' };
            $http.post('admin/alarmkundelist', param)
                .success(function(response) {
                    //                        console.debug('Response:', response);
                    $AlarmGruppe.modalData.assignedAlarmList = response.alarmlist;
                    angular.forEach($AlarmGruppe.modalData.assignedAlarmList, function(list) {
                        list.id = parseInt(list.id);
                    });
                    $AlarmGruppe.modalData.availableAlarmList = response.completelist;
                    angular.forEach($AlarmGruppe.modalData.availableAlarmList, function(list) {
                        list.id = parseInt(list.id);
                    });
                    if ($AlarmGruppe.modalData.assignedAlarmList) {
                        for (var i = 0; i < $AlarmGruppe.modalData.assignedAlarmList.length; i++) {
                            $AlarmGruppe.modalData.assigned_array[i] = $AlarmGruppe.modalData.assignedAlarmList[i].id;
                        }
                    }
                })
                //ToDo     //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.get_assigned_alarmlist_on') + ' ID:' + ID + ') failed.\nResponse: ' + response);
                });
        }
        loadAlarmList(item_details.id);


        $AlarmGruppe.modalData.isInAvailable = function(item) {
            if ($AlarmGruppe.modalData.assigned_array.indexOf(item.id) > -1)
                return false;
            else {
                return true;
            }
        };

        $AlarmGruppe.modalData.removeAlarm = function(alarmitem) {
            var param = { "groupid": item_details.id, "alarmid": alarmitem.id };
            $http.post('admin/removegruppealarm', param)
                .success(function(response) {
                    function checkArray(element) {
                        return element === alarmitem.id;
                    }
                    var posArray = $AlarmGruppe.modalData.assigned_array.findIndex(checkArray);
                    $AlarmGruppe.modalData.assigned_array.splice(posArray, 1);

                    function checkObjects(object) {
                        return object.id === alarmitem.id;
                    }
                    var posObject = $AlarmGruppe.modalData.assignedAlarmList.findIndex(checkObjects);
                    $AlarmGruppe.modalData.assignedAlarmList.splice(posObject, 1);
                })
                //ToDo   //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.remove_failed') + '.\nResponse: ' + response);
                });
        };

        $AlarmGruppe.modalData.addAlarm = function(alarmitem) {
            var param = { "groupid": item_details.id, "alarmid": alarmitem.id };
            $http.post('admin/addgruppealarm', param)
                .success(function(response) {
                    //                    console.debug('Response:', response);
                    $AlarmGruppe.modalData.assigned_array.push(alarmitem.id);
                    $AlarmGruppe.modalData.assignedAlarmList.push({
                        id: alarmitem.id,
                        alarmkunde: alarmitem.alarmkunde
                    });
                })
                //ToDo   //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', $translate.instant('message.add_failed') + '.\nResponse: ' + response);
                });
        };

        $AlarmGruppe.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };
    });




    ////////////////////////////////////////////////////////////////////////////////
    //===================
    //  Admin-Pages Roles @ roles.html
    //===================

    blids.controller('Roles_Permission_TabCtrl', function($http, $uibModal, $location, roleList, ModalService, $translate) {
        var $RolePerm = this;

        if (roleList.data.status === "failure")
            $location.path('/user/logout'); // Log Out if insufficient User Permissions


        $RolePerm.data = {
            predicateRole: 'id',
            reverseRole: false,
            predicatePermission: 'benutzername',
            reversePermission: false
        };

        /*Laden der Gruppen-Liste*/
        $RolePerm.roleList = roleList.data.rolelist;
        angular.forEach($RolePerm.roleList, function(role) {
            role.id = parseFloat(role.id);
        });
        $RolePerm.permissionList = [];

        /*Funktionalitaet fuer das Ordnen der Rollen-Tabelle*/
        $RolePerm.data.orderRole = function(predicate) {
            $RolePerm.data.reverseRole = ($RolePerm.data.predicateRole === predicate) ? !$RolePerm.data.reverseRole : false;
            $RolePerm.data.predicateRole = predicate;
        };

        /*Funktionalitaet fuer das Ordnen der Permission-Tabelle*/
        $RolePerm.data.orderPermission = function(predicate) {
            $RolePerm.data.reversePermission = ($RolePerm.data.predicatePermission === predicate) ? !$RolePerm.data.reversePermission : false;
            $RolePerm.data.predicatePermission = predicate;
        };


        function loadRoleList() {
            $http.post('admin/rolelist', {})
                .success(function(response) {
                    $RolePerm.roleList = response.rolelist;
                    angular.forEach($RolePerm.roleList, function(role) {
                        role.id = parseFloat(role.id);
                    });
                })
                //ToDo    //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', 'Getting rolelist failed.');
                });
        }
        //loadRoleList();

        // fuer manuellen Refresh der Tabelle
        $RolePerm.data.loadRoles = function() {
            loadRoleList();
        };


        $RolePerm.roleDetail = [];


        /*Oeffnen des kombinierten Dialoges zum Add-Edit-View einer Role*/
        $RolePerm.data.openRoleDialog = function(item) {

            if (item) { //Aufruf als EDIT/VIEW
                var param = { "roleid": item.id };
                $http.post('admin/roledetail', param)
                    .success(function(response) {
                        $RolePerm.roleDetail = response.roledetail;
                        var options = {
                            animation: true,
                            backdrop: "static",
                            templateUrl: "/partials/admin-pages/RoleDialog.html",
                            controller: "RoleDialogController as RoleCtrl",
                            size: "",
                            resolve: {
                                details: function() {
                                    return $RolePerm.roleDetail;
                                },
                                permission: function() {
                                        return "Edit";
                                    } //für Betrieb noch Ergaenzen mit Nutzerrechten
                            }
                        };
                        $uibModal.open(options).result.finally(function() {
                            loadRoleList(); // Angezeigte Liste auf Hauptseite neu laden, wenn Modal geschlossen
                        });
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', 'Getting roledetails ( ID:' + item.id + ', Name:' + item.rolenname + ') failed.');
                    });
            } else { //Aufruf als ADD
                var options = {
                    animation: true,
                    backdrop: "static",
                    templateUrl: "/partials/admin-pages/RoleDialog.html",
                    controller: "RoleDialogController as RoleCtrl",
                    size: "",
                    resolve: {
                        details: function() {
                            return [];
                        },
                        permission: function() {
                            return "Add";
                        }
                    }
                };
                $uibModal.open(options).result.finally(function() {
                    loadRoleList();
                });
            }
        };



        function loadPermissionList() {
            $http.post('admin/permissionlist', {})
                .success(function(response) {
                    $RolePerm.permissionList = response.permissionlist;
                })
                //ToDo    //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', 'Getting permissionlist failed.');
                });
        }
        loadPermissionList();

        function loadGroupList() {
            $http.post('admin/grouplist', {})
                .success(function(response) {
                    $RolePerm.groupList = response.grouplist;
                })
                //ToDo     //check ERROR-Habit
                .error(function(response) {
                    ModalService.alert('danger', 'Getting grouplist failed.');
                });
        }
        loadGroupList();

        // fuer manuellen Refresh der Tabelle
        $RolePerm.data.loadPermissions = function() {
            loadPermissionList();
        };

        /*Oeffnen des kombinierten Dialoges zum Add-Edit-View einer Role*/
        $RolePerm.data.openPermissionDialog = function(item) {
            var ctrlDetails = this;
            ctrlDetails.PermissionDetail = [];

            //            if (item) { //Aufruf als EDIT/VIEW
            var options = {
                animation: true,
                backdrop: "static",
                templateUrl: "/partials/admin-pages/PermissionDialog.html",
                controller: "PermissionDialogController as PermissionCtrl",
                size: "",
                resolve: {
                    details: function() {
                        return item;
                    },
                    roleList: function() {
                        return $RolePerm.roleList;
                    },
                    // Gruppenliste abfragen                        
                    groupList: function() {
                        return $RolePerm.groupList;
                    },
                    permission: function() {
                            return "Edit";
                        } //für Betrieb noch Ergaenzen mit Nutzerrechten
                }
            };
            //            }      


            $uibModal.open(options).result.finally(function() {
                loadPermissionList(); // Angezeigte Liste auf Hauptseite neu laden, wenn Modal geschlossen
            });
        };

    });


    blids.controller('RoleDialogController', function($http, $uibModalInstance, details, permission, ModalService) {
        var $Role = this;
        var art;

        // Art des Aufrufes definieren
        switch (permission) {
            case "Edit":
                art = "Edit";
                break;
            case "Add":
                art = "Add";
        }


        $Role.modalData = {};
        $Role.modalData.modalTitle = art + " Role";
        $Role.modalData.modalDescription = "";

        $Role.formRoles = {
            permissions: ['N', 'R', 'W', 'A'],
            typ: art
        };

        //Aufruf per EDIT oder VIEW
        if (art === "All" || art === "Edit" || art === "View") {
            $Role.formRoles.id = details.id;
            $Role.formRoles.rollenname = details.rollenname;
            $Role.formRoles.kann_einloggen = details.kann_einloggen;
            $Role.formRoles.alarm_admin = details.alarm_admin;
            $Role.formRoles.alarm_gebiete = details.alarm_gebiete;
            $Role.formRoles.alarm_gebietgruppen = details.alarm_gebietgruppen;
            $Role.formRoles.alarm_teilnehmer = details.alarm_teilnehmer;
            $Role.formRoles.alarm_alarme = details.alarm_alarme;
            $Role.formRoles.user_admin = details.user_admin;
            $Role.formRoles.gruppen_admin = details.gruppen_admin;
        } else { //als ADD
            $Role.formRoles.kann_einloggen = 'f';
            $Role.formRoles.alarm_admin = 'N';
            $Role.formRoles.alarm_gebiete = 'N';
            $Role.formRoles.alarm_gebietgruppen = 'N';
            $Role.formRoles.alarm_teilnehmer = 'N';
            $Role.formRoles.alarm_alarme = 'N';
            $Role.formRoles.user_admin = 'N';
            $Role.formRoles.gruppen_admin = 'N';
        }

        // Absenden des Formulars
        $Role.modalData.submit = function() {
            var param;
            if (art === "Edit") {
                param = {
                    role: {
                        id: details.id,
                        rollenname: $Role.formRoles.rollenname,
                        kann_einloggen: $Role.formRoles.kann_einloggen,
                        alarm_admin: $Role.formRoles.alarm_admin,
                        alarm_gebiete: $Role.formRoles.alarm_gebiete,
                        alarm_gebietgruppen: $Role.formRoles.alarm_gebietgruppen,
                        alarm_teilnehmer: $Role.formRoles.alarm_teilnehmer,
                        alarm_alarme: $Role.formRoles.alarm_alarme,
                        user_admin: $Role.formRoles.user_admin,
                        gruppen_admin: $Role.formRoles.gruppen_admin
                    }
                };
                //                 console.debug('param: ', param);
                $http.post('admin/updaterole', param)
                    .success(function(response) {
                        //                        console.debug('Response:', response);
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', 'Updating failed with the Parameters ID: ' + details.id + '.\nResponse: ' + response);
                    });
            } else { // Aufruf als ADD
                param = {
                    role: {
                        //                        id: details.id,
                        rollenname: $Role.formRoles.rollenname,
                        kann_einloggen: $Role.formRoles.kann_einloggen,
                        alarm_admin: $Role.formRoles.alarm_admin,
                        alarm_gebiete: $Role.formRoles.alarm_gebiete,
                        alarm_gebietgruppen: $Role.formRoles.alarm_gebietgruppen,
                        alarm_teilnehmer: $Role.formRoles.alarm_teilnehmer,
                        alarm_alarme: $Role.formRoles.alarm_alarme,
                        user_admin: $Role.formRoles.user_admin,
                        gruppen_admin: $Role.formRoles.gruppen_admin
                    }
                };
                //                console.debug('param: ', param);
                $http.post('admin/addrole', param)
                    .success(function(response) {
                        //                        console.debug('Response:', response);
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', 'Adding failed for Role: ' + param.group.rollenname + ' .\nResponse: ' + response);
                    });
            }

            // Modal schließen
            $uibModalInstance.dismiss();
        };

        $Role.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $Role.modalData.delete = function() {
            ModalService.confirm("Are you sure?").then(function(response) {
                var param = { roleid: details.id };
                //                console.debug('param: ', param);
                $http.post('admin/removerole', param)
                    .success(function(response) {
                        //                    console.debug('Role: ',details.rollenname,' has been deleted!', ' Response:', response);
                        $uibModalInstance.dismiss();
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', 'Deleting failed with the Parameters ID: ' + details.id + ' .\nResponse: ' + response);
                    });
            });
        };

    });


    //  Admin-Pages User-Permissions @ roles.html
    blids.controller('PermissionDialogController', function($http, $uibModalInstance, details, roleList, groupList, permission, ModalService) {
        var $PermissionDialog = this;
        var art;

        // Art des Aufrufes definieren
        switch (permission) {
            case "Edit":
                art = "Edit";
                break;
            default:
                art = "Add";
        }

        //        console.debug('details: ', details);

        $PermissionDialog.modalData = {};
        $PermissionDialog.modalData.modalTitle = art + " Permission";
        $PermissionDialog.modalData.modalDescription = "";

        $PermissionDialog.formPermission = {
            benutzername: details.benutzername,
            groups: groupList,
            roles: roleList,
            typ: art,
            oldgroupid: details.gruppen_id,
            oldroleid: details.rollen_id
        };

        var selectGruppe = details.gruppen_id;
        var numGruppe = 0;
        $PermissionDialog.formPermission.groups.find(function(element, index, array) {
            if (element.id === selectGruppe) {
                $PermissionDialog.formPermission.selectedgruppenname = array[index];
                return 1;
            }
        });


        var selectRole = details.rollenname;
        var numRole = 0;
        $PermissionDialog.formPermission.roles.find(function(element, index, array) {
            if (element.rollenname === selectRole) {
                $PermissionDialog.formPermission.selectedrollenname = array[index];
                return 1;
            }
        });
        //        console.debug($PermissionDialog.formPermission.selectedrollenname);


        // Absenden des Formulars
        $PermissionDialog.modalData.submit = function() {
            var param;
            if (art === "Edit") {
                param = {
                    permission: {
                        userid: details.benutzer_id,
                        groupid: $PermissionDialog.formPermission.selectedgruppenname.id,
                        roleid: $PermissionDialog.formPermission.selectedrollenname.id,
                        oldgroupid: $PermissionDialog.formPermission.oldgroupid,
                        oldroleid: $PermissionDialog.formPermission.oldroleid
                    }
                };
                $http.post('admin/updatepermission', param)
                    .success(function(response) {
                        //                    console.debug('Permission: ',details.benutzername,' ',details.gruppenname,' ',details.rollenname,' has been edited!', ' Response:', response);
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', 'Updating failed for Permission: ' + details.benutzername + ' ' + details.gruppenname + ' ' + details.rollenname + ' .\nResponse: ' + response);
                    });
            } else { // Aufruf als ADD
                param = {
                    permission: {
                        userid: details.benutzer_id,
                        groupid: $PermissionDialog.formPermission.selectedgruppenname.id,
                        roleid: $PermissionDialog.formPermission.selectedrollenname.id
                    }
                };
                //                console.debug('param: ', param);
                $http.post('admin/addpermission', param)
                    .success(function(response) {
                        //                        console.debug('Permission: ',details.benutzername,' ',details.gruppenname,' ',details.rollenname,' has been added!', ' Response:', response);
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', 'Adding failed for Permission: ' + details.benutzername + ' ' + details.gruppenname + ' ' + details.rollenname + ' has been edited! \nResponse: ' + response);
                    });
            }
            // Modal schließen
            $uibModalInstance.dismiss();

        };

        $PermissionDialog.modalData.cancel = function() {
            $uibModalInstance.dismiss();
        };

        $PermissionDialog.modalData.delete = function() {
            ModalService.confirm("Are you sure?").then(function(response) {
                var param = {
                    permission: {
                        userid: details.benutzer_id,
                        groupid: $PermissionDialog.formPermission.oldgroupid,
                        roleid: $PermissionDialog.formPermission.oldroleid
                    }
                };
                //                console.debug('param: ', param);                
                $http.post('admin/removepermission', param)
                    .success(function(response) {
                        //                    console.debug('Permission: ',details.benutzername,' ',details.gruppenname,' ',details.rollenname,'has been deleted!, Response:', response);
                        $uibModalInstance.dismiss();
                    })
                    //ToDo    //check ERROR-Habit
                    .error(function(response) {
                        ModalService.alert('danger', 'Deleting failed for Permission: ' + details.benutzername + ' ' + details.gruppenname + ' ' + details.rollenname + 'Response: ' + response);
                    });
                $uibModalInstance.dismiss();
            });
        };

    });


})();