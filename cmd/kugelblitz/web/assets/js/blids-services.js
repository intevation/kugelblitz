// Copyright 2012 - 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

(function() {

    var blids = angular.module('blidsApp');
    blids.service('Session', function() {
            this.create = function(lifespan) {
                this.active = true;
                this.lifespan = lifespan;
            };
            this.destroy = function() {
                this.active = false;
                this.lifespan = null;
            };
        })
        .service('Auth', function($http, $interval, $timeout, $q, $location, Session, UserService, $rootScope, ModalService) {

            var AuthService = this,
                refresher = null;
            this.login = function(authData) {

                var loginBox = angular.element(document.getElementById('login-box')),
                    loginLoader = angular.element(document.getElementById('loader')),
                    CSMenu = angular.element(document.getElementById('CSMenu'));
                $http.post('auth/login', authData)
                    .success(function(data, status) {

                        if (data.status === "failure") {
                            loginBox.css('display', 'none');
                            loginLoader.css('display', 'block');
                            CSMenu.css('display', 'none');
                            $timeout(function() {
                                ModalService.alert('danger', data.message);
                                loginBox.css('display', 'block');
                                loginLoader.css('display', 'none');
                                CSMenu.css('display', 'none');
                            }, 2000);
                        } else if (data.status === "success") {
                            var googleOnload = function () {
                                var bounds = new OpenLayers.Bounds();
                                var boundstring = data.User.bounds.split(/[( ,)]/);
                                var User = data.User;
                                bounds.extend(new OpenLayers.LonLat(boundstring[1], boundstring[2]));
                                bounds.extend(new OpenLayers.LonLat(boundstring[3], boundstring[4]));
                                UserService.setArchiv(User.archiv_tage, User.archiv_ab);
                                UserService.setLayer(data.Layers);
                                UserService.setLiveBlids(data.User.liveblids);
                                UserService.setGid(data.UserID);
                                UserService.setAnimation(data.User.animation);
                                UserService.setSound(data.User.sound);
                                UserService.setCenter(bounds.getCenterLonLat().transform('EPSG:4326', 'EPSG:3857'));
                                UserService.setBounds(bounds.transform('EPSG:4326', 'EPSG:3857'));
                                UserService.setMaxTime(User.max_displayzeit * 60);
                                UserService.setZoom({ 'max': parseInt(User.max_zoom), 'min': parseInt(User.min_zoom) });
                                UserService.loadTheme(User.theme_id);
                                UserService.setTheme(User.theme_id);
                                UserService.statistic = { "window": User.statistik_windowed, "area": User.statistikgebiet };
                                Session.create(null);
                                refresher = AuthService.autoRenew();
                                $location.path(PATHS.map);
                                loginBox.css('display', 'none');
                                loginLoader.css('display', 'block');
                                $rootScope.$broadcast('new navmenu');
                                CSMenu.css('display', 'block');
                            }
                            // Only create script if we have not done it before.
                            if (document.getElementById("google-maps-kugelbitz")) {
                                // console.log("already have script node");
                                googleOnload();
                            } else {
                                // console.log("create new script node");
                                var script = document.createElement("script");
                                script.id = "google-maps-kugelbitz";
                                script.src = "https://maps.googleapis.com/maps/api/js?"
                                    + "client=" +  data.GoogleMapsClient + "&"
                                    + "channel=" + data.GoogleMapsChannel + "&"
                                    + "sensor=false&language=en&callback=initMap";
                                script.onload = googleOnload;
                                document.body.appendChild(script);
                            }
                        }

                    })
                    .error(function() {
                        loginBox.css('display', 'block');
                        loginLoader.css('display', 'none');
                        CSMenu.css('display', 'none');
                    });
            };
            this.logout = function() {
                $http.get('auth/Logout', {})
                    .success(function(data, status) {

                        Session.destroy();

                        if (data.status === "success") {
                            $interval.cancel(refresher);
                            //UserService.deacLiveLayer();
                            $rootScope.$broadcast('new navmenu');
                            $location.path(PATHS.login);
                        }

                        if (data.status === "failure") {
                            alert(data.message);
                        }
                    })
                    .error(function() {

                        if (Session.active !== undefined && !Session.active) {
                          alert("Connection lost!");
                        }

                        Session.destroy();
                    });
            };
            this.isAuthenticated = function() {
                //console.debug("IS AUTH??");
                return !!Session.active;
            };
            this.autoRenew = function() {
                var stop = $interval(function() {
                    $http.post('auth/renewSession', {})
                        .success(function(data, status) {

                            if (data.status === "failure") {
                                Session.destroy();
                                $interval.cancel(stop);
                            }
                        })
                        .error(function() {
                            $location.path(PATHS.logout);
                            Session.destroy();
                            $interval.cancel(stop);
                        });
                }, 1000 * 60);
                return stop;
            };
            this.pwReset = function(username) {
                $http.post('auth/resetPW', { "user": username })
                    .success(function(data, status) {
                        $location.path(PATHS.logout);
                    })
                    .error(function() {
                        $location.path(PATHS.logout);
                    });
            };
        })

    .service('UserService', function($http, $compile) {
        var layers = [],
            LiveLayer = null;
        var archiv = { "tage": 0, "start": "" };
        var zoom = { "max": 0, "min": 0 };
        var theme = 1;
        var gid, maxTime, liveBlids, animation, sound;
        var statistic = {},
            bounds = {},
            center = {};
        return {
            statistic: statistic,
            // Functions to deactivate the LiveLayer
            setLiveLayer: function(value) {
                LiveLayer = value;
            },
            getLiveLayer: function() {
                return LiveLayer;
            },
            deacLiveLayer: function() {
                if (LiveLayer) {
                    LiveLayer.deactivate();
                }
            },
            checkDvD: function(startDate, endDate) { // Check if startDate before endeDate
                if (startDate instanceof Date && endDate instanceof Date) {
                    if (startDate <= endDate) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                } //(wenn keine Daten eingegeben sind, kann auch das 2.Datum nicht vor dem ersten sein!)
            },
            getAnimation: function() {
                return animation;
            },
            getArchiv: function() {
                return archiv;
            },
            getCenter: function() {
                return center;
            },
            getData: function() {
                return { "layers": layers, "maxTime": maxTime, "bounds": bounds };
            },
            getGid: function() {
                console.log("GOT GID " + gid);
                return gid;
            },
            getLiveBlids: function() {
                return liveBlids;
            },
            getSound: function() {
                return sound;
            },
            getZoom: function() {
                return zoom;
            },
            setAnimation: function(value) {
                animation = value;
            },
            setArchiv: function(tage, start) {
                archiv.tage = tage;
                archiv.start = new Date(start.split(/[ +]/).splice(0, 2).join('T'));
                if (archiv.start > new Date())
                    archiv.start = new Date();
                archiv.start.setDate(archiv.start.getDate() - archiv.tage); // If Archive Startdate is in future, only
                archiv.start.setHours(0);
                archiv.start.setMinutes(0);
                archiv.start.setSeconds(0);
            },
            setZoom: function(value) {
                zoom = value;
            },
            setBounds: function(value) {
                bounds = value;
            },
            setCenter: function(value) {
                center = value;
            },
            setGid: function(value) {
                gid = value;
            },
            setLayer: function(value) {
                layers = value;
            },
            setLiveBlids: function(value) {
                liveBlids = value;
            },
            setMaxTime: function(value) {
                maxTime = value;
            },
            setSound: function(value) {
                sound = value;
            },
            setTheme: function(value) {
                theme = value;
            },
            getTheme: function() {
                return theme;
            },
            loadTheme: function(theme) {
                if (theme !== undefined) {
                    $http.post('utils/theme', { "id": theme })
                        .success(function(data, status) {
                            if (data.theme.header) {
                                var h = document.getElementsByClassName('header')
                                if (h && h.length > 0) {
                                    h[0].innerHTML = data.theme.header;
                                    h[0].setAttribute('style', data.theme.style);
                                    $compile(angular.element(h[0]))(angular.element(h[0]).scope());
                                }
                            }
                            if (data.theme.footer) {
                                var f = document.getElementsByClassName('footer')
                                if (f && f.length > 0) {
                                    f[0].innerHTML = data.theme.footer;
                                    f[0].setAttribute('style', data.theme.style);
                                    $compile(angular.element(f[0]))(angular.element(f[0]).scope());
                                    $http.get('version').success(function (data, status){
                                        var items = f[0].querySelectorAll("ul li");
                                        var lastchild = items[items.length-1];
                                        lastchild.insertAdjacentHTML("afterend", "<li> Version: " + data.version + "</li>");
                                    }).error(function () {
                                        console.debug("Version could not be loaded");
                                    })
                                }
                            }
                            if (data.theme.title) {
                                document.title = data.theme.title;
                            }
                        })
                        .error(function() {
                            console.debug("Theme could not be loaded");
                        });
                }
            }
        };
    })

    .service('ModalService', function($uibModal, $translate) {
        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: '/partials/admin-pages/confirm.html'
        };

        var modalOptions = {
            closeButtonText: 'message.close', // will be translated in the HTML
            actionButtonText: 'message.ok',
            actionButtonResult: 'message.ok',
            actionButtonClass: 'btn-primary',
            headerText: 'message.attention',
            bodyText: 'message.sure'
        };

        this.showModal = function(customModalDefaults, customModalOptions) {
            if (!customModalDefaults)
                customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function(customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function($scope, $uibModalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function(result) {
                        $uibModalInstance.close(result);
                    };
                    $scope.modalOptions.close = function(result) {
                        $uibModalInstance.dismiss($translate.instant('message.cancel'));
                    };
                };
            }

            return $uibModal.open(tempModalDefaults).result;
        };

        this.alert = function(type, msg) { // type determines class of button (primary, danger, success, info, warning, link - see bootstrap.css)
            this.showModal({ templateUrl: '/partials/admin-pages/alert.html' }, {
                actionButtonClass: 'btn-' + type,
                bodyText: msg
            });
        };

        this.confirm = function(msg) {
            return this.showModal({}, { bodyText: msg });
        };
    });

})();
