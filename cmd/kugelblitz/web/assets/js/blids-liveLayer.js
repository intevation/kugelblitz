// Copyright 2012 - 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

var LiveLayerModule = (function() {
    var EVT_SRC_NAME = "/event";

    if (!window.console.debug) { //IE does not support console.debug
        window.console = {
            debug: window.console.log
        };
    }

    var connectionStatus = "disconnected";

    function FeatureStyle(graphicName, pointradius, fillcolor, strokecolor, strokewidth) {
        this.graphicName = graphicName;
        this.pointRadius = pointradius;
        this.fillColor = fillcolor;
        this.strokecolor = strokecolor;
        this.strokeWidth = strokewidth;

        this.setStyle = function(pointradius, fillcolor, strokecolor, strokewidth) {
            this.pointRadius = pointradius;
            this.fillColor = fillcolor;
            this.strokecolor = strokecolor;
            this.strokeWidth = strokewidth;
        };
        this.getNewGraphic = function(graphicname) {
            var s = {
                graphicName: graphicname,
                pointRadius: this.pointRadius,
                fillColor: this.fillColor,
                strokecolor: this.strokecolor,
                strokeWidth: this.strokeWidth
            };
            return s;
        };
    };

    /**
     * Creates a new LiveLayer
     * @param{OpenLayers.Map} openlayersmap
     * @param{bool} onlyViewPort LiveLayer only shows the lightnings in the viewport of the map
     * @constructor
     */
    function LiveLayer(openlayersmap, onlyViewPort) {
        var eventSrcActive = false;
        var layerActive = false;
        var oldFeature; //Used to change the style of features
        var animationSvg; //SVG-Layer for the animations
        var selectFeature;
        var statusDomElement;
        var deleteOldFeatureInterval;
        var clearLayerInterval;
        var animationActive = true;
        var mapUrl = "#/map"
        this.filter = "";
        this.soundState = false;
        this.deleteOnMove = true;
        this.clearDuration = 300;
        this.featureStyle = new FeatureStyle("circle", 5, '#FF0000', '#000000', 1); //Object for styling the Features.
        this.latestFeatureStyle = new FeatureStyle("cross", 8, '#ffffff', '#000000', 1);

        this.map = openlayersmap;
        this.overlay = new OpenLayers.Layer.Vector('LiveLayer', {
            isBaseLayer: false,
            displayInLayerSwitcher: false,
            geoname: "devel:live",
            displayname: "LiveLayer",
            visibility: true // TODO: Fix me! Workaraound to see live layer.
        });
        this.onlyViewPort = onlyViewPort;

        this.animation = {
            duration: 1000,
            radius: 25,
            style: {
                fill: '#ffffff',
                'fill-opacity': 0.8,
                stroke: '#000000',
                'stroke-width': 2
            }
        };
        /************************Public Functions*************************************/
        this.setFilter = function(filter) {
            this.filter = filter;
        };
        this.removeFilter = function() {
            this.filter = "";
        };

        this.Sound = function(state) {
            this.soundState = state ? true : false;
        };
        /**
         * Show the connection status of the live layer in the Dom-Element with the given destinationID
         * and refresh it when status changes
         * @param {string} destinationID
         */
        this.showConnectionStatus = function(destinationID) {
            statusDomElement = document.getElementById(destinationID);
            event_src.addEventListener("status", function(event) {
                connectionStatus = event.data;
                changeConnectionStatus();
            }.bind(this));
        };
        /**
         * Returns the given Connection Status
         * @returns {string}
         */
        this.getConnectionStatus = function() {
            return connectionStatus;
        };

        /**
         * Sets the Properties for the Animation of the lighning-features.
         * @param {int} duration Duration of the animation in seconds
         * @param {float} radius Radius of the animation
         * @param {object} style Optional Styling of the Animation
         */
        this.setAnimationProperties = function(duration, radius, style) {
            this.animation.duration = duration;
            this.animation.radius = radius;
            if (style !== undefined) {
                this.animation.style = style;
            }
        };
        this.isActive = function() {
                return eventSrcActive;
            }
            /**
             * Activates the LiveLayer and registers the EventSource
             * when it doesn't exist and adds and EventListener to the EventSource
             * which draws the Features
             */
        this.activate = function() {
            layerActive = true;
            if (!eventSrcActive) {
                console.debug("SESSION REGISTERED, OPEN EVENTSTREAM");
                event_src = new EventSource(EVT_SRC_NAME, { withCredentials: true });
                eventSrcActive = true;

                event_src.addEventListener("lightning", function(event) {
                    if (!layerActive) {
                        return;
                    }
                    var data = JSON.parse(event.data);
                   
                    document.getElementById("hbData").innerHTML = getLocalTime(new Date).slice(0,19);
                   
                    if ("heartbeat" in data) {
                        return;
                    }
                    
                    data.zeit = getLocalTime(data.zeit)

                    if (this.filter !== "") {
                        this.filter = this.filter.toLowerCase();
                        var typ = data.typ; // DO NOT REMOVE IT'S NOT UNUSED!
                        var strom = data.strom; // DO NOT REMOVE IT'S NOT UNUSED!
                        var resFilter = this.filter.replaceAll("=", "===");
                        resFilter = resFilter.replaceAll("and", "&&");
                        resFilter = resFilter.replaceAll("or", "||");
                        resFilter = resFilter.replaceAll("or", "||");
                        if (!eval(resFilter)) {
                            return;
                        }
                    }
                    //console.debug(data);
                    this.drawFeature(data);
                }.bind(this));
                window.addEventListener("hashchange", locationHashChanged);
                window.addEventListener("beforeunload", closeEventsource);

            }
            if (statusDomElement !== undefined) {
                event_src.addEventListener('open', function(e) {
                    connectionStatus = "connected";
                    changeConnectionStatus();
                }.bind(this));

                event_src.addEventListener('error', function(e) {
                    if (e.explicitOriginalTarget.readyState === event_src.CLOSED) {
                        connectionStatus = "disconnected";
                        changeConnectionStatus();
                    }
                }.bind(this));
            };


        };
        /**
         * Returns the Openlayers-layer
         */
        this.getLayer = function() {
            return this.overlay;
        };
        /**
         * Deactivates the LiveLayer.
         */
        this.deactivate = function() {
            closeEventsource();
            connectionStatus = "disconnected";
            if (statusDomElement !== undefined) {
                changeConnectionStatus();
            }
            layerActive = false;
            this.clearLayer();

        };
        /**
         * Deactivates the LiveLayer temporary, doesn't close eventsource.
         */
        this.deactivateTemp = function() {
            layerActive = false;
        };
        /**
         * Adds the LiveLayer to the Map and sets the SVG-Layer for Animation-Objects.
         */
        this.addLayerToMap = function() {
            this.map.addLayer(this.overlay);

            //Fire event when a feature is clicked
            animationSvg = new Raphael(document.getElementById(this.overlay.id).childNodes[0], "100%", "100%");
            //Fire event when a feature is clicked
            selectFeature = new OpenLayers.Control.SelectFeature(
                this.overlay, {
                    onSelect: showFeatureInfo,
                    autoActivate: true
                }
            );
            this.map.addControl(selectFeature);



        };

        /**
         * Sets an Interval with given duration in seconds which clears the live layer
         * @param {int} duration
         */
        this.setClearInterval = function(duration) {
            if (duration > 0) { this.clearDuration = duration; }
            clearInterval(clearLayerInterval);
            clearLayerInterval = setInterval(this.clearLayer.bind(this), (this.clearDuration * 1000));
        };
        /**
         * Sets an Interval with given duration in seconds which deletes the oldest features, when the given max-feautres overstepped
         * @param {int} duration
         * @param {int} maxFeatures
         */
        this.setDeleteOldInterval = function(duration, maxFeatures) {
            this.maxFeatures = maxFeatures;
            clearInterval(deleteOldFeatureInterval);
            deleteOldFeatureInterval = setInterval(this.deleteOld.bind(this), (duration * 1000));
        };

        /**
         * Deletes all Features of the Layer
         */
        this.clearLayer = function() {
            if (this.overlay.features.length === 1) {
                return;
            }
            if (this.overlay.features.length > 1) {
                var toRemove = this.overlay.features.length - 1;
                var rmFeatures = this.overlay.features.slice(0, toRemove);
                this.overlay.destroyFeatures(rmFeatures);
                this.overlay.redraw();

            }
        };
        /**
         * Deletes the oldest features, when the max-features overstepped
         */
        this.deleteOld = function() {
            if (this.overlay.features.length > this.maxFeatures) {
                var toRemove = this.overlay.features.length - this.maxFeatures;
                var rmFeatures = this.overlay.features.slice(0, toRemove);
                this.overlay.destroyFeatures(rmFeatures);
                this.overlay.redraw();
            }
        };
        /**
         * Adds a feature with the given data to the lightning-vectorlayer
         * @param{json-object} data
         */
        this.drawFeature = function(data) {
            if (oldFeature !== undefined && oldFeature.data !== null) {
                if (this.onlyViewPort) {
                    var viewPortBounds = this.map.getExtent();
                    if (!viewPortBounds.containsLonLat(oldFeature.LonLat)) {
                        this.overlay.destroyFeatures([oldFeature]);
                    }
                }
            }
            //change styling of the previous lightning
            changeOldFeature(oldFeature)
                //Creates the OpenLayersFeature for the new lightning
            var pointGeo = new OpenLayers.Geometry.Point(data.lon, data.lat).transform('EPSG:4326', 'EPSG:3857');
            var lonLat = new OpenLayers.LonLat(data.lon, data.lat).transform('EPSG:4326', 'EPSG:3857');


            var feature = new OpenLayers.Feature.Vector(pointGeo, data, this.latestFeatureStyle);

            feature.LonLat = lonLat;
            this.overlay.addFeatures([feature]);
            //Animate latest lightning
            if (animationActive) {
                animateFeature(feature.LonLat, this.animation.duration, this.animation.radius, this.animation.style);
            }

            //Play Sound if soundState is true
            if (this.soundState) {
                soundFile.play();
            };
            //Save latest lightning to change styling when new one occurs
            oldFeature = feature;
        };

        /**********************Private Functions************************************/
        String.prototype.replaceAll = function(target, replacement) {
            return this.split(target).join(replacement);
        };

        function locationHashChanged() {
            if (location.hash !== mapUrl) {
                closeEventsource();
            }
        }

        var soundFile = new Audio("assets/audio/donner.mp3");

        /**
         * Closes the eventsource and sends a http-get request to
         *  the eventstream server to disconnect the client
         *
         */
        var closeEventsource = function() {
            window.removeEventListener("hashchange", locationHashChanged);
            window.removeEventListener("beforeunload", closeEventsource);
            event_src.close();
            eventSrcActive = false;


            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("GET", "/eventclose", false); // false for synchronous request
            xmlHttp.send(null);


        };
        /**
         * Changes the value of the Status-Dom-Element to the current connection-status
         */
        var changeConnectionStatus = function() {
            statusDomElement.innerHTML = connectionStatus;
        };
        /**
         * Replaces picture of the latest lightning(cross) with data specific style
         * @param{OpenLayers.Feature.Vector} oldFeature
         */
        var changeOldFeature = function(oldFeature) {
            //square=ground negative
            //circle = ground positive
            //triangle = cloud(type 4)
            if (oldFeature !== undefined && oldFeature.data !== null) {
                if (oldFeature.data.typ === 4) {
                    oldFeature.style = this.featureStyle.getNewGraphic("triangle");
                } else {
                    if (oldFeature.data.strom < 0) {
                        oldFeature.style = this.featureStyle.getNewGraphic("square");
                    } else {
                        oldFeature.style = this.featureStyle.getNewGraphic("circle");
                    }
                }
                this.overlay.drawFeature(oldFeature);
            }
        }.bind(this);
        /**
         * Returns the html-modified data for die Feature-Info-Popup of the given Feature
         * @param{OpenLayers.Feature.Vector} oldFeature
         */
        var getFeatureInfoHtml = function(feature) {
            var resHtml = "";
            var zeit = feature.data.zeit; //($filter('date')(new Date(content[i].properties.zeit), 'dd.MM.yyyy HH:mm:ss:sss'));
            resHtml += zeit + " / " + round(feature.data.lon, 3) + "° " + round(feature.data.lat, 3) + "° / ";
            if (feature.data.typ === 1) {
                resHtml += "Ground ";
            } else if (feature.data.typ === 4) {
                resHtml += "Cloud ";
            } else {
                resHtml += "";
            }
            resHtml += round(feature.data.strom, 1) + " kA";
            resHtml += "<br/>";
            return resHtml;
        };
        /**
         * Shows the Data of a given Feature in a PopUp box
         * If Features Geometrys intersects or Features overlaps with others,the data of all is shown in one box
         * @param{event} e Event when Feature is clicked
         */
        var showFeatureInfo = function(e) {
            var feature = e;
            if (feature.data !== null) {
                var featureBounds = feature.geometry.getBounds();
                //Html-String for the text of the PopUp-Box
                var showHtml = "<div>";
                showHtml += getFeatureInfoHtml(feature);



                //Check if clicked Lightning-feature overlaps with other feature or intersects with it
                for (var i in this.overlay.features) {
                    var mFeature = this.overlay.features[i];
                    if (mFeature.data !== null && mFeature.LonLat !== undefined) {
                        var mBounds = mFeature.geometry.getBounds();
                        if (mFeature.geometry.id !== feature.geometry.id) {
                            //get Distance of the two features based on the current zoom level
                            var pxViewPortDistance = this.map.getViewPortPxFromLonLat(feature.LonLat).distanceTo(this.map.getViewPortPxFromLonLat(mFeature.LonLat));
                            if (pxViewPortDistance <= 8) {
                                showHtml += "<hr style='background-color:black;height:1px'>";
                                showHtml += getFeatureInfoHtml(mFeature);
                                continue;
                            }
                            if (featureBounds.containsLonLat(mFeature.LonLat)) {
                                showHtml += "<hr style='background-color:black;height:1px'>";
                                showHtml += getFeatureInfoHtml(mFeature);
                                continue;
                            }
                            if (featureBounds.intersectsBounds(mBounds)) {
                                showHtml += "<hr style='background-color:black;height:1px'>";
                                showHtml += getFeatureInfoHtml(mFeature);
                                continue;
                            }
                        }
                    }
                }
                showHtml += "</div>";
                var popup = new OpenLayers.Popup.FramedCloud(
                    "info",
                    new OpenLayers.LonLat(feature.data.lon, feature.data.lat).transform('EPSG:4326', 'EPSG:3857'),
                    new OpenLayers.Size(300, 110),
                    showHtml,
                    null,
                    true
                );
                this.map.addPopup(popup);
                selectFeature.unselect(e);

            }
        }.bind(this);

        /**
         * Shows an Animation with the given duration and radius on the map at the given lonlat-Position
         * @param{Openlayers.LonLat Object} lonlat
         * @param{int} duration Duration of the animation in milliseconds
         * @param{float} radius Radius of the animation circle
         * @param{Object} style Styling Object for the animation circle
         */
        var animateFeature = function(lonlat, duration, radius, style) {
            //Position on map for animation
            var px = this.map.getViewPortPxFromLonLat(lonlat);
            var circle = animationSvg.circle(px.x, px.y, radius);
            circle.node.setAttribute("class", "animation");
            circle.toFront();
            circle.attr(style);
            circle.animate({ transform: "s0.0" }, duration, "linear", function() {
                circle.remove();
            });
        }.bind(this);

    }
    /**
     * Returns the difference of to time-objects
     */
    function getLatenz(time1, time2) {
        var result = "";
        var msec = time1 - time2;;
        var hh = Math.floor(msec / 1000 / 60 / 60);
        msec -= hh * 1000 * 60 * 60;
        var mm = Math.floor(msec / 1000 / 60);
        msec -= mm * 1000 * 60;
        var ss = Math.floor(msec / 1000);
        msec -= ss * 1000;
        if (hh > 0) {
            result += "" + hh + "h";
        }
        if (mm > 0) {
            result += "" + mm + "min";
        }
        if (ss > 0) {
            result += "" + ss + "sec";
        }
        if (msec > 0) {
            result += "" + msec + "ms";
        }
        return result;
    }
    //Rundet eine Zahl auf eine bestimmte Nachkommastelle
    function round(number, decimals) {
        return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
    }
    /**
     * Returns a String with the given Time in 'dd.MM.yyyy HH:mm:ss:sss' format
     * @param {string} today Timestamp as String
     */
    function getLocalTime(today) {
        var today = new Date(today);
        var initMilliSeconds = today.getMilliseconds();
        var today = new Date(today.toString());
        var initDay = today.getDate();
        var initMonth = today.getMonth() + 1;
        var initHours = today.getHours();
        var initMinutes = today.getMinutes();
        var initSeconds = today.getSeconds();


        var day = leadingNull(initDay);
        var month = leadingNull(initMonth);
        var year = today.getFullYear();
        var hours = leadingNull(initHours);
        var minutes = leadingNull(initMinutes);
        var seconds = leadingNull(initSeconds);
        var milliseconds = initMilliSeconds;


        return day + "." + month + "." + year + " " + hours + ":" + minutes + ":" + seconds + ":" + milliseconds;

    }
    /**
     * Adds a leading null to a number smaller 10
     * @param {integer} zeit
     */
    function leadingNull(zeit) {
        var result = "";
        if (zeit < 10) {
            result = "0" + zeit;
        } else {
            result = "" + zeit;
        }
        return result;
    }
    return LiveLayer;
})();
