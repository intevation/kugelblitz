'use strict()';

// Copyright 2012 - 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

(function() {

    var blids = angular.module('blidsApp');

    blids.controller('ApplicationController', function($scope, Auth, $translate) {
        //Auth.login();
        $scope.isAuthenticated = Auth.isAuthenticated;
        //console.log($scope.isAuthenticated());

        $scope.switch_lang = function(lang) {
            $translate.use(lang);
            OpenLayers.Lang.setCode(lang);
        };
    });

    blids.controller('LoginController', function($scope, $http, $location, mainMenu, Auth, UserService, DEFAULT_THEME) {
        UserService.loadTheme(DEFAULT_THEME);
        angular.element(document.getElementById('resetPW-box')).css('display', 'none');
        $scope.imagePath = "assets/images/loader.gif";
        angular.element(document.getElementById('login-box')).css('display', 'block');
        angular.element(document.getElementById('loader')).css('display', 'none');
        angular.element(document.getElementById('CSMenu')).css('display', 'none');
        $scope.frmLogin_submit = Auth.login;

        $scope.showPWReset = function() {
            angular.element(document.getElementById('login-box')).css('display', 'none');
            angular.element(document.getElementById('resetPW-box')).css('display', 'block');
        };
        $scope.showLogin = function() {
            angular.element(document.getElementById('login-box')).css('display', 'block');
            angular.element(document.getElementById('resetPW-box')).css('display', 'none');
        };
        $scope.resetPW = Auth.pwReset;
    });

    blids.controller('HomeController', function($scope, mainMenu) {});

    blids.directive("compareTo", function() { // compare two input fields with each other
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function(modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
            }
        };
    });

    blids.controller('resetPwCtrl', function($scope, $route, $location, test, $http) {
        if (test.data.status !== "success")
            $location.path('/user/logout'); // Log Out if insufficient User Permissions
        $scope.hash = $route.current.params.hash;
        $scope.user = $route.current.params.user;
        angular.element(document.getElementById('CSMenu')).css('display', 'none');

        $scope.cancel = function() {
            $location.path("/user/logout");
        };

        $scope.save = function() {
            $http.post("auth/changePW", { "user": $scope.user, "hash": $scope.hash, "passwd": $scope.auth.password })
                .success(function(data, status) {
                    $scope.stat = status;
                    $scope.daten = data;
                    $location.path(PATHS.logout);
                })
                .error(function(data, status) {
                    $scope.daten = data || "Request failed";
                    $scope.stat = status;
                    $location.path(PATHS.logout);
                });
        };
    });

    blids.controller('MenuCtrl', function($scope, $http, Auth, UserService) {
        $scope.content = { "menu": {} };
        $scope.$on('new navmenu', function() {
            $http.post('admin/menu').then(function(menu) {
                $scope.content = (menu.data instanceof Array) ? { "menu": {} } : menu.data;
            });
        });
    });

    blids.controller('adminCtrl', function($scope, $http) {
        $scope.submitUser = function() {
            $http.post("user/InsertUser", { "benutzer": { "email": $scope.email, "gid": $scope.gid } })
                .success(function(data, status) {
                    $scope.stat = status;
                    $scope.daten = data;
                })
                .error(function(data, status) {
                    $scope.daten = data || "Request failed";
                    $scope.stat = status;
                });
        };
    });

})();
