'use strict()';

// Copyright 2012 - 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

var PARTIAL_PATH = "./partials/";

var PATHS = {
    'login': '/user/login',
    //    'register': '/user/register',
    'logout': '/user/logout',
    //    'home': '/home',
    //    'settings': '/settings',
    'map': '/map',
    //    'admin': '/admin',
    //    'help': '/help',
    'alarme': '/alarme/:groupid',
    //    'users':'/users',
    'groups': '/groups',
    'roles': '/roles',
    //    'test': '/test'
    'resetPW': '/user/resetPW/:user/:hash/'
};

var defaultThemeID = 0;

var requestDefaultThemeID = new XMLHttpRequest();
var url = "/utils/themeid";
var params = "";
requestDefaultThemeID.open("POST", url, false);

//Send the proper header information along with the request
requestDefaultThemeID.setRequestHeader("Content-type", "application/json");
requestDefaultThemeID.send(params);

if (requestDefaultThemeID.status == 200) {
    defaultThemeID = JSON.parse(requestDefaultThemeID.responseText).theme_id;
}


(function() {

    /**
     * Initializing the Blids app js
     *
     */
    var blids = angular.module('blidsApp', ['ngRoute', 'ngCookies', 'ngMessages', 'ui.bootstrap', 'pascalprecht.translate'])
        .config(function($routeProvider) {
            $routeProvider.when(PATHS.logout, {
                    template: "",
                    controller: function($scope, $http, $location, Auth) {
                        Auth.logout();
                        $location.path(PATHS.login);
                    }
                })
                .when(PATHS.login, {
                    templateUrl: 'partials/login.html',
                    controller: 'LoginController'
                })
                //                        .when(PATHS.register, {
                //                            templateUrl: url.partpath + 'register.html',
                //                            controller: 'RegisterController',
                //                            resolve: {
                //                                check: checkAuth
                //                            }
                //                        })
                //                        .when(PATHS.home, {
                //                            templateUrl: url.partpath + 'home.html',
                //                            controller: 'HomeController',
                //                            resolve: {
                //                                check: checkAuth
                //                            }
                //                        })
                .when(PATHS.map, {
                    templateUrl: 'partials/map.html',
                    controller: 'mapCtrl',
                    resolve: {
                        check: checkAuth
                    }
                })
                .when(PATHS.resetPW, {
                    templateUrl: 'partials/resetPW.html',
                    controller: 'resetPwCtrl',
                    resolve: {
                        "test": ['$http', '$route', function($http, $route) {
                            var valid = $http.post('auth/checkHash', { "user": $route.current.params.user, "hash": $route.current.params.hash });
                            return valid;
                        }]
                    }
                })
                .when(PATHS.alarme, {
                    templateUrl: 'partials/admin-pages/alarme.html',
                    controller: 'AlarmsTabCtrl as AlarmCtrl',
                    resolve: {
                        check: checkAuth,
                        "alarmlist": ['$http', '$route', function($http, $route) {
                            var list = $http.post('admin/alarmlist', { "groupid": parseInt($route.current.params.groupid) });
                            return list;
                        }]
                    }
                })
                .when(PATHS.groups, {
                    templateUrl: 'partials/admin-pages/groups.html',
                    controller: 'GroupsTabCtrl as GTCtrl',
                    resolve: {
                        check: checkAuth,
                        "GroupList": ['$http', function($http) {
                            var liste = $http.post('admin/groupview', { "groupid": null });
                            return liste;
                        }]
                    }
                })
                .when(PATHS.roles, {
                    templateUrl: 'partials/admin-pages/roles.html',
                    controller: 'Roles_Permission_TabCtrl as RolePermCtrl',
                    resolve: {
                        check: checkAuth,
                        "roleList": ['$http', function($http) {
                            var liste = $http.post('admin/rolelist', {});
                            return liste;
                        }]
                    }
                })

            .otherwise({ redirectTo: PATHS.logout });
        });

    blids.config(['$translateProvider', function($translateProvider) {
        // configures staticFilesLoader
        $translateProvider.useStaticFilesLoader({
            prefix: '../assets/i18n/locale-',
            suffix: '.json'
        });
        // load 'de' table on startup
        $translateProvider.preferredLanguage('de');
        $translateProvider.useSanitizeValueStrategy('escape');
    }]);

    blids.constant('DEFAULT_THEME', defaultThemeID);

    var checkAuth = function(Auth, $location, $q) {
        var deferred = $q.defer();
        if (Auth.isAuthenticated() === true)
            deferred.resolve(true);
        else {
            $location.path("/user/logout");
            deferred.reject(false);
        }
        return deferred.promise;
    };

})();