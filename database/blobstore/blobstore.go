// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package blobstore

import (
	"database/sql"
	"sync"

	"bitbucket.org/intevation/kugelblitz/database"
)

// Item represents a blob from the database.
type Item struct {
	Name     string
	MIMEType string
	Data     string

	hitsMu sync.Mutex
	hits   int
}

const maxCacheEntries = 32

var (
	itemsMu sync.Mutex
	items   = map[string]*Item{}
)

var loadItemStmt = database.Must(
	`SELECT mimetype, data FROM blobstore WHERE name = $1`)

// GetItem fetches a blob item from the database
// or returns an error if one occurs.
// The most used 32 entries are cached in memory to
// reduce the pressure to the database.
func GetItem(name string, fresh bool) (*Item, error) {
	itemsMu.Lock()
	defer itemsMu.Unlock()

	if !fresh {
		it := items[name]

		if it != nil {
			it.touch()
			return it, nil
		}
	}

	var mType, data string

	err := loadItemStmt.QueryRow(name).Scan(&mType, &data)

	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}

	if len(items) == maxCacheEntries {
		// Remove least used
		min := -1
		var key string
		for k, i := range items {
			if h := i.numHits(); min == -1 || h < min {
				min, key = h, k
			}
		}
		delete(items, key)
	}

	it := &Item{
		Name:     name,
		MIMEType: mType,
		Data:     data,
		hits:     1,
	}

	items[name] = it

	return it, nil
}

func (it *Item) touch() {
	it.hitsMu.Lock()
	it.hits++
	it.hitsMu.Unlock()
}

func (it *Item) numHits() int {
	it.hitsMu.Lock()
	hits := it.hits
	it.hitsMu.Unlock()
	return hits
}
