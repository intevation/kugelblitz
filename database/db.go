// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package database

import (
	"database/sql"
	"encoding/base64"
	"errors"
	"log"
	"os"
	"strings"

	// We depend on PostgreSQL.
	_ "github.com/lib/pq"
)

// DB is the global connection to the configured database.
var DB *sql.DB

var dsnReplacer = strings.NewReplacer(
	"u=", "user=",
	"pw=", "password=",
	"h=", "host=",
	"d=", "dbname=",
	"p=", "port=",
	"s=", "sslmode=")

// DSN returns the data soure name from the environment.
// If there is a configuration problem an error is returned.
func DSN() (string, error) {
	// TODO more sophistcated way to configure database connection.
	conn := os.Getenv("DB_CONNECTION")
	if conn == "" {
		return "", errors.New("Missing DB_CONNECTION env variable")
	}
	// Env vars in beanstalk are not allowed to have special chars
	// so the hole string is encoded in Base64.
	dec, err := base64.StdEncoding.DecodeString(conn)
	if err == nil {
		conn = string(dec)
	}
	return dsnReplacer.Replace(conn), nil
}

func init() {
	dsn, err := DSN()
	if err != nil {
		log.Fatalf("DSN problem: %v\n", err)
	}

	if DB, err = sql.Open("postgres", dsn); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

// Must prepares SQL statements on the global database connection.
// If the preparation fails it panics.
func Must(query string) *sql.Stmt {
	stmt, err := DB.Prepare(strings.TrimSpace(query))
	if err != nil {
		panic(err)
	}
	return stmt
}
