#!/bin/sh

go get -u -v github.com/lib/pq
go get -u -v github.com/gorilla/securecookie
go get -u -v github.com/gorilla/mux
go get -u -v gopkg.in/gomail.v2
go get -u -v github.com/mb0/wkt
go get -u -v github.com/eclipse/paho.mqtt.golang
# Used to generate eCall backend from WSDL.
# As we include the pregenerated cor this is no compile or runtime
# requirement.
# go get -u -v github.com/hooklift/gowsdl
