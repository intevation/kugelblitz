# How to setup a blids database

## Setup

To setup a blids database follow the following steps:

```shell
createdb blids_test01
psql blids_test01 -c '\i blids-roles.sql' \
                  -c '\i blids-schema.sql' \
                  -c '\i blids-schema-extensions.sql' \
                  -c '\i alarmserver/schema-extensions.sql' \
                  -c '\i alarmserver/functions.sql'
```

Then set passwords for the roles the applications going to use (See below).

## Roles

**TODO** document roles and there use.

## Notifications

The database sends notifications on the channel *blids* triggered by
various events, for use by external applications.

### Overall Notification Format

The payload of notifications on the *blids* channel are JSON objects
of the format

```json
{
   "type" : $TYPE
   "data" : $TYPE_SPECIFIC_DATA
}
```

The implemented `$TYPEs` and  associated data is documented in the
following sections.

### Stroke Notification Trigger

On new entries in the current stroke table (`blitz.blitze2018`), a
notification of the $TYPE `"stroke"` is generated.  The
`$TYPE_SPECIFIC_DATA` for this notification type is the JSON
representation of the complete new row.

A example notification of the type `"stroke"`:

```json
{
    "type": "stroke",
    "data": {
        "chiquadrat": 2.52,
        "eintrag": "2018-03-22T12:31:09.258321+00:00",
        "freigrade": 17,
        "grosseachse": 0.58,
        "hoehe": 0,
        "id": 72187,
        "kleineachse": 0.1,
        "koordinate": "POINT(11.0 52.0)",
        "sensorzahl": 10,
        "spitzenull": 29.7,
        "steigzeit": 4.6,
        "strom": -61.1,
        "tlp": 1,
        "typ": 1,
        "winkelachse": 126.88,
        "zeit": "2018-03-22T12:30:52.115+00:00"
    }
}
```

### Alarm Notification Trigger

On each new entry in `alarmserver.meldungen` a notification on the
channel *alarms* is triggered.  The `$TYPE` in this case is one of

- `"Alarm"`
- `"Entwarnung"`
- `"Testalarm"`
- `"Testentwarnung"`

There are two additional possible values: `"Vorwarnung"` and
`"Testvorwarnung"` but it seems they have never been completely
implemented and should never occur.

The `$TYPE_SPECIFIC_DATA` for this notification types is the JSON
representation of the data for the alarm messages to send and the
geometry of the affected area:

```json
{
    "id":        $ROW_ID,
    "area":      $WKT_STRING,
    "recipient": $RECIPIENT,
    "body":      $MESSAGE_TEXT,
    "subject":   $SUBJECT,
    "email":     $EMAIL_ADDRESS,
    "fax":       $FAX_NUMBER,
    "sms":       $SMS_NUMBER,
    "tel":       $TEL_NUMBER
}
```

where

- `"id"` is the id of the matching alarm message in the table
  `alarmserver.meldungen`.  This is intended for the
  alarmserver application to set the columns `erledigt`, `status` and
  `status_info`when it sends a message.

- `"area"` is the WKT encoded geometry of the concerned area.

- `"recipient"` contains the recipient name (`teilnehmer` string from
  `alarmserver.teilnehmer`)

- `"body"` contains the text for the message body.  This is the matching
  template text from `alarmserver.teilnehmer` for the given alarm type
  (`Alarm` or `Entwarnung`) with all variable values filled in.

- `"subject"` contains the subject to use in email messages matching the
  alarm type.

- `"email"`, `"sms"`, `"tel"`, `"fax"` are optional and contain the
  respective contact information from `alarmserver.teilnehmer` if
  available.
