-- Copyright 2018 by Siemens AG. All rights reserved.
-- Engineering by Intevation GmbH.
-- Use of this source code is governed by the Apache 2.0 license
-- that can be found in the LICENSE file.

BEGIN;

CREATE TABLE configuration (
    id   SERIAL PRIMARY KEY NOT NULL,
    name varchar(20)        NOT NULL UNIQUE,
    description text
);

CREATE TABLE configuration_kv (
    configuration_id integer     NOT NULL REFERENCES configuration(id),
    key              varchar(20) NOT NULL,
    value            text        NOT NULL,
    UNIQUE (configuration_id, key)
);

GRANT SELECT ON TABLE configuration TO kugelblitz;
GRANT SELECT ON TABLE configuration_kv TO kugelblitz;

COMMIT;
