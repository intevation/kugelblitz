-- Copyright 2018 by Siemens AG. All rights reserved.
-- Engineering by Intevation GmbH.
-- Use of this source code is governed by the Apache 2.0 license
-- that can be found in the LICENSE file.

BEGIN;

-- Additional stroke data:

ALTER TABLE blitz.blitze
  ADD IF NOT EXISTS maxraterise numeric,
  ADD IF NOT EXISTS hoeheungenau numeric,
  ADD IF NOT EXISTS strokezahl integer,
  ADD IF NOT EXISTS pulsezahl integer;

--
-- User for the kugelblitz server
--
CREATE ROLE kugelblitz;
ALTER ROLE kugelblitz WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;  -- Set PW for access
\echo set password for new role kugelblitz to use it with the kugelblitz application.

GRANT USAGE ON SCHEMA alarmserver TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE alarmserver.alarme TO kugelblitz;
GRANT USAGE ON SEQUENCE alarmserver.alarme_id_seq TO kugelblitz;
GRANT SELECT ON TABLE alarmserver.gebiet2gruppe TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE alarmserver.gebiete TO kugelblitz;
GRANT USAGE ON SEQUENCE alarmserver.gebiete_id_seq TO kugelblitz;
GRANT SELECT ON TABLE alarmserver.gebietgruppen TO kugelblitz;
GRANT SELECT ON TABLE alarmserver.kunden TO kugelblitz;
GRANT SELECT,INSERT ON TABLE alarmserver.meldungen TO kugelblitz;
GRANT USAGE ON SEQUENCE alarmserver.meldungen_id_seq TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE alarmserver.teilnehmer TO kugelblitz;
GRANT USAGE ON SEQUENCE alarmserver.teilnehmer_id_seq TO kugelblitz;
GRANT SELECT ON TABLE alarmserver.teilnehmer_aktiv TO kugelblitz;

GRANT USAGE ON SCHEMA benutzer TO kugelblitz;
GRANT SELECT,INSERT, UPDATE,DELETE ON TABLE benutzer.benutzer TO kugelblitz;
GRANT USAGE ON SEQUENCE benutzer.benutzer_id_seq TO kugelblitz;
GRANT SELECT,INSERT, UPDATE,DELETE ON TABLE benutzer.berechtigungen TO kugelblitz;
GRANT SELECT ON TABLE benutzer.ebenen TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE benutzer.gruppen TO kugelblitz;
GRANT USAGE ON SEQUENCE benutzer.gruppen_id_seq TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE benutzer.gruppe2alarm TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE benutzer.gruppen_ebenen TO kugelblitz;
GRANT USAGE ON SEQUENCE benutzer.gruppen_ebenen_id_seq TO kugelblitz;
GRANT INSERT ON TABLE benutzer.logging TO kugelblitz;
GRANT USAGE ON SEQUENCE benutzer.logging_id_seq TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE benutzer.resetpw TO kugelblitz;
GRANT USAGE ON SEQUENCE benutzer."resetPW_id_seq" TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE benutzer.rollen TO kugelblitz;
GRANT USAGE ON SEQUENCE benutzer.rollen_id_seq TO kugelblitz;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE benutzer.sessions TO kugelblitz;
GRANT SELECT ON TABLE benutzer.themes TO kugelblitz;

GRANT USAGE ON SCHEMA blitz TO kugelblitz;
GRANT SELECT ON TABLE blitz.blitze TO kugelblitz;

COMMIT;
