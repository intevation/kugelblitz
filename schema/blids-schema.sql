--
-- PostgreSQL database dump
--

-- Copyright 2018 by Siemens AG. All rights reserved.
-- Engineering by Intevation GmbH.
-- Use of this source code is governed by the Apache 2.0 license
-- that can be found in the LICENSE file.

-- Current schema dumped on 20180312 -- <wilde@intevation.de>
-- Added new "live" funcitons on 20180329 -- <wilde@intevation.de>

-- Dumped from database version 10.2 (Ubuntu 10.2-1.pgdg16.04+1)
-- Dumped by pg_dump version 10.2 (Ubuntu 10.2-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: alarmserver; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA alarmserver;


ALTER SCHEMA alarmserver OWNER TO postgres;

--
-- Name: benutzer; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA benutzer;


ALTER SCHEMA benutzer OWNER TO postgres;

--
-- Name: blitz; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA blitz;


ALTER SCHEMA blitz OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = alarmserver, pg_catalog;

--
-- Name: alarmtyp; Type: TYPE; Schema: alarmserver; Owner: postgres
--

CREATE TYPE alarmtyp AS ENUM (
    'Alarm',
    'Entwarnung',
    'Vorwarnung',
    'Testalarm',
    'Testentwarnung',
    'Testvorwarnung'
);


ALTER TYPE alarmtyp OWNER TO postgres;

SET search_path = benutzer, pg_catalog;

--
-- Name: rolle; Type: TYPE; Schema: benutzer; Owner: postgres
--

CREATE TYPE rolle AS (
	id integer,
	rollenname character varying,
	kann_einloggen boolean,
	alarm_admin character(1),
	alarm_gebiete character(1),
	alarm_gebietgruppen character(1),
	alarm_teilnehmer character(1),
	alarm_alarme character(1),
	user_admin character(1),
	gruppen_admin character(1)
);


ALTER TYPE rolle OWNER TO postgres;

--
-- Name: session; Type: TYPE; Schema: benutzer; Owner: postgres
--

CREATE TYPE session AS (
	id integer,
	username character varying,
	start timestamp with time zone,
	stop timestamp with time zone,
	bounds public.box2d,
	max_displayzeit integer,
	archiv_tage integer,
	archiv_ab timestamp with time zone,
	max_zoom integer,
	min_zoom integer,
	loginallowed boolean,
	liveblids boolean,
	animation boolean,
	sound boolean,
	theme_id integer,
	statistik_windowed boolean,
	statistikgebiet boolean
);


ALTER TYPE session OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: liveblitz; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE liveblitz AS (
	id integer,
	zeit timestamp with time zone,
	typ integer,
	strom numeric,
	koordinate geometry,
	tlp integer,
	unixzeit integer
);


ALTER TYPE liveblitz OWNER TO postgres;

--
-- Name: livegebiet; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE livegebiet AS (
	id integer,
	gebiet character varying,
	gebietgeom geometry,
	blitzanzahl integer,
	alarm boolean,
	aktiv boolean
);


ALTER TYPE livegebiet OWNER TO postgres;

SET search_path = alarmserver, pg_catalog;

--
-- Name: gebietalarm(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION gebietalarm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
	alarmrec RECORD;
	gebiet int;
	sql varchar;
BEGIN
	gebiet := NEW.id;
	-- Wenn Alarmierung erreicht
	IF ( NEW.blitzanzahl >= OLD.blitzezumalarm and OLD.alarm is false) THEN
		NEW.alarm := true;
		-- Alle Gruppen, die das Gebiet enthalten auf Alarm setzen
		EXECUTE 'UPDATE alarmserver.gebietgruppen set alarm = true where alarm = false 
			and id in (Select distinct gruppe from alarmserver.gebiet2gruppe where gebiet = ' || gebiet || ')';
		-- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
		sql := 'select * from alarmserver.alarme where gebiet = ' || gebiet;
		FOR alarmrec IN EXECUTE sql LOOP
			EXECUTE 'INSERT INTO alarmserver.meldungen (id, alarm, typ) values (DEFAULT,' || alarmrec.id || ',''Alarm'')';
		END LOOP;
	-- Wenn Entwarnung aus Programm kommt
	ELSEIF (OLD.alarm is true and NEW.alarm is false) THEN
		-- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
		sql := 'select * from alarmserver.alarme where gebiet = ' || gebiet;
		FOR alarmrec IN EXECUTE sql LOOP
			EXECUTE 'INSERT INTO alarmserver.meldungen (id, alarm, typ) values (DEFAULT,' || alarmrec.id || ',''Entwarnung'')';
		END LOOP;
	END IF;
        RETURN NEW;
END;
$$;


ALTER FUNCTION alarmserver.gebietalarm() OWNER TO postgres;

--
-- Name: gruppealarm(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION gruppealarm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
	alarmrec RECORD;
	gruppe int;
	sql varchar;
BEGIN
	gruppe := NEW.id;
	-- Wenn Alarmierung erreicht
	IF ( OLD.alarm is false and NEW.alarm is true) THEN
		-- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
		sql := 'select * from alarmserver.alarme where gruppe = ' || gruppe;
		FOR alarmrec IN EXECUTE sql LOOP
			EXECUTE 'INSERT INTO alarmserver.meldungen (id, alarm, typ) values (DEFAULT,' || alarmrec.id || ',''Alarm'')';
		END LOOP;
	-- Wenn Entwarnung aus Programm kommt
	ELSEIF (OLD.alarm is true and NEW.alarm is false) THEN
		-- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
		sql := 'select * from alarmserver.alarme where gruppe = ' || gruppe;
		FOR alarmrec IN EXECUTE sql LOOP
			EXECUTE 'INSERT INTO alarmserver.meldungen (id, alarm, typ) values (DEFAULT,' || alarmrec.id || ',''Entwarnung'')';
		END LOOP;
	END IF;
        RETURN NEW;
END;
$$;


ALTER FUNCTION alarmserver.gruppealarm() OWNER TO postgres;

--
-- Name: wochentag(character varying); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION wochentag(zeit character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE 
	wochentag varchar;
	sql varchar;
	ergebnis int;
BEGIN
	sql := 'Select extract (isodow from timestamp ' || quote_literal(zeit) || ')';
        EXECUTE sql INTO ergebnis;
	IF (ergebnis = 1) THEN
       		wochentag := 'mo';
       	ELSEIF (ergebnis = 2) THEN
       		wochentag := 'di';
       	ELSEIF (ergebnis = 3) THEN
       		wochentag := 'mi';
       	ELSEIF (ergebnis = 4) THEN
       		wochentag := 'don';
       	ELSEIF (ergebnis = 5) THEN
       		wochentag := 'fr';
       	ELSEIF (ergebnis = 6) THEN
       		wochentag := 'sa';
       	ELSE 
       		wochentag := 'so';
       	END IF;
	RETURN wochentag;
END;
$$;


ALTER FUNCTION alarmserver.wochentag(zeit character varying) OWNER TO postgres;

SET search_path = benutzer, pg_catalog;

--
-- Name: alarmgebietlive(character varying); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION alarmgebietlive(session_key character varying) RETURNS SETOF public.livegebiet
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE 

      	sql	        	varchar;
      	ergebnis		livegebiet;
	v_session		record;
	zeiten			record;
BEGIN

		EXECUTE 'select home_gruppen_id as group from benutzer.benutzer t1, benutzer.sessions t2 where 
					t2.sessionid =  ' || quote_literal(session_key) || '  and t2.benutzer = t1.id' into v_session;
                
		/*EXECUTE 'SELECT benutzer.id, sessions.sessionid, benutzer.alarmkundenid
   				FROM benutzer.benutzer, benutzer.sessions
  				WHERE sessions.benutzer = benutzer.id 
				AND sessions.zeit >= (now() - ''00:02:00''::interval)
				AND sessions.sessionid = ' || quote_literal(session_key) || ' '
		INTO v_session;*/

		IF v_session IS NULL THEN
			RETURN;
		END IF;

		EXECUTE 'SELECT to_char(now(),''YYYY-MM-DD HH24:MI:SS'') as startzeit, 
				to_char(now(),''YYYY-MM-DD HH24:MI:SS'') as stopzeit'
		INTO zeiten;

   		sql := ' SELECT distinct gebiete.id, gebiete.gebiet, gebiete.gebietgeom, gebiete.blitzanzahl, gebiete.alarm, 
			    ( SELECT teilnehmer_aktiv.aktiv
				   FROM alarmserver.teilnehmer_aktiv
				  WHERE teilnehmer_aktiv.id = alarme.teilnehmer) AS aktiv
			FROM alarmserver.alarme, alarmserver.gebiete
			WHERE (alarme.kunde IN  
				( SELECT gruppe2alarm.slavekunde
					FROM benutzer.gruppe2alarm
					WHERE gruppe2alarm.mastergruppe = '|| v_session.group::integer  || ' 
			)) AND alarme.gebiet = gebiete.id;';


/*   		sql := ' SELECT distinct gebiete.id, gebiete.gebiet, gebiete.gebietgeom, gebiete.blitzanzahl, gebiete.alarm, 
			    ( SELECT teilnehmer_aktiv.aktiv
				   FROM alarmserver.teilnehmer_aktiv
				  WHERE teilnehmer_aktiv.id = alarme.teilnehmer) AS aktiv
			FROM alarmserver.alarme, alarmserver.gebiete
			WHERE (alarme.kunde = '|| v_session.alarmkundenid::integer  || ' OR (alarme.kunde IN  
				( SELECT kundengruppen.slavekunde
					FROM alarmserver.kundengruppen
					WHERE kundengruppen.masterkunde = '|| v_session.alarmkundenid::integer  || ' )
			)) AND alarme.gebiet = gebiete.id;';
*/
	FOR ergebnis IN EXECUTE sql LOOP
		RETURN NEXT ergebnis;
	END LOOP;
END;
$$;


ALTER FUNCTION benutzer.alarmgebietlive(session_key character varying) OWNER TO postgres;

--
-- Name: getsession(character varying, character varying); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION getsession(ibenutzer character varying, ipasswort character varying) RETURNS SETOF session
    LANGUAGE plpgsql IMMUTABLE
    AS $_$
DECLARE
	result benutzer.session ;
	userrecord record;
	userrole int;
BEGIN
	EXECUTE 'SELECT id,home_gruppen_id as hgid from benutzer.benutzer where benutzername = $1' into userrecord using ibenutzer;
	IF userrecord IS NULL THEN
			RETURN;
		END IF;
	userrole := (select * from benutzer.getuserrole(userrecord.id,userrecord.hgid));
	RETURN QUERY SELECT 	benutzer.id, benutzer.benutzername as username, benutzer.startdatum as start,
                                                benutzer.stopdatum as stop, box2d(gruppen.blitzgebiet) as bounds, gruppen.max_displayzeit,
                                                gruppen.archiv_tage, gruppen.archiv_ab, gruppen.max_zoom, gruppen.min_zoom,
                                                rollen.kann_einloggen as LogInAllowed, gruppen.liveblids, gruppen.animation, 
                                                gruppen.sound, gruppen.theme_id, gruppen.statistik_windowed, gruppen.statistikgebiet is not null as statistikgebiet
						FROM benutzer.benutzer, benutzer.gruppen, benutzer.rollen
						WHERE rollen.id = userrole
						and gruppen.id = benutzer.home_gruppen_id
						and benutzer.benutzername = ibenutzer AND benutzer.passwort = md5(ipasswort);
						/*and rollen.kann_einloggen is true;*/
						
END;
$_$;


ALTER FUNCTION benutzer.getsession(ibenutzer character varying, ipasswort character varying) OWNER TO postgres;

--
-- Name: getuserperm(integer, integer); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION getuserperm(benutzer integer, gruppe integer) RETURNS SETOF rolle
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE
    rolle integer;
    gruppe_id integer;
    result benutzer.rolle;
    child boolean;
    permission character(1);
BEGIN
	gruppe_id := gruppe;
	child := FALSE;
	LOOP
		EXECUTE 'SELECT rollen_id from benutzer.berechtigungen WHERE benutzer_id = ' || benutzer || ' AND gruppen_id = ' || gruppe_id into rolle;
		IF rolle IS not NULL THEN
			EXIT;
		ELSE
			child := TRUE;
			execute 'SELECT vatergruppe from benutzer.gruppen where id = ' || gruppe_id into gruppe_id;
			IF gruppe_id is NULL THEN
				RETURN;
			END IF;
		END IF;
	END LOOP;
	execute 'SELECT gruppen_admin from benutzer.rollen where id = ' || rolle into permission;
	if permission = 'C' then 
		if child then 
			permission = 'A';
		end if;
	end if;
		
	RETURN QUERY SELECT id, rollenname, kann_einloggen, alarm_admin, alarm_gebiete, alarm_gebietgruppen, 
							alarm_teilnehmer, alarm_alarme, user_admin, permission as gruppen_admin 
							from benutzer.rollen where id = rolle;
END;
$$;


ALTER FUNCTION benutzer.getuserperm(benutzer integer, gruppe integer) OWNER TO postgres;

--
-- Name: getuserrole(integer, integer); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION getuserrole(benutzer integer, gruppe integer) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE
    result integer;
    gruppe_id integer;
BEGIN
	gruppe_id := gruppe;

	LOOP
		EXECUTE 'SELECT rollen_id from benutzer.berechtigungen WHERE benutzer_id = ' || benutzer || ' AND gruppen_id = ' || gruppe_id into result;
		IF result IS not NULL THEN
			EXIT;
		ELSE
			execute 'SELECT vatergruppe from benutzer.gruppen where id = ' || gruppe_id into gruppe_id;
			IF gruppe_id is NULL THEN
				EXIT;
			END IF;
		END IF;
	END LOOP;
	RETURN result;
END;
$$;


ALTER FUNCTION benutzer.getuserrole(benutzer integer, gruppe integer) OWNER TO postgres;

SET search_path = blitz, pg_catalog;

--
-- Name: eintragen(); Type: FUNCTION; Schema: blitz; Owner: postgres
--

CREATE FUNCTION eintragen() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
	blitzid int;
  searchtlp VARCHAR;
BEGIN

    IF NEW.tlp = 2 THEN
      searchtlp = 1;
    ELSIF NEW.tlp = 1 THEN
      searchtlp = 2;
    END IF;

    EXECUTE 'select id from blitz.' || TG_TABLE_NAME || ' where zeit = ''' || NEW.zeit || ''' and tlp = ''' || searchtlp || ''' and typ = ' || NEW.typ|| ' and st_distance(koordinate, ''' || NEW.koordinate::TEXT || ''') < 0.04' INTO blitzid;

    -- Falls ein doppelter existiert
    IF blitzid > 0 THEN
      -- Der alte ist BLIDS der neue ALDIS
      IF NEW.tlp = 2 THEN
        EXECUTE 'Update blitz.' || TG_TABLE_NAME || ' set tlp = 4 where id = '|| blitzid;
        RETURN NULL;
      ELSIF NEW.tlp = 1 THEN
        EXECUTE 'Update blitz.' || TG_TABLE_NAME || ' set tlp = 3 where id = '|| blitzid;
        RETURN NULL;
      END IF;

    END IF;

    RETURN NEW;
END;
$$;


ALTER FUNCTION blitz.eintragen() OWNER TO postgres;

SET search_path = public, pg_catalog;


--
-- Name: blitzlive(character varying, integer, integer, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION blitzlive(statistikbox character varying, zeit1 integer, zeit2 integer, co_left numeric, co_right numeric, co_top numeric, co_bottom numeric) RETURNS SETOF liveblitz
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE 

      	sql	        	varchar;
      	ergebnis		liveblitz;
--	v_session		record;
	zeiten			record;
BEGIN

		EXECUTE 'SELECT to_char(now() - interval ''' || zeit1 ||' seconds'',''YYYY-MM-DD HH24:MI:SS'') as startzeit, 
				to_char(now() - interval ''' || zeit2 ||' seconds'',''YYYY-MM-DD HH24:MI:SS'') as stopzeit'
		INTO zeiten;
		--WHERE st_within(koordinate,' || quote_literal(v_session.blitzgebiet::text) || '::geometry) AND

   		sql := 'SELECT id, zeit, typ, strom, koordinate, tlp, round(date_part(''EPOCH''::text, now() - zeit)) AS unixzeit
   				FROM blitz.blitze
				WHERE zeit between ' || quote_literal(zeiten.startzeit) || '::timestamp with time zone and ' || quote_literal(zeiten.stopzeit) || '::timestamp with time zone
				AND koordinate && st_transform(st_geometryfromText(''POLYGON(('|| co_left ||' '|| co_bottom ||','|| co_left ||' '|| co_top ||',
					'|| co_right ||' '|| co_top ||' ,'|| co_right ||' '|| co_bottom ||' ,'|| co_left ||' '|| co_bottom ||'))'',3857),4326) ';
		if statistikbox ilike '%polygon%' and char_length(statistikbox) > 10 then
			sql := sql || 'AND st_within(koordinate, st_geometryfromtext('''||statistikbox||''',4326)) ';
		end if;
		sql := sql || 'ORDER BY zeit';

	FOR ergebnis IN EXECUTE sql LOOP
		RETURN NEXT ergebnis;
	END LOOP;
END;
$$;


ALTER FUNCTION public.blitzlive(statistikbox character varying, zeit1 integer, zeit2 integer, co_left numeric, co_right numeric, co_top numeric, co_bottom numeric) OWNER TO postgres;

--
-- Name: blitzlivenew(integer, integer, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION blitzlivenew(zeit1 integer, zeit2 integer, co_left numeric, co_right numeric, co_top numeric, co_bottom numeric) RETURNS SETOF liveblitz
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE 

      	sql	        	varchar;
      	ergebnis		liveblitz;
	zeiten			record;
BEGIN

		EXECUTE 'SELECT to_char(now() - interval ''' || zeit1 ||' seconds'',''YYYY-MM-DD HH24:MI:SS'') as startzeit, 
				to_char(now() - interval ''' || zeit2 ||' seconds'',''YYYY-MM-DD HH24:MI:SS'') as stopzeit'
		INTO zeiten;

   		sql := 'SELECT id, zeit, typ, strom, koordinate, tlp, round(date_part(''EPOCH''::text, now() - zeit)) AS unixzeit
   				FROM blitz.blitze
				WHERE zeit between ' || quote_literal(zeiten.startzeit) || '::timestamp with time zone and ' || quote_literal(zeiten.stopzeit) || '::timestamp with time zone
				AND koordinate && st_transform(st_geometryfromText(''POLYGON(('|| co_left ||' '|| co_bottom ||','|| co_left ||' '|| co_top ||',
					'|| co_right ||' '|| co_top ||' ,'|| co_right ||' '|| co_bottom ||' ,'|| co_left ||' '|| co_bottom ||'))'',3857),4326) 
				ORDER BY zeit';

	FOR ergebnis IN EXECUTE sql LOOP
		RETURN NEXT ergebnis;
	END LOOP;
END;
$$;


ALTER FUNCTION public.blitzlivenew(zeit1 integer, zeit2 integer, co_left numeric, co_right numeric, co_top numeric, co_bottom numeric) OWNER TO postgres;


--
-- Name: process_strokes(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_strokes(v_rest integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
        DECLARE
                v_high          int8;
                v_low           int8;
                v_record        record;
                r_gebiet        record;
                sql             varchar;
        BEGIN

            execute 'select blitzid, alarmgebiet, blitzgebiet from blitz.verarbeitung where id = ' || v_rest || ' '
            into r_gebiet;

            v_high := r_gebiet.blitzid;
    execute 'select max(id) from blitz.blitze where st_within(koordinate,'|| quote_literal(r_gebiet.blitzgebiet::text)
||'::geometry)
                    and zeit > ''NOW''::timestamp with time zone - ''15 minutes''::interval' into v_low;
    IF v_low is not null then
        v_high := v_low;
    END IF;
    sql := 'with y as (
                SELECT id, zeit, koordinate
                from blitz.blitze where id > '|| r_gebiet.blitzid || '
and st_within(koordinate,'|| quote_literal(r_gebiet.blitzgebiet::text)
||'::geometry)
                and zeit > ''NOW''::timestamp with time zone - ''15
minutes''::interval
            )
            select gebiete.id, count(y.*) as anzahl , max(y.id) as
blitzid, max(y.zeit) as zeit from y, alarmserver.gebiete
            where st_within(y.koordinate,gebiete.gebietgeom)
            group by gebiete.id';

    FOR v_record IN EXECUTE sql
    LOOP
        execute 'UPDATE alarmserver.gebiete set letzterblitz = ' ||
quote_literal(v_record.zeit) ||', blitzanzahl = blitzanzahl + ' ||
v_record.anzahl || ' where id = '|| v_record.id || ' and
        st_within(st_centroid(gebietgeom),'||
quote_literal(r_gebiet.alarmgebiet::text) ||'::geometry)';
        v_high := v_record.blitzid;
            END LOOP;

            UPDATE blitz.verarbeitung SET blitzid = v_high where id =
v_rest;

        END;
$$;


ALTER FUNCTION public.process_strokes(v_rest integer) OWNER TO postgres;

SET search_path = alarmserver, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adminlogins; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE adminlogins (
    id integer NOT NULL,
    kunde integer,
    zeit timestamp with time zone DEFAULT now(),
    anmeldetyp character varying
);


ALTER TABLE adminlogins OWNER TO postgres;

--
-- Name: adminlogins_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE adminlogins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE adminlogins_id_seq OWNER TO postgres;

--
-- Name: adminlogins_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE adminlogins_id_seq OWNED BY adminlogins.id;


--
-- Name: alarme; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarme (
    id integer NOT NULL,
    gebiet integer,
    teilnehmer integer,
    kunde integer,
    gruppe integer
);


ALTER TABLE alarme OWNER TO postgres;

--
-- Name: alarme_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarme_id_seq OWNER TO postgres;

--
-- Name: alarme_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarme_id_seq OWNED BY alarme.id;


--
-- Name: kunden; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE kunden (
    id integer NOT NULL,
    kunde character varying,
    benutzer character varying,
    passwort character varying,
    gebiete boolean DEFAULT false,
    teilnehmer boolean DEFAULT false,
    gruppen boolean DEFAULT false,
    alarme boolean DEFAULT true
);


ALTER TABLE kunden OWNER TO postgres;

--
-- Name: anmeldungen; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW anmeldungen AS
 SELECT kunden.kunde,
    to_char(adminlogins.zeit, 'DD.MM.YYYY HH24:MI:SS'::text) AS anmeldezeit,
    adminlogins.anmeldetyp,
    adminlogins.zeit
   FROM adminlogins,
    kunden
  WHERE (kunden.id = adminlogins.kunde)
  ORDER BY adminlogins.zeit DESC;


ALTER TABLE anmeldungen OWNER TO postgres;

--
-- Name: gebiet2gruppe; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE gebiet2gruppe (
    id integer NOT NULL,
    gruppe integer,
    gebiet integer
);


ALTER TABLE gebiet2gruppe OWNER TO postgres;

--
-- Name: gebiet2gruppe_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE gebiet2gruppe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gebiet2gruppe_id_seq OWNER TO postgres;

--
-- Name: gebiet2gruppe_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE gebiet2gruppe_id_seq OWNED BY gebiet2gruppe.id;


--
-- Name: gebiete; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE gebiete (
    id integer NOT NULL,
    gebietgeom public.geometry(MultiPolygon,4326),
    gebiet character varying,
    kunde integer,
    letzterblitz timestamp with time zone,
    blitzezumalarm integer,
    alarmdauer integer,
    blitzanzahl integer DEFAULT 0,
    alarm boolean DEFAULT false,
    radius numeric,
    zentrum public.geometry(Point,4326),
    icon character varying,
    flaeche numeric DEFAULT 0
);


ALTER TABLE gebiete OWNER TO postgres;

--
-- Name: COLUMN gebiete.flaeche; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN gebiete.flaeche IS 'Flaeche in km²';


--
-- Name: gebiete_copy; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE gebiete_copy (
    id integer NOT NULL,
    gebietgeom public.geometry(MultiPolygon,4326),
    gebiet character varying,
    kunde integer,
    letzterblitz timestamp with time zone,
    blitzezumalarm integer,
    alarmdauer integer,
    blitzanzahl integer DEFAULT 0,
    alarm boolean DEFAULT false,
    radius numeric,
    zentrum public.geometry(Point,4326),
    icon character varying,
    flaeche numeric DEFAULT 0
);


ALTER TABLE gebiete_copy OWNER TO postgres;

--
-- Name: COLUMN gebiete_copy.flaeche; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN gebiete_copy.flaeche IS 'Flaeche in km/2';


--
-- Name: gebiete_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE gebiete_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gebiete_id_seq OWNER TO postgres;

--
-- Name: gebiete_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE gebiete_id_seq OWNED BY gebiete.id;


--
-- Name: gebietgruppen; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE gebietgruppen (
    id integer NOT NULL,
    gruppenname character varying,
    alarm boolean,
    kunde integer
);


ALTER TABLE gebietgruppen OWNER TO postgres;

--
-- Name: gebietgruppen_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE gebietgruppen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gebietgruppen_id_seq OWNER TO postgres;

--
-- Name: gebietgruppen_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE gebietgruppen_id_seq OWNED BY gebietgruppen.id;


--
-- Name: kunden_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE kunden_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE kunden_id_seq OWNER TO postgres;

--
-- Name: kunden_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE kunden_id_seq OWNED BY kunden.id;


--
-- Name: kundengruppen; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE kundengruppen (
    masterkunde integer NOT NULL,
    slavekunde integer NOT NULL
);


ALTER TABLE kundengruppen OWNER TO postgres;

--
-- Name: TABLE kundengruppen; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON TABLE kundengruppen IS 'ordnet einem Masterkunden untergeordnete Slavekunden zu.';


--
-- Name: COLUMN kundengruppen.masterkunde; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN kundengruppen.masterkunde IS 'Der Kunde der den Slavekunden bearbeiten kann';


--
-- Name: COLUMN kundengruppen.slavekunde; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN kundengruppen.slavekunde IS 'Der Kunder der vm Masterkunden bearbeitet werden kann';


--
-- Name: meldungen; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE meldungen (
    id integer NOT NULL,
    alarm integer,
    eintrag timestamp with time zone DEFAULT now(),
    erledigt timestamp with time zone,
    typ alarmtyp
);


ALTER TABLE meldungen OWNER TO postgres;

--
-- Name: meldungen_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE meldungen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE meldungen_id_seq OWNER TO postgres;

--
-- Name: meldungen_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE meldungen_id_seq OWNED BY meldungen.id;


--
-- Name: tcom_consumer; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW tcom_consumer AS
 SELECT gebiete.gebiet,
    gebiete.gebietgeom,
    gebiete.blitzanzahl,
    gebiete.alarm
   FROM gebiete
  WHERE ((gebiete.kunde = 112) AND ((gebiete.gebiet)::text ~~ 'FS%C%'::text));


ALTER TABLE tcom_consumer OWNER TO postgres;

--
-- Name: tcom_network; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW tcom_network AS
 SELECT gebiete.gebiet,
    gebiete.gebietgeom,
    gebiete.blitzanzahl,
    gebiete.alarm
   FROM gebiete
  WHERE ((gebiete.kunde = 112) AND ((gebiete.gebiet)::text ~~ 'FS%Network%'::text));


ALTER TABLE tcom_network OWNER TO postgres;

--
-- Name: tcom_onkz; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW tcom_onkz AS
 SELECT gebiete.gebiet,
    gebiete.gebietgeom,
    gebiete.blitzanzahl,
    gebiete.alarm
   FROM gebiete
  WHERE ((gebiete.kunde = 112) AND ((gebiete.gebiet)::text !~~ 'FS%'::text) AND ((gebiete.gebiet)::text !~~ '%ATS%'::text));


ALTER TABLE tcom_onkz OWNER TO postgres;

--
-- Name: tcom_onkz_num; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW tcom_onkz_num AS
 SELECT gebiete.gebiet,
    "substring"((gebiete.gebiet)::text, '[0-9]*'::text) AS onkz,
    gebiete.gebietgeom,
    gebiete.blitzanzahl,
    gebiete.alarm
   FROM gebiete
  WHERE ((gebiete.kunde = 112) AND ((gebiete.gebiet)::text !~~ 'FS%'::text) AND ((gebiete.gebiet)::text !~~ '%ATS%'::text));


ALTER TABLE tcom_onkz_num OWNER TO postgres;

--
-- Name: teilnehmer; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE teilnehmer (
    id integer NOT NULL,
    teilnehmer character varying,
    sms character varying,
    email character varying,
    tel character varying,
    fax character varying,
    alarmierung text,
    entwarnung text,
    kunde integer,
    startdatum timestamp with time zone,
    stopdatum timestamp with time zone,
    startzeit time without time zone DEFAULT '00:00:00'::time without time zone,
    stopzeit time without time zone DEFAULT '24:00:00'::time without time zone,
    mo boolean DEFAULT true,
    di boolean DEFAULT true,
    mi boolean DEFAULT true,
    don boolean DEFAULT true,
    fr boolean DEFAULT true,
    sa boolean DEFAULT true,
    so boolean DEFAULT true,
    nurlog boolean DEFAULT false,
    utc boolean DEFAULT false,
    betreffalarm character varying DEFAULT 'BLIDS Alarm'::character varying,
    betreffentwarn character varying DEFAULT 'BLIDS Entwarnung'::character varying,
    blitzprotokoll boolean DEFAULT false,
    blitzascsv boolean DEFAULT false
);


ALTER TABLE teilnehmer OWNER TO postgres;

--
-- Name: COLUMN teilnehmer.stopdatum; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN teilnehmer.stopdatum IS 'Uhrzeit wird nicht beachtet';


--
-- Name: teilnehmer_aktiv; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW teilnehmer_aktiv AS
 SELECT teilnehmer.id,
    teilnehmer.teilnehmer,
    teilnehmer.sms,
    teilnehmer.email,
    teilnehmer.tel,
    teilnehmer.fax,
    teilnehmer.alarmierung,
    teilnehmer.entwarnung,
    teilnehmer.kunde,
    teilnehmer.startdatum,
    teilnehmer.stopdatum,
    teilnehmer.startzeit,
    teilnehmer.stopzeit,
    teilnehmer.mo,
    teilnehmer.di,
    teilnehmer.mi,
    teilnehmer.don,
    teilnehmer.fr,
    teilnehmer.sa,
    teilnehmer.so,
    teilnehmer.nurlog,
    teilnehmer.utc,
    teilnehmer.betreffalarm,
    teilnehmer.betreffentwarn,
        CASE
            WHEN ((teilnehmer.startdatum)::date > ('now'::text)::date) THEN false
            WHEN ((teilnehmer.stopdatum)::date < ('now'::text)::date) THEN false
            WHEN ((teilnehmer.startzeit)::time with time zone > ('now'::text)::time with time zone) THEN false
            WHEN ((teilnehmer.stopzeit)::time with time zone < ('now'::text)::time with time zone) THEN false
            WHEN (teilnehmer.nurlog = true) THEN false
            WHEN ((teilnehmer.mo = true) AND (date_part('dow'::text, now()) = (1)::double precision)) THEN true
            WHEN ((teilnehmer.di = true) AND (date_part('dow'::text, now()) = (2)::double precision)) THEN true
            WHEN ((teilnehmer.mi = true) AND (date_part('dow'::text, now()) = (3)::double precision)) THEN true
            WHEN ((teilnehmer.don = true) AND (date_part('dow'::text, now()) = (4)::double precision)) THEN true
            WHEN ((teilnehmer.fr = true) AND (date_part('dow'::text, now()) = (5)::double precision)) THEN true
            WHEN ((teilnehmer.sa = true) AND (date_part('dow'::text, now()) = (6)::double precision)) THEN true
            WHEN ((teilnehmer.so = true) AND (date_part('dow'::text, now()) = (0)::double precision)) THEN true
            ELSE false
        END AS aktiv
   FROM teilnehmer;


ALTER TABLE teilnehmer_aktiv OWNER TO postgres;

--
-- Name: VIEW teilnehmer_aktiv; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON VIEW teilnehmer_aktiv IS 'zeigt welcher alarm aktiv, also scharf geschaltet ist';


--
-- Name: teilnehmer_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE teilnehmer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE teilnehmer_id_seq OWNER TO postgres;

--
-- Name: teilnehmer_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE teilnehmer_id_seq OWNED BY teilnehmer.id;


SET search_path = benutzer, pg_catalog;

--
-- Name: adminlogging; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE adminlogging (
    id integer NOT NULL,
    benutzer_id integer NOT NULL,
    datum timestamp with time zone DEFAULT now() NOT NULL,
    request character varying NOT NULL,
    sessionid character varying NOT NULL,
    ip character varying(45) DEFAULT '-'::character varying NOT NULL
);


ALTER TABLE adminlogging OWNER TO postgres;

--
-- Name: benutzer; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer (
    id integer NOT NULL,
    benutzername character varying,
    passwort character varying,
    home_gruppen_id integer,
    startdatum timestamp with time zone DEFAULT now() NOT NULL,
    stopdatum timestamp with time zone DEFAULT (now() + '1 year'::interval) NOT NULL,
    firstname character varying,
    lastname character varying,
    company character varying,
    division character varying,
    streetadress character varying,
    zip character varying,
    city character varying,
    phonenumber character varying,
    annotation character varying,
    mobilenumber character varying,
    email character varying
);


ALTER TABLE benutzer OWNER TO postgres;

--
-- Name: adminlog; Type: VIEW; Schema: benutzer; Owner: postgres
--

CREATE VIEW adminlog AS
 SELECT t1.id,
    t2.benutzername,
    t2.firstname,
    t2.lastname,
    t2.company,
    to_char(t1.datum, 'YYYY-MM-DD HH24:MI:SS'::text) AS datum,
    t1.request,
    t1.sessionid,
    t1.ip
   FROM adminlogging t1,
    benutzer t2
  WHERE (t2.id = t1.benutzer_id)
  ORDER BY t1.id DESC;


ALTER TABLE adminlog OWNER TO postgres;

--
-- Name: adminlogging_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE adminlogging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE adminlogging_id_seq OWNER TO postgres;

--
-- Name: adminlogging_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE adminlogging_id_seq OWNED BY adminlogging.id;


--
-- Name: benutzer_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer_id_seq OWNER TO postgres;

--
-- Name: benutzer_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer_id_seq OWNED BY benutzer.id;


--
-- Name: berechtigungen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE berechtigungen (
    benutzer_id integer NOT NULL,
    gruppen_id integer NOT NULL,
    rollen_id integer
);


ALTER TABLE berechtigungen OWNER TO postgres;

--
-- Name: ebenen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE ebenen (
    id integer NOT NULL,
    geoserver_ebene character varying,
    reload boolean DEFAULT false NOT NULL,
    baselayer boolean DEFAULT false NOT NULL,
    blitzlayer boolean DEFAULT false NOT NULL,
    singletile boolean DEFAULT false NOT NULL,
    standardname character varying DEFAULT 'Ebene'::character varying,
    externer_server character varying(200),
    externer_parameter character varying,
    archivlayer boolean DEFAULT false NOT NULL
);


ALTER TABLE ebenen OWNER TO postgres;

--
-- Name: ebenen_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE ebenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ebenen_id_seq OWNER TO postgres;

--
-- Name: ebenen_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE ebenen_id_seq OWNED BY ebenen.id;


--
-- Name: gruppe2alarm; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE gruppe2alarm (
    mastergruppe integer NOT NULL,
    slavekunde integer NOT NULL
);


ALTER TABLE gruppe2alarm OWNER TO postgres;

--
-- Name: TABLE gruppe2alarm; Type: COMMENT; Schema: benutzer; Owner: postgres
--

COMMENT ON TABLE gruppe2alarm IS 'ordnet einer Gruppe Alarmkunden zu.';


--
-- Name: COLUMN gruppe2alarm.mastergruppe; Type: COMMENT; Schema: benutzer; Owner: postgres
--

COMMENT ON COLUMN gruppe2alarm.mastergruppe IS 'Der Gruppe, der Alarmkunden zugeordnet werden';


--
-- Name: COLUMN gruppe2alarm.slavekunde; Type: COMMENT; Schema: benutzer; Owner: postgres
--

COMMENT ON COLUMN gruppe2alarm.slavekunde IS 'Der Kunder der von der Mastergruppe bearbeitet werden kann';


--
-- Name: gruppen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE gruppen (
    id integer NOT NULL,
    vatergruppe integer,
    gruppenname character varying,
    theme_id integer,
    blitzgebiet public.geometry,
    max_displayzeit integer,
    archiv_tage integer DEFAULT 0 NOT NULL,
    archiv_ab timestamp with time zone,
    max_zoom integer DEFAULT 14 NOT NULL,
    min_zoom integer DEFAULT 6 NOT NULL,
    liveblids boolean DEFAULT false,
    animation boolean DEFAULT false,
    sound boolean DEFAULT false,
    pfad character varying DEFAULT 1 NOT NULL,
    max_alarme integer DEFAULT 1 NOT NULL,
    max_alarmgebiete integer DEFAULT 1 NOT NULL,
    max_alarmteilnehmer integer DEFAULT 1 NOT NULL,
    alarmgebiet public.geometry,
    max_alarmarea integer DEFAULT 30000 NOT NULL,
    statistikgebiet public.geometry,
    statistik_windowed boolean DEFAULT true NOT NULL,
    CONSTRAINT enforce_dims_blitzgebiet CHECK ((public.st_ndims(blitzgebiet) = 2)),
    CONSTRAINT enforce_geotype_blitzgebiet CHECK (((public.geometrytype(blitzgebiet) = 'POLYGON'::text) OR (blitzgebiet IS NULL))),
    CONSTRAINT enforce_srid_blitzgebiet CHECK ((public.st_srid(blitzgebiet) = 4326))
);


ALTER TABLE gruppen OWNER TO postgres;

--
-- Name: COLUMN gruppen.max_alarmarea; Type: COMMENT; Schema: benutzer; Owner: postgres
--

COMMENT ON COLUMN gruppen.max_alarmarea IS 'Alarmgebiet in km² pro Alarm';


--
-- Name: gruppen_ebenen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE gruppen_ebenen (
    id integer NOT NULL,
    gruppen_id integer,
    ebenen_id integer,
    reload_time integer DEFAULT '-1'::integer NOT NULL,
    anzeige_name character varying DEFAULT 'Layer'::character varying NOT NULL,
    feature_info boolean DEFAULT false NOT NULL,
    checked_onlogin boolean DEFAULT false NOT NULL,
    permanent boolean DEFAULT false NOT NULL,
    single_tile boolean DEFAULT true NOT NULL,
    cql_filter json,
    popup_template varchar
);


ALTER TABLE gruppen_ebenen OWNER TO postgres;

--
-- Name: gruppen_ebenen_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE gruppen_ebenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gruppen_ebenen_id_seq OWNER TO postgres;

--
-- Name: gruppen_ebenen_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE gruppen_ebenen_id_seq OWNED BY gruppen_ebenen.id;


--
-- Name: gruppen_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE gruppen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gruppen_id_seq OWNER TO postgres;

--
-- Name: gruppen_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE gruppen_id_seq OWNED BY gruppen.id;


--
-- Name: host_themes; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE host_themes (
    host text NOT NULL,
    theme_id integer NOT NULL
);


ALTER TABLE host_themes OWNER TO postgres;

--
-- Name: logging; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE logging (
    id integer NOT NULL,
    benutzer character varying NOT NULL,
    datum timestamp with time zone DEFAULT now() NOT NULL,
    request character varying NOT NULL,
    ip character varying(45) DEFAULT '-'::character varying NOT NULL,
    session character varying
);


ALTER TABLE logging OWNER TO postgres;

--
-- Name: logging_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE logging_id_seq OWNER TO postgres;

--
-- Name: logging_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE logging_id_seq OWNED BY logging.id;


--
-- Name: resetpw; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE resetpw (
    id integer NOT NULL,
    benutzer integer NOT NULL,
    hash character varying NOT NULL,
    stopdatum timestamp with time zone NOT NULL,
    ip character varying(45) DEFAULT '-'::character varying NOT NULL
);


ALTER TABLE resetpw OWNER TO postgres;

--
-- Name: resetPW_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE "resetPW_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "resetPW_id_seq" OWNER TO postgres;

--
-- Name: resetPW_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE "resetPW_id_seq" OWNED BY resetpw.id;


--
-- Name: rollen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE rollen (
    id integer NOT NULL,
    rollenname character varying,
    kann_einloggen boolean DEFAULT false NOT NULL,
    alarm_admin character(1) DEFAULT 'N'::bpchar NOT NULL,
    alarm_gebiete character(1) DEFAULT 'N'::bpchar NOT NULL,
    alarm_gebietgruppen character(1) DEFAULT 'N'::bpchar NOT NULL,
    alarm_teilnehmer character(1) DEFAULT 'N'::bpchar NOT NULL,
    alarm_alarme character(1) DEFAULT 'N'::bpchar NOT NULL,
    user_admin character(1) DEFAULT 'N'::bpchar NOT NULL,
    gruppen_admin character(1) DEFAULT 'N'::bpchar NOT NULL,
    blids_counter character(1) DEFAULT 'N'::bpchar NOT NULL
);


ALTER TABLE rollen OWNER TO postgres;

--
-- Name: rollen_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE rollen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rollen_id_seq OWNER TO postgres;

--
-- Name: rollen_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE rollen_id_seq OWNED BY rollen.id;


--
-- Name: sessions; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE sessions (
    sessionid character varying(32) NOT NULL,
    daten text,
    zeit timestamp with time zone,
    benutzer integer
);


ALTER TABLE sessions OWNER TO postgres;

--
-- Name: themes; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE themes (
    id integer NOT NULL,
    name character varying NOT NULL,
    header character varying,
    footer character varying,
    style character varying,
    title character varying
);


ALTER TABLE themes OWNER TO postgres;

--
-- Name: themes_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE themes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE themes_id_seq OWNER TO postgres;

--
-- Name: themes_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE themes_id_seq OWNED BY themes.id;


--
-- Name: userlog; Type: VIEW; Schema: benutzer; Owner: postgres
--

CREATE VIEW userlog AS
 SELECT t1.id,
    t1.benutzer,
    t2.firstname,
    t2.lastname,
    t2.company,
    to_char(t1.datum, 'YYYY-MM-DD HH24:MI:SS'::text) AS datum,
    t1.request,
    t1.ip
   FROM logging t1,
    benutzer t2
  WHERE ((t1.benutzer)::text = (t2.benutzername)::text)
  ORDER BY t1.id DESC;


ALTER TABLE userlog OWNER TO postgres;

SET search_path = blitz, pg_catalog;

--
-- Name: blitze; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze (
    id integer NOT NULL,
    zeit timestamp with time zone,
    typ smallint,
    strom numeric,
    eintrag timestamp with time zone DEFAULT now(),
    koordinate public.geometry(Point,4326),
    grosseachse numeric,
    kleineachse numeric,
    winkelachse numeric,
    freigrade integer,
    chiquadrat numeric,
    steigzeit numeric,
    spitzenull numeric,
    sensorzahl numeric,
    hoehe numeric DEFAULT 0,
    tlp numeric DEFAULT 1
);


ALTER TABLE blitze OWNER TO postgres;

--
-- Name: blitze1992; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze1992 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1991-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '1992-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze1992 OWNER TO postgres;

--
-- Name: blitze1993; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze1993 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1992-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '1993-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze1993 OWNER TO postgres;

--
-- Name: blitze1994; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze1994 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1993-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '1994-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze1994 OWNER TO postgres;

--
-- Name: blitze1995; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze1995 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1994-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '1995-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze1995 OWNER TO postgres;

--
-- Name: blitze1996; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze1996 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1995-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '1996-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze1996 OWNER TO postgres;

--
-- Name: blitze1997; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze1997 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1996-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '1997-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze1997 OWNER TO postgres;

--
-- Name: blitze1998; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze1998 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1997-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '1998-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze1998 OWNER TO postgres;

--
-- Name: blitze1999; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze1999 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1998-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '1999-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze1999 OWNER TO postgres;

--
-- Name: blitze2000; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2000 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1999-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2000-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2000 OWNER TO postgres;

--
-- Name: blitze2001; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2001 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2000-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2001-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2001 OWNER TO postgres;

--
-- Name: blitze2002; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2002 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2001-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2002-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2002 OWNER TO postgres;

--
-- Name: blitze2003; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2003 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2002-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2003-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2003 OWNER TO postgres;

--
-- Name: blitze2004; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2004 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2003-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2004-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2004 OWNER TO postgres;

--
-- Name: blitze2005; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2005 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2004-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2005-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2005 OWNER TO postgres;

--
-- Name: blitze2006; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2006 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2005-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2006-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2006 OWNER TO postgres;

--
-- Name: blitze2007; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2007 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2006-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2007-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2007 OWNER TO postgres;

--
-- Name: blitze2008; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2008 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2007-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2008-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2008 OWNER TO postgres;

--
-- Name: blitze2009; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2009 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2008-12-31 23:00:00.01+00'::timestamp with time zone) AND (zeit <= '2009-12-31 23:00:00.01+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2009 OWNER TO postgres;

--
-- Name: blitze2010; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2010 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2009-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2010-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2010 OWNER TO postgres;

--
-- Name: blitze2011; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2011 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2010-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2011-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2011 OWNER TO postgres;

--
-- Name: blitze2012; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2012 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2011-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2012-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2012 OWNER TO postgres;

--
-- Name: blitze2013; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2013 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2012-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2013-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2013 OWNER TO postgres;

--
-- Name: blitze2014; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2014 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2013-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2014-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2014 OWNER TO postgres;

--
-- Name: blitze2015; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2015 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2014-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2015-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2015 OWNER TO postgres;

--
-- Name: blitze2016; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2016 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2015-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2016-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2016 OWNER TO postgres;

--
-- Name: blitze2017; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2017 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2016-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2017-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2017 OWNER TO postgres;

--
-- Name: blitze2018; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitze2018 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2017-12-31 23:00:00+00'::timestamp with time zone) AND (zeit <= '2018-12-31 23:00:00+00'::timestamp with time zone)))
)
INHERITS (blitze);


ALTER TABLE blitze2018 OWNER TO postgres;

--
-- Name: blitze_id_seq; Type: SEQUENCE; Schema: blitz; Owner: postgres
--

CREATE SEQUENCE blitze_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blitze_id_seq OWNER TO postgres;

--
-- Name: blitze_id_seq; Type: SEQUENCE OWNED BY; Schema: blitz; Owner: postgres
--

ALTER SEQUENCE blitze_id_seq OWNED BY blitze.id;


--
-- Name: verarbeitung; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE verarbeitung (
    id integer NOT NULL,
    rest integer,
    blitzid integer,
    alarmgebiet public.geometry(Polygon,4326),
    blitzgebiet public.geometry(Polygon,4326)
);


ALTER TABLE verarbeitung OWNER TO postgres;

--
-- Name: verarbeitung_id_seq; Type: SEQUENCE; Schema: blitz; Owner: postgres
--

CREATE SEQUENCE verarbeitung_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE verarbeitung_id_seq OWNER TO postgres;

--
-- Name: verarbeitung_id_seq; Type: SEQUENCE OWNED BY; Schema: blitz; Owner: postgres
--

ALTER SEQUENCE verarbeitung_id_seq OWNED BY verarbeitung.id;


--
-- Name: vergleich; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE vergleich (
    id integer NOT NULL,
    datum date NOT NULL,
    db_de integer,
    cats_de integer,
    db_eu integer,
    cats_eu integer,
    freigabe boolean DEFAULT false NOT NULL,
    eintrag timestamp with time zone DEFAULT now()
);


ALTER TABLE vergleich OWNER TO postgres;

--
-- Name: vergleich_id_seq; Type: SEQUENCE; Schema: blitz; Owner: postgres
--

CREATE SEQUENCE vergleich_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vergleich_id_seq OWNER TO postgres;

--
-- Name: vergleich_id_seq; Type: SEQUENCE OWNED BY; Schema: blitz; Owner: postgres
--

ALTER SEQUENCE vergleich_id_seq OWNED BY vergleich.id;


SET search_path = alarmserver, pg_catalog;

--
-- Name: adminlogins id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY adminlogins ALTER COLUMN id SET DEFAULT nextval('adminlogins_id_seq'::regclass);


--
-- Name: alarme id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarme ALTER COLUMN id SET DEFAULT nextval('alarme_id_seq'::regclass);


--
-- Name: gebiet2gruppe id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiet2gruppe ALTER COLUMN id SET DEFAULT nextval('gebiet2gruppe_id_seq'::regclass);


--
-- Name: gebiete id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiete ALTER COLUMN id SET DEFAULT nextval('gebiete_id_seq'::regclass);


--
-- Name: gebietgruppen id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebietgruppen ALTER COLUMN id SET DEFAULT nextval('gebietgruppen_id_seq'::regclass);


--
-- Name: kunden id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY kunden ALTER COLUMN id SET DEFAULT nextval('kunden_id_seq'::regclass);


--
-- Name: meldungen id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY meldungen ALTER COLUMN id SET DEFAULT nextval('meldungen_id_seq'::regclass);


--
-- Name: teilnehmer id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY teilnehmer ALTER COLUMN id SET DEFAULT nextval('teilnehmer_id_seq'::regclass);


SET search_path = benutzer, pg_catalog;

--
-- Name: adminlogging id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY adminlogging ALTER COLUMN id SET DEFAULT nextval('adminlogging_id_seq'::regclass);


--
-- Name: benutzer id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer ALTER COLUMN id SET DEFAULT nextval('benutzer_id_seq'::regclass);


--
-- Name: ebenen id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY ebenen ALTER COLUMN id SET DEFAULT nextval('ebenen_id_seq'::regclass);


--
-- Name: gruppen id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY gruppen ALTER COLUMN id SET DEFAULT nextval('gruppen_id_seq'::regclass);


--
-- Name: gruppen_ebenen id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY gruppen_ebenen ALTER COLUMN id SET DEFAULT nextval('gruppen_ebenen_id_seq'::regclass);


--
-- Name: logging id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY logging ALTER COLUMN id SET DEFAULT nextval('logging_id_seq'::regclass);


--
-- Name: resetpw id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY resetpw ALTER COLUMN id SET DEFAULT nextval('"resetPW_id_seq"'::regclass);


--
-- Name: rollen id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY rollen ALTER COLUMN id SET DEFAULT nextval('rollen_id_seq'::regclass);


--
-- Name: themes id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY themes ALTER COLUMN id SET DEFAULT nextval('themes_id_seq'::regclass);


SET search_path = blitz, pg_catalog;

--
-- Name: blitze id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1992 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1992 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1992 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1992 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1992 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1992 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1992 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1992 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1993 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1993 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1993 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1993 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1993 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1993 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1993 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1993 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1994 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1994 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1994 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1994 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1994 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1994 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1994 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1994 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1995 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1995 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1995 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1995 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1995 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1995 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1995 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1995 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1996 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1996 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1996 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1996 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1996 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1996 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1996 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1996 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1997 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1997 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1997 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1997 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1997 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1997 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1997 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1997 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1998 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1998 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1998 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1998 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1998 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1998 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1998 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1998 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1999 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1999 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze1999 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1999 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1999 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1999 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1999 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1999 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2000 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2000 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2000 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2000 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2000 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2000 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2000 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2000 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2001 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2001 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2001 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2001 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2001 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2001 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2001 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2001 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2002 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2002 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2002 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2002 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2002 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2002 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2002 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2002 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2003 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2003 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2003 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2003 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2003 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2003 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2003 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2003 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2004 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2004 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2004 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2004 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2004 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2004 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2004 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2004 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2005 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2005 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2005 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2005 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2005 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2005 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2005 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2005 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2006 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2006 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2006 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2006 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2006 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2006 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2006 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2006 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2007 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2007 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2007 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2007 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2007 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2007 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2007 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2007 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2008 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2008 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2008 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2008 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2008 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2008 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2008 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2008 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2009 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2009 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2009 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2009 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2009 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2009 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2009 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2009 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2010 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2010 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2010 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2010 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2010 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2010 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2010 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2010 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2011 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2011 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2011 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2011 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2011 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2011 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2011 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2011 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2012 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2012 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2012 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2012 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2012 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2012 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2012 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2012 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2013 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2013 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2013 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2013 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2013 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2013 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2013 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2013 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2014 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2014 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2014 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2014 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2014 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2014 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2014 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2014 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2015 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2015 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2015 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2015 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2015 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2015 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2015 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2015 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2016 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2016 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2016 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2016 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2016 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2016 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2016 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2016 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2017 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2017 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2017 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2017 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2017 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2017 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2017 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2017 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2018 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2018 ALTER COLUMN id SET DEFAULT nextval('blitze_id_seq'::regclass);


--
-- Name: blitze2018 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2018 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2018 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2018 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2018 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2018 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: verarbeitung id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY verarbeitung ALTER COLUMN id SET DEFAULT nextval('verarbeitung_id_seq'::regclass);


--
-- Name: vergleich id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY vergleich ALTER COLUMN id SET DEFAULT nextval('vergleich_id_seq'::regclass);


SET search_path = alarmserver, pg_catalog;

--
-- Name: adminlogins adminlogins_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY adminlogins
    ADD CONSTRAINT adminlogins_pkey PRIMARY KEY (id);


--
-- Name: alarme alarm_gebiet_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarme
    ADD CONSTRAINT alarm_gebiet_uni UNIQUE (gebiet, teilnehmer);


--
-- Name: alarme alarm_gruppe_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarme
    ADD CONSTRAINT alarm_gruppe_uni UNIQUE (teilnehmer, gruppe);


--
-- Name: alarme alarme_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarme
    ADD CONSTRAINT alarme_pkey PRIMARY KEY (id);


--
-- Name: gebiet2gruppe geb2grp_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiet2gruppe
    ADD CONSTRAINT geb2grp_pkey PRIMARY KEY (id);


--
-- Name: gebiet2gruppe geb2gruppe_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiet2gruppe
    ADD CONSTRAINT geb2gruppe_uni UNIQUE (gruppe, gebiet);


--
-- Name: gebietgruppen gebgruppe_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebietgruppen
    ADD CONSTRAINT gebgruppe_pkey PRIMARY KEY (id);


--
-- Name: gebiete_copy gebiete_copy_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiete_copy
    ADD CONSTRAINT gebiete_copy_pkey PRIMARY KEY (id);


--
-- Name: gebiete_copy gebiete_copy_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiete_copy
    ADD CONSTRAINT gebiete_copy_uni UNIQUE (gebiet, radius, kunde);


--
-- Name: gebiete gebiete_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiete
    ADD CONSTRAINT gebiete_pkey PRIMARY KEY (id);


--
-- Name: gebiete gebiete_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiete
    ADD CONSTRAINT gebiete_uni UNIQUE (gebiet, radius, kunde);


--
-- Name: kunden kunden_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY kunden
    ADD CONSTRAINT kunden_pkey PRIMARY KEY (id);


--
-- Name: kundengruppen kundengruppen_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY kundengruppen
    ADD CONSTRAINT kundengruppen_pkey PRIMARY KEY (masterkunde, slavekunde);


--
-- Name: meldungen meldungen_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY meldungen
    ADD CONSTRAINT meldungen_pkey PRIMARY KEY (id);


--
-- Name: gebietgruppen name_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebietgruppen
    ADD CONSTRAINT name_uni UNIQUE (gruppenname, kunde);


--
-- Name: teilnehmer teilnehmer_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY teilnehmer
    ADD CONSTRAINT teilnehmer_pkey PRIMARY KEY (id);


--
-- Name: teilnehmer teilnehmer_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY teilnehmer
    ADD CONSTRAINT teilnehmer_uni UNIQUE (teilnehmer, kunde);


SET search_path = benutzer, pg_catalog;

--
-- Name: adminlogging adminlogging_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY adminlogging
    ADD CONSTRAINT adminlogging_pkey PRIMARY KEY (id);


--
-- Name: benutzer benutzer_benutzername_key; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer
    ADD CONSTRAINT benutzer_benutzername_key UNIQUE (benutzername);


--
-- Name: benutzer benutzer_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer
    ADD CONSTRAINT benutzer_pkey PRIMARY KEY (id);


--
-- Name: berechtigungen berechtigungen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY berechtigungen
    ADD CONSTRAINT berechtigungen_pkey PRIMARY KEY (benutzer_id, gruppen_id);


--
-- Name: ebenen ebenen_geoname_key; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY ebenen
    ADD CONSTRAINT ebenen_geoname_key UNIQUE (geoserver_ebene);


--
-- Name: ebenen ebenen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY ebenen
    ADD CONSTRAINT ebenen_pkey PRIMARY KEY (id);


--
-- Name: gruppe2alarm gruppe2alarm_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY gruppe2alarm
    ADD CONSTRAINT gruppe2alarm_pkey PRIMARY KEY (mastergruppe, slavekunde);


--
-- Name: gruppen_ebenen gruppen_ebenen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY gruppen_ebenen
    ADD CONSTRAINT gruppen_ebenen_pkey PRIMARY KEY (id);


--
-- Name: gruppen gruppen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY gruppen
    ADD CONSTRAINT gruppen_pkey PRIMARY KEY (id);


--
-- Name: host_themes host_themes_host_key; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY host_themes
    ADD CONSTRAINT host_themes_host_key UNIQUE (host);


--
-- Name: logging logging_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (id);


--
-- Name: resetpw resetPW_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY resetpw
    ADD CONSTRAINT "resetPW_pkey" PRIMARY KEY (id);


--
-- Name: rollen rollen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY rollen
    ADD CONSTRAINT rollen_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (sessionid);


--
-- Name: themes themes_name_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY themes
    ADD CONSTRAINT themes_name_pkey UNIQUE (name);


--
-- Name: themes themes_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY themes
    ADD CONSTRAINT themes_pkey PRIMARY KEY (id);


SET search_path = blitz, pg_catalog;

--
-- Name: vergleich blitzdaten_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY vergleich
    ADD CONSTRAINT blitzdaten_pkey PRIMARY KEY (id);


--
-- Name: blitze1992 blitze1992_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1992
    ADD CONSTRAINT blitze1992_pkey PRIMARY KEY (id);


--
-- Name: blitze1992 blitze1992_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1992
    ADD CONSTRAINT blitze1992_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1993 blitze1993_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1993
    ADD CONSTRAINT blitze1993_pkey PRIMARY KEY (id);


--
-- Name: blitze1993 blitze1993_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1993
    ADD CONSTRAINT blitze1993_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1994 blitze1994_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1994
    ADD CONSTRAINT blitze1994_pkey PRIMARY KEY (id);


--
-- Name: blitze1994 blitze1994_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1994
    ADD CONSTRAINT blitze1994_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1995 blitze1995_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1995
    ADD CONSTRAINT blitze1995_pkey PRIMARY KEY (id);


--
-- Name: blitze1995 blitze1995_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1995
    ADD CONSTRAINT blitze1995_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1996 blitze1996_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1996
    ADD CONSTRAINT blitze1996_pkey PRIMARY KEY (id);


--
-- Name: blitze1996 blitze1996_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1996
    ADD CONSTRAINT blitze1996_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1997 blitze1997_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1997
    ADD CONSTRAINT blitze1997_pkey PRIMARY KEY (id);


--
-- Name: blitze1997 blitze1997_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1997
    ADD CONSTRAINT blitze1997_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1998 blitze1998_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1998
    ADD CONSTRAINT blitze1998_pkey PRIMARY KEY (id);


--
-- Name: blitze1998 blitze1998_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1998
    ADD CONSTRAINT blitze1998_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1999 blitze1999_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1999
    ADD CONSTRAINT blitze1999_pkey PRIMARY KEY (id);


--
-- Name: blitze1999 blitze1999_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze1999
    ADD CONSTRAINT blitze1999_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2000 blitze2000_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2000
    ADD CONSTRAINT blitze2000_pkey PRIMARY KEY (id);


--
-- Name: blitze2000 blitze2000_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2000
    ADD CONSTRAINT blitze2000_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2001 blitze2001_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2001
    ADD CONSTRAINT blitze2001_pkey PRIMARY KEY (id);


--
-- Name: blitze2001 blitze2001_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2001
    ADD CONSTRAINT blitze2001_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2002 blitze2002_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2002
    ADD CONSTRAINT blitze2002_pkey PRIMARY KEY (id);


--
-- Name: blitze2002 blitze2002_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2002
    ADD CONSTRAINT blitze2002_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2003 blitze2003_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2003
    ADD CONSTRAINT blitze2003_pkey PRIMARY KEY (id);


--
-- Name: blitze2003 blitze2003_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2003
    ADD CONSTRAINT blitze2003_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2004 blitze2004_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2004
    ADD CONSTRAINT blitze2004_pkey PRIMARY KEY (id);


--
-- Name: blitze2004 blitze2004_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2004
    ADD CONSTRAINT blitze2004_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2005 blitze2005_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2005
    ADD CONSTRAINT blitze2005_pkey PRIMARY KEY (id);


--
-- Name: blitze2005 blitze2005_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2005
    ADD CONSTRAINT blitze2005_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2006 blitze2006_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2006
    ADD CONSTRAINT blitze2006_pkey PRIMARY KEY (id);


--
-- Name: blitze2006 blitze2006_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2006
    ADD CONSTRAINT blitze2006_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2007 blitze2007_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2007
    ADD CONSTRAINT blitze2007_pkey PRIMARY KEY (id);


--
-- Name: blitze2007 blitze2007_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2007
    ADD CONSTRAINT blitze2007_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2008 blitze2008_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2008
    ADD CONSTRAINT blitze2008_pkey PRIMARY KEY (id);


--
-- Name: blitze2008 blitze2008_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2008
    ADD CONSTRAINT blitze2008_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2009 blitze2009_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2009
    ADD CONSTRAINT blitze2009_pkey PRIMARY KEY (id);


--
-- Name: blitze2009 blitze2009_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2009
    ADD CONSTRAINT blitze2009_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2010 blitze2010_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2010
    ADD CONSTRAINT blitze2010_pkey PRIMARY KEY (id);


--
-- Name: blitze2011 blitze2011_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2011
    ADD CONSTRAINT blitze2011_pkey PRIMARY KEY (id);


--
-- Name: blitze2012 blitze2012_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2012
    ADD CONSTRAINT blitze2012_pkey PRIMARY KEY (id);


--
-- Name: blitze2013 blitze2013_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2013
    ADD CONSTRAINT blitze2013_pkey PRIMARY KEY (id);


--
-- Name: blitze2014 blitze2014_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2014
    ADD CONSTRAINT blitze2014_pkey PRIMARY KEY (id);


--
-- Name: blitze2015 blitze2015_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2015
    ADD CONSTRAINT blitze2015_pkey PRIMARY KEY (id);


--
-- Name: blitze2016 blitze2016_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2016
    ADD CONSTRAINT blitze2016_pkey PRIMARY KEY (id);


--
-- Name: blitze2017 blitze2017_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2017
    ADD CONSTRAINT blitze2017_pkey PRIMARY KEY (id);


--
-- Name: blitze2018 blitze2018_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze2018
    ADD CONSTRAINT blitze2018_pkey PRIMARY KEY (id);


--
-- Name: blitze blitze_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze
    ADD CONSTRAINT blitze_pkey PRIMARY KEY (id);


--
-- Name: blitze blitze_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitze
    ADD CONSTRAINT blitze_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: verarbeitung verarbeitung_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY verarbeitung
    ADD CONSTRAINT verarbeitung_pkey PRIMARY KEY (id);


SET search_path = alarmserver, pg_catalog;

--
-- Name: alarmgebiete_idx; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX alarmgebiete_idx ON gebiete USING gist (gebietgeom);


--
-- Name: fki_gebiete_fkey; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX fki_gebiete_fkey ON gebiet2gruppe USING btree (gebiet);


--
-- Name: fki_gruppe_fkey; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX fki_gruppe_fkey ON alarme USING btree (gruppe);


--
-- Name: fki_gruppen_fkey; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX fki_gruppen_fkey ON gebiet2gruppe USING btree (gruppe);


--
-- Name: idx_alarm; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX idx_alarm ON meldungen USING btree (alarm);


--
-- Name: idx_eintrag; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX idx_eintrag ON meldungen USING btree (eintrag);


SET search_path = blitz, pg_catalog;

--
-- Name: b_koord_1992_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1992_idx ON blitze1992 USING gist (koordinate);


--
-- Name: b_koord_1993_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1993_idx ON blitze1993 USING gist (koordinate);


--
-- Name: b_koord_1994_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1994_idx ON blitze1994 USING gist (koordinate);


--
-- Name: b_koord_1995_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1995_idx ON blitze1995 USING gist (koordinate);


--
-- Name: b_koord_1996_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1996_idx ON blitze1996 USING gist (koordinate);


--
-- Name: b_koord_1997_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1997_idx ON blitze1997 USING gist (koordinate);


--
-- Name: b_koord_1998_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1998_idx ON blitze1998 USING gist (koordinate);


--
-- Name: b_koord_1999_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1999_idx ON blitze1999 USING gist (koordinate);


--
-- Name: b_koord_2000_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2000_idx ON blitze2000 USING gist (koordinate);


--
-- Name: b_koord_2001_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2001_idx ON blitze2001 USING gist (koordinate);


--
-- Name: b_koord_2002_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2002_idx ON blitze2002 USING gist (koordinate);


--
-- Name: b_koord_2003_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2003_idx ON blitze2003 USING gist (koordinate);


--
-- Name: b_koord_2004_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2004_idx ON blitze2004 USING gist (koordinate);


--
-- Name: b_koord_2005_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2005_idx ON blitze2005 USING gist (koordinate);


--
-- Name: b_koord_2006_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2006_idx ON blitze2006 USING gist (koordinate);


--
-- Name: b_koord_2007_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2007_idx ON blitze2007 USING gist (koordinate);


--
-- Name: b_koord_2008_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2008_idx ON blitze2008 USING gist (koordinate);


--
-- Name: b_koord_2009_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2009_idx ON blitze2009 USING gist (koordinate);


--
-- Name: b_koord_2010_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2010_idx ON blitze2010 USING gist (koordinate);


--
-- Name: b_koord_2011_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2011_idx ON blitze2011 USING gist (koordinate);


--
-- Name: b_koord_2012_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2012_idx ON blitze2012 USING gist (koordinate);


--
-- Name: b_koord_2013_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2013_idx ON blitze2013 USING gist (koordinate);


--
-- Name: b_koord_2014_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2014_idx ON blitze2014 USING gist (koordinate);


--
-- Name: b_koord_2015_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2015_idx ON blitze2015 USING gist (koordinate);


--
-- Name: b_koord_2016_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2016_idx ON blitze2016 USING gist (koordinate);


--
-- Name: b_koord_2017_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2017_idx ON blitze2017 USING gist (koordinate);


--
-- Name: b_koord_2018_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2018_idx ON blitze2018 USING gist (koordinate);


--
-- Name: b_zeit_1992_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1992_idx ON blitze1992 USING btree (zeit);


--
-- Name: b_zeit_1993_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1993_idx ON blitze1993 USING btree (zeit);


--
-- Name: b_zeit_1994_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1994_idx ON blitze1994 USING btree (zeit);


--
-- Name: b_zeit_1995_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1995_idx ON blitze1995 USING btree (zeit);


--
-- Name: b_zeit_1996_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1996_idx ON blitze1996 USING btree (zeit);


--
-- Name: b_zeit_1997_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1997_idx ON blitze1997 USING btree (zeit);


--
-- Name: b_zeit_1998_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1998_idx ON blitze1998 USING btree (zeit);


--
-- Name: b_zeit_1999_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1999_idx ON blitze1999 USING btree (zeit);


--
-- Name: b_zeit_2000_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2000_idx ON blitze2000 USING btree (zeit);


--
-- Name: b_zeit_2001_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2001_idx ON blitze2001 USING btree (zeit);


--
-- Name: b_zeit_2002_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2002_idx ON blitze2002 USING btree (zeit);


--
-- Name: b_zeit_2003_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2003_idx ON blitze2003 USING btree (zeit);


--
-- Name: b_zeit_2004_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2004_idx ON blitze2004 USING btree (zeit);


--
-- Name: b_zeit_2005_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2005_idx ON blitze2005 USING btree (zeit);


--
-- Name: b_zeit_2006_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2006_idx ON blitze2006 USING btree (zeit);


--
-- Name: b_zeit_2007_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2007_idx ON blitze2007 USING btree (zeit);


--
-- Name: b_zeit_2008_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2008_idx ON blitze2008 USING btree (zeit);


--
-- Name: b_zeit_2009_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2009_idx ON blitze2009 USING btree (zeit);


--
-- Name: b_zeit_2010_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2010_idx ON blitze2010 USING btree (zeit);


--
-- Name: b_zeit_2011_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2011_idx ON blitze2011 USING btree (zeit);


--
-- Name: b_zeit_2012_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2012_idx ON blitze2012 USING btree (zeit);


--
-- Name: b_zeit_2013_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2013_idx ON blitze2013 USING btree (zeit);


--
-- Name: b_zeit_2014_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2014_idx ON blitze2014 USING btree (zeit);


--
-- Name: b_zeit_2015_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2015_idx ON blitze2015 USING btree (zeit);


--
-- Name: b_zeit_2016_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2016_idx ON blitze2016 USING btree (zeit);


--
-- Name: b_zeit_2017_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2017_idx ON blitze2017 USING btree (zeit);


--
-- Name: b_zeit_2018_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2018_idx ON blitze2018 USING btree (zeit);


--
-- Name: blitze1992 blids1992_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1992_del_protect AS
    ON DELETE TO blitze1992 DO INSTEAD NOTHING;


--
-- Name: blitze1993 blids1993_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1993_del_protect AS
    ON DELETE TO blitze1993 DO INSTEAD NOTHING;


--
-- Name: blitze1994 blids1994_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1994_del_protect AS
    ON DELETE TO blitze1994 DO INSTEAD NOTHING;


--
-- Name: blitze1995 blids1995_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1995_del_protect AS
    ON DELETE TO blitze1995 DO INSTEAD NOTHING;


--
-- Name: blitze1996 blids1996_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1996_del_protect AS
    ON DELETE TO blitze1996 DO INSTEAD NOTHING;


--
-- Name: blitze1997 blids1997_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1997_del_protect AS
    ON DELETE TO blitze1997 DO INSTEAD NOTHING;


--
-- Name: blitze1998 blids1998_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1998_del_protect AS
    ON DELETE TO blitze1998 DO INSTEAD NOTHING;


--
-- Name: blitze1999 blids1999_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1999_del_protect AS
    ON DELETE TO blitze1999 DO INSTEAD NOTHING;


--
-- Name: blitze2000 blids2000_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2000_del_protect AS
    ON DELETE TO blitze2000 DO INSTEAD NOTHING;


--
-- Name: blitze2001 blids2001_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2001_del_protect AS
    ON DELETE TO blitze2001 DO INSTEAD NOTHING;


--
-- Name: blitze2002 blids2002_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2002_del_protect AS
    ON DELETE TO blitze2002 DO INSTEAD NOTHING;


--
-- Name: blitze2003 blids2003_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2003_del_protect AS
    ON DELETE TO blitze2003 DO INSTEAD NOTHING;


--
-- Name: blitze2004 blids2004_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2004_del_protect AS
    ON DELETE TO blitze2004 DO INSTEAD NOTHING;


--
-- Name: blitze2005 blids2005_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2005_del_protect AS
    ON DELETE TO blitze2005 DO INSTEAD NOTHING;


--
-- Name: blitze2006 blids2006_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2006_del_protect AS
    ON DELETE TO blitze2006 DO INSTEAD NOTHING;


--
-- Name: blitze2007 blids2007_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2007_del_protect AS
    ON DELETE TO blitze2007 DO INSTEAD NOTHING;


--
-- Name: blitze2008 blids2008_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2008_del_protect AS
    ON DELETE TO blitze2008 DO INSTEAD NOTHING;


--
-- Name: blitze2009 blids2009_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2009_del_protect AS
    ON DELETE TO blitze2009 DO INSTEAD NOTHING;


--
-- Name: blitze2010 blids2010_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2010_del_protect AS
    ON DELETE TO blitze2010 DO INSTEAD NOTHING;


--
-- Name: blitze2011 blids2011_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2011_del_protect AS
    ON DELETE TO blitze2011 DO INSTEAD NOTHING;


--
-- Name: blitze2012 blids2012_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2012_del_protect AS
    ON DELETE TO blitze2012 DO INSTEAD NOTHING;


--
-- Name: blitze2013 blids2013_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2013_del_protect AS
    ON DELETE TO blitze2013 DO INSTEAD NOTHING;


--
-- Name: blitze2014 blids2014_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2014_del_protect AS
    ON DELETE TO blitze2014 DO INSTEAD NOTHING;


--
-- Name: blitze2015 blids2015_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2015_del_protect AS
    ON DELETE TO blitze2015 DO INSTEAD NOTHING;


--
-- Name: blitze2016 blids2016_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2016_del_protect AS
    ON DELETE TO blitze2016 DO INSTEAD NOTHING;


--
-- Name: blitze2018 blids2018_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2018_del_protect AS
    ON DELETE TO blitze2018 DO INSTEAD NOTHING;


SET search_path = alarmserver, pg_catalog;

--
-- Name: gebiete gebietalarm; Type: TRIGGER; Schema: alarmserver; Owner: postgres
--

CREATE TRIGGER gebietalarm BEFORE UPDATE ON gebiete FOR EACH ROW EXECUTE PROCEDURE gebietalarm();


--
-- Name: gebietgruppen gruppealarm; Type: TRIGGER; Schema: alarmserver; Owner: postgres
--

CREATE TRIGGER gruppealarm BEFORE UPDATE ON gebietgruppen FOR EACH ROW EXECUTE PROCEDURE gruppealarm();


SET search_path = blitz, pg_catalog;

--
-- Name: blitze2018 doppelt; Type: TRIGGER; Schema: blitz; Owner: postgres
--

CREATE TRIGGER doppelt BEFORE INSERT ON blitze2018 FOR EACH ROW EXECUTE PROCEDURE eintragen();


SET search_path = alarmserver, pg_catalog;

--
-- Name: alarme alarme_gebiet_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarme
    ADD CONSTRAINT alarme_gebiet_fkey FOREIGN KEY (gebiet) REFERENCES gebiete(id);


--
-- Name: alarme alarme_kunde_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarme
    ADD CONSTRAINT alarme_kunde_fkey FOREIGN KEY (kunde) REFERENCES kunden(id);


--
-- Name: alarme alarme_teilnehmer_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarme
    ADD CONSTRAINT alarme_teilnehmer_fkey FOREIGN KEY (teilnehmer) REFERENCES teilnehmer(id);


--
-- Name: gebiet2gruppe gebiet2gruppe_gebiet_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiet2gruppe
    ADD CONSTRAINT gebiet2gruppe_gebiet_fkey FOREIGN KEY (gebiet) REFERENCES gebiete(id);


--
-- Name: gebiet2gruppe gruppen_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY gebiet2gruppe
    ADD CONSTRAINT gruppen_fkey FOREIGN KEY (gruppe) REFERENCES gebietgruppen(id) ON UPDATE CASCADE ON DELETE CASCADE;


SET search_path = benutzer, pg_catalog;

--
-- Name: host_themes host_themes_theme_id_fkey; Type: FK CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY host_themes
    ADD CONSTRAINT host_themes_theme_id_fkey FOREIGN KEY (theme_id) REFERENCES themes(id);


--
-- Name: SCHEMA alarmserver; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA alarmserver TO current;


--
-- Name: SCHEMA benutzer; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA benutzer TO geoserver;
GRANT USAGE ON SCHEMA benutzer TO aktuell;
GRANT USAGE ON SCHEMA benutzer TO blidspusher;
GRANT USAGE ON SCHEMA benutzer TO alarmadmin;
GRANT USAGE ON SCHEMA benutzer TO current;


--
-- Name: SCHEMA blitz; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA blitz TO blitze;
GRANT USAGE ON SCHEMA blitz TO geoserver;


--
-- Name: FUNCTION alarmgebietlive(session_key character varying); Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT ALL ON FUNCTION alarmgebietlive(session_key character varying) TO current;


SET search_path = alarmserver, pg_catalog;

--
-- Name: TABLE alarme; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE alarme TO geoserver;


--
-- Name: TABLE gebiete; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE gebiete TO current;
GRANT SELECT ON TABLE gebiete TO geoserver;


--
-- Name: TABLE gebiete_copy; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE gebiete_copy TO current;
GRANT SELECT ON TABLE gebiete_copy TO geoserver;


--
-- Name: TABLE kundengruppen; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE kundengruppen TO geoserver;


--
-- Name: TABLE teilnehmer_aktiv; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE teilnehmer_aktiv TO geoserver;


SET search_path = benutzer, pg_catalog;

--
-- Name: TABLE benutzer; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE benutzer TO aktuell;
GRANT SELECT ON TABLE benutzer TO blidspusher;
GRANT SELECT ON TABLE benutzer TO alarmadmin;
GRANT SELECT ON TABLE benutzer TO current;


--
-- Name: TABLE berechtigungen; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE berechtigungen TO alarmadmin;


--
-- Name: TABLE gruppe2alarm; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE gruppe2alarm TO geoserver;


--
-- Name: TABLE gruppen; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE gruppen TO aktuell;
GRANT SELECT ON TABLE gruppen TO blidspusher;
GRANT SELECT ON TABLE gruppen TO alarmadmin;


--
-- Name: TABLE rollen; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE rollen TO alarmadmin;


--
-- Name: TABLE sessions; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE sessions TO aktuell;
GRANT SELECT ON TABLE sessions TO geoserver;
GRANT SELECT,UPDATE ON TABLE sessions TO current;
GRANT SELECT ON TABLE sessions TO blidspusher;


SET search_path = blitz, pg_catalog;

--
-- Name: TABLE blitze; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze TO blitze;
GRANT SELECT ON TABLE blitze TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze TO blitzsammler;


--
-- Name: TABLE blitze1992; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze1992 TO blitze;
GRANT SELECT ON TABLE blitze1992 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze1992 TO blitzsammler;


--
-- Name: TABLE blitze1993; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze1993 TO blitze;
GRANT SELECT ON TABLE blitze1993 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze1993 TO blitzsammler;


--
-- Name: TABLE blitze1994; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze1994 TO blitze;
GRANT SELECT ON TABLE blitze1994 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze1994 TO blitzsammler;


--
-- Name: TABLE blitze1995; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze1995 TO blitze;
GRANT SELECT ON TABLE blitze1995 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze1995 TO blitzsammler;


--
-- Name: TABLE blitze1996; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze1996 TO blitze;
GRANT SELECT ON TABLE blitze1996 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze1996 TO blitzsammler;


--
-- Name: TABLE blitze1997; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze1997 TO blitze;
GRANT SELECT ON TABLE blitze1997 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze1997 TO blitzsammler;


--
-- Name: TABLE blitze1998; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze1998 TO blitze;
GRANT SELECT ON TABLE blitze1998 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze1998 TO blitzsammler;


--
-- Name: TABLE blitze1999; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze1999 TO blitze;
GRANT SELECT ON TABLE blitze1999 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze1999 TO blitzsammler;


--
-- Name: TABLE blitze2000; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2000 TO blitze;
GRANT SELECT ON TABLE blitze2000 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2000 TO blitzsammler;


--
-- Name: TABLE blitze2001; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2001 TO blitze;
GRANT SELECT ON TABLE blitze2001 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2001 TO blitzsammler;


--
-- Name: TABLE blitze2002; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2002 TO blitze;
GRANT SELECT ON TABLE blitze2002 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2002 TO blitzsammler;


--
-- Name: TABLE blitze2003; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2003 TO blitze;
GRANT SELECT ON TABLE blitze2003 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2003 TO blitzsammler;


--
-- Name: TABLE blitze2004; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2004 TO blitze;
GRANT SELECT ON TABLE blitze2004 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2004 TO blitzsammler;


--
-- Name: TABLE blitze2005; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2005 TO blitze;
GRANT SELECT ON TABLE blitze2005 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2005 TO blitzsammler;


--
-- Name: TABLE blitze2006; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2006 TO blitze;
GRANT SELECT ON TABLE blitze2006 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2006 TO blitzsammler;


--
-- Name: TABLE blitze2007; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2007 TO blitze;
GRANT SELECT ON TABLE blitze2007 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2007 TO blitzsammler;


--
-- Name: TABLE blitze2008; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2008 TO blitze;
GRANT SELECT ON TABLE blitze2008 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2008 TO blitzsammler;


--
-- Name: TABLE blitze2009; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2009 TO blitze;
GRANT SELECT ON TABLE blitze2009 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2009 TO blitzsammler;


--
-- Name: TABLE blitze2010; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2010 TO blitze;
GRANT SELECT ON TABLE blitze2010 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2010 TO blitzsammler;


--
-- Name: TABLE blitze2011; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2011 TO blitze;
GRANT SELECT ON TABLE blitze2011 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2011 TO blitzsammler;


--
-- Name: TABLE blitze2012; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2012 TO blitze;
GRANT SELECT ON TABLE blitze2012 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2012 TO blitzsammler;


--
-- Name: TABLE blitze2013; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2013 TO blitze;
GRANT SELECT ON TABLE blitze2013 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2013 TO blitzsammler;


--
-- Name: TABLE blitze2014; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2014 TO blitze;
GRANT SELECT ON TABLE blitze2014 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2014 TO blitzsammler;


--
-- Name: TABLE blitze2015; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2015 TO blitze;
GRANT SELECT ON TABLE blitze2015 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2015 TO blitzsammler;


--
-- Name: TABLE blitze2016; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2016 TO blitze;
GRANT SELECT ON TABLE blitze2016 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2016 TO blitzsammler;


--
-- Name: TABLE blitze2017; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2017 TO blitze;
GRANT SELECT ON TABLE blitze2017 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2017 TO blitzsammler;


--
-- Name: TABLE blitze2018; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitze2018 TO blitze;
GRANT SELECT ON TABLE blitze2018 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitze2018 TO blitzsammler;


--
-- Name: SEQUENCE blitze_id_seq; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT,UPDATE ON SEQUENCE blitze_id_seq TO blitzsammler;


--
-- Name: TABLE vergleich; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE vergleich TO blitze;


--
-- PostgreSQL database dump complete
--

