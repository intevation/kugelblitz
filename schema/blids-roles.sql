--
-- PostgreSQL database cluster dump
--

-- Copyright 2018 by Siemens AG. All rights reserved.
-- Engineering by Intevation GmbH.
-- Use of this source code is governed by the Apache 2.0 license
-- that can be found in the LICENSE file.

-- Started on 2018-03-26 10:24:42 UTC

-- Edited by Sascha Wilde <wilde@intevation.de>
-- Removed: * paswords
--          * system super user
--          * some customer specific accounts
--          * unused roles

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE adressen;
ALTER ROLE adressen WITH NOSUPERUSER NOINHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION NOBYPASSRLS;
CREATE ROLE aktuell;
ALTER ROLE aktuell WITH NOSUPERUSER NOINHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION NOBYPASSRLS;
CREATE ROLE alarmadmin;
ALTER ROLE alarmadmin WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;  -- Set PW for access
CREATE ROLE blidspusher;
ALTER ROLE blidspusher WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;
CREATE ROLE blitze;
ALTER ROLE blitze WITH NOSUPERUSER NOINHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION NOBYPASSRLS;
CREATE ROLE blitzsammler;
ALTER ROLE blitzsammler WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;  -- Set PW for access
CREATE ROLE current;
ALTER ROLE current WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;  -- Set PW for access 
CREATE ROLE geoserver;
ALTER ROLE geoserver WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;  -- Set PW for access 
CREATE ROLE reseller;
ALTER ROLE reseller WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;  -- Set PW for access 
CREATE ROLE schnell;
ALTER ROLE schnell WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;  -- Set PW for access 

--
-- Role memberships
--

GRANT adressen TO reseller GRANTED BY postgres;
GRANT blitze TO blitzsammler GRANTED BY postgres;
GRANT blitze TO current GRANTED BY postgres;
GRANT blitze TO reseller GRANTED BY postgres;
GRANT blitze TO schnell GRANTED BY postgres;
GRANT geoserver TO current GRANTED BY postgres;

-- Completed on 2018-03-26 10:24:42 UTC

--
-- PostgreSQL database cluster dump complete
--
