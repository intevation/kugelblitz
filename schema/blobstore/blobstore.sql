-- Copyright 2018 by Siemens AG. All rights reserved.
-- Engineering by Intevation GmbH.
-- Use of this source code is governed by the Apache 2.0 license
-- that can be found in the LICENSE file.

BEGIN;

CREATE TABLE blobstore (
    name     varchar PRIMARY KEY UNIQUE NOT NULL,
    mimetype varchar(80)                NOT NULL DEFAULT ('application/octet-stream'),
    data     bytea                      NOT NULL
);

GRANT SELECT,INSERT ON TABLE blobstore TO kugelblitz;

COMMIT;
