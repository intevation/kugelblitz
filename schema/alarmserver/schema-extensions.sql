-- Copyright 2018 by Siemens AG. All rights reserved.
-- Engineering by Intevation GmbH.
-- Use of this source code is governed by the Apache 2.0 license
-- that can be found in the LICENSE file.

--
-- Author: Sascha Wilde <wilde@intevation.de>

--
-- Add transfer status information to messages table:
--
CREATE TYPE alarmserver.send_state AS ENUM ('ignored',
                                            'success',
                                            'partial success',
                                            'error');

ALTER TABLE alarmserver.meldungen
  ADD IF NOT EXISTS status alarmserver.send_state,
  ADD IF NOT EXISTS status_info text;


--
-- Add id of area which triggered areagroup alarm to areagroup table:
--
ALTER TABLE alarmserver.gebietgruppen
  ADD IF NOT EXISTS area_triggering_alarm integer
    REFERENCES alarmserver.gebiete (id) DEFERRABLE INITIALLY DEFERRED;

--
-- Drop old functions
--
DROP FUNCTION public.process_strokes;


--
-- Add user for alarmserver application
--
CREATE ROLE alarmserver;
ALTER ROLE alarmserver WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS;  -- Set PW for access
\echo set password for new role alarmserver to use it with the alarmserver application.
GRANT USAGE ON SCHEMA alarmserver TO alarmserver;
GRANT SELECT,UPDATE ON TABLE alarmserver.meldungen TO alarmserver;

--
-- Config for alarmserver app
--
CREATE TYPE alarmserver.conf_option AS ENUM ('Debug',
                                             'SmtpHost',
                                             'SmtpPort',
                                             'SmtpHelo',
                                             'MailFrom',
                                             'EcallUser',
                                             'EcallPasswd',
                                             'EcallVoiceFrom',
                                             'EcallFaxFrom');

-- Update for smtp authentication:
ALTER TYPE alarmserver.conf_option ADD VALUE IF NOT EXISTS 'SmtpUser' AFTER 'SmtpHelo';
ALTER TYPE alarmserver.conf_option ADD VALUE IF NOT EXISTS 'SmtpPasswd' AFTER 'SmtpUser';

CREATE TABLE IF NOT EXISTS alarmserver.config (
  option alarmserver.conf_option PRIMARY KEY,
  string varchar,
  int    integer,
  bool   boolean,
  CHECK ((CASE WHEN string IS NULL THEN 0 ELSE 1 END
          + CASE WHEN int IS NULL THEN 0 ELSE 1 END
          + CASE WHEN bool IS NULL THEN 0 ELSE 1 END) = 1)
);

GRANT SELECT ON TABLE alarmserver.config TO alarmserver;


--
-- Add inner radius for annulus shaped areas
--
ALTER TABLE alarmserver.gebiete
  ADD IF NOT EXISTS innerradius numeric;


--
-- MQTT support
--
ALTER TABLE alarmserver.teilnehmer
  ADD IF NOT EXISTS mqtt character varying;
