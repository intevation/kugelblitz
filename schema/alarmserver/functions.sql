-- Copyright 2018 by Siemens AG. All rights reserved.
-- Engineering by Intevation GmbH.
-- Use of this source code is governed by the Apache 2.0 license
-- that can be found in the LICENSE file.

--
-- In parts based on SQL statments form java alarmserver implementation.
--
-- Author: Sascha Wilde <wilde@intevation.de>

--
-- Update stroked areas on new registered lightning strokes.
--
CREATE OR REPLACE FUNCTION alarmserver.insert_stroke()
 RETURNS trigger
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
  _area  record;
BEGIN
  FOR _area IN (
      SELECT gebiete.id, NEW.zeit
              FROM alarmserver.gebiete
              WHERE st_within(NEW.koordinate, gebiete.gebietgeom)
  )
  LOOP
    UPDATE alarmserver.gebiete
       SET letzterblitz = _area.zeit,
           blitzanzahl = blitzanzahl + 1
     WHERE id = _area.id;
  END LOOP;
  RETURN NEW;
END;
$function$;

CREATE TRIGGER update_areas_on_stroke
  AFTER INSERT ON blitz.blitze2018
  FOR EACH ROW EXECUTE PROCEDURE alarmserver.insert_stroke();


--
-- Rewrite of gebietalarm()
--
-- Extended functionallity: set area_triggering_alarm for group alarms
CREATE OR REPLACE FUNCTION alarmserver.gebietalarm() RETURNS trigger
  LANGUAGE plpgsql
  AS $$
DECLARE
  _alarmrec RECORD;
BEGIN
  -- Wenn Alarmierung erreicht
  IF ( NEW.blitzanzahl >= OLD.blitzezumalarm AND OLD.alarm IS false ) THEN
     NEW.alarm := true;
     -- Alle Gruppen, die das Gebiet enthalten auf Alarm setzen
     UPDATE alarmserver.gebietgruppen
       SET alarm = true, area_triggering_alarm = NEW.id
       WHERE alarm IS NOT TRUE
         AND id IN (SELECT DISTINCT gruppe
                      FROM alarmserver.gebiet2gruppe
                      WHERE gebiet = NEW.id);
     -- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
     FOR _alarmrec IN ( SELECT * FROM alarmserver.alarme
                          WHERE gebiet = NEW.id )
     LOOP
       INSERT INTO alarmserver.meldungen (id, alarm, typ)
         VALUES (DEFAULT, _alarmrec.id ,'Alarm');
     END LOOP;
  -- Wenn Entwarnung aus Programm kommt
  ELSEIF ( OLD.alarm IS true AND NEW.alarm IS false ) THEN
    -- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
    FOR _alarmrec IN ( SELECT * FROM alarmserver.alarme
                        WHERE gebiet = NEW.id )
    LOOP
      INSERT INTO alarmserver.meldungen (id, alarm, typ)
        VALUES (DEFAULT, _alarmrec.id,'Entwarnung');
    END LOOP;
  END IF;
  RETURN NEW;
END;
$$;


--
-- Send notification on new registered lightning strokes.
--
CREATE OR REPLACE FUNCTION alarmserver.notify_stroke()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
 _data jsonb;
BEGIN
  _data := row_to_json(NEW)::jsonb - 'koordinate'
           || json_build_object ('koordinate',
                                st_astext(NEW.koordinate))::jsonb;
  PERFORM pg_notify('blids',
                    json_build_object ('type', 'stroke',
                                       'data', _data)::text);
  RETURN NEW;
END;
$function$;

CREATE TRIGGER notify_on_stroke
  BEFORE INSERT ON blitz.blitze2018
  FOR EACH ROW EXECUTE PROCEDURE alarmserver.notify_stroke();

--
-- Send notification on new alarms:
--

-- Helper Function to fill out template texts for notification messages
--
CREATE OR REPLACE FUNCTION alarmserver.fill_template(
                             _templ     text,
                             _typ       alarmserver.alarmtyp,
                             _eintrag   timestamp with time zone,
                             _gebiet_id integer,
                             _gruppe_id integer,
                             utc       bool
                           ) RETURNS jsonb LANGUAGE plpgsql
AS $function$
DECLARE
 __Deci_Fmt CONSTANT text := '990.000';
 __Date_Fmt CONSTANT text := 'DD.MM.YYYY';
 __Time_Fmt CONSTANT text := 'HH24:MI:SS';
 _gebiet_r         record;
 _gebiet_gruppen_r record;
 _laenge   text;
 _breite   text;
 _gebiet   text;
 _gruppe   text;
 _radius   text;
 _schwelle text;
 _anzahl   text;
 _dichte   text;
 _minuten  text;
 _jdata    jsonb;
BEGIN
  IF utc THEN
    SET TIME ZONE 'UTC';
  ELSE
    SET TIME ZONE 'CET';
  END IF;

  IF _gebiet_id IS NOT NULL THEN
    SELECT gebiet, st_x(st_centroid(gebietgeom)) AS centerx,
           st_y(st_centroid(gebietgeom)) AS centery, radius,
           st_area(st_transform(gebietgeom,31467)) AS flaeche,
           blitzanzahl, blitzezumalarm, alarmdauer
      INTO _gebiet_r
      FROM alarmserver.gebiete WHERE id = _gebiet_id;
  ELSIF _gruppe_id IS NOT NULL THEN
    SELECT gebiet, st_x(st_centroid(gebietgeom)) AS centerx,
           st_y(st_centroid(gebietgeom)) AS centery, radius,
           st_area(st_transform(gebietgeom,31467)) AS flaeche,
           blitzanzahl, blitzezumalarm, alarmdauer
      INTO _gebiet_r
      FROM alarmserver.gebiete
      WHERE id = (SELECT area_triggering_alarm
                    FROM alarmserver.gebietgruppen
                    WHERE id = _gruppe_id);
    SELECT gruppenname INTO _gebiet_gruppen_r
      FROM alarmserver.gebietgruppen WHERE id = _gruppe_id;
    _gruppe := _gebiet_gruppen_r.gruppenname;
  END IF;

  _laenge := to_char(COALESCE(_gebiet_r.centerx, 0.0),
                     __Deci_Fmt);
  _breite := to_char(COALESCE(_gebiet_r.centery, 0.0),
                     __Deci_Fmt);
  _gebiet := _gebiet_r.gebiet;
  _radius := _gebiet_r.radius::text;
  _schwelle := _gebiet_r.blitzezumalarm::text;
  -- As the alarm is triggered _befor_ the latest stoke is inserted
  -- into "alarmserver.gebiete" we have to add one to "blitzanzahl".
  _anzahl := (_gebiet_r.blitzanzahl + 1)::text;
  _dichte := ((_gebiet_r.blitzanzahl + 1) * 10000 / _gebiet_r.flaeche)::text;
  _minuten := _gebiet_r.alarmdauer::text;


  _templ := replace(_templ, '%TYP', _typ::text);
  _templ := replace(_templ, '%DATUM', to_char(_eintrag, __Date_Fmt));
  _templ := replace(_templ, '%ZEIT', to_char(_eintrag, __Time_Fmt));
  _templ := replace(_templ, '%LÄNGE', COALESCE(_laenge, ''));
  _templ := replace(_templ, '%BREITE', COALESCE(_breite, ''));
  _templ := replace(_templ, '%GEBIET', COALESCE(_gebiet, ''));
  _templ := replace(_templ, '%GRUPPE', COALESCE(_gruppe, ''));
  _templ := replace(_templ, '%RADIUS', COALESCE(_radius, ''));
  _templ := replace(_templ, '%SCHWELLE', COALESCE(_schwelle, ''));
  _templ := replace(_templ, '%ANZAHL', COALESCE(_anzahl, ''));
  _templ := replace(_templ, '%DICHTE', COALESCE(_dichte, ''));
  _templ := replace(_templ, '%MINUTEN', COALESCE(_minuten, ''));

  _jdata := json_build_object ('type', _typ::text,
                               'time', _eintrag,
                               'lon', _laenge::float,
                               'lat', _breite::float,
                               'alarmarea', COALESCE(_gebiet, ''),
                               'group', COALESCE(_gruppe, ''),
                               'radius', _radius::float,
                               'threshold', _schwelle::int,
                               'count', _anzahl::int,
                               'density', _dichte::float,
                               'minutes', _minuten::int);

  RETURN json_build_object ('body', _templ,
                            'raw_data', _jdata);
END;
$function$;

-- Notification trigger function for alarm message
--
CREATE OR REPLACE FUNCTION alarmserver.notify_alarm()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
  _matching_dow bool;
  _msg_data     jsonb;
  _alarm_r      record;
  _recipient_r  record;
  _startdate    timestamp with time zone;
  _starttime    timestamp with time zone;
  _stopdate     timestamp with time zone;
  _stoptime     timestamp with time zone;
BEGIN
  SELECT * INTO _alarm_r FROM alarmserver.alarme
    WHERE id = NEW.alarm;
  SELECT * INTO _recipient_r FROM alarmserver.teilnehmer
    WHERE id = _alarm_r.teilnehmer;

  -- Reimplement the date time logic of the legacy system:
  -- Only supports UTC and CET via a flag at the subscription.
  -- FIXME: We should change the schema to use timestamp with
  --        tz everywhere and use it to safe the actually
  --        intended time zone.  That would give us Flexibility
  --        and correct handling of DST.
  IF _recipient_r.utc THEN
    _startdate := _recipient_r.startdatum::timestamp AT TIME ZONE 'UTC';
    _stopdate  := _recipient_r.stopdatum::timestamp AT TIME ZONE 'UTC';
    _starttime := (_recipient_r.startzeit
                  + NEW.eintrag::date)::timestamp AT TIME ZONE 'UTC';
    _stoptime  := (_recipient_r.stopzeit
                  + NEW.eintrag::date)::timestamp AT TIME ZONE 'UTC';
  ELSE
    _startdate := _recipient_r.startdatum::timestamp AT TIME ZONE 'CET';
    _stopdate  := _recipient_r.stopdatum::timestamp AT TIME ZONE 'CET';
    _starttime := (_recipient_r.startzeit
                  + NEW.eintrag::date)::timestamp AT TIME ZONE 'CET';
    _stoptime  := (_recipient_r.stopzeit
                  + NEW.eintrag::date)::timestamp AT TIME ZONE 'CET';
  END IF;

  -- check for matching week day:
  EXECUTE 'SELECT '
    || alarmserver.wochentag(to_char(now(),'YYYY-MM-DD'))
    || ' FROM alarmserver.teilnehmer' INTO _matching_dow;

  -- Check if alarm message is still open.
  -- This is so that other triggers could process messages before us.
  IF NEW.erledigt IS NULL THEN
    IF NEW.eintrag < _startdate     -- check subscription period
       OR NEW.eintrag > (_stopdate + interval '1' day)
       OR NEW.eintrag < _starttime     -- check time of day
       OR NEW.eintrag > _stoptime
       OR NOT _matching_dow            -- check day of week
       OR (NEW.typ IN ('Alarm', 'Testalarm')  -- check message text
           AND _recipient_r.alarmierung <> '' IS NOT TRUE)
       OR (NEW.typ IN ('Entwarnung', 'Testentwarnung')
           AND _recipient_r.entwarnung <> '' IS NOT TRUE)
    THEN
      NEW.erledigt := now();
      NEW.status := 'ignored';
      NEW.status_info := 'Notification not desired by recipient.';
    ELSE
      _msg_data := json_build_object ('id', NEW.id,
                                      'groupids',
                                      (SELECT array_to_json(array_agg(mastergruppe))
                                         FROM benutzer.gruppe2alarm
                                         WHERE slavekunde = _alarm_r.kunde));

      _msg_data := json_build_object ('type', NEW.typ,
                                      'data', _msg_data);
      PERFORM pg_notify('blids', _msg_data::text);
    END IF;
  END IF;
  RETURN NEW;
END;
$function$;

CREATE TRIGGER notify_on_alarm
  BEFORE INSERT ON alarmserver.meldungen
  FOR EACH ROW EXECUTE PROCEDURE alarmserver.notify_alarm();


-- Get alarm message data for an entry in `alarmserver.meldungen'
--
CREATE OR REPLACE FUNCTION alarmserver.get_alarm_msg(_id integer)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
  _msg_data     jsonb;
  _subject      text;
  _body_templ   text;
  _message_r    record;
  _recipient_r  record;
  _alarm_r      record;
  _area_wkt     text;
BEGIN

  SELECT * INTO _message_r FROM alarmserver.meldungen
    WHERE id = _id;
  SELECT * INTO _alarm_r FROM alarmserver.alarme
    WHERE id = _message_r.alarm;
  SELECT * INTO _recipient_r FROM alarmserver.teilnehmer
    WHERE id = _alarm_r.teilnehmer;

  _msg_data := json_build_object ('recipient', _recipient_r.teilnehmer,
                                  'logonly', _recipient_r.nurlog);

  -- the following code is ugly, hints on how to make it less redundant
  -- are highly appreciated... <wilde@intevation.de>
  IF _recipient_r.sms <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('sms', _recipient_r.sms)::jsonb;
  END IF;
  IF _recipient_r.email <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('email', _recipient_r.email)::jsonb;
  END IF;
  IF _recipient_r.tel <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('phone', _recipient_r.tel)::jsonb;
  END IF;
  IF _recipient_r.fax <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('fax', _recipient_r.fax)::jsonb;
  END IF;
  IF _recipient_r.mqtt <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('mqtt', _recipient_r.mqtt)::jsonb;
  END IF;

  IF _message_r.typ IN ('Alarm', 'Testalarm') THEN
    _subject := _recipient_r.betreffalarm;
    _body_templ := _recipient_r.alarmierung;
  ELSE
  -- FIXME: 'Vorwarnung' is not really handled.
  --        The legacy java code didn't handle it neither.
    _subject := _recipient_r.betreffentwarn;
    _body_templ := _recipient_r.entwarnung;
  END IF;
  _msg_data := _msg_data
    || json_build_object ('subject', _subject)::jsonb
    || alarmserver.fill_template(_body_templ,
                                 _message_r.typ,
                                 _message_r.eintrag,
                                 _alarm_r.gebiet,
                                 _alarm_r.gruppe,
                                 _recipient_r.utc);
  RETURN _msg_data::text;
END;
$function$;

-- Get alarm geometry for entry in alarmserver.meldungen
--
CREATE OR REPLACE FUNCTION alarmserver.get_alarm_geom(_id integer)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
  _alarm_r   record;
  _gebiet_id int;
  _gebiet_r  record;
BEGIN

  SELECT a.* INTO _alarm_r
    FROM alarmserver.meldungen m,
         alarmserver.alarme a
    WHERE m.id = _id AND a.id = m.alarm;

  IF _alarm_r.gebiet IS NOT NULL THEN
    _gebiet_id = _alarm_r.gebiet;
  ELSE
    SELECT area_triggering_alarm INTO _gebiet_id
      FROM alarmserver.gebietgruppen
      WHERE id = _alarm_r.gruppe;
  END IF;

  SELECT gebiet AS name,
         blitzanzahl AS count,
         st_astext(gebietgeom) AS geom
    INTO _gebiet_r
    FROM alarmserver.gebiete
    WHERE id = _gebiet_id;

  RETURN row_to_json(_gebiet_r);
END;
$function$;


--
-- House keeping
--

-- Keep area size up to date
--
CREATE OR REPLACE FUNCTION alarmserver.update_surface_area()
 RETURNS trigger
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
BEGIN
  NEW.flaeche = st_area(st_transform(NEW.gebietgeom, 31467));
  RETURN NEW;
END;
$function$;

CREATE TRIGGER update_surface_area
  BEFORE INSERT OR UPDATE OF gebietgeom
  ON alarmserver.gebiete
  FOR EACH ROW EXECUTE
  PROCEDURE alarmserver.update_surface_area();

-- Expire old alarms:
--
-- this function must run on a regular basis (every few minutes) and
-- should be started by an cron job.
CREATE OR REPLACE FUNCTION alarmserver.expire_alarms()
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$
BEGIN
  -- Reset stroke counter an alamr status for all areas where no new
  -- strokes were registered for the max. alert time (`alarmdauer'):
  UPDATE alarmserver.gebiete SET blitzanzahl = 0, alarm = FALSE
    WHERE EXTRACT(EPOCH FROM now() - letzterblitz)::int/60 > alarmdauer
          AND blitzanzahl > 0;
  -- Reset alarms in all goups with no alarms in there areas left:
  UPDATE alarmserver.gebietgruppen SET alarm = FALSE
    WHERE alarm AND id NOT IN ( SELECT DISTINCT g2g.gruppe
                                  FROM alarmserver.gebiet2gruppe g2g,
                                       alarmserver.gebiete g
                                  WHERE g2g.gebiet = g.id AND g.alarm );
END;
$function$;


-- re-trigger or cleanup messages, not send for some time:
--
-- this function must run on a regular basis but in sensible intervals
-- to give triggered messages time to get processed...  It should be
-- started by an cron job maybe every 3 Minutes.
CREATE OR REPLACE FUNCTION alarmserver.trigger_messages()
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$
DECLARE
  -- Maybe the thresholds should be configurable, but constants should
  -- be good enough for now:
  --
  -- Time after which a not processed message is triggered again
  -- (in minutes):
  __retrigger_threshold CONSTANT numeric := 5;
  -- Time after which a not processed message is unconditionally set
  -- to error (in minutes):
  __ditch_threshold CONSTANT numeric := 120;

  _msg      record;
  _msg_data jsonb;
BEGIN
  FOR _msg IN (
    SELECT id, eintrag, typ FROM alarmserver.meldungen
      WHERE erledigt IS NULL
        AND EXTRACT(EPOCH FROM now() - eintrag)::int/60
            > __retrigger_threshold
        )
  LOOP
     -- Set everything non processed message older than
     -- __ditch_thrshold to error.
     --
     -- There are numerouse improved heuristics possible, but they
     -- seem not worth the extra complexity, as this really shouldn't
     -- happen at all... -- sw
    IF EXTRACT(EPOCH FROM now() - _msg.eintrag)::int/60
       > __ditch_threshold
    THEN
      UPDATE alarmserver.meldungen
        SET erledigt = now(),
            status = 'error',
            status_info = 'Message wasn''t processed in time'
        WHERE id = _msg.id;
      ELSE
        -- Msg is still young enough, re-trigger it.
        --
        -- The groupid is intentionally left blank in the
        -- notification: The groupid is only for the realtime
        -- visualisation, to determine if it might be shown.  The
        -- re-triggered alarms are not supposed to be shown so we
        -- leave the groupid out.
        _msg_data := json_build_object ('id', _msg.id,
                                        'groupids', null);

        _msg_data := json_build_object ('type', _msg.typ,
                                        'data', _msg_data);
        PERFORM pg_notify('blids', _msg_data::text);
    END IF;
  END LOOP;
END;
$function$;
