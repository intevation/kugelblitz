-- Copyright 2018 by Siemens AG. All rights reserved.
-- Engineering by Intevation GmbH.
-- Use of this source code is governed by the Apache 2.0 license
-- that can be found in the LICENSE file.

-- Cleaned up functions for layer in geoserver.

CREATE OR REPLACE FUNCTION benutzer.alarmgebietlive(_userid integer)
 RETURNS SETOF livegebiet
 LANGUAGE plpgsql
 STABLE
AS $$
DECLARE
  _v_session  record;
BEGIN

  SELECT home_gruppen_id AS group
  FROM benutzer.benutzer b
  WHERE b.id = _userid
  INTO _v_session;

  IF _v_session IS NULL THEN
    RETURN;
  END IF;

  RETURN QUERY SELECT DISTINCT gebiete.id, gebiete.gebiet,
                               gebiete.gebietgeom,
                               gebiete.blitzanzahl, gebiete.alarm,
                               ( SELECT teilnehmer_aktiv.aktiv
                                   FROM alarmserver.teilnehmer_aktiv
                                   WHERE teilnehmer_aktiv.id
                                         = alarme.teilnehmer) AS aktiv
                 FROM alarmserver.alarme, alarmserver.gebiete
                 WHERE alarme.kunde IN ( SELECT gruppe2alarm.slavekunde
                                           FROM benutzer.gruppe2alarm
                                           WHERE gruppe2alarm.mastergruppe
                                                 = _v_session.group )
                       AND alarme.gebiet = gebiete.id;

  RETURN;
END;
$$;

CREATE OR REPLACE FUNCTION blitzlivenew(
  _userid integer,
  _zeit1 integer, _zeit2 integer,
  _co_left numeric, _co_right numeric,
  _co_top numeric, _co_bottom numeric)
 RETURNS SETOF liveblitz
 LANGUAGE plpgsql
 STABLE
AS $$
DECLARE
  _zeiten         record;
  _areaofinterest geometry;
BEGIN
  SELECT now() - make_interval(secs => _zeit1) AS startzeit,
         now() - make_interval(secs => _zeit2) AS stopzeit
    INTO _zeiten;

  SELECT st_intersection(
    st_transform(st_makeenvelope(_co_left, _co_top, _co_right, _co_bottom, 3857), 4326),
    g.blitzgebiet)
  FROM benutzer.benutzer b JOIN benutzer.gruppen g ON b.home_gruppen_id = g.id
  WHERE b.id = _userid
  INTO _areaofinterest;

  -- EXECUTE is used because of performance reasons.
  -- For the exact reasons see siemens - blids / issue65 for details.
  RETURN QUERY EXECUTE
    'SELECT id, zeit, typ::integer, strom, koordinate, tlp::integer,
           round(date_part(''EPOCH''::text, now() - zeit))::integer AS unixzeit
    FROM blitz.blitze
    WHERE zeit BETWEEN $1 AND $2
          AND koordinate && $3
    ORDER BY zeit;'
    USING _zeiten.startzeit, _zeiten.stopzeit, _areaofinterest;
  RETURN;
END;
$$;
