// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"bitbucket.org/intevation/kugelblitz/broadcast"
	"bitbucket.org/intevation/kugelblitz/database"
	"bitbucket.org/intevation/kugelblitz/session"
)

const (
	pointstreamHeartbeatType = "hb-light"
	alarmHeartbeatType       = "hb-alarm"
	pointstreamType          = "stroke"
	alarmType                = "Alarm"
	allClearType             = "Entwarnung"
	testAlarmType            = "Testalarm"
	testAllClearType         = "Testentwarnung"
)

// Controller encapsulates the state known to all endpoints.
type Controller struct {
	psb *broadcast.PointstreamBroker
	asb *broadcast.AlarmStreamBroker
}

// NewController creates a new controller.
func NewController() *Controller {
	return &Controller{
		psb: broadcast.NewPointStreamBroker(),
		asb: broadcast.NewAlarmStreamBroker(),
	}
}

// Start starts the logic of the controller running in background.
func (c *Controller) Start() error {
	dsn, err := database.DSN()
	if err != nil {
		return err
	}
	go c.psb.Run()
	go c.asb.Run()

	rcv := broadcast.NewReceiver(dsn)
	rcv.AddHandler(pointstreamType, c.psb.Handle)
	rcv.AddHandler(pointstreamHeartbeatType, c.psb.HandleHeartbeat)
	rcv.AddHandler(alarmType, c.asb.HandleAlarm)
	rcv.AddHandler(alarmHeartbeatType, c.asb.HandleHeartbeat)
	rcv.AddHandler(allClearType, c.asb.HandleAllClear)
	rcv.AddHandler(testAlarmType, c.asb.HandleTestAlarm)
	rcv.AddHandler(testAllClearType, c.asb.HandleTestAllClear)

	go rcv.Run()

	session.AddSessionDeathCallback(c.psb.SessionDied)
	session.AddSessionDeathCallback(c.asb.SessionDied)
	return nil
}
