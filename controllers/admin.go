// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/intevation/kugelblitz/model"
	"bitbucket.org/intevation/kugelblitz/utils"
)

// addalarm: POST
func (c *Controller) addalarm(rw http.ResponseWriter, req *http.Request) {

	// WIP -- sw
	log.Println("called admin/addalarm: POST")

	type input struct {
		GroupID int64 `json:"groupid"`
		Alarm   *struct {
			Area        *model.AlarmArea       `json:"gebiet"`
			Customer    *string                `json:"kunde"`
			Participant *model.ParticipantInfo `json:"teilnehmer"`
		} `json:"alarm"`
	}

	var data input

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Alarm == nil ||
		data.Alarm.Area == nil ||
		(data.Alarm.Area.AreaLat == nil && data.Alarm.Area.AreaLon == nil) ||
		data.Alarm.Area.AreaRadius == nil ||
		data.Alarm.Customer == nil ||
		data.Alarm.Participant == nil ||
		data.Alarm.Participant.Participant == "" ||
		data.Alarm.Participant.Start == "" ||
		data.Alarm.Participant.End == "" ||
		(data.Alarm.Participant.SMS == "" && data.Alarm.Participant.EMail == "") {
		BadRequest(rw, errors.New("Missing parameter"))
		return
	}

	t := model.FakeBool(true)
	data.Alarm.Participant.Default = &t
	data.Alarm.Participant.GroupID = data.GroupID

	userID := UserID(req)
	perm, err := model.GetUserPermission(userID, data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perm == nil ||
		!perm.AlarmAlarms.Any("A") ||
		!perm.UserAdmin.Any("A") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	customerList, err := model.GetAlarmCustomerList(data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	var customerID int64
	if customerList != nil && customerList[0].ID > 0 {
		customerID = customerList[0].ID
	} else {
		customerID, err = model.AddAlarmCustomer(data.GroupID, *data.Alarm.Customer)
		if err != nil {
			InternalServerError(rw, err)
			return
		}
	}

	data.Alarm.Participant.CustomerID = customerID

	participantID, err := model.UpdateAlarmParticipant(data.Alarm.Participant)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if participantID == nil {
		BadRequest(rw, errors.New("Missing parameter: Teilnehmer"))
		return
	}

	data.Alarm.Area.CustomerID = &customerID
	areaID, err := model.UpdateAlarmArea(data.Alarm.Area)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if areaID == nil {
		BadRequest(rw, errors.New("Missing parameter: Gebiet"))
		return
	}

	alarmID, err := model.UpdateAlarm(*areaID, *participantID, nil, customerID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	Logging(
		req, "SUCCESS: Kompakt User %s with group %s has been added with alarm.",
		data.Alarm.Participant.Participant,
		*data.Alarm.Customer)

	var result = struct {
		Result int64 `json:"result"`
	}{
		Result: alarmID,
	}

	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// addgroup: POST
func (c *Controller) addgroup(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/addgroup: POST")

	var data struct {
		Group *model.GroupParameters `json:"group"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Group == nil || data.Group.ParentID == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	userID := UserID(req)
	perm, err := model.GetUserPermission(userID, *data.Group.ParentID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perm == nil || !perm.GroupAdmin.Any("A", "C") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	// XXX: This all should be in a transaction.

	groupID, err := model.AddGroup(data.Group)
	if err != nil {
		Logging(
			req, "Fail: Group has not been added to parent %d", *data.Group.ParentID)
		InternalServerError(rw, err)
		return
	}

	// get ID of Blidslayer for Parent Group in Layer Table
	assigned, err := model.GetLayerList(*data.Group.ParentID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	var parentLayerID int64
	parentLayerID = 3 // ID 3 is the Blidslayer of Group Blids, used as fallback if parentgroup has no blidslayer
	for _, elem := range assigned {
		if *elem.LayerID == 187 {
			parentLayerID = elem.ID
		} // ID of Blidslayer of Parent Group to copy values from
	}

	// XXX: These constants dont belong here.
	const (
		blitzLayer = 187
	)

	if layerID, err := model.AddLayer(groupID, blitzLayer); err != nil {
		log.Printf("add layer error: %v\n", err)
	} else if err := model.CopyLayer(layerID, parentLayerID); err != nil {
		log.Printf("copy layer error: %v\n", err)
	}

	// create Alarmkunde for new group as requested in internal issue #16
	if _, err := model.AddAlarmCustomer(groupID, *data.Group.GroupName); err != nil {
		log.Printf("error adding alarm group: %v\n", err)
	}

	Logging(req, "SUCCESS: Group %d has been added", groupID)

	var result = struct {
		Result int64                  `json:"result"`
		Group  *model.GroupParameters `json:"gruppe"`
	}{
		Result: groupID,
		Group:  data.Group,
	}

	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// addgruppealarm: POST
func (c *Controller) addgruppealarm(rw http.ResponseWriter, req *http.Request) {
	var data struct {
		// The naming in the JSON object is misleading, the id
		// transmitted as "alarmid" is really the id of an alarm
		// customer!  -- sw
		CustomerID *int64 `json:"alarmid"`
		GroupID    *int64 `json:"groupid"`
	}

	log.Println("called admin/addgruppealarm: POST")
	defer req.Body.Close()

	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.CustomerID == nil || data.GroupID == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	// Check permissions:
	userID := UserID(req)
	usrperms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if usrperms == nil || !usrperms.AlarmAdmin.Any("A") {
		http.Error(
			rw, "insufficient admin rights",
			http.StatusUnauthorized)
		return
	}

	// perform update
	_, err = model.AddGroup2Alarm(*data.GroupID, *data.CustomerID)

	if err != nil {
		Logging(
			req, "FAIL: Alarm ID %d has not been added to group %d",
			data.CustomerID, data.GroupID)
		InternalServerError(rw, err)
		return
	}

	Logging(
		req, "SUCCESS: Alarm ID %d has been added to group %d",
		data.CustomerID, data.GroupID)

	// XXX: Reverse engeniering of the original php code suggests that the
	// controller returns juzt an empty array as result, so we emulate this
	// behaviour...  -- sw
	var result = struct {
		Result []int `json:"result"`
	}{[]int{}}

	SendJSON(rw, http.StatusOK, &result)
}

// addlayer: POST
func (c *Controller) addlayer(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/addlayer: POST")

	var data struct {
		GroupID *int64 `json:"groupid"`
		Layer   struct {
			ID      *int64 `json:"id"`
			LayerID *int64 `json:"ebenen_id"`
		} `json:"layer"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}
	if data.GroupID == nil {
		BadRequest(rw, errors.New("Missing groupid"))
		return
	}
	if data.Layer.ID == nil {
		BadRequest(rw, errors.New("Missing layer.id"))
		return
	}
	if data.Layer.LayerID == nil {
		BadRequest(rw, errors.New("Missing layer.ebenen_id"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A", "W") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	// TODO: Place this into a transaction.

	id, err := model.AddLayer(*data.GroupID, *data.Layer.LayerID)
	if err != nil {
		Logging(
			req, "FAIL: Layer %d added to group %d",
			*data.Layer.LayerID,
			*data.GroupID)
		InternalServerError(rw, err)
		return
	}
	if err = model.CopyLayer(id, *data.Layer.ID); err != nil {
		log.Printf("error: copy layer %d failed\n", *data.Layer.ID)
	}
	Logging(
		req, "SUCCESS: Ebene %d added to group %d as layer %d",
		*data.Layer.LayerID,
		*data.GroupID,
		id)

	var result = struct {
		ID  int64 `json:"result"`
		Mod bool  `json:"mod"`
	}{
		ID:  id,
		Mod: true,
	}
	SendJSON(rw, http.StatusOK, &result)
}

// addpermission: POST
func (c *Controller) addpermission(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/addpermission: POST")
	utils.DumpJSON(req.Body)

	var data struct {
		Perm *model.PermissionParameters `json:"permission"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	if data.Perm == nil || data.Perm.UserID == nil ||
		data.Perm.GroupID == nil || data.Perm.RoleID == nil {
		BadRequest(rw, errors.New("missing paramter"))
		return
	}

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || perms.GroupAdmin != "A" {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	err = model.AddPerm(*data.Perm.UserID, *data.Perm.GroupID, *data.Perm.RoleID)
	if err != nil {
		Logging(
			req, "FAIL: Failed to add Role %d for User %d in group %d",
			data.Perm.RoleID,
			data.Perm.UserID,
			data.Perm.RoleID)
		InternalServerError(rw, err)
		return
	}

	Logging(
		req, "SUCCESS: Role %d for User %d in group %d has been added",
		data.Perm.RoleID,
		data.Perm.UserID,
		data.Perm.RoleID)

	// XXX: Reverse engeniering of the original php code suggests that the
	// controller returns juzt an empty array as result, so we emulate this
	// behaviour...  -- sw
	var result = struct {
		Result []int `json:"result"`
	}{[]int{}}

	SendJSON(rw, http.StatusOK, &result)
}

// addrole: POST
func (c *Controller) addrole(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/addrole: POST")
	var data struct {
		Role *model.RoleDetailParameters `json:"role"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	if data.Role == nil {
		BadRequest(rw, errors.New("missing argument"))
		return
	}

	userID := UserID(req)

	groupID, err := model.GetHomeGroup(userID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if groupID == nil {
		http.NotFound(rw, req)
		return
	}

	perms, err := model.GetUserPermission(userID, *groupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	// Get roleID from new user.
	newID, err := model.AddRole(data.Role)
	if err != nil {
		Logging(
			req, "FAIL: Get no role ID for %s", *data.Role.RoleName)
		InternalServerError(rw, err)
		return
	}
	Logging(req, "SUCCESS: Role %d has been added", newID)

	var result = struct {
		Result int64                       `json:"result"`
		Role   *model.RoleDetailParameters `json:"role"`
	}{
		Result: newID,
		Role:   data.Role,
	}

	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// adduser: POST
func (c *Controller) adduser(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/adduser: POST")

	var data struct {
		User *model.UserDetails `json:"user"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	if data.User == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	userID := UserID(req)

	groupID, err := model.GetHomeGroup(userID)
	if err != nil {
		http.NotFound(rw, req)
		return
	}

	perms, err := model.GetUserPermission(userID, *groupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || !perms.UserAdmin.Any("A") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	// Get userID from new user.
	UserNewID, err := model.AddUser(data.User)
	if err != nil {
		Logging(
			req, "FAIL: Get no user ID for %s", data.User.UserName)
		InternalServerError(rw, err)
		return
	}

	err = model.AddPerm(UserNewID, data.User.HomeGroupID, 1)
	if err != nil {
		Logging(
			req, "FAIL: Add permission to user %d", UserNewID)
		InternalServerError(rw, err)
		return
	}

	Logging(
		req, "SUCCESS: User %s / ID: %d has been added to group %d",
		data.User.LastName, UserNewID, data.User.HomeGroupID)

	var result = struct {
		UserID int64 `json:"result"`
	}{
		UserID: UserNewID,
	}

	SendJSON(rw, http.StatusOK, &result)
}

// alarmgebietegrouplist: POST
func (c *Controller) alarmgebietegrouplist(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/alarmgebietegrouplist: POST")

	var data struct {
		GroupID *int64 `json:"groupid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing parameter (GroupID)"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || !perms.AlarmAdmin.Any("A", "W", "R") {
		http.Error(rw, "FAIL: Not allowed to get alarm area group list", http.StatusUnauthorized)
		return
	}

	alarmAreaGroupList, err := model.GetAlarmAreaGroupList(*data.GroupID)
	if err != nil {
		Logging(
			req, "FAIL: Get no alarm area group list from Group %d.", *data.GroupID)
		InternalServerError(rw, err)
		return
	}

	// The list could be empty which is valid in this case.
	if alarmAreaGroupList == nil {
		alarmAreaGroupList = []*model.AlarmAreaGroup{}
	}

	for _, alarmArea := range alarmAreaGroupList {

		alarmAreaGroup, err := model.GetAlarmAreaGroup(alarmArea.ID)
		if err != nil {
			Logging(
				req, "FAIL: Get no areas from AlarmAreaGroupList %d.", alarmArea.ID)
			InternalServerError(rw, err)
			return
		}
		if alarmAreaGroup == nil {
			http.NotFound(rw, req)
			return
		}
		alarmArea.Areas = alarmAreaGroup
	}
	var result = struct {
		AlarmAreaGroupList []*model.AlarmAreaGroup `json:"gebietegruppenlist"`
	}{
		AlarmAreaGroupList: alarmAreaGroupList,
	}

	SendJSON(rw, http.StatusOK, &result)
}

// alarmgebietelist: POST
func (c *Controller) alarmgebietelist(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/alarmgebietelist: POST")

	var data struct {
		GroupID *int64 `json:"groupid"`
		AreaID  *int64 `json:"gebietid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing parameter (GroupID)"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || !perms.AlarmAdmin.Any("A", "W", "R") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	areaList, err := model.GetAlarmGebietList(*data.GroupID, data.AreaID)
	if err != nil {
		Logging(
			req, "FAIL: Get no alarm area list from Group %d.", *data.GroupID)
		InternalServerError(rw, err)
		return
	}
	// An empty list is valid in this case.
	if areaList == nil {
		areaList = []*model.AlarmAreaList{}
	}

	var result = struct {
		AreaList []*model.AlarmAreaList `json:"gebietelist"`
	}{
		AreaList: areaList,
	}
	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// alarmkundelist: POST
func (c *Controller) alarmkundelist(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/alarmkundelist: POST")

	var data struct {
		GroupID *int64  `json:"groupid"`
		Show    *string `json:"show"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing GroupID"))
		return
	}

	userID := UserID(req)

	if *data.GroupID > 0 {
		perms, err := model.GetUserPermission(userID, *data.GroupID)
		if err != nil {
			InternalServerError(rw, err)
			return
		}
		if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || !perms.AlarmAdmin.Any("A", "W", "R") {
			http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
			return
		}
	} else {
		perms, err := model.GetUserPermission(userID, 1)
		if err != nil {
			InternalServerError(rw, err)
			return
		}
		if perms == nil || !perms.GroupAdmin.Any("A") {
			http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
			return
		}
	}

	alarmCustomerList, err := model.GetAlarmCustomerList(*data.GroupID)
	if err != nil {
		Logging(
			req, "FAIL: Get no Alarm Customer List from Group %d.", *data.GroupID)
		InternalServerError(rw, err)
		return
	}
	if alarmCustomerList == nil {
		alarmCustomerList = []*model.AlarmCustomer{}
	}

	var alarmCustomerListAll []*model.AlarmCustomer
	if data.Show != nil && *data.Show == "all" {
		alarmCustomerListAll, err = model.GetAlarmCustomerList(0)
		if err != nil {
			Logging(
				req, "FAIL: Get no Alarm Customer List.")
			InternalServerError(rw, err)
			return
		}
		if alarmCustomerListAll == nil {
			http.NotFound(rw, req)
			return
		}

	}

	var result = struct {
		AlarmCustomerList    []*model.AlarmCustomer `json:"alarmlist"`
		AlarmCustomerListAll []*model.AlarmCustomer `json:"completelist"`
	}{
		AlarmCustomerList:    alarmCustomerList,
		AlarmCustomerListAll: alarmCustomerListAll,
	}
	//utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)

}

// alarmlist: POST
func (c *Controller) alarmlist(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/alarmlist: POST")

	var data struct {
		GroupID *int64 `json:"groupid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	//utils.DumpJSON(data)

	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing GroupID"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.AlarmAdmin.Any("A", "W", "R") {
		log.Println("not enough rights")
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	alarmList, err := model.GetAlarmList(*data.GroupID)
	if err != nil {
		Logging(
			req, "FAIL: Get no AlarmList from Group %d.", *data.GroupID)
		InternalServerError(rw, err)
		return
	}
	if alarmList == nil {
		alarmList = []*model.AlarmDetail{}
	}

	currentGroup, err := model.GetGroupDetailSmall(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if currentGroup == nil {
		http.NotFound(rw, req)
		return
	}

	areaList, err := model.GetAlarmAreaListSimple(*data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if areaList == nil {
		areaList = []*model.AlarmAreaListSimple{}
	}

	participantsList, err := model.GetAlarmParticipantsListSimple(*data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if participantsList == nil {
		participantsList = []*model.AlarmParticipantsListSimple{}
	}

	var result = struct {
		AlarmList        []*model.AlarmDetail                 `json:"alarmlist"`
		GroupInfo        *model.GroupDetailSmall              `json:"group"`
		AreaList         []*model.AlarmAreaListSimple         `json:"gebietelist"`
		ParticipantsList []*model.AlarmParticipantsListSimple `json:"teilnehmerlist"`
	}{
		AlarmList:        alarmList,
		GroupInfo:        currentGroup,
		AreaList:         areaList,
		ParticipantsList: participantsList,
	}

	//utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// alarmteilnehmerlist: POST
func (c *Controller) alarmteilnehmerlist(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/alarmteilnehmerlist: POST")

	var data struct {
		GroupID       *int64 `json:"groupid"`
		ParticipantID *int64 `json:"teilnehmerid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}
	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing GroupID"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || !perms.AlarmAdmin.Any("A", "W", "R") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	alarmParticipants, err := model.GetAlarmParticipantsList(*data.GroupID, data.ParticipantID)
	if err != nil {
		Logging(
			req, "FAIL: Get no AlarmParticipantsList from group %d", *data.GroupID)
		InternalServerError(rw, err)
		return
	}
	if alarmParticipants == nil {
		alarmParticipants = []*model.AlarmParticipants{}
		return
	}

	var result = struct {
		AlarmParticipantsList []*model.AlarmParticipants `json:"teilnehmerlist"`
	}{
		AlarmParticipantsList: alarmParticipants,
	}
	//utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)

}

// checkemail: POST
func (c *Controller) checkemail(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/checkemail: POST")

	var data struct {
		Email *string `json:"email"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Email == nil {
		BadRequest(rw, errors.New("missing email"))
		return
	}

	userID, err := model.CheckUser(*data.Email)
	if err != nil {
		Logging(
			req, "FAIL: Email %s not checked.", *data.Email)
		InternalServerError(rw, err)
		return
	}

	var userValid int
	if userID != nil {
		userValid = -1
	} else {
		userValid = 1
	}

	var result = struct {
		UserValid int `json:"valid"`
	}{
		UserValid: userValid,
	}

	SendJSON(rw, http.StatusOK, &result)

	Logging(req, "SUCCESS: Email %s checked.", *data.Email)
}

// getAlarmMeldungen: POST
func (c *Controller) getAlarmMeldungen(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/getAlarmMeldungen: POST")

	type alarm struct {
		AlarmID    *int64  `json:"alarmid"`
		End        *string `json:"ende"`
		GroupID    *int64  `json:"groupid"`
		CustomerID *int64  `json:"kundeid"`
		Start      *string `json:"start"`
	}

	var data struct {
		Alarm *alarm `json:"alarm"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Alarm == nil {
		BadRequest(rw, errors.New("missing Alarm"))
		return
	}
	if data.Alarm.AlarmID == nil {
		BadRequest(rw, errors.New("missing Alarm.AlarmID"))
		return
	}
	if data.Alarm.GroupID == nil {
		BadRequest(rw, errors.New("missing Alarm.GroupID"))
		return
	}
	if data.Alarm.CustomerID == nil {
		BadRequest(rw, errors.New("missing Alarm.CustomerID"))
		return
	}
	if data.Alarm.Start == nil {
		BadRequest(rw, errors.New("missing Alarm.Start"))
		return
	}
	if data.Alarm.End == nil {
		BadRequest(rw, errors.New("missing Alarm.End"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.Alarm.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	permsUser, err := model.CheckGroupAlarmCustomer(*data.Alarm.GroupID, *data.Alarm.CustomerID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	permsCustomer, err := model.CheckCustomerAlarm(*data.Alarm.CustomerID, *data.Alarm.AlarmID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || !perms.AlarmAdmin.Any("A", "W", "R") || !permsUser || !permsCustomer {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	alarmMessagesList, err := model.GetAlarmMessages(*data.Alarm.AlarmID, *data.Alarm.Start, *data.Alarm.End)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	var result = struct {
		AlarmMessagesList []*model.AlarmMessages `json:"result"`
	}{
		AlarmMessagesList: alarmMessagesList,
	}

	SendJSON(rw, http.StatusOK, &result)

}

// groupdetail: POST
func (c *Controller) groupdetail(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/groupdetail: POST")
	var data struct {
		GroupID *int64 `json:"groupid"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}
	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing groupid"))
		return
	}
	gd, err := model.GetGroupDetail(*data.GroupID, UserID(req))
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	var result = struct {
		GroupDetail *model.GroupDetail `json:"groupdetail"`
	}{
		GroupDetail: gd,
	}

	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// grouplist: POST
func (c *Controller) grouplist(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/grouplist: POST")
	groups, err := model.GetGroupList()
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	var result = struct {
		GroupList []*model.Group `json:"grouplist"`
	}{
		GroupList: groups,
	}

	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// groupview: POST
func (c *Controller) groupview(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/groupview: POST")

	var data struct {
		GroupID *int64 `json:"groupid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	userID := UserID(req)

	if data.GroupID == nil {
		hg, err := model.GetHomeGroup(userID)
		if err != nil || hg == nil {
			http.NotFound(rw, req)
			return
		}
		data.GroupID = hg
	}

	// log.Printf("user id: %d %d\n", userID, *data.GroupID)

	currentGroup, err := model.GetGroupDetailSmall(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if currentGroup == nil {
		http.NotFound(rw, req)
		return
	}

	adminRights := []string{"R", "W", "A", "C"}

	var parent *model.GroupDetailSmall

	if currentGroup.FatherID != nil {
		parentGroup, err := model.GetGroupDetailSmall(
			userID, *currentGroup.FatherID)
		if err != nil {
			InternalServerError(rw, err)
			return
		}
		// Should not happen
		if parentGroup == nil {
			InternalServerError(rw, fmt.Errorf(
				"error expected father id %d", *currentGroup.FatherID))
			return
		}

		if r := parentGroup.Rights; r != nil && r.UserAdmin.Any(adminRights...) {
			parent = parentGroup
		}
	}

	children := make([]*model.GroupDetailSmall, 0, 4)

	// get ChildGroups if allowed to
	if r := currentGroup.Rights; r != nil && r.GroupAdmin.Any(adminRights...) {
		childIDs, err := model.GetChildren(*data.GroupID)
		if err != nil {
			InternalServerError(rw, err)
			return
		}

		for _, childID := range childIDs {
			child, err := model.GetGroupDetailSmall(userID, childID)
			if err != nil {
				InternalServerError(rw, err)
				return
			}
			children = append(children, child)
		}
	}

	// get User who have this Group as Homegroup, if allowed to
	var users []*model.UserShort
	if currentGroup.Rights.UserAdmin.Any(adminRights...) {
		if users, err = model.GetUsers(*data.GroupID); err != nil {
			InternalServerError(rw, err)
			return
		}
	}

	alarmPerm, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if alarmPerm != nil && alarmPerm.AlarmAdmin == "A" {
		currentGroup.Rights.AlarmSuperAdmin = "A"
	}

	var result = struct {
		Group    *model.GroupDetailSmall   `json:"group"`
		Parent   *model.GroupDetailSmall   `json:"parent"`
		Children []*model.GroupDetailSmall `json:"children,omitempty"`
		Users    []*model.UserShort        `json:"users,omitempty"`
	}{
		Group:    currentGroup,
		Parent:   parent,
		Children: children,
		Users:    users,
	}

	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// layerview: POST
func (c *Controller) layerview(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/layerview: POST")
	var data struct {
		GroupID  *int64 `json:"groupid"`
		ParentID *int64 `json:"parentid"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("R", "W", "A", "C") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	assigned, err := model.GetLayerList(*data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	var available []*model.LayerInfo
	if data.ParentID != nil {
		if !perms.GroupAdmin.Any("W", "A") {
			http.Error(
				rw, "insufficient admin rights", http.StatusUnauthorized)
			return
		}
		available, err = model.GetLayerList(*data.ParentID)
		if err != nil {
			InternalServerError(rw, err)
			return
		}
	}

	var result = struct {
		Assigned  []*model.LayerInfo `json:"assignedlayerlist"`
		Available []*model.LayerInfo `json:"availablelayerlist"`
	}{
		Assigned:  assigned,
		Available: available,
	}

	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}

// menu: POST
func (c *Controller) menu(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/menu: POST")

	userID := UserID(req)

	menu, err := model.GetMenu(userID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	result := struct {
		Menu []interface{} `json:"menu"`
	}{
		Menu: menu,
	}

	// utils.DumpJSON(&result)

	SendJSON(rw, http.StatusOK, &result)
}

// permissionlist: POST
func (c *Controller) permissionlist(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/permissionlist: POST")

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "W") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	PermissionList, err := model.GetPermissionList()
	if err != nil {
		Logging(
			req, "FAIL: ")
		InternalServerError(rw, err)
		return
	}

	if PermissionList == nil {
		http.NotFound(rw, req)
		return
	}

	result := struct {
		PermissionsDetail []*model.PermissionList `json:"permissionlist"`
	}{
		PermissionsDetail: PermissionList,
	}

	SendJSON(rw, http.StatusOK, &result)
}

// removealarm: POST
func (c *Controller) removealarm(rw http.ResponseWriter, req *http.Request) {
	var data struct {
		Alarm *model.AlarmIDs `json:"data"`
	}

	log.Println("called admin/removealarm: POST")
	defer req.Body.Close()

	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
	}

	if data.Alarm == nil ||
		data.Alarm.ID == nil ||
		data.Alarm.GroupID == nil ||
		data.Alarm.CustomerID == nil {

		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	// Check permissions:
	var perms utils.BoolErrList

	userID := UserID(req)
	usrperms, err := model.GetUserPermission(userID, *data.Alarm.GroupID)

	perms.Append(err == nil && usrperms != nil &&
		usrperms.AlarmAlarms.Any("A", "W"),
		err)

	perms.Append(model.CheckGroupAlarmCustomer(
		*data.Alarm.GroupID,
		*data.Alarm.CustomerID))

	perms.Append(model.CheckCustomerAlarm(
		*data.Alarm.CustomerID,
		*data.Alarm.ID,
	))
	perm, err := perms.Reduce()

	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if !perm {
		http.Error(
			rw, "insufficient admin rights",
			http.StatusUnauthorized)
		return
	}

	// perform delete
	affectedRows, err := model.RemoveAlarm(*data.Alarm)
	if err != nil {
		Logging(req, "FAIL: Alarm %d has not been removed",
			data.Alarm.ID)
		InternalServerError(rw, err)
		return
	}

	var result = struct {
		Result int64 `json:"result"`
	}{
		Result: affectedRows,
	}

	Logging(req, "SUCCESS: Alarm %d updated.", data.Alarm.ID)

	SendJSON(rw, http.StatusOK, &result)
}

// removealarmgebiet: POST
func (c *Controller) removealarmgebiet(rw http.ResponseWriter, req *http.Request) {
	var data struct {
		Area *model.AlarmAreaIDs `json:"gebiet"`
	}
	log.Println("called admin/removealarmgebiet: POST")

	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
	}

	if data.Area == nil ||
		data.Area.ID == nil ||
		data.Area.GroupID == nil ||
		data.Area.CustomerID == nil ||
		data.Area.Cascade == nil {

		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	// Check permissions:
	var perms utils.BoolErrList

	userID := UserID(req)
	usrperms, err := model.GetUserPermission(userID, *data.Area.GroupID)

	perms.Append(err == nil && usrperms != nil &&
		usrperms.AlarmAreas.Any("A", "W"),
		err)

	perms.Append(model.CheckGroupAlarmCustomer(
		*data.Area.GroupID,
		*data.Area.CustomerID))

	perms.Append(model.CheckCustomerArea(
		*data.Area.CustomerID,
		*data.Area.ID))

	perm, err := perms.Reduce()

	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if !perm {
		http.Error(
			rw, "insufficient admin rights",
			http.StatusUnauthorized)
		return
	}

	// perform delete
	_, err = model.RemoveAlarmArea(*data.Area)

	var result struct {
		Result *string `json:"result"`
		Used   *int64  `json:"Used"`
	}

	if err != nil {
		if e, ok := err.(*model.DependendObjectsError); ok {
			result.Used = &e.ObjCount
		} else {
			Logging(req, "FAIL: Alarm Area %d has not been removed",
				data.Area.ID)
			InternalServerError(rw, err)
			return
		}
	} else {
		Logging(req, "SUCCESS: Alarm Area %d has been removed",
			data.Area.ID)

		val := "Success"
		result.Result = &val
	}

	SendJSON(rw, http.StatusOK, &result)
}

// removealarmteilnehmer: POST
func (c *Controller) removealarmteilnehmer(rw http.ResponseWriter, req *http.Request) {
	var data struct {
		Participant *model.AlarmParticipantIDs `json:"teilnehmer"`
	}
	log.Println("called admin/removealarmteilnehmer: POST")

	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
	}

	if data.Participant == nil ||
		data.Participant.ID == nil ||
		data.Participant.GroupID == nil ||
		data.Participant.CustomerID == nil ||
		data.Participant.Cascade == nil {

		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	// Check permissions:
	var perms utils.BoolErrList

	userID := UserID(req)
	usrperms, err := model.GetUserPermission(userID, *data.Participant.GroupID)

	perms.Append(err == nil && usrperms != nil &&
		usrperms.AlarmParticipant.Any("A", "W"),
		err)

	perms.Append(model.CheckGroupAlarmCustomer(
		*data.Participant.GroupID,
		*data.Participant.CustomerID))

	perms.Append(model.CheckCustomerParticipant(
		*data.Participant.CustomerID,
		*data.Participant.ID))

	perm, err := perms.Reduce()

	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if !perm {
		http.Error(
			rw, "insufficient admin rights",
			http.StatusUnauthorized)
		return
	}

	// perform delete
	_, err = model.RemoveAlarmParticipant(data.Participant)

	var result struct {
		Result *string `json:"result"`
		Used   *int64  `json:"Used"`
	}

	if err != nil {
		if e, ok := err.(*model.DependendObjectsError); ok {
			result.Used = &e.ObjCount
		} else {
			Logging(req, "FAIL: Alarm Participant %d has not been removed",
				data.Participant.ID)
			InternalServerError(rw, err)
			return
		}
	} else {
		Logging(req, "SUCCESS: Alarm Participant %d has been removed",
			data.Participant.ID)

		val := "Success"
		result.Result = &val
	}

	SendJSON(rw, http.StatusOK, &result)
}

// removegroup: POST
func (c *Controller) removegroup(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/removegroup: POST")

	var data struct {
		GroupID *int64 `json:"groupid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	if *data.GroupID == 1 {
		http.Error(rw, "♫♬ Blids will Survive! ♫♬", http.StatusUnauthorized)
		return
	}

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || perms.GroupAdmin != "A" {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	// TODO: Place this into a transaction.

	if err := model.RemoveGroupLayers(*data.GroupID); err != nil {
		Logging(
			req, "FAIL: Layers of group %d have not been deleted", *data.GroupID)
		InternalServerError(rw, err)
		return
	}

	if err := model.RemoveGroup(*data.GroupID); err != nil {
		Logging(req, "FAIL: Group %d has not been deleted", *data.GroupID)
		InternalServerError(rw, err)
		return
	}

	Logging(req,
		"SUCCESS: Group %d and its layers have been deleted", *data.GroupID)

	SendSuccess(rw)
}

// removegruppealarm: POST
func (c *Controller) removegruppealarm(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/removegruppealarm: POST")

	var data struct {
		// The naming in the JSON object is misleading, the id
		// transmitted as "alarmid" is really the id of an alarm
		// customer!  -- sw
		CustomerID *int64 `json:"alarmid"`
		GroupID    *int64 `json:"groupid"`
	}

	log.Println("called admin/addgruppealarm: POST")
	defer req.Body.Close()

	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.CustomerID == nil || data.GroupID == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	// Check permissions:
	userID := UserID(req)
	usrperms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if usrperms == nil || !usrperms.AlarmAdmin.Any("A") {
		http.Error(
			rw, "insufficient admin rights",
			http.StatusUnauthorized)
		return
	}

	// perform delete
	_, err = model.RemoveGroup2Alarm(*data.GroupID, *data.CustomerID)

	if err != nil {
		Logging(
			req, "FAIL: Alarm ID %d has not been removed from group %d",
			data.CustomerID, data.GroupID)
		InternalServerError(rw, err)
		return
	}

	Logging(
		req, "SUCCESS: Alarm ID %d has been removed from group %d",
		data.CustomerID, data.GroupID)

	// XXX: Reverse engeniering of the original php code suggests that the
	// controller returns juzt an empty array as result, so we emulate this
	// behaviour...  -- sw
	var result = struct {
		Result []int `json:"result"`
	}{[]int{}}

	SendJSON(rw, http.StatusOK, &result)
}

// removelayer: POST
func (c *Controller) removelayer(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/removelayer: POST")
	var data struct {
		LayerID *int64 `json:"layerid"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}
	if data.LayerID == nil {
		BadRequest(rw, errors.New("Missing parameter"))
		return
	}
	userID := UserID(req)
	ok, err := model.RemoveLayer(*data.LayerID, userID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if !ok {
		Logging(req, "FAIL: Layer %d has not been removed", *data.LayerID)
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	Logging(req, "SUCCESS: Layer %d has been removed", *data.LayerID)

	SendJSON(rw, http.StatusOK, map[string]bool{
		"result": true,
	})
}

// removepermission: POST
func (c *Controller) removepermission(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/removepermission: POST")

	var data struct {
		Permission *model.PermissionParameters `json:"permission"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Permission == nil {
		BadRequest(rw, errors.New("Missing parameters"))
		return
	}

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || perms.GroupAdmin != "A" {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	if err := model.RemovePerm(*data.Permission.UserID); err != nil {
		Logging(
			req, "FAIL: User %d has not been deleted", data.Permission.UserID)
		InternalServerError(rw, err)
		return
	}

	Logging(
		req, "SUCCESS: Permission for user %d in group %d deleted.",
		data.Permission.UserID, data.Permission.GroupID)

	SendResult(rw,
		"Permission for user %d in group %d deleted.",
		data.Permission.UserID, data.Permission.GroupID)
}

// removerole: POST
func (c *Controller) removerole(rw http.ResponseWriter, req *http.Request) {

	log.Println("called admin/removerole: POST")

	var data struct {
		RoleID *int64 `json:"roleid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.RoleID == nil {
		BadRequest(rw, errors.New("Missing parameter: RoleID"))
		return
	}

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || perms.GroupAdmin != "A" {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	if err := model.RemoveRole(*data.RoleID); err != nil {
		Logging(req, "FAIL: RoleID %d has not been deleted", *data.RoleID)
		InternalServerError(rw, err)
		return
	}

	SendSuccess(rw)
}

// removeuser: POST
func (c *Controller) removeuser(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/removeuser: POST")

	var data struct {
		GroupID *int64 `json:"groupid"`
		UserID  *int64 `json:"userid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.GroupID == nil {
		BadRequest(rw, errors.New("Missing parameter: GroupID"))
		return
	}
	if data.UserID == nil {
		BadRequest(rw, errors.New("Missing parameter: UserID"))
		return
	}

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || perms.UserAdmin != "A" {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	if err := model.RemoveUser(*data.UserID); err != nil {
		Logging(req, "FAIL: User %d has not been deleted", *data.UserID)
		InternalServerError(rw, err)
		return
	}
	if err := model.RemovePerm(*data.UserID); err != nil {
		Logging(req, "FAIL: User %d has not been deleted", *data.UserID)
		InternalServerError(rw, err)
		return
	}

	// TODO: Implement more!
}

// roledetail: POST
func (c *Controller) roledetail(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/roledetail: POST")
	var data struct {
		RoleID *int64 `json:"roleid"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}
	if data.RoleID == nil {
		BadRequest(rw, errors.New("Missing roleid"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A", "W") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}
	detail, err := model.GetRoleDetail(*data.RoleID)
	if err != nil {
		Logging(req, "FAIL: %v", err)
		InternalServerError(rw, err)
		return
	}

	if detail == nil {
		http.NotFound(rw, req)
		return
	}

	var result = struct {
		RoleDetail *model.RoleDetail `json:"roledetail"`
	}{
		RoleDetail: detail,
	}

	SendJSON(rw, http.StatusOK, &result)
}

// rolelist: POST
func (c *Controller) rolelist(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/rolelist: POST")

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "W") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	rowsDetail, err := model.GetRoleList()
	if err != nil {
		Logging(
			req, "FAIL: ")
		InternalServerError(rw, err)
		return
	}

	if rowsDetail == nil {
		http.NotFound(rw, req)
		return
	}

	result := struct {
		RowsDetail []*model.RoleDetail `json:"rolelist"`
	}{
		RowsDetail: rowsDetail,
	}

	SendJSON(rw, http.StatusOK, &result)

}

// sendTestAlarm: POST
func (c *Controller) sendTestAlarm(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/sendTestAlarm: POST")

	var data struct {
		Alarm *model.TestAlarm
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
	}

	if data.Alarm == nil ||
		data.Alarm.ID == nil ||
		data.Alarm.GroupID == nil ||
		data.Alarm.CustomerID == nil ||
		data.Alarm.Typ == nil {

		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	// Check permissions:
	var perms utils.BoolErrList

	userID := UserID(req)
	usrperms, err := model.GetUserPermission(userID, *data.Alarm.GroupID)

	perms.Append(err == nil && usrperms != nil &&
		usrperms.AlarmAlarms.Any("A", "W"),
		err)

	perms.Append(model.CheckGroupAlarmCustomer(
		*data.Alarm.GroupID,
		*data.Alarm.CustomerID))

	perms.Append(model.CheckCustomerAlarm(
		*data.Alarm.CustomerID,
		*data.Alarm.ID,
	))
	perm, err := perms.Reduce()

	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if !perm {
		http.Error(
			rw, "insufficient admin rights",
			http.StatusUnauthorized)
		return
	}

	// create test alarm
	newID, err := model.SendTestAlarm(*data.Alarm)
	if err != nil {
		Logging(
			req, "FAIL: Testalarm %d has not been sent", data.Alarm.ID)
		InternalServerError(rw, err)
		return
	}
	Logging(req, "SUCCESS: Testalarm %d has been sent", data.Alarm.ID)

	var result = struct {
		Result int64 `json:"result"`
	}{
		Result: newID,
	}

	SendJSON(rw, http.StatusOK, &result)
}

// updatealarm: POST
func (c *Controller) updatealarm(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/updatealarm: POST")

	var data struct {
		AlarmID       *int64 `json:"alarmid"`
		AreaID        *int64 `json:"gebietid"`
		GroupID       *int64 `json:"groupid"`
		CustomerID    *int64 `json:"kundeid"`
		ParticipantID *int64 `json:"teilnehmerid"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	// utils.DumpJSON(&data)

	if data.AreaID == nil {
		BadRequest(rw, errors.New("Missing parameter gebietid"))
		return
	}
	if data.GroupID == nil {
		BadRequest(rw, errors.New("Missing parameter groupid"))
		return
	}
	if data.CustomerID == nil {
		BadRequest(rw, errors.New("Missing parameter kundeid"))
		return
	}
	if data.ParticipantID == nil {
		BadRequest(rw, errors.New("Missing parameter teilnehmerid"))
		return
	}

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	permsGroupAlarmCustomer, err := model.CheckGroupAlarmCustomer(*data.GroupID, *data.CustomerID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	permsCustomerAlarm := data.AlarmID == nil
	if !permsCustomerAlarm {
		var err error
		permsCustomerAlarm, err = model.CheckCustomerAlarm(*data.CustomerID, *data.AlarmID)
		if err != nil {
			InternalServerError(rw, err)
			return
		}
	}

	permsCustomerParticipant, err := model.CheckCustomerParticipant(*data.CustomerID, *data.ParticipantID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	permsCustomerArea, err := model.CheckCustomerArea(*data.CustomerID, *data.AreaID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil ||
		!perms.GroupAdmin.Any("A", "W", "R", "C") ||
		!perms.AlarmAdmin.Any("A", "W") ||
		!perms.AlarmAlarms.Any("A", "W") ||
		!permsGroupAlarmCustomer ||
		!permsCustomerAlarm ||
		!permsCustomerParticipant ||
		!permsCustomerArea {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	affectedRows, err := model.UpdateAlarm(
		*data.AreaID,
		*data.ParticipantID,
		data.AlarmID,
		*data.CustomerID)

	if err != nil {
		Logging(req, "FAIL: Alarm not updated %v.", err)
		SendFailure(rw, "FAIL: Alarm not updated %v.", err)
		return
	}

	switch {
	case data.AlarmID == nil && affectedRows == 0:
		Logging(req, "FAILED: Alarm insert.")
		SendFailure(rw, "FAILED: Alarm insert.")
		return
	case data.AlarmID == nil && affectedRows > 0:
		Logging(req, "SUCCESS: %d Alarms inserted.", affectedRows)
	default:
		Logging(req, "SUCCESS: %d Alarms updated.", affectedRows)
	}
	var result = struct {
		Result int64 `json:"result"`
	}{
		Result: affectedRows,
	}
	SendJSON(rw, http.StatusOK, &result)
}

// updatealarmgebiet: POST
func (c *Controller) updatealarmgebiet(rw http.ResponseWriter, req *http.Request) {
	var data struct {
		Area *model.AlarmArea `json:"gebiet"`
	}

	log.Println("called admin/updatealarmgebiet: POST")
	defer req.Body.Close()

	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Area == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	// Check permissions:
	var perms utils.BoolErrList

	userID := UserID(req)
	usrperms, err := model.GetUserPermission(userID, *data.Area.GroupID)

	perms.Append(err == nil && usrperms != nil &&
		usrperms.AlarmAreas.Any("A", "W"),
		err)

	perms.Append(model.CheckGroupAlarmCustomer(
		*data.Area.GroupID,
		*data.Area.CustomerID))

	if data.Area.ID != nil && *data.Area.ID > 0 {
		perms.Append(model.CheckCustomerArea(
			*data.Area.CustomerID,
			*data.Area.ID))
	}
	perm, err := perms.Reduce()

	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if !perm {
		http.Error(
			rw, "insufficient admin rights",
			http.StatusUnauthorized)
		return
	}

	// perform update
	id, err := model.UpdateAlarmArea(data.Area)
	if err != nil {
		Logging(req, "FAIL: Area not updated.")
		InternalServerError(rw, err)
		return
	}

	var result = struct {
		Result int64 `json:"result"`
	}{
		Result: *id,
	}
	SendJSON(rw, http.StatusOK, &result)
}

// updatealarmteilnehmer: POST
func (c *Controller) updatealarmteilnehmer(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/updatealarmteilnehmer: POST")

	var data struct {
		Participant *model.ParticipantInfo `json:"teilnehmer"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	// utils.DumpJSON(&data)

	if data.Participant == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, data.Participant.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	permsUser, err := model.CheckGroupAlarmCustomer(data.Participant.GroupID, data.Participant.CustomerID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || !perms.AlarmParticipant.Any("A", "W") || !permsUser {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	id, err := model.UpdateAlarmParticipant(data.Participant)
	if err != nil {
		Logging(
			req, "FAIL: Participant not updated.")
		InternalServerError(rw, err)
		return
	}

	var result = struct {
		UserID *int64 `json:"result"`
	}{
		UserID: id,
	}

	//utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)

}

// updategroup: POST
func (c *Controller) updategroup(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/updategroup: POST")

	var data struct {
		Group *model.GroupParameters `json:"group"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		InternalServerError(rw, err)
		return
	}

	if data.Group == nil || data.Group.ID == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}
	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.Group.ID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "W") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	res, err := model.UpdateGroup(data.Group)
	if err != nil {
		Logging(
			req, "FAIL: Group %d has not been updated", *data.Group.ID)
		InternalServerError(rw, err)
		return
	}
	Logging(
		req, "SUCCESS: Group %d has been updated", *data.Group.ID)

	var result = struct {
		Result *model.GroupUpdateResult `json:"result"`
	}{
		Result: res,
	}

	SendJSON(rw, http.StatusOK, &result)
}

// updatelayer: POST
func (c *Controller) updatelayer(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/updatelayer: POST")

	var data struct {
		GroupID *int64             `json:"groupid"`
		Layers  []*model.LayerInfo `json:"layers"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	// utils.DumpJSON(data)

	if data.GroupID == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	userID := UserID(req)

	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A", "W") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	// TODO: Place these updates into a transaction.

	for _, layer := range data.Layers {
		if err := model.UpdateLayer(layer, *data.GroupID); err != nil {
			Logging(
				req, "FAIL: Layers not updated for group %d", *data.GroupID)
			InternalServerError(rw, err)
			return
		}
	}

	Logging(req, "SUCCESS: Layers updated for group %d", *data.GroupID)

	SendResult(rw, "Layers updated for group %d", *data.GroupID)
}

// updatepermission: POST
func (c *Controller) updatepermission(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/updatepermission: POST")

	var data struct {
		Permission *model.PermissionParameters `json:"permission"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Permission == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	if err := model.UpdatePermission(data.Permission); err != nil {
		Logging(
			req, "FAIL: User %d not updated.", data.Permission.UserID)
		InternalServerError(rw, err)
		return
	}

	Logging(
		req, "SUCCESS: Permission for user %d in group %d has been updated to role %d.",
		data.Permission.UserID, data.Permission.GroupID, data.Permission.RoleID)

	SendResult(rw,
		"Permission for user %d in group %d updated.",
		data.Permission.UserID, data.Permission.GroupID)
}

// updaterole: POST
func (c *Controller) updaterole(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/updaterole: POST")

	var data struct {
		Role *model.RoleDetail `json:"role"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Role == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, 1)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	if err := model.UpdateRole(data.Role); err != nil {
		Logging(
			req, "FAIL: Role %d not updated.", data.Role.ID)
		InternalServerError(rw, err)
		return
	}

	Logging(req, "SUCCESS: Role %d updated.", data.Role.ID)

	SendResult(rw, "Role %d updated", data.Role.ID)
}

// updateuser: POST
func (c *Controller) updateuser(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/updateuser: POST")

	var data struct {
		User *model.UserDetails `json:"user"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.User == nil {
		BadRequest(rw, errors.New("missing parameter"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, data.User.HomeGroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if perms == nil || !perms.GroupAdmin.Any("A", "W", "R", "C") || !perms.UserAdmin.Any("A", "W") {
		http.Error(rw, "insufficient admin rights", http.StatusUnauthorized)
		return
	}

	if err := model.UpdateUser(data.User); err != nil {
		Logging(
			req, "FAIL: User %d not updated.", data.User.ID)
		InternalServerError(rw, err)
		return
	}

	Logging(req, "SUCCESS: User %d updated.", data.User.ID)

	SendResult(rw, "User %d updated", data.User.ID)
}

// userdetail: POST
func (c *Controller) userdetail(rw http.ResponseWriter, req *http.Request) {
	log.Println("called admin/userdetail: POST")
	var data struct {
		UserID *int64 `json:"userid"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}
	if data.UserID == nil {
		BadRequest(rw, errors.New("Missing userid"))
		return
	}

	ud, err := model.GetUserDetails(*data.UserID, UserID(req))
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if ud == nil {
		log.Printf("user %d not found\n", *data.UserID)
		http.NotFound(rw, req)
		return
	}
	var result = struct {
		UserDetail *model.UserDetails `json:"userdetail"`
	}{
		UserDetail: ud,
	}

	// utils.DumpJSON(&result)
	SendJSON(rw, http.StatusOK, &result)
}
