// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/kugelblitz/common"
	"bitbucket.org/intevation/kugelblitz/model"
)

func (c *Controller) version(rw http.ResponseWriter, req *http.Request) {
	log.Println("called utils/version")
	var result = struct {
		Version string `json:"version"`
	}{
		Version: common.Version,
	}
	SendJSON(rw, http.StatusOK, &result)
}

func (c *Controller) themeID(rw http.ResponseWriter, req *http.Request) {
	log.Println("called utils/themeid: POST")
	host := req.Host
	var themeID int64
	if host != "" {
		var err error
		if themeID, err = model.ThemeIDFromHost(host, 1); err != nil {
			InternalServerError(rw, err)
			return
		}
	}
	var result = struct {
		ThemeID int64 `json:"theme_id"`
	}{
		ThemeID: themeID,
	}
	SendJSON(rw, http.StatusOK, &result)
}

// theme: POST
func (c *Controller) theme(rw http.ResponseWriter, req *http.Request) {
	log.Println("called utils/theme: POST")
	var data struct {
		ID int64 `json:"id"`
	}
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	theme, err := model.LoadTheme(data.ID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if theme == nil {
		log.Printf("No theme found for %d\n", data.ID)
		http.NotFound(rw, req)
		return
	}

	var result = struct {
		Theme *model.Theme `json:"theme"`
	}{
		Theme: theme,
	}
	SendJSON(rw, http.StatusOK, &result)
}

// theme: POST
func (c *Controller) statistic(rw http.ResponseWriter, req *http.Request) {
	log.Println("called utils/statistic: POST")

	var data struct {
		Bound *model.Box `json:"bound"`
		Start string     `json:"start"`
		Stop  string     `json:"stop"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	start, err := strconv.ParseInt(data.Start, 10, 64)
	if err != nil {
		BadRequest(rw, err)
		return
	}
	stop, err := strconv.ParseInt(data.Stop, 10, 64)
	if err != nil {
		BadRequest(rw, err)
		return
	}

	// utils.DumpJSON(&data)

	userID := UserID(req)
	box, err := model.GetBox(userID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	var result struct {
		Statistic    []*model.StatisticsItem `json:"statistic"`
		HitStatistic []*model.StatisticsItem `json:"hitstatistic"`
		CountTotal   int64                   `json:"countTotal"`
		CountCloud   int64                   `json:"countCloud"`
		CountPos     int64                   `json:"countPos"`
		CountNeg     int64                   `json:"countNeg"`
	}

	for _, x := range []struct {
		stat   *[]*model.StatisticsItem
		filter model.StatisticsFilter
	}{
		{&result.Statistic, model.StatisticsWithoutFilter},
		{&result.HitStatistic, model.StatisticsType1Filter},
	} {
		if *x.stat, err = model.GetStatistics(
			start, stop, data.Bound, box, x.filter); err != nil {
			InternalServerError(rw, err)
		}
	}

	for _, x := range []struct {
		count  *int64
		filter model.CountFilter
	}{
		{&result.CountTotal, model.CountWithoutFilter},
		{&result.CountCloud, model.CountType4Filter},
		{&result.CountPos, model.CountType1PosFilter},
		{&result.CountNeg, model.CountType1NegFilter},
	} {
		if *x.count, err = model.GetCount(
			start, stop, data.Bound, box, x.filter); err != nil {
			InternalServerError(rw, err)
			return
		}
	}

	//utils.DumpJSON(&result)

	SendJSON(rw, http.StatusOK, &result)
}
