// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"log"
	"net/http"
	"net/http/httputil"

	"bitbucket.org/intevation/kugelblitz/model"
	"github.com/gorilla/mux"

	s "bitbucket.org/intevation/kugelblitz/session"
)

// permissions is a s.Checking checker for user rights.
func permissions(perms ...string) func(*s.Session) bool {
	return func(s *s.Session) bool {
		id, _ := s.GetUser()
		p, err := model.LoadAdminRights(id)
		if err != nil {
			log.Printf("error: %v\n", err)
			return false
		}
		return p.Any(perms...)
	}
}

// BindRoutes binds all the the endpoints to the controller.
func (c *Controller) BindRoutes(m *mux.Router) {

	loggedIn := s.Checking(s.LoggedIn, http.StatusUnauthorized)
	allowHGA := s.Checking(permissions("HG", "A"), http.StatusUnauthorized)
	allowA := s.Checking(permissions("A"), http.StatusUnauthorized)

	// Standard chain of having a session and being logged in.
	std := func(f http.HandlerFunc) http.Handler {
		return s.Chain(s.Sessioned, loggedIn, s.Term(f))
	}

	proxy := httputil.ReverseProxy{Director: c.wmsDirector}
	m.PathPrefix("/wms").Methods("GET").Handler(std(proxy.ServeHTTP))

	admin := m.PathPrefix("/admin").Subrouter()
	adminPOST := admin.Methods("POST").Subrouter()
	adminPOST.Handle("/addalarm", std(c.addalarm))
	adminPOST.Handle("/addgroup", std(c.addgroup))
	adminPOST.Handle("/addgruppealarm", std(c.addgruppealarm))
	adminPOST.Handle("/addlayer", std(c.addlayer))
	adminPOST.Handle("/addpermission", std(c.addpermission))
	adminPOST.Handle("/addrole", std(c.addrole))
	adminPOST.Handle("/adduser", std(c.adduser))
	adminPOST.Handle("/alarmgebietegrouplist", std(c.alarmgebietegrouplist))
	adminPOST.Handle("/alarmgebietelist", std(c.alarmgebietelist))
	adminPOST.Handle("/alarmkundelist", std(c.alarmkundelist))
	adminPOST.Handle("/alarmlist", std(c.alarmlist))
	adminPOST.Handle("/alarmteilnehmerlist", std(c.alarmteilnehmerlist))
	adminPOST.Handle("/checkemail", std(c.checkemail))
	adminPOST.Handle("/getAlarmMeldungen", std(c.getAlarmMeldungen))
	// The PHP original has no right restriction on this. Why?
	adminPOST.Handle("/groupdetail", std(c.groupdetail))
	adminPOST.Handle("/grouplist",
		s.Chain(s.Sessioned, loggedIn, allowA, s.Term(c.grouplist)))
	adminPOST.Handle("/groupview",
		s.Chain(s.Sessioned, loggedIn, allowHGA, s.Term(c.groupview)))
	adminPOST.Handle("/layerview", std(c.layerview))
	adminPOST.Handle("/menu", std(c.menu))
	adminPOST.Handle("/permissionlist", std(c.permissionlist))
	adminPOST.Handle("/removealarm", std(c.removealarm))
	adminPOST.Handle("/removealarmgebiet", std(c.removealarmgebiet))
	adminPOST.Handle("/removealarmteilnehmer", std(c.removealarmteilnehmer))
	adminPOST.Handle("/removegroup", std(c.removegroup))
	adminPOST.Handle("/removegruppealarm", std(c.removegruppealarm))
	adminPOST.Handle("/removelayer", std(c.removelayer))
	adminPOST.Handle("/removepermission", std(c.removepermission))
	adminPOST.Handle("/removerole", std(c.removerole))
	adminPOST.Handle("/removeuser", std(c.removeuser))
	adminPOST.Handle("/roledetail", std(c.roledetail))
	adminPOST.Handle("/rolelist", std(c.rolelist))
	adminPOST.Handle("/sendTestAlarm", std(c.sendTestAlarm))
	adminPOST.Handle("/updatealarm", std(c.updatealarm))
	adminPOST.Handle("/updatealarmgebiet", std(c.updatealarmgebiet))
	adminPOST.Handle("/updatealarmteilnehmer", std(c.updatealarmteilnehmer))
	adminPOST.Handle("/updategroup", std(c.updategroup))
	adminPOST.Handle("/updatelayer", std(c.updatelayer))
	adminPOST.Handle("/updatepermission", std(c.updatepermission))
	adminPOST.Handle("/updaterole", std(c.updaterole))
	adminPOST.Handle("/updateuser", std(c.updateuser))
	adminPOST.Handle("/userdetail", std(c.userdetail))

	auth := m.PathPrefix("/auth").Subrouter()
	authGET := auth.Methods("GET").Subrouter()
	authGET.Handle("/Logout", std(c.logout))
	authPOST := auth.Methods("POST").Subrouter()
	authPOST.Handle("/changePW", http.HandlerFunc(c.changePW))
	authPOST.Handle("/checkHash", http.HandlerFunc(c.checkHash))
	authPOST.Handle("/login", s.Chain(s.Sessioned, s.Term(c.login)))
	authPOST.Handle("/renewSession", std(c.renewSession))
	authPOST.Handle("/resetPW", http.HandlerFunc(c.resetPW))

	report := m.PathPrefix("/report").Subrouter()
	reportPOST := report.Methods("POST").Subrouter()
	reportPOST.Handle("/showlog", std(c.showlog))

	utils := m.PathPrefix("/utils").Subrouter()
	utilsPOST := utils.Methods("POST").Subrouter()
	utilsPOST.Handle("/statistic", std(c.statistic))
	utilsPOST.Handle("/theme", http.HandlerFunc(c.theme))
	utilsPOST.Handle("/themeid", http.HandlerFunc(c.themeID))

	m.Path("/event").Methods("GET").Handler(std(c.event))
	m.Path("/eventclose").Methods("GET").Handler(std(c.eventclose))

	m.Path("/alarmevent").Methods("GET").Handler(std(c.alarmEvent))
	m.Path("/alarmeventclose").Methods("GET").Handler(
		std(c.alarmEventClose))

	m.Path("/content/{name:.+}").Methods("GET").HandlerFunc(c.blob)

	m.HandleFunc("/version", c.version).Methods("GET", "POST")
}
