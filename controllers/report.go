// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"bitbucket.org/intevation/kugelblitz/model"
)

// showlog: POST
func (c *Controller) showlog(rw http.ResponseWriter, req *http.Request) {
	log.Println("called report/showlog: POST")

	var data model.ShowLogParameters
	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}
	if data.GroupID == nil {
		BadRequest(rw, errors.New("Missing parameter"))
		return
	}

	userID := UserID(req)
	perms, err := model.GetUserPermission(userID, *data.GroupID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if perms == nil || !perms.GroupAdmin.Any("A", "C", "W") {
		http.Error(rw, "insufficient admin rights", http.StatusOK)
		return
	}

	logs, err := model.ShowLog(&data)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	var result = struct {
		Answer []*model.Log `json:"answer"`
		Count  int          `json:"count"`
	}{
		Answer: logs,
		Count:  model.CountLoginSuccess(logs),
	}

	SendJSON(rw, http.StatusOK, &result)
}
