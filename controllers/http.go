// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"

	"bitbucket.org/intevation/kugelblitz/model"
	"bitbucket.org/intevation/kugelblitz/session"
	"bitbucket.org/intevation/kugelblitz/utils"
)

// SendEmptyJSON sends an empty JSON document to the client.
func SendEmptyJSON(rw http.ResponseWriter, code int) {
	rw.Header().Add("Content-Type", "application/json")
	rw.Header().Set("X-Content-Type-Options", "nosniff")
	rw.WriteHeader(code)
	fmt.Fprintln(rw, "{}")
}

// SendJSON sends a JSON representation of the given data to the client.
func SendJSON(rw http.ResponseWriter, code int, data interface{}) {
	rw.Header().Add("Content-Type", "application/json")
	rw.WriteHeader(code)
	if err := json.NewEncoder(rw).Encode(data); err != nil {
		log.Printf("error: %v\n", err)
	}
}

var successMsg = struct {
	Status string `json:"status"`
}{
	Status: "success",
}

// SendSuccess sends a common success message to the client.
func SendSuccess(rw http.ResponseWriter) {
	SendJSON(rw, http.StatusOK, &successMsg)
}

// SendResult sends result message to the client.
func SendResult(rw http.ResponseWriter, format string, args ...interface{}) {
	var result = struct {
		Result string `json:"result"`
	}{
		Result: fmt.Sprintf(format, args...),
	}
	SendJSON(rw, http.StatusOK, &result)
}

// SendFailure sends a failure message to the client.
func SendFailure(rw http.ResponseWriter, format string, args ...interface{}) {
	var result = struct {
		Status  string `json:"status"`
		Message string `json:"message"`
	}{
		Status:  "failure",
		Message: fmt.Sprintf(format, args...),
	}
	SendJSON(rw, http.StatusOK, &result)
}

// InternalServerError sends an internal server error to the client.
func InternalServerError(rw http.ResponseWriter, err error) {
	msg := fmt.Sprintf("error: %s %v", utils.Trace(1), err)
	log.Println(msg)
	http.Error(rw, msg, http.StatusInternalServerError)
}

// BadRequest sends a bad request message to the client.
func BadRequest(rw http.ResponseWriter, err error) {
	msg := fmt.Sprintf("error: %s %v", utils.Trace(1), err)
	log.Println(msg)
	http.Error(rw, msg, http.StatusBadRequest)
}

// Session extracts a session from a given request.
func Session(req *http.Request) *session.Session {
	s, _ := session.FromContext(req.Context())
	return s
}

// UserID extracts the ID of the logged in user from the
// given request and its associated session.
func UserID(req *http.Request) int64 {
	userID, _ := Session(req).GetUser()
	return userID
}

// IPFromRequest extracts the IP from the given request.
func IPFromRequest(req *http.Request) net.IP {

	addr := realIP(req)
	if addr == "" {
		addr = req.RemoteAddr
	}
	idx := strings.LastIndexByte(addr, ':')
	if idx > 0 {
		addr = addr[:idx]
	}
	addr = strings.Trim(addr, "[]")
	ip := net.ParseIP(addr)
	if ip == nil {
		return nil
	}

	return ip
}

// See https://github.com/zenazn/goji/blob/master/web/middleware/realip.go
var (
	xForwardedFor = http.CanonicalHeaderKey("X-Forwarded-For")
	xRealIP       = http.CanonicalHeaderKey("X-Real-IP")
)

func realIP(r *http.Request) string {
	var ip string

	if xff := r.Header.Get(xForwardedFor); xff != "" {
		i := strings.Index(xff, ", ")
		if i == -1 {
			i = len(xff)
		}
		ip = xff[:i]
	} else if xrip := r.Header.Get(xRealIP); xrip != "" {
		ip = xrip
	}

	return ip
}

// Logging writes a message to the logging table in the database.
func Logging(req *http.Request, format string, args ...interface{}) {
	ip := IPFromRequest(req)
	s, ok := session.FromContext(req.Context())
	if ok {
		userID, ok := s.GetUser()
		if ok {
			if err := model.UserLogging(
				userID,
				fmt.Sprintf(format, args...),
				ip,
				s.Key()); err != nil {
				log.Printf("logging error (%s): %v\n", utils.Trace(1), err)
			}
			return
		}
	}
	if err := model.UserLoggingNoSession(
		fmt.Sprintf(format, args...),
		ip); err != nil {
		log.Printf("logging error (%s): %v\n", utils.Trace(1), err)
	}
}
