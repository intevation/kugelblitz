// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/intevation/kugelblitz/broadcast"
	"bitbucket.org/intevation/kugelblitz/model"
)

// event: GET
func (c *Controller) event(rw http.ResponseWriter, req *http.Request) {
	log.Println("called pointstream/event: GET")

	s := Session(req)
	userID, _ := s.GetUser()

	geom, err := model.GetLiveBlitzgebiet(userID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if geom == nil {
		http.NotFound(rw, req)
		return
	}

	flusher, closeNotifier, ok := setupStreaming(rw)
	if !ok {
		return
	}

	queue := broadcast.NewQueue()

	c.psb.RegisterClient(s.Key(), geom, queue)

	snd := sender{
		eventType:     "lightning",
		queue:         queue,
		flusher:       flusher,
		closeNotifier: closeNotifier,
		unregister:    func() { c.psb.UnregisterClient(s.Key(), queue) },
	}
	snd.sendTo(rw)
}

// eventclose: GET
func (c *Controller) eventclose(rw http.ResponseWriter, req *http.Request) {
	log.Println("called pointstream/eventclose: GET")
	// XXX: Is this too extreme?
	c.psb.SessionDied(Session(req).Key())
	SendEmptyJSON(rw, http.StatusOK)
}

func (c *Controller) alarmEvent(rw http.ResponseWriter, req *http.Request) {
	log.Println("called alarmstream/alarmEvent: GET")

	s := Session(req)
	userID, _ := s.GetUser()
	groupID, err := model.GetHomeGroup(userID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	if groupID == nil {
		InternalServerError(rw, fmt.Errorf("User %d has no home group", userID))
		return
	}

	// TODO: Check if user is allowed to get alarm streams.

	flusher, closeNotifier, ok := setupStreaming(rw)
	if !ok {
		return
	}

	queue := broadcast.NewQueue()

	c.asb.RegisterClient(s.Key(), *groupID, queue)

	snd := sender{
		eventType:     "alarm",
		queue:         queue,
		flusher:       flusher,
		closeNotifier: closeNotifier,
		unregister:    func() { c.asb.UnregisterClient(s.Key(), queue) },
	}
	snd.sendTo(rw)
}

func (c *Controller) alarmEventClose(rw http.ResponseWriter, req *http.Request) {
	log.Println("called alarmstream/alarmEventClose: GET")
	// XXX: Is this too extreme?
	c.asb.SessionDied(Session(req).Key())
	SendEmptyJSON(rw, http.StatusOK)
}

func setupStreaming(rw http.ResponseWriter) (http.Flusher, http.CloseNotifier, bool) {
	// Make sure that the writer supports flushing.
	flusher, ok := rw.(http.Flusher)
	if !ok {
		http.Error(rw, "Streaming unsupported!", http.StatusInternalServerError)
		return nil, nil, false
	}

	closeNotifier, ok := rw.(http.CloseNotifier)
	if !ok {
		http.Error(rw, "CloseNotifier unsupported!", http.StatusInternalServerError)
		return nil, nil, false
	}

	rw.Header().Set("Content-Type", "text/event-stream")
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Connection", "keep-alive")
	rw.Header().Set("Access-Control-Allow-Origin", "*")

	return flusher, closeNotifier, true
}

type sender struct {
	eventType     string
	queue         *broadcast.Queue
	flusher       http.Flusher
	closeNotifier http.CloseNotifier
	unregister    func()
}

var sseSuffix = []byte("\n\n")

func (s *sender) sendTo(rw http.ResponseWriter) {
	type msg struct {
		msg []byte
		ok  bool
	}

	notify := s.closeNotifier.CloseNotify()
	msgs := make(chan msg)

	go func() {
		for {
			var m msg
			m.msg, m.ok = s.queue.Remove()
			msgs <- m
			if !m.ok {
				break
			}
		}
		close(msgs)
	}()

	prefix := []byte("event: " + s.eventType + "\ndata: ")

	write := func(msg []byte) (err error) {
		if _, err = rw.Write(prefix); err != nil {
			return
		}
		if _, err = rw.Write(msg); err != nil {
			return
		}
		_, err = rw.Write(sseSuffix)
		return
	}

	var dying bool
	for {
		select {
		case m := <-msgs:
			if !m.ok {
				return
			}

			if !dying {
				if write(m.msg) != nil {
					dying = true
					s.unregister()
				} else {
					s.flusher.Flush()
				}
			}

		case <-notify:
			dying = true
			s.unregister()
		}
	}
}
