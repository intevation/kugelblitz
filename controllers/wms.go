// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"

	"bitbucket.org/intevation/kugelblitz/utils"
)

const defaultRemote = "http://127.0.0.1:8080/geoserver/"

var (
	wmsMu  sync.Mutex
	wmsURL int

	wmsRemoteURLs = func() []*url.URL {
		servers := utils.Env("GEOSERVER", defaultRemote)
		var urls []*url.URL
		for _, s := range strings.Split(servers, ";") {
			u, err := url.Parse(s)
			if err != nil {
				panic(fmt.Errorf("Invalid GEOSERVER URL %s: %v", s, err))
			}
			urls = append(urls, u)
		}
		return urls
	}()
)

func nexWMSRemoteURL() *url.URL {
	switch len(wmsRemoteURLs) {
	case 0:
		return nil
	case 1:
		return wmsRemoteURLs[0]
	}

	wmsMu.Lock()
	defer wmsMu.Unlock()

	url := wmsRemoteURLs[wmsURL]
	wmsURL = (wmsURL + 1) % len(wmsRemoteURLs)
	return url
}

// parseQuery is a modified version of the internal query
// parser of the url.parseQuery of the standard library.
func parseQuery(
	m url.Values,
	query string,
	keySep, valueSep string,
	unescape func(string) (string, error)) error {

	for query != "" {
		key := query
		if i := strings.Index(key, keySep); i >= 0 {
			key, query = key[:i], key[i+1:]

		} else {
			query = ""
		}
		if key == "" {
			continue
		}
		value := ""
		if i := strings.Index(key, valueSep); i >= 0 {
			key, value = key[:i], key[i+1:]
		}
		key, err := unescape(key)
		if err != nil {
			return err
		}
		value, err = unescape(value)
		if err != nil {
			return err
		}
		m[key] = append(m[key], value)
	}
	return nil
}

func asInt(s string) int64 {
	v, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0
	}
	return v
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

func concat(params url.Values) string {
	var sb strings.Builder
	for k, v := range params {
		if sb.Len() > 0 {
			sb.WriteByte(';')
		}
		sb.WriteString(k)
		sb.WriteByte(':')
		var s string
		if len(v) > 0 {
			s = v[0]
		}
		sb.WriteString(s)
	}
	return sb.String()
}

func timeSlices(time1, time7, n int64) string {
	var sb strings.Builder
	if timeDiff := (time1 - time7) / n; timeDiff != 0 {
		for i := time1 - timeDiff; i >= time7; i -= timeDiff {
			if sb.Len() > 0 {
				sb.WriteByte(',')
			}
			fmt.Fprintf(&sb, "unixzeit>=%d AND unixzeit<=%d",
				i, i+timeDiff)
		}
	}
	return sb.String()
}

func (c *Controller) wmsDirector(req *http.Request) {
	log.Println("called /wms (director): GET")

	userID := UserID(req)

	// VIEWPARAMS contains ';' as key separators.
	// If we would use req.URL.Query() this would be split
	// at the wrong level resulting in broken key/value pairs.
	// So we do the splitting ourselves.

	geoserver := make(url.Values)
	if err := parseQuery(
		geoserver, req.URL.RawQuery, "&", "=", url.QueryUnescape); err != nil {
		log.Printf("error: %v\n", err)
		panic(http.ErrAbortHandler)
	}

	geoserver.Del("source_url")

	viewparam := make(url.Values)

	var animate bool

	if vp := geoserver.Get("VIEWPARAMS"); vp != "" {
		// strings are already unescaped
		parseQuery(viewparam, vp, ";", ":",
			func(s string) (string, error) { return s, nil })

		if geoserver.Get("ANIMATE") == "TRUE" {
			animate = true
			geoserver.Del("SERVICE")
			geoserver.Del("VERSION")
			geoserver.Set(
				"FORMAT",
				"image/gif;subtype=animated")

			time1 := asInt(viewparam.Get("zeit1"))
			time7 := asInt(viewparam.Get("zeit7"))
			// Create 60 time slices for the animation.
			geoserver.Set("AVALUES", timeSlices(time1, time7, 60))
			geoserver.Set("APARAM", "CQL_FILTER")
			// do a animation loop and set frame delay
			geoserver.Set(
				"FORMAT_OPTIONS",
				"gif_loop_continuosly:true;gif_frames_delay:750")
			// CQL FILTER already included in animation
			geoserver.Del("CQL_FILTER")
		}
	}

	viewparam.Set("userid", strconv.FormatInt(userID, 10))

	bbox := strings.Split(geoserver.Get("BBOX"), ",")
	if len(bbox) < 4 {
		log.Println("error: too less BBOX parts")
		panic(http.ErrAbortHandler)
	}
	viewparam.Set("left", bbox[0])
	viewparam.Set("right", bbox[2])
	viewparam.Set("top", bbox[3])
	viewparam.Set("bottom", bbox[1])

	// log.Printf("layers: %s\n", geoserver.Get("LAYERS"))
	// log.Printf("zeit1: %s\n", viewparam.Get("zeit1"))
	// log.Printf("zeit7: %s\n", viewparam.Get("zeit7"))
	// log.Printf("left: %s\n", viewparam.Get("left"))
	// log.Printf("right: %s\n", viewparam.Get("right"))
	// log.Printf("top: %s\n", viewparam.Get("top"))
	// log.Printf("bottom: %s\n", viewparam.Get("bottom"))

	// create VIEWPARAMS string
	geoserver.Set("VIEWPARAMS", concat(viewparam))

	if animate {
		if strings.HasSuffix(req.URL.Path, "/") {
			req.URL.Path += "animate"
		} else {
			req.URL.Path += "/animate"
		}
	}

	req.URL.RawQuery = geoserver.Encode()

	wmsRemoteURL := nexWMSRemoteURL()

	targetQuery := wmsRemoteURL.RawQuery

	req.URL.Scheme = wmsRemoteURL.Scheme
	req.URL.Host = wmsRemoteURL.Host

	req.URL.Path = singleJoiningSlash(wmsRemoteURL.Path, req.URL.Path)

	if targetQuery == "" || req.URL.RawQuery == "" {
		req.URL.RawQuery = targetQuery + req.URL.RawQuery
	} else {
		req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
	}

	if _, ok := req.Header["User-Agent"]; !ok {
		// explicitly disable User-Agent so it's not set to default value
		req.Header.Set("User-Agent", "")
	}

	// log.Println(req.URL.String())
}
