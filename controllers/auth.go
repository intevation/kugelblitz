// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"bitbucket.org/intevation/kugelblitz/database/config"
	"bitbucket.org/intevation/kugelblitz/model"
	"bitbucket.org/intevation/kugelblitz/utils"
)

// logout: GET
func (c *Controller) logout(rw http.ResponseWriter, req *http.Request) {
	log.Println("called auth/Logout: GET")
	s := Session(req)
	Logging(req, "Logout Successfull")
	s.Logout()
	s.Invalidate()
	SendEmptyJSON(rw, http.StatusOK)
}

// changePW: POST
func (c *Controller) changePW(rw http.ResponseWriter, req *http.Request) {
	// TODO: Implement me!
	log.Println("called auth/changePW: POST")

	var data struct {
		Hash     *string `json:"hash"`
		User     *string `json:"user"` // user is email
		Password *string `json:"passwd"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.Hash == nil {
		BadRequest(rw, errors.New("missing hash"))
		return
	}
	if data.User == nil {
		BadRequest(rw, errors.New("missing user"))
		return
	}
	if data.Password == nil {
		BadRequest(rw, errors.New("missing password"))
		return
	}

	userID, err := model.CheckUser(*data.User)
	if err != nil {
		Logging(
			req, "FAIL: Email %s not checked.", *data.User)
		InternalServerError(rw, err)
		return
	}

	if userID == nil {
		Logging(req, "Invalid User")
		return
	}

	checkLink, err := model.CheckLink(*userID, *data.Hash)
	if err != nil {
		Logging(req, "FAIL: Hash not checked.")
		InternalServerError(rw, err)
		return
	}
	if !checkLink {
		Logging(req, "Invalid Link")
		return
	}

	ip := IPFromRequest(req)
	if err := model.UpdatePassword(*data.Password, *userID, *data.Hash, ip.String()); err != nil {
		Logging(req, "FAIL: Password not updated.")
		InternalServerError(rw, err)
	}
	Logging(req, "User %s has successfully changed his password", *data.User)

	SendSuccess(rw)
}

// checkHash: POST
func (c *Controller) checkHash(rw http.ResponseWriter, req *http.Request) {
	// TODO: Implement me!
	log.Println("called auth/checkHash: POST")

	var data struct {
		Hash *string `json:"hash"`
		User *string `json:"user"` // user is email
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}
	if data.Hash == nil {
		BadRequest(rw, errors.New("missing hash"))
		return
	}
	if data.User == nil {
		BadRequest(rw, errors.New("missing user"))
		return
	}

	userID, err := model.CheckUser(*data.User)
	if err != nil {
		Logging(
			req, "FAIL: Email %s not checked.", *data.User)
		InternalServerError(rw, err)
		return
	}

	if userID == nil {
		Logging(req, "Invalid User")
		return
	}

	checkLink, err := model.CheckLink(*userID, *data.Hash)
	if err != nil {
		Logging(req, "FAIL: Hash not checked.")
		InternalServerError(rw, err)
		return
	}
	if !checkLink {
		Logging(req, "Invalid Link")
		return
	}

	SendSuccess(rw)
}

// login: POST
func (c *Controller) login(rw http.ResponseWriter, req *http.Request) {
	log.Println("called auth/login: POST")

	s := Session(req)

	if _, LoggedIn := s.GetUser(); LoggedIn {
		Logging(req, "Logout Successfull")
		s.Logout()
	}

	var cred struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&cred); err != nil {
		BadRequest(rw, err)
		return
	}

	user, err := model.ValidateUser(cred.Username, cred.Password)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if msg := user.Check(); msg != "" {
		Logging(req, "Login Failed: %s", msg)
		SendFailure(rw, "%s", msg)
		return
	}

	layers, err := model.LoadLayers(user.ID)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	s.SetUser(user.ID)
	Logging(req, "Login Successfull")

	cfg, err := config.GetConfiguration(utils.Env("CONFIG", ""))
	if err != nil {
		log.Printf("warn: %v\n", err)
	}

	var result = struct {
		UserID            int64          `json:"UserID"`
		User              *model.User    `json:"User"`
		Layers            []*model.Layer `json:"Layers"`
		GoogleMapsClient  string         `json:"GoogleMapsClient"`
		GoogleMapsChannel string         `json:"GoogleMapsChannel"`
		Status            string         `json:"status"`
	}{
		UserID:            user.ID,
		User:              user,
		Layers:            layers,
		GoogleMapsClient:  cfg.String("google-maps-client", ""),
		GoogleMapsChannel: cfg.String("google-maps-channel", ""),
		Status:            "success",
	}

	// utils.DumpJSON(&result)

	SendJSON(rw, http.StatusOK, &result)
}

// renewSession: POST
func (c *Controller) renewSession(rw http.ResponseWriter, req *http.Request) {
	SendSuccess(rw)
}

// resetPW: POST
func (c *Controller) resetPW(rw http.ResponseWriter, req *http.Request) {
	log.Println("called auth/resetPW: POST")

	var data struct {
		EMail *string `json:"user"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
		BadRequest(rw, err)
		return
	}

	if data.EMail == nil {
		Logging(req, "Get no EMail")
		SendFailure(rw, "No known EMail address")
		return
	}

	userID, err := model.CheckUser(*data.EMail)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if userID == nil {
		SendFailure(rw, "Invalid User")
		return
	}

	serverName := req.Host

	// https := strings.ToLower(req.URL.Scheme) == "https"
	// Always force HTTPS here!
	https := true

	_, err = model.GenerateLink(https, *data.EMail, *userID, serverName)
	if err != nil {
		InternalServerError(rw, err)
		return
	}
	Logging(req, "User: %s has requested a new password", *data.EMail)
	SendSuccess(rw)
}
