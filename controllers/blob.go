// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/intevation/kugelblitz/database/blobstore"
	"github.com/gorilla/mux"
)

func (c *Controller) blob(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	name := vars["name"]

	fresh := req.FormValue("cache") == "fresh"

	it, err := blobstore.GetItem(name, fresh)
	if err != nil {
		InternalServerError(rw, err)
		return
	}

	if it == nil {
		http.NotFound(rw, req)
		return
	}

	rw.Header().Set("Content-Type", it.MIMEType)
	rw.Header().Set("Content-Length", strconv.Itoa(len(it.Data)))

	_, err = io.Copy(rw, strings.NewReader(it.Data))
	if err != nil {
		log.Printf("warn: %v\n", err)
	}
}
