# kugelblitz

A web application to display lightning events on a map in realtime.

## Requirements

- Lightning location sensors for providing the lightning data
- [PostgreSQL](https://www.postgresql.org/) / [PostGIS](http://postgis.net/)
  for storing and processing the lightning data from the sensors
- [Go](https://golang.org) for building `kugelblitz`

## Usage

1. Prepare the database. See the file [schema/README.md](schema/README.md)
   for a sample database.
2. Set the environment variables. See the file [doc/ENV.md](doc/ENV.md) for
   details.
3. The software utility `cron` is used for maintenance and cleaning work. See
   [cron/README.md](cron/README.md) for setting up cron properly.
4. Start the kugelblitz with `./kugelblitz`.
5. Testing in the browser.

Goto `http://<SERVER>:5000/` to test the application.

## Development

See the file [doc/DEVELOPMENT.md](doc/DEVELOPMENT.md) for build instructions
on GNU/Linux.

For using `kugelblitz` in an AWS Elastic Beanstalk (ELB) Environment see file
[doc/ELB.md](doc/ELB.md) for more details.

## Support

Please use Bitbucket for questions:
<https://bitbucket.org/intevation/kugelblitz/issues>

## License

This is Free Software governed by the terms of the Apache 2.0 license.
See [LICENSE](LICENSE) for details.
Copyright 2012 - 2018 Siemens AG.
Engineering by Intevation GmbH.
